/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "omp_credentials.h"

/**
 * @file omp_credentials.cpp
 * @brief omp entity-class to store logindata
 *
 * Stores username, password, serveraddress and serverport used for the OpenVAS
 * Manager-login.
 *
 */

omp_credentials::omp_credentials ()
{
  userName = new QString ();
  password = new QString ();
  serverAddress  = new QString ();
  serverPort = 0;
}

omp_credentials::~omp_credentials ()
{
}


/**
 * @brief Getter
 *
 * @return returns the username
 */
QString
omp_credentials::getUserName ()
{
  return *userName;
}


/**
 * @brief Getter
 *
 * @return returns the password
 */
QString
omp_credentials::getPassword ()
{
  return *password;
}


/**
 * @brief Getter
 *
 * @return returns the serveraddress
 */
QString
omp_credentials::getServerAddress ()
{
  return *serverAddress;
}


/**
 * @brief Getter
 *
 * @return returns the username
 */
int
omp_credentials::getServerPort ()
{
  return serverPort;
}


/**
 * @brief Setter
 *
 * Sets username to credentials
 *
 * @param name Username
 */
void
omp_credentials::setUserName (const QString &name)
{
  this->userName = new QString (name.toLatin1 ());
}


/**
 * @brief Setter
 *
 * Sets password to credentials
 *
 * @param pass Password
 */
void
omp_credentials::setPassword (const QString &pass)
{
  this->password = new QString (pass.toLatin1 ());
}


/**
 * @brief Setter
 *
 * Sets serveraddress to credentials
 *
 * @param addr Serveraddress
 */
void
omp_credentials::setServerAddress (const QString &addr)
{
  this->serverAddress = new QString (addr.toLatin1 ());
}


/**
 * @brief Setter
 *
 * @param port Serverport
 */
void
omp_credentials::setServerPort (int port)
{
  this->serverPort = port;
}

