/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "omp_connector.h"

/**
 * @file omp_connector.cpp
 * @brief Connector between omp functionality and GSD UI.
 */


/**
 * @brief Creates a new connector including the taskupdater- and commandobjects.
 *
 * Creates all nessesary objects to communicate with the OpenVAS Manager, this
 * includes the omp_updater, omp_update_helper and the omp_commands.
 * Configures the signal/slot connections to the helper.
 */
omp_connector::omp_connector ()
{
  credentials = NULL;
  command = new omp_command ();
  request = new omp_command ();
  update = new omp_update ();
  updateHelper = new omp_thread_helper ();
  update->setHelper (updateHelper);
  commandHelper = new omp_thread_helper ();
  command->setHelper (commandHelper);
  requestHelper = new omp_thread_helper ();
  request->setHelper (requestHelper);


  connect (commandHelper,
           SIGNAL (sig_request_failed (int, int, int)),
           this,
           SLOT (request_failed (int, int, int)));
  connect (commandHelper,
           SIGNAL (sig_request_finished (int, int, QString)),
           this,
           SLOT (request_finished (int, int, QString)));
  connect (commandHelper,
           SIGNAL (sig_log (QString, int)),
           this,
           SLOT (log (QString, int)));
  connect (requestHelper,
           SIGNAL (sig_request_finished (int, int, QString)),
           this,
           SLOT (request_finished (int, int, QString)));
  connect (requestHelper,
           SIGNAL (sig_request_failed (int, int, int)),
           this,
           SLOT (request_failed (int, int, int)));
  connect (requestHelper,
           SIGNAL (sig_update_finished ()),
           this,
           SLOT (update_finished ()));
  connect (requestHelper,
           SIGNAL (sig_thread_started ()),
           this,
           SLOT (thread_started ()));
  connect (requestHelper,
           SIGNAL (sig_thread_finished ()),
           this,
           SLOT (thread_finished ()));
  connect (requestHelper,
           SIGNAL (sig_request_failed (int, int, int)),
           this,
           SLOT (request_failed (int, int, int)));
  connect (requestHelper,
           SIGNAL (sig_log (QString, int)),
           this,
           SLOT (log (QString, int)));
  connect (update,
           SIGNAL (sig_update_trigger ()),
           this,
           SLOT (update_trigger ()));
  connect (updateHelper,
           SIGNAL (sig_request_finished (int, int, QString)),
           this,
           SLOT (request_finished (int, int, QString)));
  connect (updateHelper,
           SIGNAL (sig_thread_started ()),
           this,
           SLOT (thread_started ()));
  connect (updateHelper,
           SIGNAL (sig_thread_finished ()),
           this,
           SLOT (thread_finished ()));
}

omp_connector::omp_connector (omp_credentials crd)
{
  omp_connector ();
  setCredentials (crd);
}

omp_connector::omp_connector (QString addr, int port, QString name, QString pwd)
{
  omp_connector ();
  setCredentials (addr, port, name, pwd);
}

omp_connector::~omp_connector ()
{
  free (command);
  free (request);
  free (update);
  free (commandHelper);
  free (requestHelper);
  free (updateHelper);
  free (credentials);
}


/**
 * @brief Setter for logindata and connectiondetails.
 *
 * Sets the logindata, serveraddress and port used to connect to the OpenVAS
 * Manager.
 *
 * @param addr  Serveraddress
 * @param port  Serverport
 * @param name  Username
 * @param pwd   Password
 */
void
omp_connector::setCredentials (QString addr, int port, QString name,
                               QString pwd)
{
  this->credentials = new omp_credentials ();
  this->credentials->setServerAddress (addr);
  this->credentials->setServerPort (port);
  this->credentials->setUserName (name);
  this->credentials->setPassword (pwd);

  command->setCredentials (this->credentials->getServerAddress (),
                          this->credentials->getServerPort (),
                          this->credentials->getUserName (),
                          this->credentials->getPassword ());
  request->setCredentials (this->credentials->getServerAddress (),
                          this->credentials->getServerPort (),
                          this->credentials->getUserName (),
                          this->credentials->getPassword ());
  update->setCredentials (this->credentials->getServerAddress (),
                          this->credentials->getServerPort (),
                          this->credentials->getUserName (),
                          this->credentials->getPassword ());



}

/**
 * @brief Setter for logindata and connectiondetails.
 *
 * Sets the logindata, serveraddress and port used to connect to the OpenVAS
 * Manager.
 *
 * @param crd logindata encapsulted in the entityobject omp_credentials
 */
void
omp_connector::setCredentials (omp_credentials &crd)
{
  this->setCredentials (crd.getServerAddress (), crd.getServerPort (),
                        crd.getUserName (), crd.getPassword ());
}


/**
 * @brief Getter
 *
 * @return Current logindata
 */
omp_credentials *
omp_connector::getCredentials ()
{
  return this->credentials;
}


/**
 * @brief Removes logindata from the connector.
 */
void
omp_connector::removeCredentials ()
{
  this->credentials = NULL;
}


/**
 * @brief Autenticates with the manager.
 *
 * @return 0 if authentication was successful, 1 if manager closed connection,
 *         2 if authentication failed, -1 on error.
 */
int
omp_connector::authenticate ()
{
  int ret = -1;
  if (this->credentials != NULL)
    {
      ret = command->openConnection ();
      if (ret == 0)
        command->closeConnection ();
      return ret;
    }
  return -1;
}


/**
 * @brief Requests an Entity with parameters.
 *
 * @param[in]   type      omp entity type.
 * @param[in]   interval  Update Interval.
 *                        0   Requests the entity once.
 *                        >0  Requests the entity every "interval" seconds.
 * @param[out]  model     Datamodel containing the manager response.
 * @param[in]   parameter omp parameter
 *
 * @return  0   Success.
 *         -1   Missing model
 *         -2   NONE entity type.
 */
int
omp_connector::getEntity (omp_utilities::omp_type type, int interval,
                          model_omp_entity *model,
                          QMap<QString, QString> parameter)
{
  if (model == NULL || this->credentials == NULL)
    return -1;

  if (type == omp_utilities::NONE)
    return -2;

  omp_string_builder *build = new omp_string_builder ();
  QString command = build->requestString (type, parameter);
  if (interval >= 1)
    {
      update->setInterval (interval);
      update->addModel (type, model);
      update->addCommand (type, command);
      update->addToQueue (type);
      if (!update->isRunning ())
        update->start ();
    }
  else
    request->requestEntity (command, model);

  return 0;
}


/**
 * @brief Creates an Entity with parameters.
 *
 * @param[in]   type      omp entity type.
 * @param[out]  model     Datamodel containing the manager response.
 * @param[in]   parameter omp parameter
 *
 * @return  0   Success.
 *         -1   Missing model
 *         -2   NONE entity type.
 */
int
omp_connector::createEntity (omp_utilities::omp_type type,
                             model_omp_entity *model,
                             QMap<QString, QString> parameter)
{
  if (model == NULL || this->credentials == NULL)
    return -1;

  if (type == omp_utilities::NONE)
    return -2;

  omp_string_builder *build = new omp_string_builder ();
  QString commandString = build->createString (type, parameter);

  return command->createEntity (commandString, model);
}


/**
 * @brief Deletes an Entity.
 *
 * @param[in]   type   omp entity type.
 * @param[in]   id     omp parameter
 *
 * @return  0   Success.
 *         -1   NONE entity type.
 */
int
omp_connector::deleteEntity (omp_utilities::omp_type type, QString id)
{
  if (type == omp_utilities::NONE || this->credentials == NULL)
    return -1;

  omp_string_builder *build = new omp_string_builder ();
  QString commandString = build->deleteString (type, id);

  return command->removeEntity (commandString);
}


/**
 * @brief Modifies an Entity with parameters
 *
 * @param[in]     type      omp entity type.
 * @param[in]     id        omp parameter
 * @param[inout]  model     Datamodel containing the data.
 *                          The model has to contain at least the data that is
 *                          changed with this command. Contains the manager
 *                          response on successful edit, else the model is not
 *                          changed.
 * @param[in]     parameter omp parameter
 *
 * @return  0   Success.
 *         -1   Missing model
 *         -2   NONE entity type.
 */
int
omp_connector::modifyEntity (omp_utilities::omp_type type, QString id,
                             model_omp_entity* model,
                             QMap<QString, QString> parameter)
{
  if (this->credentials == NULL)
    return -1;

  if (type == omp_utilities::NONE)
    return -2;

  omp_string_builder *build = new omp_string_builder ();
  QString commandString = build->modifyString (type, id, parameter, model);
  return command->modifyEntity (commandString);
}


/**
 * @brief Starts the task with the given id.
 *
 * @param[in]   id  Task id.
 *
 * @return   0  Success.
 *          -1  Id is NULL.
 */
int
omp_connector::startTask (QString id)
{
  if (id.isNull () || this->credentials == NULL)
    return -1;

  omp_string_builder *build = new omp_string_builder ();
  QString commandString = build->taskString (omp_utilities::START, id);
  return command->runTaskCommand (commandString);
}


/**
 * @brief Stops the task with the given id.
 *
 * @param[in]   id  Task id.
 *
 * @return   0  Success.
 *          -1  Id is NULL.
 */
int
omp_connector::stopTask (QString id)
{
  if (id.isNull () || this->credentials == NULL)
    return -1;

  omp_string_builder *build = new omp_string_builder ();
  QString commandString = build->taskString (omp_utilities::STOP, id);

  return command->runTaskCommand (commandString);
}


/**
 * @brief Pauses the task with the given id.
 *
 * @param[in]   id  Task id.
 *
 * @return   0  Success.
 *          -1  Id is NULL.
 */
int
omp_connector::pauseTask (QString id)
{
  if (id.isNull () || this->credentials == NULL)
    return -1;

  omp_string_builder *build = new omp_string_builder ();
  QString commandString = build->taskString (omp_utilities::PAUSE, id);

  free (build);

  return command->runTaskCommand (commandString);
}


/**
 * @brief Resumes the paused task with the given id.
 *
 * @param[in]   id  Task id.
 *
 * @return   0  Success.
 *          -1  Id is NULL.
 */
int
omp_connector::resumePausedTask (QString id)
{
  if (id.isNull () || this->credentials == NULL)
    return -1;

  omp_string_builder *build = new omp_string_builder ();
  QString commandString = build->taskString (omp_utilities::RESUME_PAUSED, id);

  return command->runTaskCommand (commandString);
}


/**
 * @brief Resumes the stopped task with the given id.
 *
 * @param[in]   id  Task id.
 *
 * @return   0  Success.
 *          -1  Id is NULL.
 */
int
omp_connector::resumeStoppedTask (QString id)
{
  if (id.isNull () || this->credentials == NULL)
    return -1;

  omp_string_builder *build = new omp_string_builder ();
  QString commandString = build->taskString (omp_utilities::RESUME_STOPPED, id);

  return command->runTaskCommand (commandString);
}


/**
 * @brief Stops updating Tasks.
 *
 * Emits sig_stopped_taskupdate ().
 */
void
omp_connector::stop_update ()
{
  update->stop ();
  emit sig_update_stopped ();
}


/**
 * @brief The update interval has started.
 *
 * Emits sig_update_started();
 */
void
omp_connector::update_started ()
{
  emit sig_update_started ();
}

/**
 * @brief Shows if the updaterthread is updating.
 *
 * @return true, if an updatetask is running, else returns false
 */
bool
omp_connector::updateRunning ()
{
  if (update->isRunning ())
    {
      return true;
    }
  return false;
}


/**
 * @brief Sends sig_triggered_taskupdate () to synchronize with the update.
 */
void
omp_connector::update_finished ()
{
  emit sig_update_finished ();
}


/**
 * @brief Sends sig_started_taskupdate () to indicate that update has started.
 */
void
omp_connector::update_trigger ()
{
  emit sig_update_trigger ();
}


/**
 * @brief Sends sig_thread_started to indicate that a thead started.
 */
void
omp_connector::thread_started ()
{
  emit sig_thread_started ();
}


/**
 * @brief Sends sig_thread_finished () to indicate that a thread finished.
 */
void
omp_connector::thread_finished ()
{
  emit sig_thread_finished ();
}

/**
 * @brief Sends sig_finished_taskupdate () to indicate that update has finished
 */
void
omp_connector::request_finished (int type, int com, QString userData)
{
  emit sig_request_finished (type, com, userData);
}

/**
 * @brief Sends sig_failed_taskupdate () to indicate that an error occured
 */
void
omp_connector::request_failed (int type,int com, int err)
{
  emit sig_request_failed (type, com, err);
}


/**
 * @brief Sends sig_log () to write a log message
 */
void
omp_connector::log (QString msg, int prio)
{
  emit sig_log(msg, prio);
}

