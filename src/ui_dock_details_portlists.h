/********************************************************************************
** Form generated from reading UI file 'dock_details_portlists.ui'
**
** Created: Tue Apr 17 21:16:09 2012
**      by: Qt User Interface Compiler version 4.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DOCK_DETAILS_PORTLISTS_H
#define UI_DOCK_DETAILS_PORTLISTS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDockWidget>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QScrollArea>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QTableView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_dock_details_portlists
{
public:
    QWidget *dockWidgetContents;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *tab_5;
    QGridLayout *gridLayout_3;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_4;
    QLabel *la_name;
    QLabel *la_name_v;
    QLabel *la_comment;
    QLabel *la_comment_v;
    QLabel *la_count;
    QLabel *la_hosts_v;
    QLabel *la_max_hosts;
    QLabel *la_max_hosts_v;
    QLabel *la_sshcredential_v;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QLabel *la_smbcredential_v;
    QLabel *label;
    QLabel *la_port_list_v;
    QWidget *tab_6;
    QGridLayout *gridLayout_2;
    QTableView *tv_targets_using;
    QWidget *tab_7;
    QGridLayout *gridLayout_21;
    QTableView *tv_targets_using1;

    void setupUi(QDockWidget *dock_details_portlists)
    {
        if (dock_details_portlists->objectName().isEmpty())
            dock_details_portlists->setObjectName(QString::fromUtf8("dock_details_portlists"));
        dock_details_portlists->resize(499, 294);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/img/favicon.gif"), QSize(), QIcon::Normal, QIcon::Off);
        dock_details_portlists->setWindowIcon(icon);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QString::fromUtf8("dockWidgetContents"));
        gridLayout = new QGridLayout(dockWidgetContents);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tabWidget = new QTabWidget(dockWidgetContents);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab_5 = new QWidget();
        tab_5->setObjectName(QString::fromUtf8("tab_5"));
        gridLayout_3 = new QGridLayout(tab_5);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        scrollArea = new QScrollArea(tab_5);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 463, 217));
        gridLayout_4 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        la_name = new QLabel(scrollAreaWidgetContents);
        la_name->setObjectName(QString::fromUtf8("la_name"));
        la_name->setMargin(1);

        gridLayout_4->addWidget(la_name, 0, 0, 1, 1);

        la_name_v = new QLabel(scrollAreaWidgetContents);
        la_name_v->setObjectName(QString::fromUtf8("la_name_v"));
        la_name_v->setMargin(1);

        gridLayout_4->addWidget(la_name_v, 0, 1, 1, 1);

        la_comment = new QLabel(scrollAreaWidgetContents);
        la_comment->setObjectName(QString::fromUtf8("la_comment"));
        la_comment->setMargin(1);

        gridLayout_4->addWidget(la_comment, 1, 0, 1, 1);

        la_comment_v = new QLabel(scrollAreaWidgetContents);
        la_comment_v->setObjectName(QString::fromUtf8("la_comment_v"));
        la_comment_v->setMargin(1);

        gridLayout_4->addWidget(la_comment_v, 1, 1, 1, 1);

        la_count = new QLabel(scrollAreaWidgetContents);
        la_count->setObjectName(QString::fromUtf8("la_count"));
        la_count->setMargin(1);

        gridLayout_4->addWidget(la_count, 2, 0, 1, 1);

        la_hosts_v = new QLabel(scrollAreaWidgetContents);
        la_hosts_v->setObjectName(QString::fromUtf8("la_hosts_v"));
        la_hosts_v->setMargin(1);

        gridLayout_4->addWidget(la_hosts_v, 2, 1, 1, 1);

        la_max_hosts = new QLabel(scrollAreaWidgetContents);
        la_max_hosts->setObjectName(QString::fromUtf8("la_max_hosts"));
        la_max_hosts->setMargin(1);

        gridLayout_4->addWidget(la_max_hosts, 3, 0, 1, 1);

        la_max_hosts_v = new QLabel(scrollAreaWidgetContents);
        la_max_hosts_v->setObjectName(QString::fromUtf8("la_max_hosts_v"));
        la_max_hosts_v->setMargin(1);

        gridLayout_4->addWidget(la_max_hosts_v, 3, 1, 1, 1);

        la_sshcredential_v = new QLabel(scrollAreaWidgetContents);
        la_sshcredential_v->setObjectName(QString::fromUtf8("la_sshcredential_v"));
        la_sshcredential_v->setMargin(1);

        gridLayout_4->addWidget(la_sshcredential_v, 5, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer, 5, 0, 3, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_2, 7, 1, 1, 1);

        la_smbcredential_v = new QLabel(scrollAreaWidgetContents);
        la_smbcredential_v->setObjectName(QString::fromUtf8("la_smbcredential_v"));

        gridLayout_4->addWidget(la_smbcredential_v, 6, 1, 1, 1);

        label = new QLabel(scrollAreaWidgetContents);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_4->addWidget(label, 4, 0, 1, 1);

        la_port_list_v = new QLabel(scrollAreaWidgetContents);
        la_port_list_v->setObjectName(QString::fromUtf8("la_port_list_v"));

        gridLayout_4->addWidget(la_port_list_v, 4, 1, 1, 1);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout_3->addWidget(scrollArea, 4, 0, 1, 2);

        tabWidget->addTab(tab_5, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QString::fromUtf8("tab_6"));
        gridLayout_2 = new QGridLayout(tab_6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        tv_targets_using = new QTableView(tab_6);
        tv_targets_using->setObjectName(QString::fromUtf8("tv_targets_using"));
        tv_targets_using->setAlternatingRowColors(true);
        tv_targets_using->setSelectionMode(QAbstractItemView::SingleSelection);
        tv_targets_using->setSelectionBehavior(QAbstractItemView::SelectRows);
        tv_targets_using->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
        tv_targets_using->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        tv_targets_using->setCornerButtonEnabled(false);

        gridLayout_2->addWidget(tv_targets_using, 0, 0, 1, 1);

        tabWidget->addTab(tab_6, QString());
        tab_7 = new QWidget();
        tab_7->setObjectName(QString::fromUtf8("tab_7"));
        gridLayout_21 = new QGridLayout(tab_7);
        gridLayout_21->setObjectName(QString::fromUtf8("gridLayout_21"));
        tv_targets_using1 = new QTableView(tab_7);
        tv_targets_using1->setObjectName(QString::fromUtf8("tv_targets_using1"));
        tv_targets_using1->setAlternatingRowColors(true);
        tv_targets_using1->setSelectionMode(QAbstractItemView::SingleSelection);
        tv_targets_using1->setSelectionBehavior(QAbstractItemView::SelectRows);
        tv_targets_using1->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
        tv_targets_using1->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        tv_targets_using1->setCornerButtonEnabled(false);

        gridLayout_21->addWidget(tv_targets_using1, 0, 0, 1, 1);

        tabWidget->addTab(tab_7, QString());

        gridLayout->addWidget(tabWidget, 0, 0, 1, 1);

        dock_details_portlists->setWidget(dockWidgetContents);

        retranslateUi(dock_details_portlists);

        tabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(dock_details_portlists);
    } // setupUi

    void retranslateUi(QDockWidget *dock_details_portlists)
    {
        dock_details_portlists->setWindowTitle(QApplication::translate("dock_details_portlists", "Details Port List", 0, QApplication::UnicodeUTF8));
        la_name->setText(QApplication::translate("dock_details_portlists", "Name:", 0, QApplication::UnicodeUTF8));
        la_name_v->setText(QString());
        la_comment->setText(QApplication::translate("dock_details_portlists", "Comment:", 0, QApplication::UnicodeUTF8));
        la_comment_v->setText(QString());
        la_count->setText(QApplication::translate("dock_details_portlists", "Port count:", 0, QApplication::UnicodeUTF8));
        la_hosts_v->setText(QString());
        la_max_hosts->setText(QApplication::translate("dock_details_portlists", "TCP Port count:", 0, QApplication::UnicodeUTF8));
        la_max_hosts_v->setText(QString());
        la_sshcredential_v->setText(QString());
        la_smbcredential_v->setText(QString());
        label->setText(QApplication::translate("dock_details_portlists", "UDP Port count:", 0, QApplication::UnicodeUTF8));
        la_port_list_v->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QApplication::translate("dock_details_portlists", "Summary", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_6), QApplication::translate("dock_details_portlists", "Targets using this Port List", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_7), QApplication::translate("dock_details_portlists", "Port Ranges", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class dock_details_portlists: public Ui_dock_details_portlists {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DOCK_DETAILS_PORTLISTS_H
