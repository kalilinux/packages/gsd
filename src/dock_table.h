/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file dock_table.h
 * @class dock_table
 * @brief Protos and data structures for dock_table.
 */

#ifndef DOCK_TABLE_H
#define DOCK_TABLE_H

#include <QtGui>
#include <QObject>
#include <qdom.h>
#include "ui_dock_table.h"
#include "model_omp_entity.h"

class dock_table : public QDockWidget, private Ui::dock_table
{
  Q_OBJECT

  public:
    dock_table();
    ~dock_table();

    void setModel (QAbstractTableModel *model);
    QTableView *getTable ();
    void addActionToToolBar (QAction *action);
    void addActionToMenu (QAction *action);
    void addSeparator ();
    void addSeparatorToMenu ();
    void hideToolBar ();
    void showToolTip (QPoint pos_in, QPoint pos_out);

  private slots:
    void contextmenu (const QPoint &);

  private:
    QToolBar *toolBar;
    QMenu *menu;
    QAction *menu_title;
};
#endif

