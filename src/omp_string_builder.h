/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file omp_string_builder.h
 * @class omp_string_builder
 * @brief Protos and data structures for omp_string_builder.
 */

#ifndef OMP_STRING_BUILDER_H
#define OMP_STRING_BUILDER_H

#include <QObject>

#include <openvas/omp/xml.h>
#include <openvas/omp/omp.h>
#include <openvas/misc/openvas_server.h>

extern "C"
{
#include <openvas/base/openvas_file.h>
}

#include "omp_utilities.h"

class omp_string_builder : public QObject
{
  Q_OBJECT

  private:
    QString appendCreateParameter (omp_utilities::omp_type type,
                                     QMap<QString, QString> parameter);
    QString appendModificationParameter (omp_utilities::omp_type type,
                                         QMap<QString, QString> parameter);
    QString appendConfigParameter (QString command, QString, model_omp_entity *m);
  public:
    omp_string_builder ();
    ~omp_string_builder ();

    QString requestString (omp_utilities::omp_type type,
                           QMap<QString, QString> parameter);
    QString createString (omp_utilities::omp_type type,
                          QMap<QString, QString> parameter);
    QString deleteString (omp_utilities::omp_type type, QString id);
    QString modifyString (omp_utilities::omp_type type,
                          QString id,
                          QMap<QString, QString> parameter,
                          model_omp_entity *m);
    QString taskString (omp_utilities::omp_task_command command,
                        QString id);
};
#endif

