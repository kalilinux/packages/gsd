/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file omp_connector.h
 * @class omp_connector
 * @brief Protos and data structures for omp_connector.
 */

#ifndef OMP_CONNECTOR_H
#define OMP_CONNECTOR_H

#include <QObject>
#include "omp_credentials.h"
#include "omp_command.h"
#include "omp_update.h"
#include "omp_thread_helper.h"
#include "omp_string_builder.h"

class omp_connector : public QObject
{
  Q_OBJECT

  signals:
/**
 * @brief SIGNAL to synchronize with updates.
 */
    void sig_update_trigger ();

/**
 * @brief SIGNAL indicating that an update has finished.
 */
    void sig_update_finished ();

/**
 * @brief SIGNAL indicating that an update has stopped.
 */
    void sig_update_stopped ();

/**
 * @brief SIGNAL inicating that an update started.
 */
    void sig_update_started ();

/**
 * @brief SIGNAL indicating that a request finished successfully.
 */
    void sig_request_finished (int, int, QString);

/**
 * @brief SIGNAL indicating that a request has failed.
 *
 * @param[out]  -1  Internal error (http error 500).
 *              -2  Http error 40X.
 *              -3  Logindata missing.
 *              -4  Connection failed
 *              -5  Message tranfer failed.
 *              -6  Failed to read response.
 *              -7  Response was a NULL node.
 *              -8  QDom conversion failed
 */
    void sig_request_failed (int, int, int);

/**
 * @brief SIGNAL indicating that a thread started.
 */
    void sig_thread_started ();

/**
 * @brief SIGNAL indicating that a thread finished.
 */
    void sig_thread_finished ();
/**
 * @brief SIGNAL containing log messages.
 *
 * @param[out]  Log message.
 * @param[out]  Log level
 */
    void sig_log (QString, int);

  private slots:
    void update_trigger ();
    void update_finished ();
    void update_started ();
    void thread_started ();
    void thread_finished ();
    void request_finished (int type, int com, QString userData);
    void request_failed (int type, int com, int err);
    void log (QString msg, int prio);

  public slots:
    void stop_update ();

  private:
    omp_credentials *credentials;
    omp_command *request;
    omp_command *command;
    omp_update *update;
    omp_thread_helper *commandHelper;
    omp_thread_helper *requestHelper;
    omp_thread_helper *updateHelper;

  public:
    omp_connector ();
    omp_connector (omp_credentials crd);
    omp_connector (QString addr, int port, QString name, QString pwd);
    ~omp_connector ();

    void setCredentials (QString addr, int port, QString name, QString pwd);
    void setCredentials (omp_credentials &crd);
    omp_credentials * getCredentials ();
    void removeCredentials ();

    int authenticate ();

    bool updateRunning ();
    QList<omp_utilities::omp_type> updatesRunning();

    int startTask (QString id);
    int stopTask (QString id);
    int pauseTask (QString id);
    int resumePausedTask (QString id);
    int resumeStoppedTask (QString id);

    int getEntity (omp_utilities::omp_type type, int interval,
                    model_omp_entity *model, QMap<QString, QString> parameter);
    int createEntity (omp_utilities::omp_type type, model_omp_entity *model,
                      QMap<QString, QString> parameter);
    int deleteEntity (omp_utilities::omp_type type, QString id);
    int modifyEntity (omp_utilities::omp_type type, QString id,
                      model_omp_entity *model,
                      QMap<QString, QString> parameter);

    int downloadConfig (QString id, model_omp_entity *m);
    int downloadSystemReports (model_omp_entity *m);
    int downloadSystemReport (QString name, int duration, model_omp_entity *m);
/*
    int createTask (QString name, QString configId, QString targetId,
                    QString scheduleId =NULL, QString escalatorId = NULL,
                    QString comment = NULL);
    int deleteTask (QString id);

    int createConfig (QString name, QString base, QString comment = NULL);
    int deleteConfig (QString id);

    int createTarget (QString name, QString hosts, QString lsc_id,
                      QString comment = NULL);
    int deleteTarget (QString id);

    int createSchedule (QString name, int first_min,
                        int first_hour, int first_day, int first_month,
                        int first_year, int duration,
                        int period, QString dura_unit = NULL,
                        QString period_unit = NULL, QString comment = NULL);
    int deleteSchedule (QString id);

    int createEscalator (QString name, QString condition,
                         QStringList con_data, QString event,
                         QStringList eve_data, QString method,
                         QStringList meth_data, QString comment = NULL);
    int deleteEscalator (QString id);

    int createCredential (QString name, QString login, QString password,
                          QString type, QString comment = NULL);
    int deleteCredential (QString id);

    int createAgent (QString name, QString comment, QString installer,
                     QString signature);
    int deleteAgent (QString id);

    int createNote (QString text, QString nvt_id, QString hosts = NULL,
                    QString port = NULL, QString task_id = NULL,
                    omp_utilities::omp_threat threat = 0, QString result_id,
                    QString comment = NULL);
    int deleteNote (QString id);
    int modifyNote (QString id, QString text, QString hosts = NULL,
                    QString port = NULL, omp_utilities::omp_threat = 0,
                    QString task_id = NULL, QString result_id);

    int createOverride (QString text, omp_utilities::omp_threat threat,
                        QString nvt_id, QString hosts = NULL,
                        QString port = NULL, QString task_id,
                        omp_utilities::omp_threat threat = 0,
                        QString result_id = NULL);
    int deleteOverride (QString id);
    int modifyOverride (QString id, QString text, QString hosts = NULL, QString port = NULL,
                        omp_utilities::omp_threat threat = 0, QString task = NULL, QString threat = NULL,
                        QString result = NULL);
*/

};
#endif

