/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file gsd_config.h
 * @class gsd_config
 * @brief Protos and data structures for gsd_config.
 */

#ifndef GSD_CONFIG_H
#define GSD_CONFIG_H

#include <QtXml>
#include <QtGui>
#include "omp_credentials.h"

class gsd_config : public QObject
{
  Q_OBJECT

  private:
/**
 * @brief Dom-Rootdocument
 *
 * Contains the configuration written to the file
 */
    QDomDocument *rootDoc;

/**
 * @brief Name for configurationfile
 */
    QString *filename;

/**
 * @brief Rootelement for XML-document
 */
    QDomElement rootElement;

  public:
    gsd_config ();
    ~gsd_config ();

    bool readConfig (QIODevice *device);
    int writeConfig (QIODevice *device);
    QString *getFilename ();

    void addCredentials (QString profile, omp_credentials crd);
    int removeCredentials (QString profile);
    omp_credentials *getCredentials (QString profile);

    int getProfileCount ();
    QString getProfile (int number);
};
#endif

