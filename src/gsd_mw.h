/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file gsd_mw.h
 * @class gsd_mw
 * @brief Protos and data structures for GSD main window.
 */

#ifndef GSD_MW_H
#define GSD_MW_H

#include <QtGui>
#include <QWebFrame>

#include "ui_gsd_mainwindow.h"

#include "model_omp_entity.h"
#include "delegate_progress.h"
#include "delegate_trend.h"
#include "delegate_threat.h"
#include "delegate_icon.h"
#include "delegate_date_time.h"
#include "delegate_text.h"
#include "dock_logging.h"
#include "dock_table.h"
#include "dock_performance.h"
#include "dock_details.h"
#include "dock_reports.h"
#include "gsd_dialogs.h"

class gsd_control;

class gsd_mw : public QMainWindow , private Ui::MainWindow
{
  Q_OBJECT

  signals:
    void sig_req_tasks (int);
    void sig_req_schedules (int);
    void sig_req_targets (int);
    void sig_req_configs (int);
    void sig_req_escalators (int);
    void sig_req_agents (int);
    void sig_req_lsc_credentials (int);
    void sig_req_notes (int);
    void sig_req_overrides (int);
    void sig_req_nvt_families (int);
    void sig_req_slaves (int);
    void sig_req_portlists (int);

    void sig_req_report (QMap<QString, QString>);

    void sig_login ();
    void sig_logout ();
    void sig_about ();
    void sig_start_gsa ();
    void sig_stop_update ();
    void sig_start_update ();
    void sig_thread_started ();
    void sig_thread_stopped ();

    void sig_start_task (int);
    void sig_stop_task (int);
    void sig_pause_task (int);
    void sig_resume_task (int);

    void sig_delete_task (int);
    void sig_create_task ();

    void sig_delete_config (int);
    void sig_create_config ();

    void sig_delete_target (int);
    void sig_create_target ();

    void sig_delete_schedule (int);
    void sig_create_schedule ();

    void sig_delete_escalator (int);
    void sig_create_escalator ();

    void sig_delete_credential (int);
    void sig_create_credential ();

    void sig_delete_agent (int);
    void sig_create_agent ();

    void sig_delete_note (int);

    void sig_delete_override (int);

    void sig_delete_slave (int);
    void sig_create_slave ();

    void sig_delete_port_list (int);
    void sig_create_port_list ();

    void sig_details_config (int);
    void sig_details_credential (int);
    void sig_details_escalator (int);
    void sig_details_schedule (int);
    void sig_details_target (int);
    void sig_details_task (int);
    void sig_details_note (QString);
    void sig_details_override (QString);
    void sig_details_family (QString, QString);
    void sig_details_nvt (QString, QString);
    void sig_details_slave (int);
    void sig_details_port_list (QString);
    void sig_nvts (QString);
    void sig_modify_note (int);
    void sig_modify_override (int);
    void sig_modify_config (int);
    void sig_modify_task (int);

    void sig_download_config (int);
    void sig_upload (int, QMap<QString, QString>);
    void sig_download_system_report (QString, int);
    void sig_nvt_family_details (QString);

  public slots:
    void logout ();
    void authentication_failed (int);
    void authentication_successful ();
    void update_stopped ();

    void focus_tasks (bool focus);
    void focus_schedules (bool focus);
    void focus_targets (bool focus);
    void focus_configs (bool focus);
    void focus_escalators (bool focus);
    void focus_credentials (bool focus);
    void focus_agents (bool focus);
    void focus_notes (bool focus);
    void focus_overrides (bool focus);
    void focus_slaves (bool focus);
    void focus_performance (bool focus);
    void focus_reportformats (bool focus);
    void focus_portlists (bool focus);

    void save_config ();

    void setLanguage (QString l);
    void updateDashboard ();

  private slots:
    void about_dlg ();
    void activate_port_list_actions ();
    void login_dlg ();
    void start_gsa ();
    void new_task_dlg ();
    void new_config_dlg ();
    void new_target_dlg ();
    void new_schedule_dlg ();
    void new_escalator_dlg ();
    void new_credential_dlg ();
    void new_agent_dlg ();
    void new_slave_dlg ();
    void new_port_list_dlg ();
    void modify_note_dlg ();
    void modify_override_dlg ();
    void modify_config_dlg ();
    void modify_task_dlg ();
    void details_config_dlg ();
    void details_credential_dlg ();
    void details_escalator_dlg ();
    void details_schedule_dlg ();
    void details_target_dlg ();
    void details_note_dlg ();
    void details_override_dlg ();
    void details_slave_dlg ();
    void details_task_dlg ();
    void details_port_list_dlg ();
    void details_family_dlg (QString config, QString name);
    void details_nvt_dlg (QString config, QString oid);
    void delete_config ();
    void delete_target ();
    void delete_schedule ();
    void delete_escalator ();
    void delete_credential ();
    void delete_agent ();
    void delete_note ();
    void delete_override ();
    void delete_slave ();
    void delete_port_list ();

    void show_report (QString id, QString task);

    void update ();
    void update_start ();
    void update_stop ();

    void in_progress ();
    void progress_ready ();

    void update_trigger ();
    void update_started ();
    void update_finished ();
    void progress_timeout ();

    void task_start ();
    void task_stop ();
    void task_delete ();
    void task_pause ();
    void task_resume ();

    void config_download ();
    void config_upload ();
    void system_report_download (QString name, int duration);

    void schedule_dw ();
    void target_dw ();
    void config_dw ();
    void escalator_dw ();
    void credential_dw ();
    void agent_dw ();
    void note_dw ();
    void override_dw ();
    void slave_dw ();
    void performance_dw ();
    void update_dw ();
    void reportformat_dw ();
    void portlists_dw ();

    void task_selectionchanged (const QItemSelection &sel,
                                const QItemSelection &desel);
    void config_selectionchanged (const QItemSelection &sel,
                                  const QItemSelection &desel);
    void target_selectionchanged (const QItemSelection &sel,
                                  const QItemSelection &desel);
    void schedule_selectionchanged (const QItemSelection &sel,
                                    const QItemSelection &desel);
    void escalator_selectionchanged (const QItemSelection &sel,
                                     const QItemSelection &desel);
    void credential_selectionchanged (const QItemSelection &sel,
                                      const QItemSelection &desel);
    void agent_selectionchanged (const QItemSelection &sel,
                                 const QItemSelection &desel);
    void note_selectionchanged (const QItemSelection &sel,
                                const QItemSelection &desel);
    void override_selectionchanged (const QItemSelection &sel,
                                    const QItemSelection &desel);
    void slave_selectionchanged (const QItemSelection &sel,
                                 const QItemSelection &desel);
    void portlist_selectionchanged (const QItemSelection &sel,
                                    const QItemSelection &desel);
    void write_settings ();
    void read_settings ();
    void clear_dock_widget_settings ();

    void setLangDefault ();
    void setLangEnglish ();
    void setLangGerman ();
    void setLangFrench ();
    void setLangIndonesian ();
    void setLangSpanish ();
    void languageChanged ();

  protected:
    void closeEvent (QCloseEvent *event);
  private:
    gsd_control *control;

    QLabel *la_update;
    QSpinBox *sb_update;
    QLabel *la_sec;
    QToolButton *tb_startUpdate;
    QProgressBar *pbar_update;
    QLabel *la_prog_update;
    QDockWidget *dw_update;
    QWidget *dockWidgetContents_update;
    QGridLayout *updateLayout;
    QToolButton *tb_set_refresh;
    QToolButton *tb_stop_refresh;
    QLabel *la_stat_update;
    QLabel *la_val_update;
    QTableView *tw_tasks;
    QToolBar *scheduleBar;

    QMovie *progressIcon;
    QLabel *la_status_progress;

    int updateInterval;
    QLabel *statuslabel;

    QString lang;

    dock_performance *performance;
    dock_table *tasks;
    dock_table *schedules;
    dock_table *targets;
    dock_table *configs;
    dock_table *escalators;
    dock_table *credentials;
    dock_table *agents;
    dock_table *notes;
    dock_table *overrides;
    dock_table *slaves;
    dock_table *reportformats;
    dock_table *quickInfo;
    dock_table *portlists;
    delegate_progress *taskProgress;
    delegate_trend *trendIcon;
    delegate_threat *threatIcon;
    delegate_icon *icon;
    delegate_date_time *date_time;
    delegate_text *text;

    QMap<QString, dock_details*> details_widgets;
    QTimer *progressUpdateTimer;
    void createToolBars ();
    void createContextMenu ();
    void createStatusBar ();
    void createUpdateWidget ();
    void createConnections ();
    void prepareWidgets ();
    void setLoggedOut ();
    void selectRow (QTableView *table, model_omp_entity *model);
    bool eventFilter (QObject *obj, QEvent *e);
    void closeDockWidgets ();
    void clearDashboard ();

  public:
    gsd_mw (gsd_control *ctl);
    ~gsd_mw ();
};
#endif

