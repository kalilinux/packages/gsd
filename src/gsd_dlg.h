/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file gsd_dlg.h
 * @class gsd_dlg
 * @brief Protos and data structures for gsd_dlg.
 */

#ifndef GSD_DLG_H
#define GSD_DLG_H

#include <QtGui>

#include "omp_utilities.h"
#include "gsd_dialogs.h"


class gsd_control;

class gsd_dlg : public QObject
{

  Q_OBJECT

  signals:
    void sig_login (QString, int, QString, QString);
    void sig_check_server (QString, int, QString, QString);
    void sig_create (int, QMap<QString, QString>);
    void sig_modify (int, QMap<QString, QString>);

  private slots:
    void login_update (int);
    void accept_login (QString, int, QString, QString);
    void check_server (QString, int, QString, QString);
    void create (int, QMap<QString, QString>);
    void modify (int, QMap<QString, QString>);
    void authentication_successful ();
    void check_result (int);

    void update_task_config (const QModelIndex&, int, int);
    void update_task_target (const QModelIndex&, int, int);
    void update_task_schedule (const QModelIndex&, int, int);
    void update_task_escalator (const QModelIndex&, int, int);
    void update_task_slave (const QModelIndex&, int, int);
    void update_task_schedule_modify (const QModelIndex&, int, int);
    void update_task_escalator_modify (const QModelIndex&, int, int);
    void update_task_slave_modify (const QModelIndex&, int, int);

    void update_target_credentials (const QModelIndex&, int, int);
    void update_target_port_lists (const QModelIndex&, int, int);
  public slots:
    void login_dlg ();
    void about_dlg ();
    void start_gsa_dlg ();
    void new_agent_dlg ();
    void new_config_dlg ();
    void new_credential_dlg ();
    void new_escalator_dlg ();
    void new_schedule_dlg ();
    void new_target_dlg ();
    void new_task_dlg ();
    void new_slave_dlg ();
    void new_port_list_dlg ();
    void modify_task_dlg (int pos);

    void request_failed (int, int, int);
  private:
    gsd_control *control;

    dlg_about *about;
    dlg_login *login;
    dlg_start_gsa *start_gsa;

    dlg_new_task *new_task;
    dlg_new_target *new_target;
    dlg_new_config *new_config;
    dlg_new_schedule *new_schedule;
    dlg_new_escalator *new_escalator;
    dlg_new_credential *new_credential;
    dlg_new_agent *new_agent;
    dlg_new_slave *new_slave;
    dlg_modify_task *modify_task;
    dlg_new_port_list *new_port_list;

  public:
    gsd_dlg (gsd_control *ctl);
    ~gsd_dlg ();

    bool loginOpen ();
};
#endif

