/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "dock_details.h"
#include "gsd_control.h"
/**
 * @file dock_details.cpp
 * @brief Implements the slots and functions for details dock widgets.
 */

/**
 * @section class dock_details_task.
 */

/**
 * @brief Shows content in the dialog.
 */
void
dock_details_task::load ()
{
  QToolBar *toolBar = new QToolBar ();
  gridLayout_2->addWidget (toolBar, 0, 0, 1, 1);
  toolBar->setToolButtonStyle (Qt::ToolButtonIconOnly);
  toolBar->setMinimumSize(QSize(0, 0));
  toolBar->setSizeIncrement(QSize(28, 28));
  toolBar->setBaseSize(QSize(28, 28));
  toolBar->setIconSize(QSize(18, 18));
  toolBar->setFloatable (false);
  toolBar->setMovable (false);
  toolBar->toggleViewAction ()->setVisible (false);
  toolBar->addAction (actionResults);
  toolBar->addAction (actionDelete);
  actionResults->setEnabled (false);
  actionDelete->setEnabled (false);
  connect (actionResults,
           SIGNAL (triggered (bool)),
           this,
           SLOT (report ()));
  connect (actionDelete,
           SIGNAL (triggered (bool)),
           this,
           SLOT (delete_report ()));
  tv_reports->setModel (reports);
  connect (tv_reports->selectionModel (),
           SIGNAL (currentChanged (const QModelIndex&, const QModelIndex&)),
           this,
           SLOT (report_selection_changed (const QModelIndex&,
                                            const QModelIndex&)));
  connect (tv_reports,
           SIGNAL (doubleClicked (const QModelIndex&)),
           this,
           SLOT (report ()));
}


void
dock_details_task::update ()
{
  model_omp_entity *tasks = this->control->getTaskModel ();
  QDomElement task;
  int i = 0;
  while (i < tasks->rowCount())
    {
      QString attr = tasks->getAttr(tasks->getEntity(i), "task id");

      if (attr.compare (this->id) == 0)
        {
          task = tasks->getEntity (i);
        }
      i++;
    }
  this->setWindowTitle ("Task " + tasks->getValue (task, "name"));
  this->la_name->setText (tasks->getValue (task, "name"));
  this->la_comment->setText (tasks->getValue (task, "comment"));
  this->la_config->setText (tasks->getValue (task, "config name"));
  this->la_escalator->setText (tasks->getValue (task, "escalator name"));
  this->la_schedule->setText (tasks->getValue (task, "schedule name"));
  this->la_target->setText (tasks->getValue (task, "target name"));
  this->la_slave->setText (tasks->getValue (task, "slave name"));
  this->la_status->setText (tasks->getValue (task, "status"));
  this->la_reports->setText (tasks->getValue (task, "report_count") +
                             " (finished: " +
                             tasks->getValue (task, "report_count finished") +
                             ")");

  delegate_threat *delegate = new delegate_threat ();
  delegate_date_time *d_time = new delegate_date_time ();

  tv_reports->setItemDelegateForColumn (0, d_time);
  tv_reports->setItemDelegateForColumn (1, delegate);
  reports->removeEntities ();
  QDomElement rep = tasks->getEntities (task, "reports report", "reports");
  if (rep.hasChildNodes ())
    {
      QDomNodeList list = rep.childNodes ();
      int j = 0;
      while (j < list.size ())
        {
          reports->addEntity (list.at (j).toElement (), j);
          j++;
        }
    }

  model_omp_entity *notes = this->control->getNoteModel ();
  notes->setFilterType (model_omp_entity::ATTRIBUTE_FILTER);
  notes->setFilter ("note task id " + id);
  this->tv_notes->setModel (notes);

  QList <int> list = notes->getFilterIndexList ();
  for (int i = 0; i < notes->rowCount (); i++)
    {
      if (!list.contains (i))
        tv_notes->hideRow (i);
      else if (tv_notes->isRowHidden(i))
        tv_notes->showRow (i);
    }
  notes->resetFilter ();

  model_omp_entity *overrides = this->control->getOverrideModel ();
  overrides->setFilterType (model_omp_entity::ATTRIBUTE_FILTER);
  overrides->setFilter ("override task id " + id);
  this->tv_overrides->setModel (overrides);
  list = overrides->getFilterIndexList ();
  for (int i = 0; i < overrides->rowCount (); i++)
    {
      if (!list.contains (i))
        tv_overrides->hideRow (i);
      else if (tv_notes->isRowHidden(i))
        tv_overrides->showRow (i);
    }
  overrides->resetFilter ();
  tv_reports->verticalHeader ()->setVisible (false);
  tv_reports->horizontalHeader ()->setHighlightSections (false);
  tv_reports->horizontalHeader ()->setMinimumSectionSize (60);
  tv_reports->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_reports->setSelectionMode (QAbstractItemView::SingleSelection);
  tv_reports->setAlternatingRowColors (true);
  tv_reports->setSelectionBehavior (QAbstractItemView::SelectRows);
  tv_reports->setColumnWidth (0, 170);
  tv_reports->setColumnWidth (1, 80);
  tv_reports->setColumnWidth (2, 70);
  tv_reports->setColumnWidth (3, 70);
  tv_reports->setColumnWidth (4, 70);
  tv_reports->setColumnWidth (5, 70);
  tv_reports->setColumnWidth (6, 70);

  tv_overrides->setColumnWidth (0, 185);
  tv_overrides->setColumnWidth (1, 80);
  tv_overrides->setColumnWidth (2, 80);
  tv_overrides->setColumnWidth (3, 250);
  tv_overrides->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_overrides->verticalHeader ()->setVisible (false);
  tv_overrides->horizontalHeader ()->setHighlightSections (false);
  tv_overrides->horizontalHeader ()->setMinimumSectionSize (75);
  tv_overrides->verticalHeader ()->setVisible (false);
  tv_overrides->setSelectionMode (QAbstractItemView::SingleSelection);
  tv_overrides->setAlternatingRowColors (true);
  tv_overrides->setSelectionBehavior (QAbstractItemView::SelectRows);

  tv_notes->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_notes->setColumnWidth (0, 150);
  tv_notes->setColumnWidth (1, 300);
  tv_notes->verticalHeader ()->setVisible (false);
  tv_notes->horizontalHeader ()->setHighlightSections (false);
  tv_notes->horizontalHeader ()->setMinimumSectionSize (75);
  tv_notes->verticalHeader ()->setVisible (false);
  tv_notes->setSelectionMode (QAbstractItemView::SingleSelection);
  tv_notes->setAlternatingRowColors (true);
  tv_notes->setSelectionBehavior (QAbstractItemView::SelectRows);
}


void
dock_details_task::report ()
{
  QModelIndex ndx = tv_reports->selectionModel ()->currentIndex ();
  if (!ndx.isValid ())
    return;

  QDomElement task;
  int i = 0;
  while (i < control->getTaskModel ()->rowCount())
    {
      QString attr = control->getTaskModel ()
                              ->getAttr(control->getTaskModel ()->getEntity(i),
                                        "task id");

      if (attr.compare (this->id) == 0)
        {
          task = control->getTaskModel ()->getEntity (i);
        }
      i++;
    }

  QString report = reports->getAttr (reports->getEntity (ndx.row ()), "report id");
  QString task_id = control->getTaskModel ()->getAttr (task, "task id");
  emit sig_report (report, task_id);
}


void
dock_details_task::report_selection_changed (const QModelIndex &sel,
                                             const QModelIndex &desel)
{
  if (sel.isValid ())
    {
      actionResults->setEnabled (true);
      actionDelete->setEnabled (true);
    }
  else
    {
      actionResults->setEnabled (false);
      actionDelete->setEnabled (false);
    }
}


void
dock_details_task::delete_report ()
{
  QModelIndex ndx = tv_reports->selectionModel ()->currentIndex ();
  if (!ndx.isValid ())
    {
      QMessageBox::information (NULL,tr ("Status Error"),
                                tr ("No report selected!"));
      return;
    }
  QString id = reports->getAttr (reports->getEntity (ndx.row ()), "report id");
  emit sig_delete_report (id);
}

void
dock_details_port_list::load ()
{

}

void
dock_details_port_list::update (QString id)
{
  Q_UNUSED (id);
  update();
}

void
dock_details_port_list::update ()
{
  if (id.compare ("") == 0)
    return;

  model_omp_entity *targets = this->control->getTargetModel();
  tv_targets_using->setModel (targets);
  targets->setFilterType (model_omp_entity::ATTRIBUTE_FILTER);
  targets->setFilter ("target port_list id " + id);

  QList <int> filterList = targets->getFilterIndexList ();
  for (int i = 0; i < targets->rowCount (); i++)
    {
      if (!filterList.contains (i))
        tv_targets_using->hideRow (i);
      else if (tv_targets_using->isRowHidden(i))
        tv_targets_using->showRow (i);
    }

  targets->resetFilter ();

  model_omp_entity *port_lists = this->control->getPortListModel();
  QDomElement showElement;
  int i = 0;
  while (i < port_lists->rowCount())
    {
      QString attr = port_lists->getAttr(port_lists->getEntity(i),
                                      "port_list id");

      if (attr.compare (id) == 0)
        {
          showElement = port_lists->getEntity (i);
          break;
        }
      i++;
    }

  QDomElement port_range;

  port_range = this->control
                 ->getPortListDetailsModel ()
                   ->getEntities (this->control
                                        ->getPortListDetailsModel ()
                                          ->getEntity (0),
                                  "port_ranges",
                                  "port_list");

  QDomNodeList rangeList = port_range.elementsByTagName ("port_range");
  portRanges->removeEntities ();
  i = 0;
  while (i < rangeList.length ())
    {
      portRanges->addEntity (rangeList.item (i).toElement (), i);
      i++;
    }
  portRanges->addHeader (tr ("Start"), "start");
  portRanges->addHeader (tr ("End"), "end");
  portRanges->addHeader (tr ("Type"), "type");
  tv_port_ranges->setModel (portRanges);

  setWindowTitle ("Port List - " + port_lists->getValue (showElement,
                                                      "name"));
  la_name_v->setText (port_lists->getValue (showElement, "name"));
  la_comment_v->setText (port_lists->getValue (showElement, "comment"));
  la_count_v->setText (port_lists->getValue (showElement,
                                          "port_count all"));
  la_tcp_ports_v->setText (port_lists->getValue (showElement,
                                            "port_count tcp"));
  la_udp_ports_v->setText (port_lists->getValue (showElement,
                                             "port_count udp"));
  load ();
}

void
dock_details_target::load ()
{
  delegate_date_time *d_time = new delegate_date_time ();
  delegate_icon *d_icon = new delegate_icon ();
  delegate_progress *d_prog = new delegate_progress ();
  delegate_text *d_text = new delegate_text ();
  delegate_threat *d_thre = new delegate_threat ();
  delegate_trend *d_tren = new delegate_trend ();

  tv_table->setItemDelegateForColumn (1, d_icon);
  tv_table->setItemDelegateForColumn (2, d_prog);
  tv_table->setItemDelegateForColumn (4, d_time);
  tv_table->setItemDelegateForColumn (5, d_time);
  tv_table->setItemDelegateForColumn (6, d_thre);
  tv_table->setItemDelegateForColumn (7, d_tren);
  tv_table->verticalHeader ()->setVisible (false);
  tv_table->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_table->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_table->setColumnWidth (0, 120);
  tv_table->horizontalHeader ()->setHighlightSections (false);
  tv_table->horizontalHeader ()->setMinimumSectionSize (75);
  tv_table->setAlternatingRowColors (true);
  tv_table->setSelectionBehavior (QAbstractItemView::SelectRows);
  tv_table->hideColumn (1);
  tv_table->hideColumn (2);
  tv_table->hideColumn (4);
  tv_table->hideColumn (5);
  tv_table->hideColumn (6);

}


void
dock_details_target::update ()
{
  if (id.compare ("") == 0)
    return;

  model_omp_entity *tasks = this->control->getTaskModel ();
  tv_table->setModel (tasks);
  tasks->setFilterType (model_omp_entity::ATTRIBUTE_FILTER);
  tasks->setFilter ("task target id " + id);

  QList <int> filterList = tasks->getFilterIndexList ();
  for (int i = 0; i < tasks->rowCount (); i++)
    {
      if (!filterList.contains (i))
        tv_table->hideRow (i);
      else if (tv_table->isRowHidden(i))
        tv_table->showRow (i);
    }

  tasks->resetFilter ();

  model_omp_entity *targets = this->control->getTargetModel ();
  QStringList list = this->id.split (" ");
  QDomElement showElement;
  int i = 0;
  while (i < targets->rowCount())
    {
      QString attr = targets->getAttr(targets->getEntity(i), "target id");

      if (attr.compare (id) == 0)
        {
          showElement = targets->getEntity (i);
          break;
        }
      i++;
    }

  this->setWindowTitle ("Target " + targets->getValue (showElement, "name"));
  this->la_name_v->setText (targets->getValue (showElement, "name"));
  this->la_comment_v->setText (targets->getValue (showElement, "comment"));
  this->la_hosts_v->setText (targets->getValue (showElement, "hosts"));
  this->la_max_hosts_v->setText (targets->getValue (showElement, "max_hosts"));
  this->la_sshcredential_v->setText (targets->getValue (showElement,
                                                        "ssh_lsc_credential"
                                                        " name"));
  this->la_smbcredential_v->setText (targets->getValue (showElement,
                                                        "smb_lsc_credential"
                                                        " name"));
  this->la_port_list_v->setText (targets->getValue (showElement,
                                                    "port_list"
                                                    " name"));
  load ();
}


void
dock_details_credential::load ()
{
  tv_table->verticalHeader ()->setVisible (false);
  tv_table->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_table->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_table->setColumnWidth (0, 120);
  tv_table->horizontalHeader ()->setHighlightSections (false);
  tv_table->horizontalHeader ()->setMinimumSectionSize (75);
  tv_table->setAlternatingRowColors (true);
  tv_table->setSelectionBehavior (QAbstractItemView::SelectRows);
  tv_table->hideColumn (2);
  tv_table->hideColumn (3);

}


void
dock_details_credential::update ()
{
  if (id == NULL || id.compare ("") == 0)
    return;
  model_omp_entity *credentials = this->control->getCredentialModel ();
  QStringList list = id.split (" ");
  QDomElement showElement;
  int i = 0;
  while (i < credentials->rowCount())
    {
      QString attr = credentials->getAttr(credentials->getEntity(i),
                                          "credential id");

      if (attr.compare (id) == 0)
        {
          showElement = credentials->getEntity (i);
        }
      i++;
    }
  this->setWindowTitle ("Credential " +
                        credentials->getValue (showElement, "name"));
  this->la_name_v->setText (credentials->getValue (showElement, "name"));
  this->la_comment_v->setText (credentials->getValue (showElement, "comment"));
  this->la_login_v->setText (credentials->getValue (showElement, "login"));
  if (credentials->getValue (showElement, "in_use").compare ("1") == 0)
    tb_edit->setEnabled (false);
  else
    tb_edit->setEnabled (true);

  model_omp_entity *targets = this->control->getTargetModel ();
  tv_table->setModel (targets);
  targets->setFilterType (model_omp_entity::ATTRIBUTE_FILTER);
  targets->setFilter ("target ssh_lsc_credential id " + id);
  QList <int> filterList = targets->getFilterIndexList ();
  targets->setFilterType (model_omp_entity::NO_FILTER);
  targets->setFilter ("");
  targets->setFilterType (model_omp_entity::ATTRIBUTE_FILTER);
  targets->setFilter ("target smb_lsc_credential id " + id);
  filterList += targets->getFilterIndexList ();
  for (int i = 0; i < targets->rowCount (); i++)
    {
      if (!filterList.contains (i))
        tv_table->hideRow (i);
      else if (tv_table->isRowHidden(i))
        tv_table->showRow (i);
    }

  targets->resetFilter ();
  load ();
}


void
dock_details_credential::modify_credential ()
{
  dlg_modify_credential *modify_credential = new dlg_modify_credential ();
  connect (modify_credential,
           SIGNAL (sig_modify (QString, QString, QString, QString)),
           this,
           SLOT (exec_modify_credential (QString, QString, QString, QString)));

  model_omp_entity *credentials = this->control->getCredentialModel ();
  QStringList list = id.split (" ");
  QDomElement showElement;
  int i = 0;
  while (i < credentials->rowCount())
    {
      QString attr = credentials->getAttr(credentials->getEntity(i),
                                          "credential id");

      if (attr.compare (id) == 0)
        {
          showElement = credentials->getEntity (i);
        }
      i++;
    }
  modify_credential->setName (credentials->getValue (showElement, "name"));
  modify_credential->setComment (credentials->getValue (showElement,
                                                        "comment"));
  modify_credential->setLogin (credentials->getValue (showElement, "login"));
  if (credentials->getValue (showElement, "type").compare ("gen") == 0)
    {
      modify_credential->setAutogenerated (true);
    }
  else
    {
      modify_credential->setAutogenerated (false);
    }
  modify_credential->show ();
}


void
dock_details_credential::exec_modify_credential (QString name,
                                                 QString comment,
                                                 QString login,
                                                 QString password)
{
  QMap<QString, QString> parameter;
  parameter.insert ("id", id);
  parameter.insert ("name", name);
  parameter.insert ("comment", comment);
  if (login.compare ("") != 0)
    parameter.insert ("login", login);
  if (password.compare ("") != 0)
    parameter.insert ("password", password);

  emit sig_modify (omp_utilities::LSC_CREDENTIAL, id, NULL, parameter);
}



void
dock_details_escalator::load ()
{
  delegate_date_time *d_time = new delegate_date_time ();
  delegate_icon *d_icon = new delegate_icon ();
  delegate_progress *d_prog = new delegate_progress ();
  delegate_text *d_text = new delegate_text ();
  delegate_threat *d_thre = new delegate_threat ();
  delegate_trend *d_tren = new delegate_trend ();

  tv_table->verticalHeader ()->setVisible (false);
  tv_table->setItemDelegateForColumn (1, d_icon);
  tv_table->setItemDelegateForColumn (2, d_prog);
  tv_table->setItemDelegateForColumn (4, d_time);
  tv_table->setItemDelegateForColumn (5, d_time);
  tv_table->setItemDelegateForColumn (6, d_thre);
  tv_table->setItemDelegateForColumn (7, d_tren);
  tv_table->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_table->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_table->setColumnWidth (0, 120);
  tv_table->horizontalHeader ()->setHighlightSections (false);
  tv_table->horizontalHeader ()->setMinimumSectionSize (75);
  tv_table->setAlternatingRowColors (true);
  tv_table->setSelectionBehavior (QAbstractItemView::SelectRows);
  tv_table->hideColumn (1);
  tv_table->hideColumn (2);
  tv_table->hideColumn (4);
  tv_table->hideColumn (5);
  tv_table->hideColumn (6);
}


void
dock_details_escalator::update ()
{
  QVariant var_prot_version = qApp->property("protocol_version");
  float protocol_version = -1;
  if (var_prot_version.isValid())
    protocol_version = var_prot_version.toFloat();

  if (id == NULL || id.compare ("") == 0)
    return;
  model_omp_entity *escalators = this->control->getEscalatorModel ();
  QStringList list = id.split (" ");
  QDomElement showElement;
  int i = 0;
  while (i < escalators->rowCount())
    {
      QString attr;
      if (protocol_version <= 3)
        attr = escalators->getAttr(escalators->getEntity(i),
                                         "escalator id");
      else
        attr = escalators->getAttr(escalators->getEntity(i),
                                         "alert id");

      if (attr.compare (id) == 0)
        {
          showElement = escalators->getEntity (i);
        }
      i++;
    }

  this->setWindowTitle ("Escalator " +
                        escalators->getValue (showElement, "name"));
  this->la_name_v->setText (escalators->getValue (showElement, "name"));
  this->la_comment_v->setText (escalators->getValue (showElement, "comment"));

  QString out;
  QString s1 = escalators->getValue (showElement, "condition");
  if (s1.compare ("Always") == 0)
    out = s1;
  else if (s1.compare ("Threat level changed") == 0)
    {
      QDomNodeList nodelist = showElement.namedItem ("condition").childNodes ();
      unsigned int i = 0;
      while (i < nodelist.length ())
        {
          if (nodelist.at (i).firstChildElement ().text ().compare ("direction")
               == 0)
            {
              out = s1 + " (" +
                    nodelist.at (i).toElement ().text ().replace ("direction",
                                                                  "") +
                    ")";
              break;
            }
          else
            out = s1;
          i++;
        }
    }
  else if (s1.compare ("Threat level at least") == 0)
    {
      QDomNodeList nodelist = showElement.namedItem ("condition").childNodes ();
      unsigned int i = 0;
      while (i < nodelist.length ())
        {
          if (nodelist.at (i).firstChildElement ().text ().compare ("level")
              == 0)
            out = s1 + " (" +
                  nodelist.at (i).toElement ().text ().replace ("level", "") +
                  ")";
          i++;
        }
    }
  this->la_condition_v->setText (out);

  s1 = escalators->getValue (showElement, "event");
  QString s2 = escalators->getValue (showElement, "event data");
  out = s1 + tr (" (to ") + s2 + ")";

  this->la_event_v->setText (out);

  out = "";
  s1 = escalators->getValue (showElement, "method");
  QDomNodeList nodelist = showElement.namedItem ("method").childNodes ();
  i = 0;
  if (s1.compare ("Email") == 0)
    {
      this->la_method_v_1->setText (s1);
      out = nodelist.at (1).toElement ().text ()
                                        .replace ("to_address", "");
      this->la_method_v_2->setText (tr ("To address: %1").arg (out));
      out = nodelist.at (2).toElement ().text ()
                                        .replace ("from_address", "");
      this->la_method_v_3->setText (tr ("From address: %1").arg (out));

      if (nodelist.at (3).toElement ().text ().compare ("0notice") == 0)
        {
          this->la_method_v_4->setText (tr ("Format: Summary"
                                      " (can include vulnerability"
                                      " details)"));
        }
      else
        {
          this->la_method_v_4->setText (tr ("Format: Simple Notice"));
        }
    }
  else if (s1.compare ("Syslog") == 0 )
    {
      if (nodelist.at (1).toElement ().text ().compare ("SNMPsubmethod") == 0)
        out = "SNMP";
      else
        out = "Syslog";
      this->la_method_v_1->setText (out);
    }

  model_omp_entity *tasks = this->control->getTaskModel ();
  tv_table->setModel (tasks);
  tasks->setFilterType(model_omp_entity::ATTRIBUTE_FILTER);
  if (protocol_version <= 3)
    tasks->setFilter ("task escalator id "+ id);
  else
    tasks->setFilter ("task alert id "+ id);
  QList <int> filterList = tasks->getFilterIndexList ();
  for (int i = 0; i < tasks->rowCount (); i++)
    {
      if (!filterList.contains (i))
        tv_table->hideRow (i);
      else if (tv_table->isRowHidden(i))
        tv_table->showRow (i);
    }

  tasks->resetFilter ();
  load ();
}


void
dock_details_schedule::load ()
{
  delegate_date_time *d_time = new delegate_date_time ();
  delegate_icon *d_icon = new delegate_icon ();
  delegate_progress *d_prog = new delegate_progress ();
  delegate_text *d_text = new delegate_text ();
  delegate_threat *d_thre = new delegate_threat ();
  delegate_trend *d_tren = new delegate_trend ();

  tv_table->verticalHeader ()->setVisible (false);
  tv_table->setItemDelegateForColumn (1, d_icon);
  tv_table->setItemDelegateForColumn (2, d_prog);
  tv_table->setItemDelegateForColumn (4, d_time);
  tv_table->setItemDelegateForColumn (5, d_time);
  tv_table->setItemDelegateForColumn (6, d_thre);
  tv_table->setItemDelegateForColumn (7, d_tren);
  tv_table->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_table->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_table->setColumnWidth (0, 120);
  tv_table->horizontalHeader ()->setHighlightSections (false);
  tv_table->horizontalHeader ()->setMinimumSectionSize (75);
  tv_table->setAlternatingRowColors (true);
  tv_table->setSelectionBehavior (QAbstractItemView::SelectRows);
  tv_table->hideColumn (1);
  tv_table->hideColumn (2);
  tv_table->hideColumn (4);
  tv_table->hideColumn (5);
  tv_table->hideColumn (6);

}


void
dock_details_schedule::update ()
{
  if (id == NULL || id.compare ("") == 0)
    return;

  model_omp_entity *schedules = this->control->getScheduleModel ();
  QStringList list = id.split (" ");
  QDomElement showElement;
  int i = 0;
  while (i < schedules->rowCount())
    {
      QString attr = schedules->getAttr (schedules->getEntity(i),
                                         "schedule id");

      if (attr.compare (id) == 0)
        {
          showElement = schedules->getEntity (i);
        }
      i++;
    }

  model_omp_entity *tasks = this->control->getTaskModel ();
  tv_table->setModel (tasks);
  tasks->setFilterType (model_omp_entity::ATTRIBUTE_FILTER);
  tasks->setFilter ("task schedule id " + id);

  QList <int> filterList = tasks->getFilterIndexList ();
  for (int i = 0; i < tasks->rowCount (); i++)
    {
      if (!filterList.contains (i))
        tv_table->hideRow (i);
      else if (tv_table->isRowHidden(i))
        tv_table->showRow (i);
    }

  tasks->resetFilter ();

  delegate_date_time *dateDelegate = new delegate_date_time ();

  this->setWindowTitle ("Schedule " +
                        schedules->getValue (showElement, "name"));
  this->la_name_v->setText (schedules->getValue (showElement, "name"));
  this->la_comment_v->setText (schedules->getValue (showElement, "comment"));
  QString first =
    dateDelegate->formatDate (schedules->getValue (showElement, "first_time"),
                              true);
  this->la_first_time_v->setText (first);
  QString next =
    dateDelegate->formatDate (schedules->getValue (showElement, "next_time"),
                              true);
  this->la_next_time_v->setText (next);
  this->la_period_v->setText (schedules->getValue (showElement, "period"));
  this->la_duration_v->setText (schedules->getValue (showElement, "duration"));
  load ();
}


void
dock_details_config::selectionChanged_family (const QModelIndex &sel,
                                              const QModelIndex &desel)
{
  if (sel.isValid ())
    {
      if (editable)
        actionModify_Family->setEnabled (true);
      actionDetails_Family->setEnabled (true);
    }
}


void
dock_details_config::selectionChanged_scanPref (const QModelIndex &sel,
                                                const QModelIndex &desel)
{
  if (sel.isValid ())
    {
      if (editable)
        actionModify_ScanPref->setEnabled (true);
    }
}


void
dock_details_config::selectionChanged_nvtPref (const QModelIndex &sel,
                                               const QModelIndex &desel)
{
  if (sel.isValid ())
    {
      if (editable)
        actionModify_NVTPref->setEnabled (true);
    }
}


void
dock_details_config::modify_scan_pref ()
{
  QModelIndex ndx = tv_scan_pref->selectionModel ()->currentIndex ();
  if (ndx.isValid () && editable)
    {
      QVariant data = tv_scan_pref->model ()->data (ndx);
      dlg_modify_scan_pref *modify_scan_pref = new dlg_modify_scan_pref ();
      connect (modify_scan_pref,
               SIGNAL (sig_modify (QString)),
               this,
               SLOT (exec_modify_scanpref (QString)));
      QString name = preferences->getValue (preferences->getEntity (ndx.row ()),
                                                                    "name");
      QString value = preferences->getValue (preferences->getEntity (ndx.row ()),
                                                                     "value");
      if (value.compare ("yes") == 0 || value.compare ("no") == 0)
        {
         QStringList list;
         if (value.compare ("yes") == 0)
            {
              list.append ("yes");
              list.append ("no");
            }
          else
            {
              list.append ("no");
              list.append ("yes");
            }
            modify_scan_pref->setComboBox (list);
        }
      else
        {
          modify_scan_pref->setText (value);
        }

      modify_scan_pref->setName (data.toString ());
      modify_scan_pref->show ();
    }
}


void
dock_details_config::modify_config ()
{
  QModelIndex ndx = tv_nvt_fam->selectionModel ()->currentIndex ();
  dlg_modify_config *modify_config_dlg = new dlg_modify_config ();
  connect (modify_config_dlg,
           SIGNAL (sig_modify (bool, bool)),
           this,
           SLOT (exec_modify_config (bool, bool)));
  connect (modify_config_dlg,
           SIGNAL (sig_add_family (QString)),
           this,
           SLOT (add_family (QString)));
  QDomElement family = families->getEntity (ndx.row ());
  QString name = families->getValue (family, "name");
  modify_config_dlg->setName (name);
  if (families->getValue (family, "growing").compare ("1") == 0)
    {
      modify_config_dlg->setTrend (true);
    }
  else
    {
      modify_config_dlg->setTrend (false);
    }
  bool *ok = false;
  int count = families->getValue (family, "nvt_count").toInt (ok, 10);
  int max_count = families->getValue (family, "max_nvt_count").toInt (ok,
                                                                      10);
  if (count == max_count)
    modify_config_dlg->setSelected (true);
  else
    modify_config_dlg->setSelected (false);

  if (families->rowCount () != this->control->getFamilyModel ()->rowCount ())
    {
      int i = 0, j = 0;
      while (i <= families->rowCount () &&
             j < this->control->getFamilyModel ()->rowCount ())
        {
          QString name_used = families->getValue (families->getEntity (i),
                                                  "name");
          QString name_unused =
            this->control->getFamilyModel ()
                           ->getValue (this->control
                                             ->getFamilyModel ()
                                               ->getEntity (j), "name");
          if (name_used.compare (name_unused) != 0)
            modify_config_dlg->addFamily (name_unused);
          else
            i++;
          j++;
        }
    }
  modify_config_dlg->show ();
}


void
dock_details_config::exec_modify_config (bool trend, bool selected)
{
  QModelIndex ndx = tv_nvt_fam->selectionModel ()->currentIndex ();
  if (!ndx.isValid ())
    return;
  QDomElement family = families->getEntity (ndx.row ());
  QString name = families->getValue (family, "name");
  QString growing = families->getValue (family, "growing");
  QString nvt_count = families->getValue (family, "nvt_count");
  QString max_nvt_count = families->getValue (family, "max_nvt_count");

  QString all;
  bool changed = false, *ok = false;
  if ((nvt_count != max_nvt_count && selected) ||
      (nvt_count == max_nvt_count && !selected))
    {
      if (selected)
        all = "1";
      else
        all = "0";
      changed = true;
    }
  else
    {
      if (nvt_count != max_nvt_count)
        all = "0";
      else
        all = "1";
    }
  QString trend_t;
  if ((growing.compare ("0") == 0 && trend) ||
      (growing.compare ("1") == 0 && !trend))
    {
      if (trend)
        trend_t ="1";
      else
        trend_t ="0";
      changed = true;
    }
  else
    {
      trend_t = growing;
    }
  if (changed)
    {
      QMap<QString, QString> parameter;
      parameter.insert ("id", id);
      parameter.insert ("nvt family name", name);
      parameter.insert ("nvt family growing", trend_t);
      parameter.insert ("nvt family all", all);
      emit sig_modify (omp_utilities::CONFIG, id, families, parameter);
    }
}


void
dock_details_config::add_family (QString name)
{
  QMap<QString, QString> parameter;
  parameter.insert ("id", id);
  parameter.insert ("nvt family name", name);
  parameter.insert ("nvt family growing", "1");
  parameter.insert ("nvt family all", "1");
  emit sig_modify (omp_utilities::CONFIG, id, families, parameter);
}


void
dock_details_config::exec_modify_scanpref (QString value)
{
  QModelIndex ndx = tv_scan_pref->selectionModel ()->currentIndex ();
  if (!ndx.isValid ())
    return;

  QString name = preferences->getValue (preferences->getEntity (ndx.row ()),
                                                                "name");
  QString new_value;
  if (value.compare ("1") == 0)
    new_value = "yes";
  else if (value.compare ("0") == 0)
    new_value = "no";
  else
    new_value = value;

  QMap<QString, QString> parameter;
  parameter.insert ("id", id);
  parameter.insert ("preference name", "scanner[scanner]:"+name);
  parameter.insert ("preference value", new_value);
  emit sig_modify (omp_utilities::CONFIG, id, NULL, parameter);
}


void
dock_details_config::update (QString config_id)
{
  if (id.compare ("") == 0 || config_id.compare (id) != 0)
    return;

  QDomElement family;
  family = this->control
                 ->getConfigDetailsModel ()
                   ->getEntities (this->control
                                        ->getConfigDetailsModel ()
                                          ->getEntity (0),
                                  "families family",
                                  "families");
  QDomNodeList famList = family.elementsByTagName ("family");
  families->removeEntities ();
  int i = 0;
  while (i < famList.length ())
    {
      families->addEntity (famList.item (i).toElement (), i);
      i++;
    }

  QDomElement preference;
  preference = this->control
                     ->getConfigDetailsModel ()
                       ->getEntities (this->control
                                            ->getConfigDetailsModel ()
                                              ->getEntity (0),
                                      "preferences preference",
                                      "preferences");
  QDomNodeList prefList = preference.elementsByTagName ("preference");
  preferences->removeEntities ();
  i = 0;
  while (i < prefList.length ())
    {
      preferences->addEntity (prefList.item (i).toElement (), i);
      i++;
    }
  QToolBar *toolBar = new QToolBar ();
  // to add the toolbar to the dialog widget uncomment next line
  //gridLayout->addWidget (toolBar, 0,0,1,4);
  // to remove the toolbar from the tab comment this line
  gridLayout_2->addWidget (toolBar, 0, 0, 1, 1);
  toolBar->setToolButtonStyle (Qt::ToolButtonIconOnly);
  toolBar->setMinimumSize(QSize(0, 0));
  toolBar->setSizeIncrement(QSize(28, 28));
  toolBar->setBaseSize(QSize(28, 28));
  toolBar->setIconSize(QSize(18, 18));
  toolBar->setFloatable (false);
  toolBar->setMovable (false);
  toolBar->toggleViewAction ()->setVisible (false);
  toolBar->addAction (actionDetails_Family);
  toolBar->addAction (actionModify_Family);

  QToolBar *toolBarScan = new QToolBar ();
  gridLayout_3->addWidget (toolBarScan, 0, 0, 1, 1);
  toolBarScan->setToolButtonStyle (Qt::ToolButtonIconOnly);
  toolBarScan->setMinimumSize(QSize(0, 0));
  toolBarScan->setSizeIncrement(QSize(28, 28));
  toolBarScan->setBaseSize(QSize(28, 28));
  toolBarScan->setIconSize(QSize(18, 18));
  toolBarScan->setFloatable (false);
  toolBarScan->setMovable (false);
  toolBarScan->toggleViewAction ()->setVisible (false);

  QToolBar *toolBarNVT = new QToolBar ();
  gridLayout_4->addWidget (toolBarNVT, 0, 0, 1, 1);
  toolBarNVT->setToolButtonStyle (Qt::ToolButtonIconOnly);
  toolBarNVT->setMinimumSize(QSize(0, 0));
  toolBarNVT->setSizeIncrement(QSize(28, 28));
  toolBarNVT->setBaseSize(QSize(28, 28));
  toolBarNVT->setIconSize(QSize(18, 18));
  toolBarNVT->setFloatable (false);
  toolBarNVT->setMovable (false);
  toolBarNVT->toggleViewAction ()->setVisible (false);

  toolBar->addAction (actionDetails_Family);
  toolBar->addAction (actionModify_Family);
  toolBarScan->addAction (actionModify_ScanPref);
  toolBarNVT->addAction (actionModify_NVTPref);

  actionDetails_Family->setEnabled (false);
  actionModify_Family->setEnabled (false);
  actionModify_ScanPref->setEnabled (false);
  actionModify_NVTPref->setEnabled (false);

  model_omp_entity *configs = this->control->getConfigModel ();
  i = 0;
  QDomElement config;
  while (i < configs->rowCount())
    {
      QString attr =configs->getAttr (configs->getEntity(i), "config id");
      if (attr.compare (this->id) == 0)
        {
          config = configs->getEntity (i);
        }
      i++;
    }

  this->setWindowTitle ("Scan config " + configs->getValue (config, "name"));
  this->la_name->setText (configs->getValue (config, "name"));
  this->la_comment->setText (configs->getValue (config, "comment"));

  model_omp_entity *allFamilies = this->control->getFamilyModel ();

  delegate_trend *trend = new delegate_trend ();
  delegate_text *text = new delegate_text ();
  tv_nvt_fam->setItemDelegateForColumn (2, trend);
  tv_nvt_fam->setItemDelegateForColumn (1, text);
  families->addHeader ("Family", "name");
  families->addHeader ("NVT's selected", "nvt_count");
  families->addHeader ("Trend", "growing");
  tv_nvt_fam->setModel (families);
  tv_nvt_fam->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_nvt_fam->setColumnWidth (0, 150);
  tv_nvt_fam->setColumnWidth (1, 100);
  tv_nvt_fam->setColumnWidth (2, 60);
  tv_nvt_fam->verticalHeader ()->setVisible (false);
  tv_nvt_fam->horizontalHeader ()->setHighlightSections (false);
  tv_nvt_fam->horizontalHeader ()->setMinimumSectionSize (75);
  tv_nvt_fam->verticalHeader ()->setVisible (false);
  tv_nvt_fam->setSelectionMode (QAbstractItemView::SingleSelection);
  tv_nvt_fam->setAlternatingRowColors (true);
  tv_nvt_fam->setSelectionBehavior (QAbstractItemView::SelectRows);
  connect (tv_nvt_fam->selectionModel (), SIGNAL (currentChanged
                                                 (const QModelIndex &,
                                                  const QModelIndex &)), this,
           SLOT (selectionChanged_family (const QModelIndex &,
                                          const QModelIndex &)));

  preferences->addHeader ("NVT", "nvt name");
  preferences->addHeader ("Name", "name");
  preferences->addHeader ("Value", "value");
  preferences->setFilterType (model_omp_entity::VALUE_FILTER);
  preferences->setFilter ("preference nvt name ");
  QList <int> list = preferences->getFilterIndexList ();
  tv_nvt_pref->setModel (preferences);
  tv_nvt_pref->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_nvt_pref->setColumnWidth (0, 150);
  tv_nvt_pref->setColumnWidth (1, 180);
  tv_nvt_pref->setColumnWidth (2, 80);
  tv_nvt_pref->verticalHeader ()->setVisible (false);
  tv_nvt_pref->horizontalHeader ()->setHighlightSections (false);
  tv_nvt_pref->horizontalHeader ()->setMinimumSectionSize (75);
  tv_nvt_pref->verticalHeader ()->setVisible (false);
  tv_nvt_pref->setSelectionMode (QAbstractItemView::SingleSelection);
  tv_nvt_pref->setAlternatingRowColors (true);
  tv_nvt_pref->setSelectionBehavior (QAbstractItemView::SelectRows);
  connect (tv_nvt_pref->selectionModel (),
           SIGNAL (currentChanged (const QModelIndex &, const QModelIndex &)),
           this,
           SLOT (selectionChanged_nvtPref (const QModelIndex &,
                                            const QModelIndex &)));
  connect (tv_nvt_pref,
           SIGNAL (doubleClicked (const QModelIndex &)),
           this,
           SLOT (details_nvt ()));
  connect (actionModify_NVTPref,
           SIGNAL (triggered (bool)),
           this,
           SLOT (details_nvt ()));
  for (int i = 0; i < preferences->rowCount (); i++)
    {
      if (list.contains (i))
        {
          tv_nvt_pref->hideRow (i);
        }
      else if (tv_nvt_pref->isRowHidden(i))
        tv_nvt_pref->showRow (i);
    }

  tv_scan_pref->setModel (preferences);
  tv_scan_pref->horizontalHeader ()->setResizeMode (1, QHeaderView::Stretch);
  tv_scan_pref->setColumnWidth (1, 150);
  tv_scan_pref->setColumnWidth (2, 180);
  tv_scan_pref->verticalHeader ()->setVisible (false);
  tv_scan_pref->horizontalHeader ()->setHighlightSections (false);
  tv_scan_pref->horizontalHeader ()->setMinimumSectionSize (75);
  tv_scan_pref->verticalHeader ()->setVisible (false);
  tv_scan_pref->setSelectionMode (QAbstractItemView::SingleSelection);
  tv_scan_pref->setAlternatingRowColors (true);
  tv_scan_pref->setSelectionBehavior (QAbstractItemView::SelectRows);
  tv_scan_pref->hideColumn (0);
  for (int i = 0; i < preferences->rowCount (); i++)
    {
      if (!list.contains (i))
        {
          tv_scan_pref->hideRow (i);
        }
      else if (tv_scan_pref->isRowHidden(i))
        tv_scan_pref->showRow (i);
    }
  connect (tv_scan_pref->selectionModel (),
           SIGNAL (currentChanged (const QModelIndex &, const QModelIndex &)),
           this,
           SLOT (selectionChanged_scanPref (const QModelIndex &,
                                            const QModelIndex &)));


  model_omp_entity *tasks = this->control->getTaskModel ();
  tasks->setFilterType (model_omp_entity::ATTRIBUTE_FILTER);
  tasks->setFilter ("task config id " + id);

  delegate_trend *trend_task = new delegate_trend ();
  tv_tasks->setModel (tasks);
  tv_tasks->setItemDelegateForColumn (7, trend_task);

  tv_tasks->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_tasks->setColumnWidth (0, 120);
  tv_tasks->setColumnWidth (3, 60);
  tv_tasks->setColumnWidth (7, 60);
  tv_tasks->verticalHeader ()->setVisible (false);
  tv_tasks->horizontalHeader ()->setHighlightSections (false);
  tv_tasks->horizontalHeader ()->setMinimumSectionSize (75);
  tv_tasks->verticalHeader ()->setVisible (false);
  tv_tasks->setSelectionMode (QAbstractItemView::SingleSelection);
  tv_tasks->setAlternatingRowColors (true);
  tv_tasks->setSelectionBehavior (QAbstractItemView::SelectRows);

  list = tasks->getFilterIndexList ();

  for (int i = 0; i < tasks->rowCount (); i++)
    {
      if (!list.contains (i))
        {
          tv_tasks->hideRow (i);
        }
      else if (tv_tasks->isRowHidden(i))
        tv_tasks->showRow (i);
    }

  tv_tasks->hideColumn (1);
  tv_tasks->hideColumn (2);
  tv_tasks->hideColumn (4);
  tv_tasks->hideColumn (5);
  tv_tasks->hideColumn (6);
  tasks->resetFilter ();

  if (configs->getValue (config, "in_use").compare ("0") == 0)
    {
      this->editable = true;
      if (families->rowCount () == 0)
        actionModify_Family->setEnabled (true);
    }
  else
    {
      this->editable = false;
    }
}


void
dock_details_config::load ()
{
  actionDetails_Family->setEnabled (false);
  actionModify_Family->setEnabled (false);
  actionModify_ScanPref->setEnabled (false);
  actionModify_NVTPref->setEnabled (false);

  connect (tv_nvt_fam,
           SIGNAL (doubleClicked (const QModelIndex &)),
           this,
           SLOT (details_family ()));
  connect (actionDetails_Family,
           SIGNAL (triggered (bool)),
           this,
           SLOT (details_family ()));
  connect (actionModify_Family,
           SIGNAL (triggered (bool)),
           this,
           SLOT (modify_config ()));
  connect (actionModify_ScanPref,
           SIGNAL (triggered (bool)),
           this,
           SLOT (modify_scan_pref ()));
  connect (tv_scan_pref,
           SIGNAL (doubleClicked (const QModelIndex &)),
           this,
           SLOT (modify_scan_pref ()));
}

void
dock_details_config::details_family ()
{
  QModelIndex ndx = tv_nvt_fam->selectionModel ()->currentIndex ();
  if (ndx.isValid ())
    {
      QString name = families->getValue (families->getEntity (ndx.row ()),
                                                              "name");
      emit sig_details_family (this->id, name);
    }
}


void
dock_details_config::details_nvt ()
{
  QModelIndex ndx = tv_nvt_pref->selectionModel ()->currentIndex ();
  QString oid;

  if (ndx.isValid ())
    {
      oid = preferences->getAttr (preferences->getEntity (ndx.row ()),
                                                          "preference"
                                                          " nvt oid");
    }
  emit sig_details_nvt (id , oid);
}


void
dock_details_family::load ()
{


  nvtModel->addHeader (tr ("Name"), "name");
  nvtModel->addHeader (tr ("OID"), "oid");
  nvtModel->addHeader (tr ("Risk"), "risk_factor");
  nvtModel->addHeader (tr ("CVSS"), "cvss_base");
  nvtModel->addHeader (tr ("Timeout"), "timeout");
  nvtModel->addHeader (tr ("Prefs"), "preference_count");

  la_name_v->setText ("");
  la_family_v->setText ("");
  nvtModel->removeEntities ();
  QToolBar *toolBar = new QToolBar (this);
  toolBar->setToolButtonStyle (Qt::ToolButtonIconOnly);
  toolBar->setMinimumSize(QSize(0, 0));
  toolBar->setSizeIncrement(QSize(28, 28));
  toolBar->setBaseSize(QSize(28, 28));
  toolBar->setIconSize(QSize(18, 18));
  toolBar->setFloatable (false);
  toolBar->setMovable (false);
  toolBar->toggleViewAction ()->setVisible (false);
  this->gridLayout_2->addWidget (toolBar,3,0,1,2);
  actionDetails_NVT->setObjectName(QString::fromUtf8("actionDetails_Families"));
  QIcon icon1;
  icon1.addPixmap(QPixmap(QString::fromUtf8(":/img/details.png")),
                  QIcon::Normal, QIcon::Off);
  actionDetails_NVT->setIcon(icon1);
  actionDetails_NVT->setEnabled (false);
  toolBar->addAction (actionDetails_NVT);
  toolBar->addAction (actionModify_Family);
  if (editable)
    {
      actionModify_Family->setEnabled (true);
    }
  else
    {
      actionModify_Family->setEnabled (false);
    }
  connect (tv_table,
           SIGNAL (doubleClicked (const QModelIndex &)),
           this,
           SLOT (details_nvt ()));
  connect (actionDetails_NVT,
           SIGNAL (triggered (bool)),
           this,
           SLOT (details_nvt ()));
  connect (actionModify_Family,
           SIGNAL (triggered (bool)),
           this,
           SLOT (modify_family ()));
}


void
dock_details_family::update (QString family_name, QString config_id)
{
  if (family_name.compare (name) != 0 || config_id.compare (id) != 0)
    return;

  nvtModel->removeEntities ();
  int i = 0;
  while (i < this->control->getFamilyDetailsModel ()->rowCount ())
    {
      nvtModel->addEntity (this->control->getFamilyDetailsModel ()->getEntity (i), i);
      i++;
    }
  tv_table->setModel (nvtModel);
  tv_table->setColumnWidth (0, 160);
  tv_table->setColumnWidth (1, 180);
  tv_table->setColumnWidth (3, 60);
  tv_table->setColumnWidth (5, 60);

  delegate_text *text = new delegate_text ();
  tv_table->setItemDelegateForColumn (1, text);
  tv_table->setItemDelegateForColumn (4, text);
  tv_table->setItemDelegateForColumn (5, text);
  tv_table->verticalHeader ()->setVisible (false);
  tv_table->horizontalHeader ()->setHighlightSections (false);
  tv_table->horizontalHeader ()->setMinimumSectionSize (75);
  tv_table->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_table->setSelectionMode (QAbstractItemView::SingleSelection);
  tv_table->setAlternatingRowColors (true);
  tv_table->setSelectionBehavior (QAbstractItemView::SelectRows);
  tv_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
  tv_table->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
  tv_table->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
  connect (tv_table->selectionModel (),
           SIGNAL (currentChanged (const QModelIndex &,
                                   const QModelIndex &)),
           this,
           SLOT (nvt_selection_changed (const QModelIndex &,
                                        const QModelIndex &)));

  QString config;
  i = 0;
  while (i < this->control->getConfigModel ()->rowCount ())
    {
      QDomElement e = this->control->getConfigModel ()->getEntity (i);
      if (this->control
                ->getConfigModel ()
                  ->getAttr (e, "config id").compare (id) == 0)
        {
          config = this->control->getConfigModel ()->getValue (e, "name");
          break;
        }
      i++;
    }

  this->setWindowTitle ("Family " + name);
  la_name_v->setText (config);
  la_family_v->setText (name);
}


void
dock_details_family::modify_family ()
{
  dlg_modify_family *modify_family_dlg = new dlg_modify_family ();
  if (tv_table->selectionModel () != NULL)
    {
      QModelIndex ndx = tv_table->selectionModel ()->currentIndex ();
      if (!ndx.isValid ())
        return;
      else
        {
          modify_family_dlg->setNVT (nvtModel->getValue
                                      (nvtModel->getEntity (ndx.row ()),
                                                            "name"));
          modify_family_dlg->setSelected (true);
        }
    }
  modify_family_dlg->setName (name);

  model_omp_entity *allNVTsModel = this->control->getNVTModel ();

  int count = nvtModel->rowCount ();
  connect (modify_family_dlg,
           SIGNAL (sig_modify (bool)),
           this,
           SLOT (exec_modify_family (bool)));
  connect (modify_family_dlg,
           SIGNAL (sig_add_nvt (QString, QString)),
           this,
           SLOT (add_nvt (QString, QString)));
  if (allNVTsModel != NULL &&
      allNVTsModel->rowCount () > nvtModel->rowCount () &&
      editable)
    {
      int i = 0, j = 0, ndx = 0;
      bool contains = false;
      QString name_unused, name_used;
      while (i < allNVTsModel->rowCount ())
        {
          name_unused = nvtModel->getValue (allNVTsModel->getEntity (i),
                                            "name");
          j = 0;
          contains = false;
          while (j < nvtModel->rowCount ())
            {
              name_used = nvtModel->getValue (nvtModel->getEntity (j),
                                                "name");
              if (name_used.compare (name_unused) == 0)
                {
                  contains = true;
                }
              j++;
            }
          if  (!contains)
            {
              QString attr = allNVTsModel->getAttr (allNVTsModel->getEntity (i),
                                                    "nvt oid");
              modify_family_dlg->addNVT (name_unused, attr);
            }
          i++;
        }
    }
  modify_family_dlg->show ();
}


void
dock_details_family::exec_modify_family (bool value)
{
  QModelIndex ndx = tv_table->selectionModel ()->currentIndex ();
  if (!ndx.isValid ())
    return;
  QDomElement nvt = nvtModel->getEntity (ndx.row ());
  bool changed = false, *ok = false;
  if (!value)
    changed = true;
  if (changed)
    {
      QMap<QString, QString> parameter;
      parameter.insert ("id", id);
      parameter.insert ("nvt selection", "remove");
      parameter.insert ("nvt selection name", name);
      parameter.insert ("nvt oid", nvtModel->getAttr (nvt, "nvt oid"));
      emit sig_modify (omp_utilities::CONFIG, id, nvtModel, parameter);
    }
}


void
dock_details_family::add_nvt (QString oid, QString name)
{
  QMap<QString, QString> parameter;
  parameter.insert ("id", id);
  parameter.insert ("nvt selection", "add");
  parameter.insert ("nvt selection name", name);
  parameter.insert ("nvt oid", oid);
  emit sig_modify (omp_utilities::CONFIG, id, nvtModel, parameter);
}


void
dock_details_family::nvt_selection_changed (const QModelIndex &sel,
                                           const QModelIndex &desel)
{
  if (sel.isValid ())
    actionDetails_NVT->setEnabled (true);
}


void
dock_details_family::details_nvt ()
{
  QModelIndex ndx = tv_table->selectionModel ()->currentIndex ();
  QString oid;
  if (ndx.isValid ())
    {
      oid = nvtModel->getAttr (nvtModel->getEntity (ndx.row ()),
                                                          "nvt oid");
    }
  emit sig_details_nvt (id , oid);

}

void
dock_details_nvt::load ()
{
  QToolBar *toolBar = new QToolBar ();
  gridLayout_3->addWidget (toolBar, 0, 0, 1, 1);
  toolBar->setToolButtonStyle (Qt::ToolButtonIconOnly);
  toolBar->setMinimumSize(QSize(0, 0));
  toolBar->setSizeIncrement(QSize(28, 28));
  toolBar->setBaseSize(QSize(28, 28));
  toolBar->setIconSize(QSize(18, 18));
  toolBar->setFloatable (false);
  toolBar->setMovable (false);
  toolBar->toggleViewAction ()->setVisible (false);

  toolBar->addAction (actionModify_NVT);
  if (editable)
    {
      actionModify_NVT->setEnabled (true);
    }
  else
    {
      actionModify_NVT->setEnabled (false);
    }

}

void
dock_details_nvt::update (QString config_id, QString nvt_oid)
{
  if (nvt_oid.compare (this->oid) != 0 || config_id.compare (this->id) != 0)
    return;

  nvtModel->removeEntities ();
  prefModel->removeEntities ();

  int i = 0;
  while (i < this->control->getNVTDetailsModel ()->rowCount ())
    {
      nvtModel->addEntity (this->control->getNVTDetailsModel ()->getEntity (i), i);
      i++;
    }

  int ndx = 0;
  for (int i = 0; i < nvtModel->rowCount (); i++)
    {
      if (nvtModel->getAttr (nvtModel->getEntity (i), "nvt oid").compare (oid) == 0)
        {
          ndx = i;
          break;
        }
    }

  QDomElement element = nvtModel->getEntity (ndx);
  la_name->setText (nvtModel->getValue (element, "name"));
  la_summary->setText (nvtModel->getValue (element, "summary"));
  la_family->setText (nvtModel->getValue (element, "family"));
  la_oid->setText (this->oid);
  QString tmp = nvtModel->getValue (element, "cve_id");
  if (tmp.compare ("NOCVE") == 0)
    tmp = "";
  la_cve->setText (tmp);
  tmp = nvtModel->getValue (element, "bugtraq_id");
  if (tmp.compare ("NOBID") == 0)
    tmp = "";
  la_bugtraq->setText (tmp);
  tmp = nvtModel->getValue (element, "xrefs");
  if (tmp.compare ("NOXREF") == 0)
    tmp = "";
  la_references->setText (tmp);
  tmp = nvtModel->getValue (element, "tags");
  if (tmp.compare ("NOTAG") == 0)
    tmp = "";
  la_tags->setText (tmp);
  la_cvss->setText (nvtModel->getValue (element, "cvss_base"));
  la_risk->setText (nvtModel->getValue (element, "risk_factor"));
  this->setWindowTitle ("NVT " + nvtModel->getValue (element, "name"));
  te_description->setText (nvtModel->getValue (element, "description"));

  QString timeout = nvtModel->getValue (nvtModel->getEntity(ndx), "timeout");
  QDomDocument *doc = new QDomDocument ();
  QString temp =  QString ("<preference><name>Timeout</name>"
                           "<value>%1</value></preference>").arg (timeout);
  doc->setContent (temp);
  prefModel->addEntity (doc->documentElement (), 0);

  QDomElement e = nvtModel->getEntities (nvtModel->getEntity (ndx),
                                      "preferences preference", "pref");
  if (e.hasChildNodes ())
    {
      QDomNodeList list = e.childNodes ();
      int j = 1, i = 0;
      while (i < list.size ())
        {
          prefModel->addEntity (list.at (i).toElement (), j);
          j++;
          i++;
        }
    }

  tv_preferences->setModel (prefModel);
  tv_preferences->verticalHeader ()->setVisible (false);
  tv_preferences->setSelectionMode (QAbstractItemView::SingleSelection);
  tv_preferences->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_preferences->horizontalHeader ()->setResizeMode (0,
                                                      QHeaderView::Stretch);
  tv_preferences->setColumnWidth (0, 120);
  tv_preferences->horizontalHeader ()->setHighlightSections (false);
  tv_preferences->horizontalHeader ()->setMinimumSectionSize (75);
  tv_preferences->setAlternatingRowColors (true);
  tv_preferences->setSelectionBehavior (QAbstractItemView::SelectRows);

}


void
dock_details_nvt::modify_nvt ()
{
  QModelIndex ndx = tv_preferences->selectionModel ()->currentIndex ();
  if (ndx.isValid ())
    {
      dlg_modify_nvt *modify_nvt_dlg = new dlg_modify_nvt ();
      connect (modify_nvt_dlg,
               SIGNAL (sig_modify (QString)),
               this,
               SLOT (exec_modify_nvt (QString)));
      QDomElement pref = prefModel->getEntity (ndx.row ());
      QString name = prefModel->getValue (pref, "name");
      modify_nvt_dlg->setName (name);

      QString type = prefModel->getValue (pref, "type");
      if (type.compare ("radio") == 0)
        {
          QStringList list;
          list.append (prefModel->getValue (pref, "value"));
          QDomElement e = prefModel->getEntities (pref, "alt", NULL);
          if (e.hasChildNodes ())
            {
              QDomNodeList nodeList = e.childNodes ();
              for (int i = 0; i < nodeList.size (); i++)
                {
                  list.append (nodeList.at (i).toElement ().text ());
                }
            }
          modify_nvt_dlg->setComboBox (list);
        }
      else if (type.compare ("checkbox") == 0)
        {
          QStringList list;
          if (prefModel->getValue (pref, "value").compare ("yes") == 0)
            {
              list.append ("yes");
              list.append ("no");
            }
          else
            {
              list.append ("no");
              list.append ("yes");
            }
          modify_nvt_dlg->setComboBox (list);
        }
      else if (type.compare ("entry") == 0)
        {
          modify_nvt_dlg->setText (prefModel->getValue (pref, "value"));
        }
      else if (type.compare ("file") == 0)
        {
          modify_nvt_dlg->setFile ();
        }
      else if (type.compare ("password") == 0)
        {
          modify_nvt_dlg->setPassword ();
        }
      else if (type.compare ("") == 0)
        {
        }
      if (editable)
        modify_nvt_dlg->show ();
    }

}


void
dock_details_nvt::exec_modify_nvt (QString val)
{
  QModelIndex ndx = tv_preferences->selectionModel ()->currentIndex ();
  if (!ndx.isValid ())
    return;
  QDomElement pref = prefModel->getEntity (ndx.row ());

  QString name = prefModel->getValue (pref, "name");
  QString nvt_name = nvtModel->getValue (nvtModel->getEntity(0), "name");
  QString type = prefModel->getValue (pref, "type");
  QString value;
  if (val.compare ("1") == 0)
    value = "yes";
  else if (val.compare ("0") == 0)
    value = "no";
  else
    value = val;

  name = nvt_name + "[" + type + "]:" + name;
  QMap<QString, QString> parameter;
  parameter.insert ("id", id);
  parameter.insert ("preference name", name);
  parameter.insert ("preference value", value);
  parameter.insert ("nvt oid", oid);
  emit sig_modify (omp_utilities::CONFIG, id, prefModel, parameter);
}


void
dock_details_note::load ()
{

}


void
dock_details_note::update (QString note_id)
{
  if (note_id.compare (id) != 0)
    return;

  for (int i = 0; i < control->getNoteDetailsModel ()->rowCount (); i++)
    {
      note->addEntity (control->getNoteDetailsModel ()->getEntity (i), i);
    }

  QDomElement elem;
  int i = 0;
  while (i < note->rowCount ())
    {
      if (note->getAttr (note->getEntity (i), "note id").compare (id) == 0)
        {
          elem = note->getEntity (i);
          break;
        }
      i++;
    }

  delegate_date_time *dateDelegate = new delegate_date_time ();

  this->setWindowTitle ("Note " + note->getValue (elem, "nvt name"));
  this->task_id = note->getAttr (elem, "note task id");
  this->la_name_v->setText (note->getValue(elem, "nvt name"));
  this->la_oid_v->setText (note->getAttr (elem, "note nvt oid"));
  QString creation =
    dateDelegate->formatDate (note->getValue (elem, "creation_time"), true);
  this->la_created_v->setText (creation);
  QString modification = 
    dateDelegate->formatDate (note->getValue (elem, "modification_time"), true);
  this->la_modified_v->setText (modification);
  this->la_hosts_v->setText (note->getValue (elem, "hosts"));
  this->la_port_v->setText (note->getValue (elem, "port"));
  this->la_threat_v->setText (note->getValue (elem, "threat"));
  this->la_task_v->setText (note->getValue (elem, "task name"));
  QString res = note->getAttr (elem, "note result id");
  if (res.compare ("") == 0)
    res = "Any";
  this->la_result_v->setText (res);
  this->te_text->setText (note->getValue (elem, "text"));
}


void
dock_details_note::modify_note ()
{
  dlg_modify_note *modify_note_dlg = new dlg_modify_note ();
  connect (modify_note_dlg,
           SIGNAL (sig_modify (QString, QString, QString,
                               QString, QString, QString)),
           this,
           SLOT (exec_modify_note (QString, QString, QString,
                                   QString, QString, QString)));
  QDomElement elem;
  int i = 0;
  while (i < note->rowCount ())
    {
      if (note->getAttr (note->getEntity (i), "note id").compare (id) == 0)
        {
          elem = note->getEntity (i);
          break;
        }
      i++;
    }
  QString hosts, port, threat, task, result;
  hosts = note->getValue (elem, "hosts");
  port = note->getValue (elem, "port");
  threat = note->getValue (elem, "threat");
  task = note->getValue (elem, "task name");
  result = note->getAttr (elem, "note result id");

  if (hosts.compare ("") == 0)
    modify_note_dlg->setHosts ("any");
  else
    modify_note_dlg->setHosts (hosts);
  if (port.compare ("") == 0)
    modify_note_dlg->setPort ("any");
  else
    modify_note_dlg->setPort (port);
  if (threat.compare ("") == 0)
    modify_note_dlg->setThreat ("any");
  else
    modify_note_dlg->setThreat (threat);
  if (task.compare ("") == 0)
    modify_note_dlg->setTask ("any");
  else
    modify_note_dlg->setTask (task);
  if (result.compare ("") == 0)
    modify_note_dlg->setResult ("any");
  else
    modify_note_dlg->setResult (result);

  modify_note_dlg->setName (note->getValue (elem, "nvt name"));
  modify_note_dlg->setOID (note->getAttr (elem, "note nvt oid"));
  modify_note_dlg->setText (note->getValue (elem, "text"));
  modify_note_dlg->show ();
}


void
dock_details_note::exec_modify_note (QString hosts,
                                     QString port,
                                     QString threat,
                                     QString task,
                                     QString result,
                                     QString text)
{
  QDomElement elem;
  int i = 0;
  while (i < note->rowCount ())
    {
      if (note->getAttr (note->getEntity (i), "note id").compare (id) == 0)
        {
          elem = note->getEntity (i);
          break;
        }
      i++;
    }

  QMap<QString, QString> parameter;
  parameter.insert ("note id", note->getAttr (elem, "note id"));
  parameter.insert ("text", text);
  parameter.insert ("hosts", hosts);
  parameter.insert ("port", port);
  parameter.insert ("threat", threat);
  if (task.compare ("") != 0)
    task = task_id;
  parameter.insert ("task", task);
  parameter.insert ("result", result);

  emit sig_modify (omp_utilities::NOTE,
                   note->getAttr (elem, "note id"),
                   NULL,
                   parameter);
}


void
dock_details_override::load ()
{

}


void
dock_details_override::update (QString override_id)
{
  if (override_id.compare (id) != 0)
    return;

  for (int i = 0; i < control->getOverrideDetailsModel ()->rowCount (); i++)
    {
      override->addEntity (control->getOverrideDetailsModel ()
                                    ->getEntity (i), i);
    }

  QDomElement elem;
  int i = 0;
  while (i < override->rowCount ())
    {
      if (override->getAttr (override->getEntity (i),
                             "override id").compare (id) == 0)
        {
          elem = override->getEntity (i);
          break;
        }
      i++;
    }

  delegate_date_time *dateDelegate = new delegate_date_time ();

  this->setWindowTitle ("Override " + override->getValue (elem, "nvt name"));
  this->la_name_v->setText (override->getValue(elem, "nvt name"));
  this->la_oid_v->setText (override->getAttr (elem, "override nvt oid"));
  QString creation =
    dateDelegate->formatDate (override->getValue (elem, "creation_time"), true);
  this->la_created_v->setText (creation);
  QString modification =
    dateDelegate->formatDate (override->getValue (elem, "modification_time"),
                              true);
  this->la_modified_v->setText (modification);
  this->la_hosts_v->setText (override->getValue (elem, "hosts"));
  this->la_port_v->setText (override->getValue (elem, "port"));
  this->la_threat_v->setText (override->getValue (elem, "threat"));
  this->la_task_v->setText (override->getValue (elem, "task name"));
  QString res = override->getValue (elem, "result");
  if (res.compare ("") == 0)
    res = "Any";
  this->la_result_v->setText (res);
  this->la_nthreat_v->setText (override->getValue (elem, "new_threat"));
  this->te_text->setText (override->getValue (elem, "text"));

}


void
dock_details_override::modify_override ()
{
  dlg_modify_override *modify_override_dlg = new dlg_modify_override ();

  connect (modify_override_dlg,
           SIGNAL (sig_modify (QString, QString, QString, QString,
                               QString, QString, QString)),
           this,
           SLOT (exec_modify_override (QString, QString, QString, QString,
                                       QString , QString, QString)));
  QDomElement elem;
  int i = 0;
  while (i < override->rowCount ())
    {
      if (override->getAttr (override->getEntity (i),
                             "override id").compare (id) == 0)
        {
          elem = override->getEntity (i);
          break;
        }
      i++;
    }
  QString hosts, port, newThreat, threat, task, result;
  hosts = override->getValue (elem, "hosts");
  port = override->getValue (elem, "port");
  newThreat = override->getValue (elem, "new_threat");
  threat = override->getValue (elem, "threat");
  task = override->getValue (elem, "task name");
  result = override->getAttr (elem, "override result id");

  modify_override_dlg->addNewThreat ("High");
  modify_override_dlg->addNewThreat ("Medium");
  modify_override_dlg->addNewThreat ("Low");
  modify_override_dlg->addNewThreat ("Log");
  modify_override_dlg->addNewThreat ("False Positive");
  modify_override_dlg->setName (override->getValue (override->getEntity (0),
                                                    "nvt name"));
  modify_override_dlg->setOID (override->getAttr (override->getEntity (0),
                                                  "override nvt oid"));
  if (hosts.compare ("") == 0)
    modify_override_dlg->setHosts ("any");
  else
    modify_override_dlg->setHosts (hosts);
  if (port.compare ("") == 0)
    modify_override_dlg->setPort ("any");
  else
    modify_override_dlg->setPort (port);
  if (threat.compare ("") == 0)
    modify_override_dlg->setThreat ("any");
  else
    modify_override_dlg->setThreat (threat);
    modify_override_dlg->setNewThreat (newThreat);
  if (task.compare ("") == 0)
    modify_override_dlg->setTask ("any");
  else
    modify_override_dlg->setTask (task);
  if (result.compare ("") == 0)
    modify_override_dlg->setResult ("any");
  else
    modify_override_dlg->setResult (result);

  modify_override_dlg->setName (override->getValue (elem, "nvt name"));
  modify_override_dlg->setText (override->getValue (elem, "text"));
  modify_override_dlg->show ();
}


void
dock_details_override::exec_modify_override (QString hosts,
                                             QString port,
                                             QString threat,
                                             QString newThreat,
                                             QString task,
                                             QString result,
                                             QString text)
{
  QDomElement elem;
  int i = 0;
  while (i < override->rowCount ())
    {
      if (override->getAttr (override->getEntity (i),
                             "override id").compare (id) == 0)
        {
          elem = override->getEntity (i);
          break;
        }
      i++;
    }
  if (task.compare ("") != 0)
    task = override->getAttr (override->getEntity (0), "override task id");

  QMap<QString, QString> parameter;
  parameter.insert ("override id", override->getAttr (elem, "override id"));
  parameter.insert ("text", text);
  parameter.insert ("hosts", hosts);
  parameter.insert ("port", port);
  parameter.insert ("new threat", newThreat);
  parameter.insert ("threat", threat);
  parameter.insert ("task", task);
  parameter.insert ("result", result);

  emit sig_modify (omp_utilities::OVERRIDE,
                   override->getAttr (elem, "override id"),
                   NULL,
                   parameter);
}


/**
 * @brief Save the current geometry and state.
 */
void
dock_details::closeEvent (QCloseEvent *event)
{
  QSettings settings ("Greenbone Networks GmbH", "GSD");
  settings.beginGroup ("DockWidgets");
  settings.setValue ("geometrie", saveGeometry ());
  settings.endGroup ();
}


void
dock_details_slave::load ()
{
  delegate_date_time *d_time = new delegate_date_time ();
  delegate_icon *d_icon = new delegate_icon ();
  delegate_progress *d_prog = new delegate_progress ();
  delegate_text *d_text = new delegate_text ();
  delegate_threat *d_thre = new delegate_threat ();
  delegate_trend *d_tren = new delegate_trend ();

  tv_table->setItemDelegateForColumn (1, d_icon);
  tv_table->setItemDelegateForColumn (2, d_prog);
  tv_table->setItemDelegateForColumn (4, d_time);
  tv_table->setItemDelegateForColumn (5, d_time);
  tv_table->setItemDelegateForColumn (6, d_thre);
  tv_table->setItemDelegateForColumn (7, d_tren);
  tv_table->verticalHeader ()->setVisible (false);
  tv_table->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_table->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_table->setColumnWidth (0, 120);
  tv_table->horizontalHeader ()->setHighlightSections (false);
  tv_table->horizontalHeader ()->setMinimumSectionSize (75);
  tv_table->setAlternatingRowColors (true);
  tv_table->setSelectionBehavior (QAbstractItemView::SelectRows);
  tv_table->hideColumn (1);
  tv_table->hideColumn (2);
  tv_table->hideColumn (4);
  tv_table->hideColumn (5);
  tv_table->hideColumn (6);

}


void
dock_details_slave::update ()
{
  if (id.compare ("") == 0)
    return;

  model_omp_entity *tasks = this->control->getTaskModel ();
  tv_table->setModel (tasks);
  tasks->setFilterType (model_omp_entity::ATTRIBUTE_FILTER);
  tasks->setFilter ("task slave id " + id);

  QList <int> filterList = tasks->getFilterIndexList ();
  for (int i = 0; i < tasks->rowCount (); i++)
    {
      if (!filterList.contains (i))
        tv_table->hideRow (i);
      else if (tv_table->isRowHidden(i))
        tv_table->showRow (i);
    }

  tasks->resetFilter ();

  model_omp_entity *slaves = this->control->getSlaveModel ();
  QStringList list = this->id.split (" ");
  QDomElement showElement;
  int i = 0;
  while (i < slaves->rowCount())
    {
      QString attr = slaves->getAttr(slaves->getEntity(i), "slave id");

      if (attr.compare (id) == 0)
        {
          showElement = slaves->getEntity (i);
          break;
        }
      i++;
    }

  this->setWindowTitle ("Slave " + slaves->getValue (showElement, "name"));
  this->la_name_v->setText (slaves->getValue (showElement, "name"));
  this->la_comment_v->setText (slaves->getValue (showElement, "comment"));
  this->la_host_v->setText (slaves->getValue (showElement, "host"));
  this->la_port_v->setText (slaves->getValue (showElement, "port"));
  this->la_login_v->setText (slaves->getValue (showElement,
                                                        "login"));
  load ();
}
