/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file dock_reports.h
 * @class dock_reports
 * @brief Protos and data structures for report dock widgets.
 */

#ifndef DOCK_REPORTS_H
#define DOCK_REPORTS_H

#include <QtGui>

#include "ui_dock_reports.h"
#include "dock_details.h"
#include "gsd_dialogs.h"
#include "model_omp_entity.h"
#include "delegate_date_time.h"
class gsd_control;

class dock_reports : public dock_details,
                     private Ui::dock_reports
{
  Q_OBJECT

  signals:
    void sig_request_report (QMap<QString, QString>);
    void sig_details_nvt (QString, QString);
    void sig_report_download (QMap<QString, QString>);

  public slots:
    void update (QString id);
    void request_update ();

  private slots:
    void web_link (const QUrl&);
    void cvss_changed (int);
    void download_report ();
    void next_page ();
    void previous_page ();
    void new_request ();
    void load_started ();
    void load_finished ();
    void updateReportFormats ();

  private:
    gsd_control *control;
    model_omp_entity *report;
    model_omp_entity *htmlReport;
    QString id, task_id;
    QString fileName;
    QString xml_format_id, html_format_id;
    int firstResult;
    int maxResults;

    void saveReport ();
    void emptyFilter ();
  public:
    dock_reports (gsd_control *ctl);
    ~dock_reports () {};
    void setTask (QString id) {this->task_id = id;};
    void setId (QString id) {this->id = id;};
    void load ();
};

#endif
