/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file gsd_control.h
 * @class gsd_control
 * @brief Protos and data structures for GS-Desktop controller
 */

#ifndef GSA_DESKTOP_CONTROL_H
#define GSA_DESKTOP_CONTROL_H

#include <QObject>
#include <QFile>
#include <glib.h>

#include "omp_connector.h"
#include "gsd_mw.h"
#include "gsd_dlg.h"
#include "gsd_config.h"

#ifndef WIN32
extern "C"
{
#include <openvas/misc/openvas_logging.h>
}
#endif

/**
 * @brief Protos and data structures for gsd controller.
 */
class gsd_control : public QObject
{
  Q_OBJECT

  signals:
    void sig_authentication_failed (int);
    void sig_authentication_successful ();
    void sig_check_result (int);
    void sig_config_finished (int, int);
    void sig_config_details_finished (QString);
    void sig_family_finished (int, int);
    void sig_family_details_finished (QString, QString);
    void sig_nvt_details_finished (QString, QString);
    void sig_note_finished ();
    void sig_note_details_finished (QString);
    void sig_override_finished ();
    void sig_override_details_finished (QString);
    void sig_report_finished (QString);
    void sig_report_format_finished ();
    void sig_task_details_finished ();
    void sig_lsc_credential_finished ();
    void sig_request_report ();
    void sig_port_list_finished ();
    void sig_port_list_details_finished (QString);

  private:
    gsd_config *config;
    gsd_mw *mainwindow;
    gsd_dlg *dialogs;
    omp_connector *connector;
    omp_credentials *loginData;
    QFutureWatcher<int> *connectWatcher;

    int protocol_version;

    model_omp_entity *taskModel;
    model_omp_entity *targetModel;
    model_omp_entity *configModel;
    model_omp_entity *configDetailsModel;
    model_omp_entity *scheduleModel;
    model_omp_entity *escalatorModel;
    model_omp_entity *credentialModel;
    model_omp_entity *agentModel;
    model_omp_entity *noteModel;
    model_omp_entity *noteDetailsModel;
    model_omp_entity *overrideModel;
    model_omp_entity *overrideDetailsModel;
    model_omp_entity *slaveModel;
    model_omp_entity *systemReportModel;
    model_omp_entity *nvtModel;
    model_omp_entity *nvtDetailsModel;
    model_omp_entity *familyModel;
    model_omp_entity *familyDetailsModel;
    model_omp_entity *performanceModel;
    model_omp_entity *htmlReportModel;
    model_omp_entity *xmlReportModel;
    model_omp_entity *configDownloadModel;
    model_omp_entity *reportFormatModel;
    model_omp_entity *tmpReportModel;
    model_omp_entity *portListModel;
    model_omp_entity *portListDetailsModel;
    GSList *log_config;

    void createConnections ();

    void notifyConfigDetails ();
    void notifyFamilyDetails (QString config);
    void notifyNVTDetails (QString config);
    void notifyNoteDetails (QString id);
    void notifyOverrideDetails (QString id);
    void notifyReport (QString id);
    void notifyPortListDetails (QString id);
    void notifyTasks ();
    void notifyConfigDownload ();
    void notifyVersion (QString version);
    void notifyTaskDetails ();

    void loadData ();

  private slots:
    void login(QString addr, int port, QString name, QString pwd);
    void check_server (QString, int, QString, QString);
    void logout ();

    void profile_save (QString name, omp_credentials& crd);
    void profile_remove (int index);

    void stop_update ();
    void request_tasks (int);
    void request_schedules (int);
    void request_configs (int);
    void request_targets (int);
    void request_escalators (int);
    void request_lsc_credentials (int);
    void request_agents (int);
    void request_notes (int);
    void request_overrides (int);
    void request_slaves (int);
    void request_system_reports (int);
    void request_system_reports (QString, long);
    void request_portlists (int);
    void request_port_list_details (QString);
    void request_families ();
    void request_nvts (QString family);
    void request_nvt_details (QString, QString);
    void request_note_details (QString);
    void request_override_details (QString id);
    void request_config_details (int index);
    void request_family_details (QString config_id, QString name);
    void request_report (QMap<QString, QString> parameter);
    void request_version ();
    void request_report_formats ();
    void update_failed (int);
    void task_start (int);
    void task_stop (int);
    void task_pause (int);
    void task_resume (int);
    void task_delete (int);
    void config_delete (int);
    void target_delete (int);
    void schedule_delete (int);
    void escalator_delete (int);
    void credential_delete (int);
    void agent_delete (int);
    void note_delete (int);
    void note_modify (int);
    void override_delete (int);
    void override_modify (int);
    void slave_delete (int);
    void report_delete (QString);
    void port_list_delete (int);
    void config_download (int);
    void system_report_download ();
    void system_report_download (QString name, int duration);
    void report_download (QMap<QString, QString> parameter);
    void create (int, QMap<QString, QString>);
    void modify (int, QString, model_omp_entity*, QMap<QString, QString>);
    void modify (int, QMap<QString, QString>);
    void request_finished_listener (int, int, QString);
    void request_failed_listener (int, int, int);
    void connect_finished();

  public:
    gsd_control ();
    ~gsd_control ();

    void setConnector(omp_connector *con);
    void setMainwindow (gsd_mw *mw);
    void setDialogs (gsd_dlg *dlg);
    void setupLogging ();
    int start ();
    int getProtocolVersion ();

    model_omp_entity *getTaskModel ();
    model_omp_entity *getScheduleModel ();
    model_omp_entity *getConfigModel ();
    model_omp_entity *getConfigDetailsModel ();
    model_omp_entity *getTargetModel ();
    model_omp_entity *getEscalatorModel ();
    model_omp_entity *getCredentialModel ();
    model_omp_entity *getAgentModel ();
    model_omp_entity *getNoteModel ();
    model_omp_entity *getNoteDetailsModel ();
    model_omp_entity *getOverrideModel ();
    model_omp_entity *getOverrideDetailsModel ();
    model_omp_entity *getSlaveModel ();
    model_omp_entity *getReportFormatModel ();
    model_omp_entity *getSystemDiagramModel ();
    model_omp_entity *getSystemReportModel ();
    model_omp_entity *getFamilyModel ();
    model_omp_entity *getFamilyDetailsModel ();
    model_omp_entity *getNVTModel ();
    model_omp_entity *getNVTDetailsModel ();
    model_omp_entity *getHtmlReportModel ();
    model_omp_entity *getXmlReportModel ();
    model_omp_entity *getConfigDownloadModel ();
    model_omp_entity *getReportDownloadModel ();
    model_omp_entity *getPortListModel ();
    model_omp_entity *getPortListDetailsModel ();
    omp_credentials *getProfile (QString name);
    QString getProfile (int);
    int getProfileCount ();
    omp_credentials *getLoginData ();
};
#endif

