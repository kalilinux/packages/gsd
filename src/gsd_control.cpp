/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file gsd_control.cpp
 * @brief The GS-Desktop controller
 * @class gsd_control
 * This file is a controller for communication between the Greenbone
 * Security Desktop UI and its omp functionality. It contains
 * the omp-controller, the configuration and the main view components.
 */

#include "gsd_control.h"

/**
 * @brief Creates a new GS-Desktop controller
 *
 * Creates the mainwindow and the GS-Desktop configuration. The signal-slot
 * configuration to the mainwindow is set here.
 */
gsd_control::gsd_control ()
{
  config = new gsd_config ();
  connectWatcher = new QFutureWatcher<int>(this);
  connector = NULL;
  mainwindow = NULL;
  dialogs = NULL;
  log_config = NULL;
  loginData = new omp_credentials ();

  taskModel = new model_omp_entity ();
  taskModel->addHeader (tr ("Name"), "name");
  taskModel->addHeader ("", "schedule");
  taskModel->addHeader (tr ("Status"), "status");
  taskModel->addHeader (tr ("Reports"), "report_count finished");
  taskModel->addHeader (tr ("First"), "first_report report timestamp");
  taskModel->addHeader (tr ("Last"), "last_report report timestamp");
  taskModel->addHeader (tr ("Threat"), "messages");
  taskModel->addHeader (tr ("Trend"), "trend");

  targetModel = new model_omp_entity ();
  targetModel->addHeader (tr ("Name"), "name");
  targetModel->addHeader (tr ("Hosts"), "hosts");
  targetModel->addHeader (tr ("IPs"), "max_hosts");
  targetModel->addHeader (tr ("Port List"), "port_list name");
  targetModel->addHeader (tr ("SSH Credential"), "ssh_lsc_credential name");
  targetModel->addHeader (tr ("SMB Credential"), "smb_lsc_credential name");

  configModel = new model_omp_entity ();
  configModel->addHeader (tr ("Name"), "name");
  configModel->addHeader (tr ("Total Families"), "family_count");
  configModel->addHeader (tr ("Trend"), "family_count growing");
  configModel->addHeader (tr ("Total NVTs"), "nvt_count");
  configModel->addHeader (tr ("Trend"), "nvt_count growing");

  scheduleModel = new model_omp_entity ();
  scheduleModel->addHeader (tr ("Name"),"name");
  scheduleModel->addHeader (tr ("First Run"), "first_time");
  scheduleModel->addHeader (tr ("Next Run"), "next_time");
  scheduleModel->addHeader (tr ("Period"), "period");
  scheduleModel->addHeader (tr ("Duration"), "duration");

  escalatorModel = new model_omp_entity ();
  escalatorModel->addHeader (tr ("Name"), "name");
  escalatorModel->addHeader (tr ("Event"), "event");
  escalatorModel->addHeader (tr ("Condition"), "condition");
  escalatorModel->addHeader (tr ("Method"), "method");

  credentialModel = new model_omp_entity ();
  credentialModel->addHeader (tr ("Name"), "name");
  credentialModel->addHeader (tr ("Login"), "login");
  credentialModel->addHeader (tr ("Comment"), "comment");

  agentModel = new model_omp_entity ();
  agentModel->addHeader (tr ("Name"), "name");
  agentModel->addHeader (tr ("Trust"), "installer trust");
  agentModel->addHeader (tr ("Comment"), "comment");

  noteModel = new model_omp_entity ();
  noteModel->addHeader (tr ("NVT"), "nvt name");
  noteModel->addHeader (tr ("Text"), "text");

  overrideModel = new model_omp_entity ();
  overrideModel->addHeader (tr ("NVT"), "nvt name");
  overrideModel->addHeader (tr ("From"), "threat");
  overrideModel->addHeader (tr ("To"), "new_threat");
  overrideModel->addHeader (tr ("Text"), "text");

  slaveModel = new model_omp_entity ();
  slaveModel->addHeader (tr ("Name"), "name");
  slaveModel->addHeader (tr ("Host"), "host");
  slaveModel->addHeader (tr ("Port"), "port");
  slaveModel->addHeader (tr ("Login"), "login");

  portListModel = new model_omp_entity ();
  portListModel->addHeader (tr ("Name"), "name");
  portListModel->addHeader (tr ("Comment"), "comment");
  portListModel->addHeader (tr ("Total Ports"), "port_count all");
  portListModel->addHeader (tr ("TCP"), "port_count tcp");
  portListModel->addHeader (tr ("UDP"), "port_count udp");

  systemReportModel = new model_omp_entity ();

  familyModel =new model_omp_entity ();
  performanceModel = new model_omp_entity ();
  configDetailsModel = new model_omp_entity ();
  familyDetailsModel = new model_omp_entity ();
  portListDetailsModel = new model_omp_entity ();
  nvtModel = new model_omp_entity ();
  nvtDetailsModel = new model_omp_entity ();
  noteDetailsModel = new model_omp_entity ();
  overrideDetailsModel = new model_omp_entity ();
  htmlReportModel = new model_omp_entity ();
  xmlReportModel = new model_omp_entity ();
  configDownloadModel = new model_omp_entity ();
  reportFormatModel = new model_omp_entity ();
  reportFormatModel->addHeader (tr ("Name"), "name");
  reportFormatModel->addHeader (tr ("Extension"), "extension");
  reportFormatModel->addHeader (tr ("Content Type"), "content_type");
  reportFormatModel->addHeader (tr ("Trust"), "trust");
  reportFormatModel->addHeader (tr ("Last verified"), "trust time");
  reportFormatModel->addHeader (tr ("Active"), "active");

  tmpReportModel = new model_omp_entity ();
}

gsd_control::~gsd_control ()
  {
    free (config);
#ifndef WIN32
    free_log_configuration (log_config);
#endif
  }


/**
 * @brief Configures the communication between the ui objects and the omp layer.
 */
int
gsd_control::start ()
{
  if (connector == NULL || mainwindow == NULL || dialogs == NULL)
    return -1;
  mainwindow->show ();
  connect (this,
           SIGNAL (sig_authentication_failed (int)),
           mainwindow,
           SLOT (authentication_failed (int)));
  connect (this,
           SIGNAL (sig_authentication_successful ()),
           mainwindow,
           SLOT (authentication_successful ()));
  connect (this,
           SIGNAL (sig_authentication_successful ()),
           dialogs,
           SLOT (authentication_successful ()));
  connect (this,
           SIGNAL (sig_check_result (int)),
           dialogs,
           SLOT (check_result (int)));
  connect (connectWatcher,
           SIGNAL (finished()),
           this,
           SLOT (connect_finished()));
  connect (dialogs,
           SIGNAL (sig_login (QString, int, QString, QString)),
           this,
           SLOT (login (QString, int, QString, QString)));
  connect (dialogs,
           SIGNAL (sig_check_server (QString, int, QString, QString)),
           this,
           SLOT (check_server (QString, int, QString, QString)));
  connect (connector,
           SIGNAL (sig_request_finished (int, int, QString)),
           this,
           SLOT (request_finished_listener (int, int, QString)));
  connect (connector,
           SIGNAL (sig_request_failed (int, int, int)),
           this,
           SLOT (request_failed_listener (int, int, int)));
  connect (connector,
           SIGNAL (sig_update_trigger ()),
           mainwindow,
           SLOT (update_trigger ()));
  connect (dialogs,
           SIGNAL (sig_create (int, QMap<QString, QString>)),
           this,
           SLOT (create (int, QMap<QString, QString>)));
  connect (dialogs,
           SIGNAL (sig_modify (int, QMap<QString, QString>)),
           this,
           SLOT (modify (int, QMap<QString, QString>)));
  connect (mainwindow,
           SIGNAL (sig_login ()),
           dialogs,
           SLOT (login_dlg ()));
  connect (mainwindow,
           SIGNAL (sig_about ()),
           dialogs,
           SLOT (about_dlg ()));
  connect (mainwindow,
           SIGNAL (sig_start_gsa ()),
           dialogs,
           SLOT (start_gsa_dlg ()));
  connect (mainwindow,
           SIGNAL (sig_logout ()),
           this,
           SLOT (logout ()));
  connect (mainwindow,
           SIGNAL (sig_req_tasks (int)),
           this,
           SLOT (request_tasks (int)));
  connect (mainwindow,
           SIGNAL (sig_req_schedules (int)),
           this,
           SLOT (request_schedules (int)));
  connect (mainwindow,
           SIGNAL (sig_req_targets (int)),
           this,
           SLOT (request_targets (int)));
  connect (mainwindow,
           SIGNAL (sig_req_configs (int)),
           this,
           SLOT (request_configs (int)));
  connect (mainwindow,
           SIGNAL (sig_req_escalators (int)),
           this,
           SLOT (request_escalators (int)));
  connect (mainwindow,
           SIGNAL (sig_req_agents (int)),
           this,
           SLOT (request_agents (int)));
  connect (mainwindow,
           SIGNAL (sig_req_lsc_credentials (int)),
           this,
           SLOT (request_lsc_credentials (int)));
  connect (mainwindow,
           SIGNAL (sig_req_notes (int)),
           this,
           SLOT (request_notes (int)));
  connect (mainwindow,
           SIGNAL (sig_details_port_list (QString)),
           this,
           SLOT (request_port_list_details (QString)));
  connect (mainwindow,
           SIGNAL (sig_details_note (QString)),
           this,
           SLOT (request_note_details (QString)));
  connect (mainwindow,
           SIGNAL (sig_req_overrides (int)),
           this,
           SLOT (request_overrides (int)));
  connect (mainwindow,
           SIGNAL (sig_details_override (QString)),
           this,
           SLOT (request_override_details (QString)));
  connect (mainwindow,
           SIGNAL (sig_req_slaves (int)),
           this,
           SLOT (request_slaves (int)));
  connect (mainwindow,
           SIGNAL (sig_start_task (int)),
           this,
           SLOT (task_start (int)));
  connect (mainwindow,
           SIGNAL (sig_stop_task (int)),
           this,
           SLOT (task_stop (int)));
  connect (mainwindow,
           SIGNAL (sig_pause_task (int)),
           this,
           SLOT (task_pause (int)));
  connect (mainwindow,
           SIGNAL (sig_resume_task (int)),
           this,
           SLOT (task_resume (int)));
  connect (mainwindow,
           SIGNAL (sig_delete_task (int)),
           this,
           SLOT (task_delete (int)));
  connect (mainwindow,
           SIGNAL (sig_delete_config (int)),
           this,
           SLOT (config_delete (int)));
  connect (mainwindow,
           SIGNAL (sig_delete_target (int)),
           this,
           SLOT (target_delete (int)));
  connect (mainwindow,
           SIGNAL (sig_delete_schedule (int)),
           this,
           SLOT (schedule_delete (int)));
  connect (mainwindow,
           SIGNAL (sig_delete_escalator (int)),
           this,
           SLOT (escalator_delete (int)));
  connect (mainwindow,
           SIGNAL (sig_delete_credential (int)),
           this,
           SLOT (credential_delete (int)));
  connect (mainwindow,
           SIGNAL (sig_delete_agent (int)),
           this,
           SLOT (agent_delete (int)));
  connect (mainwindow,
           SIGNAL (sig_delete_note (int)),
           this,
           SLOT (note_delete (int)));
  connect (mainwindow,
           SIGNAL (sig_delete_override (int)),
           this,
           SLOT (override_delete (int)));
  connect (mainwindow,
           SIGNAL (sig_delete_slave (int)),
           this,
           SLOT (slave_delete (int)));
  connect (mainwindow,
           SIGNAL (sig_delete_port_list (int)),
           this,
           SLOT (port_list_delete (int)));
  connect (mainwindow,
           SIGNAL (sig_create_task ()),
           dialogs,
           SLOT (new_task_dlg ()));
  connect (mainwindow,
           SIGNAL (sig_modify_task (int)),
           dialogs,
           SLOT (modify_task_dlg (int)));
  connect (mainwindow,
           SIGNAL (sig_create_config ()),
           dialogs,
           SLOT (new_config_dlg ()));
  connect (mainwindow,
           SIGNAL (sig_create_target ()),
           dialogs,
           SLOT (new_target_dlg ()));
  connect (mainwindow,
           SIGNAL (sig_create_schedule ()),
           dialogs,
           SLOT (new_schedule_dlg ()));
  connect (mainwindow,
           SIGNAL (sig_create_escalator ()),
           dialogs,
           SLOT (new_escalator_dlg ()));
  connect (mainwindow,
           SIGNAL (sig_create_credential ()),
           dialogs,
           SLOT (new_credential_dlg ()));
  connect (mainwindow,
           SIGNAL (sig_create_port_list ()),
           dialogs,
           SLOT (new_port_list_dlg ()));
  connect (mainwindow,
           SIGNAL (sig_create_agent ()),
           dialogs,
           SLOT (new_agent_dlg ()));
  connect (mainwindow,
           SIGNAL (sig_create_slave ()),
           dialogs,
           SLOT (new_slave_dlg ()));
  connect (mainwindow,
           SIGNAL (sig_details_config (int)),
           this,
           SLOT (request_config_details (int)));
  connect (mainwindow,
           SIGNAL (sig_details_family (QString, QString)),
           this,
           SLOT (request_family_details (QString, QString)));
  connect (mainwindow,
           SIGNAL (sig_details_nvt (QString, QString)),
           this,
           SLOT (request_nvt_details (QString, QString)));
  connect (mainwindow,
           SIGNAL (sig_nvts (QString)),
           this,
           SLOT (request_nvts (QString)));
  connect (mainwindow,
           SIGNAL (sig_stop_update ()),
           connector,
           SLOT (stop_update ()));
  connect (connector,
           SIGNAL (sig_update_stopped ()),
           mainwindow,
           SLOT (update_stopped ()));
  connect (connector,
           SIGNAL (sig_update_started ()),
           mainwindow,
           SLOT (update_started ()));
  connect (connector,
           SIGNAL (sig_thread_finished ()),
           mainwindow,
           SLOT (update_finished ()));
  connect (connector,
           SIGNAL (sig_thread_started ()),
           mainwindow,
           SLOT (in_progress ()));
  connect (connector,
           SIGNAL (sig_thread_finished ()),
           mainwindow,
           SLOT (progress_ready ()));
  connect (mainwindow,
           SIGNAL (sig_req_report (QMap<QString, QString>)),
           this,
           SLOT (request_report (QMap<QString, QString>)));
  connect (mainwindow,
           SIGNAL (sig_download_config (int)),
           this,
           SLOT (config_download (int)));
  dialogs->login_dlg ();

  return 0;
}


/**
 * @brief Set omp-connector
 *
 * Sets the omp-connector and configures the signal-slot connections between
 * mainwindow and omp-connector.
 *
 * @param[in]  con   omp-connector, managing the communication with OpenVAS
 *                   Manager
 */
void
gsd_control::setConnector (omp_connector *con)
{
  this->connector = con;
}


/**
 * @brief Set Main window
 *
 * @param[in]   mw  Greenbone Secuirty Desktop Mainwindow.
 */
void
gsd_control::setMainwindow (gsd_mw *mw)
{
  this->mainwindow = mw;
}


/**
 * @brief Set Main window
 *
 * @param[in]   mw  Greenbone Security Desktop Mainwindow.
 */
void
gsd_control::setDialogs (gsd_dlg *dlg)
{
  this->dialogs = dlg;
}


/**
 * @brief SLOT to get the login data
 *
 * Takes the login data send with the signal and puts them in the omp-connector
 *
 * @param[in]  addr    Serveraddress.
 * @param[in]  port    Serverport.
 * @param[in]  uname   Username.
 * @param[in]  pwd     Password.
 */
void
gsd_control::login (QString addr,
                            int port,
                            QString uname,
                            QString pwd)
{
  while (connectWatcher->isRunning ())
    {
      // Wait for the Connection check to finish
      qApp->processEvents();
    }
  this->connector->setCredentials (addr, port, uname, pwd);
  int err = this->connector->authenticate ();

  if (err != 0)
    {
      emit sig_authentication_failed (err);
      dialogs->login_dlg ();
      return;
    }
  loginData->setServerAddress(addr);
  loginData->setServerPort(port);
  loginData->setUserName(uname);
  loginData->setPassword(pwd);
  emit sig_authentication_successful ();
  request_version ();
}

/**
 * @brief SLOT to check if an omp service is reachable
 *
 * @param[in]  addr    Serveraddress.
 * @param[in]  port    Serverport.
 * @param[in]  user    Username.
 * @param[in]  pwd     Password.
 */
void
gsd_control::check_server (QString addr,
                           int port,
                           QString user,
                           QString pwd)
{
  this->connector->setCredentials (addr, port, user, pwd);
  connectWatcher->setFuture (QtConcurrent::run(connector,
              &omp_connector::authenticate));
}

/**
 * @brief SLOT to evaluate the result of a connection attempt
 */
void
gsd_control::connect_finished()
{
  int res = connectWatcher->result();
  if (res == 0)
    {
      request_version ();
    }
  else if (res == 2)
    emit sig_check_result (1);
  else
    emit sig_check_result (-1);
}


void
gsd_control::loadData ()
{
  request_tasks (0);
  request_schedules (0);
  request_configs (0);
  request_targets (0);
  request_escalators (0);
  request_lsc_credentials (0);
  request_agents (0);
  request_notes (0);
  request_overrides (0);
  request_slaves (0);
  request_portlists (0);
  if (protocol_version >= 2)
    request_report_formats ();
  request_system_reports (0);
  request_system_reports (NULL, 3600);
  request_families ();
}


/**
 * @brief SLOT to remove the connectors credentials
 *
 */
void
gsd_control::logout ()
{
  taskModel->removeEntities ();
  scheduleModel->removeEntities ();
  credentialModel->removeEntities ();
  agentModel->removeEntities ();
  targetModel->removeEntities ();
  configModel->removeEntities ();
  escalatorModel->removeEntities ();
  noteModel->removeEntities ();
  overrideModel->removeEntities ();
  slaveModel->removeEntities ();
  systemReportModel->removeEntities ();
  familyModel->removeEntities ();
  performanceModel->removeEntities ();
  configDetailsModel->removeEntities ();
  familyDetailsModel->removeEntities ();
  nvtModel->removeEntities ();
  nvtDetailsModel->removeEntities ();
  noteDetailsModel->removeEntities ();
  overrideDetailsModel->removeEntities ();
  htmlReportModel->removeEntities ();
  xmlReportModel->removeEntities ();
  configDownloadModel->removeEntities ();
  reportFormatModel->removeEntities ();
  portListModel->removeEntities ();
  portListDetailsModel->removeEntities ();

  connector->removeCredentials();
}


/**
 * @brief SLOT to update the login dialog
 *
 * Updates the main window login dialog, if the selected profile changed.
 *
 * @param[in]  name    new profile name
 *
 * @return  Credentials to the requested profile.
 */
omp_credentials*
gsd_control::getProfile (QString name)
{
  return config->getCredentials (name);
}


/**
 * @brief Getter for login profile name.
 *
 * @param[in]  index   Profile index.
 *
 * @return  Profile name.
 */
QString
gsd_control::getProfile (int index)
{
  return config->getProfile (index);
}


/**
 * @brief Getter
 *
 * @return  Profile count.
 */
int
gsd_control::getProfileCount ()
{
  return config->getProfileCount ();
}


/**
 * @brief Getter
 *
 * @return  Current login data.
 */
omp_credentials*
gsd_control::getLoginData ()
{
  return loginData;
}

/**
 * @brief SLOT to save new profile data
 *
 * Adds new login data to the configuration and saves the config to the xml
 * file.
 *
 * @param[in]  name    new profile name
 * @param[in]  crd     new login data
 */
void
gsd_control::profile_save (QString name, omp_credentials& crd)
{

  this->config->addCredentials (name, crd);
  QFile file (*this->config->getFilename());

  if (!file.open (QFile::WriteOnly| QFile::Text))
    {
      return;
    }

  this->config->writeConfig (&file);
  file.close ();
}


/**
 * @brief SLOT to remove a profile
 *
 * Removes the complete profile from the configuration.
 *
 * @param[in]  name    profilename
 */
void
gsd_control::profile_remove (int index)
{
  config->removeCredentials (config->getProfile (index));
}


/**
 * @brief SLOT that starts a task update-request
 *
 * A task update for the task widget is requested, 0 as parameter starts a
 * single update.
 *
 * @param[in]  interval    Time in seconds between two updates
 */
void
gsd_control::request_tasks (int interval)
{
  QMap<QString, QString> parameter;
  parameter.insert ("details", "1");
  parameter.insert ("apply_overrides", "1");
  this->connector->getEntity (omp_utilities::TASK,
                              interval,
                              taskModel,
                              parameter);
}


/**
 * @brief SLOT that starts a schedulesupdate-request.
 *
 * An update for the schedules widget is requested.
 *
 * @param[in]  interval   Time in seconds between two updates.
 */
void
gsd_control::request_schedules (int interval)
{
  QMap<QString, QString> parameter;
  parameter.insert ("details", "1");
  this->connector->getEntity (omp_utilities::SCHEDULE,
                              interval,
                              scheduleModel,
                              parameter);
}


/**
 * @brief SLOT that starts a config update-request.
 *
 * An update for the config widget is requested.
 *
 * @param[in]  interval   Time in seconds between two updates.
 */
void
gsd_control::request_configs (int interval)
{
  QMap<QString, QString> parameter;
  parameter.insert ("details", "1");
  this->connector->getEntity (omp_utilities::CONFIG,
                              interval,
                              configModel,
                              parameter);
}


/**
 * @brief SLOT that starts a target update-request.
 *
 * An update for the config widget is requested.
 *
 * @param[in]  interval   Time in seconds between two updates.
 */
void
gsd_control::request_targets (int interval)
{
  QMap<QString, QString> parameter;
  parameter.insert ("details", "1");
  this->connector->getEntity (omp_utilities::TARGET,
                              interval,
                              targetModel,
                              parameter);
}


/**
 * @brief SLOT that starts an escalator update-request.
 *
 * An update for the escalator widget is requested.
 *
 * @param[in]  interval   Time in seconds between two updates.
 */
void
gsd_control::request_escalators (int interval)
{
  this->connector->getEntity (omp_utilities::ESCALATOR,
                              interval,
                              escalatorModel,
                              QMap<QString, QString> ());
}


/**
 * @brief SLOT that starts an lsc_credential update-request.
 *
 * An update for the lsc_credentials widget is requested.
 *
 * @param[in]  interval   Time in seconds between two updates.
 */
void
gsd_control::request_lsc_credentials (int interval)
{
  this->connector->getEntity (omp_utilities::LSC_CREDENTIAL,
                              interval,
                              credentialModel,
                              QMap<QString, QString> ());
}


/**
 * @brief SLOT that starts an agent update-request.
 *
 * An update for the agent widget is requested.
 *
 * @param[in]  interval   Time in second between two updates.
 */
void
gsd_control::request_agents (int interval)
{
  this->connector->getEntity (omp_utilities::AGENT,
                              interval,
                              agentModel,
                              QMap<QString, QString> ());
}


/**
 * @brief SLOT that starts an notes update-request.
 *
 * An update for the notes widget is requested.
 *
 * @param[in]  interval   Time in second between two updates.
 */
void
gsd_control::request_notes (int interval)
{
  QMap<QString, QString> parameter;
  parameter.insert ("details", "1");
  this->connector->getEntity (omp_utilities::NOTE,
                              interval,
                              noteModel,
                              parameter);
}


void
gsd_control::request_version ()
{
  QMap<QString, QString> parameter;
  this->connector->getEntity (omp_utilities::VERSION,
                              0,
                              new model_omp_entity (),
                              parameter);
}


/**
 * @brief SLOT that starts an overrides update-request.
 *
 * An update for the overrides widget is requested.
 *
 * @param[in]  interval   Time in second between two updates.
 */
void
gsd_control::request_overrides (int interval)
{
  QMap<QString, QString> parameter;
  parameter.insert ("details", "1");
  this->connector->getEntity (omp_utilities::OVERRIDE,
                              interval,
                              overrideModel,
                              parameter);
}


/**
 * @brief SLOT that starts an slave update-request.
 *
 * An update for the slave widget is requested.
 *
 * @param[in]  interval   Time in second between two updates.
 */
void
gsd_control::request_slaves (int interval)
{
  QMap<QString, QString> parameter;
  this->connector->getEntity (omp_utilities::SLAVE,
                              interval,
                              slaveModel,
                              parameter);
}

/**
 * @brief SLOT that starts an portlist update-request.
 *
 * An update for the portlist widget is requested.
 *
 * @param[in]  interval   Time in seconds between two updates.
 */
void
gsd_control::request_portlists (int interval)
{
  QMap<QString, QString> parameter;
  this->connector->getEntity (omp_utilities::PORT_LIST,
                              interval,
                              portListModel,
                              parameter);
}

/**
 * @brief SLOT that stops frequently running updates.
 *
 * The task update for the task widget is stopped.
 */
void
gsd_control::stop_update ()
{
  this->connector->stop_update ();
}


/**
 * @brief SLOT that starts a task.
 *
 * Start running the task, identified by the id.
 *
 * @param[in]  index   Index in task model/table.
 */
void
gsd_control::task_start (int index)
{
  QDomElement ent = taskModel->getEntity (index);
  QString stat = taskModel->getValue (ent.toElement (), "status");
  if (stat.compare ("New") == 0 ||
      stat.compare ("Done") == 0 ||
      stat.compare ("Stopped") == 0 ||
      stat.compare ("Paused") == 0)
    {
      connector->startTask (taskModel->getAttr (ent, "task id"));
    }
  else
    dialogs->request_failed (omp_utilities::TASK,
                             omp_utilities::START,
                             -1);
}



/**
 * @brief SLOT that stops a running task.
 *
 * Stops a running task, identified by its id.
 *
 * @param[in]  index   Index in task model/table.
 */
void
gsd_control::task_stop (int index)
{
  QDomElement ent = taskModel->getEntity (index);
  QString stat = taskModel->getValue (ent, "status");
  if (stat.compare ("Running") == 0 ||
      stat.compare ("Paused") == 0)
    {
      connector->stopTask (taskModel->getAttr (ent, "task id"));
    }
  else
    dialogs->request_failed (omp_utilities::TASK,
                             omp_utilities::STOP,
                             -1);

}


/**
 * @brief SLOT that pauses a running task.
 *
 * Pauses a running task, identified by its id.
 *
 * @param[in]  index   Index in task model/table.
 */
void
gsd_control::task_pause (int index)
{
  QDomElement ent = taskModel->getEntity (index);
  QString stat = taskModel->getValue (ent, "status");
  if (stat.compare ("Running") == 0 ||
      stat.compare ("Requested") == 0)
    {
      connector->pauseTask (taskModel->getAttr (ent.toElement (), "task id"));
    }
  else
    dialogs->request_failed (omp_utilities::TASK,
                             omp_utilities::PAUSE,
                             -1);
}


/**
 * @brief SLOT that resumes a paused task.
 *
 * Resumes task, identified by its id.
 *
 * @param[in]  index   Index in task model/table.
 */
void
gsd_control::task_resume (int index)
{
  QDomNode ent;
  ent = taskModel->getEntity (index);
  QString stat = taskModel->getValue (ent.toElement (), "status");
  if (stat.compare ("Paused") == 0 )
    {
      connector->resumePausedTask (taskModel->getAttr (ent.toElement (),
                                                       "task id"));
    }
  else if (stat.compare ("Stopped") == 0 )
    {
      connector->resumeStoppedTask (taskModel->getAttr (ent.toElement (),
                                                        "task id"));
    }
}


/**
 * @brief SLOT that deletes a task.
 *
 * Deletes a task, identified by its id.
 *
 * @param[in]  index   Index in task model/table.
 */
void
gsd_control::task_delete (int index)
{
  QDomNode ent;
  ent = taskModel->getEntity (index);
  QString id = taskModel->getAttr (ent.toElement (), "task id");

  int ret = QMessageBox::warning (NULL,
              tr ("Delete?"),
              QString (tr ("Do you really want delete the Task :<b>%1</b>")
                        .arg (taskModel->getValue (ent.toElement (), "name"))),
              QMessageBox::Yes,
              QMessageBox::No);
  switch (ret)
    {
      case QMessageBox::Yes:
        {
          if (this->connector->deleteEntity (omp_utilities::TASK, id) != 0)
            {
              dialogs->request_failed (omp_utilities::TASK,
                                       omp_utilities::STOP,
                                       -1);
              return;
            }
          taskModel->removeEntity (index);
          return;
        }
      case QMessageBox::No:
        {
          return;
        }
      default:
        {
          return;
        }
    }
}


/**
 * @brief SLOT that deletes a scan config.
 *
 * Deletes a scan config, identified by its id.
 *
 * @param[in]  index   Index in config model/table.
 */
void
gsd_control::config_delete (int index)
{
  QDomElement ent = configModel->getEntity (index);
  QString id = configModel->getAttr (ent, "config id");

  int ret = QMessageBox::warning (NULL,
              tr ("Delete?"),
              QString (tr ("Do you really want delete the Config:<b>%1</b>")
                        .arg (configModel->getValue (ent, "name"))),
              QMessageBox::Yes,
              QMessageBox::No);
  switch (ret)
    {
      case QMessageBox::Yes:
        {
          if (this->connector->deleteEntity (omp_utilities::CONFIG, id) != 0)
            {
              dialogs->request_failed (omp_utilities::CONFIG,
                                       omp_utilities::OMP_DELETE,
                                       -1);
              return;
            }
          configModel->removeEntity (index);
          return;
        }
      case QMessageBox::No:
        {
          return;
        }
      default:
        {
          return;
        }
    }
}


/**
 * @brief SLOT that deletes a target.
 *
 * Deletes a target, identified by its id.
 *
 * @param[in]  index   Index in target model/table.
 */
void
gsd_control::target_delete (int index)
{
  QDomElement ent = targetModel->getEntity (index);
  QString id = targetModel->getAttr (ent, "target id");

  int ret = QMessageBox::warning (NULL,
              tr ("Delete?"),
              QString (tr ("Do you really want delete the Target:<b>%1</b>")
                        .arg (targetModel->getValue (ent, "name"))),
              QMessageBox::Yes,
              QMessageBox::No);
  switch (ret)
    {
      case QMessageBox::Yes:
        {
          if (this->connector->deleteEntity (omp_utilities::TARGET, id) != 0)
            {
              dialogs->request_failed (omp_utilities::TARGET,
                                       omp_utilities::OMP_DELETE,
                                       -1);
              return;
            }
          targetModel->removeEntity (index);
          return;
        }
      case QMessageBox::No:
        {
          return;
        }
      default:
        {
          return;
        }
    }
}


/**
 * @brief SLOT that deletes a schedule.
 *
 * Deletes a schedule, identified by its id.
 *
 * @param[in]  index   Index in schedule model/table.
 */
void
gsd_control::schedule_delete (int index)
{
  QDomElement ent = scheduleModel->getEntity (index);
  QString id = scheduleModel->getAttr (ent, "schedule id");

  int ret = QMessageBox::warning (NULL,
              tr ("Delete?"),
              QString (tr ("Do you really want delete the Schedule:<b>%1</b>")
                        .arg (scheduleModel->getValue (ent, "name"))),
              QMessageBox::Yes,
              QMessageBox::No);
  switch (ret)
    {
      case QMessageBox::Yes:
        {
          if (this->connector->deleteEntity (omp_utilities::SCHEDULE, id) != 0)
            {
              dialogs->request_failed (omp_utilities::SCHEDULE,
                                       omp_utilities::OMP_DELETE,
                                       -1);
              return;
            }
          scheduleModel->removeEntity (index);
          return;
        }
      case QMessageBox::No:
        {
          return;
        }
      default:
        {
          return;
        }
    }
}


/**
 * @brief SLOT that deletes an escalator.
 *
 * Deletes an escalator, identified by its id.
 *
 * @param[in]  index   Index in escalator model/table.
 */
void
gsd_control::escalator_delete (int index)
{
  QDomElement ent = escalatorModel->getEntity (index);
  QString id = escalatorModel->getAttr (ent, "escalator id");

  int ret = QMessageBox::warning (NULL,
              tr ("Delete?"),
              QString (tr ("Do you really want delete the Escalator:<b>%1</b>")
                        .arg (escalatorModel->getValue (ent, "name"))),
              QMessageBox::Yes,
              QMessageBox::No);
  switch (ret)
    {
      case QMessageBox::Yes:
        {
          if (this->connector->deleteEntity (omp_utilities::ESCALATOR, id) != 0)
            {
              dialogs->request_failed (omp_utilities::ESCALATOR,
                                       omp_utilities::OMP_DELETE,
                                       -1);
              return;
            }
          escalatorModel->removeEntity (index);
          return;
        }
      case QMessageBox::No:
        {
          return;
        }
      default:
        {
          return;
        }
    }
}


/**
 * @brief SLOT that deletes a credential.
 *
 * Deletes a credential, identified by its id.
 *
 * @param[in]  index   Index in credential model/table.
 */
void
gsd_control::credential_delete (int index)
{
  QDomElement ent = credentialModel->getEntity (index);
  QString id = credentialModel->getAttr (ent, "credential id");

  int ret = QMessageBox::warning (NULL,
              tr ("Delete?"),
              QString (tr ("Do you really want delete the Credential:<b>%1</b>")
                        .arg (credentialModel->getValue (ent, "name"))),
              QMessageBox::Yes,
              QMessageBox::No);
  switch (ret)
    {
      case QMessageBox::Yes:
        {
          if (this->connector->deleteEntity (omp_utilities::LSC_CREDENTIAL,
                                             id) != 0)
            {
              dialogs->request_failed (omp_utilities::LSC_CREDENTIAL,
                                       omp_utilities::OMP_DELETE,
                                       -1);
              return;
            }
          credentialModel->removeEntity (index);
          return;
        }
      case QMessageBox::No:
        {
          return;
        }
      default:
        {
          return;
        }
    }
}


/**
 * @brief SLOT that deletes an agent.
 *
 * Deletes an agent, identified by its id.
 *
 * @param[in]  index   Index in agent model/table.
 */
void
gsd_control::agent_delete (int index)
{
  QDomElement ent = agentModel->getEntity (index);
  QString id = agentModel->getAttr (ent, "agent id");

  int ret = QMessageBox::warning (NULL,
              tr ("Delete?"),
              QString (tr ("Do you really want delete the Agent:<b>%1</b>")
                        .arg (agentModel->getValue (ent, "name"))),
              QMessageBox::Yes,
              QMessageBox::No);
  switch (ret)
    {
      case QMessageBox::Yes:
        {
          if (this->connector->deleteEntity (omp_utilities::AGENT,
                                             id) != 0)
            {
              dialogs->request_failed (omp_utilities::AGENT,
                                       omp_utilities::OMP_DELETE,
                                       -1);
              return;
            }
          agentModel->removeEntity (index);
          return;
        }
      case QMessageBox::No:
        {
          return;
        }
      default:
        {
          return;
        }
    }
}


/**
 * @brief SLOT that deletes a note.
 *
 * Deletes a note, identified by its id.
 *
 * @param[in]  index   Index in note model/table.
 */
void
gsd_control::note_delete (int index)
{
  QDomElement ent = noteModel->getEntity (index);
  QString id = noteModel->getAttr (ent, "note id");

  int ret = QMessageBox::warning (NULL,
              tr ("Delete?"),
              QString (tr ("Do you really want delete the Note:<b>%1</b>")
                        .arg (noteModel->getValue (ent, "name"))),
              QMessageBox::Yes,
              QMessageBox::No);
  switch (ret)
    {
      case QMessageBox::Yes:
        {
          if (this->connector->deleteEntity (omp_utilities::NOTE,
                                             id) != 0)
            {
              dialogs->request_failed (omp_utilities::NOTE,
                                       omp_utilities::OMP_DELETE,
                                       -1);
              return;
            }
          noteModel->removeEntity (index);
          return;
        }
      case QMessageBox::No:
        {
          return;
        }
      default:
        {
          return;
        }
    }
}


/**
 * @brief SLOT that deletes a note.
 *
 * Deletes a note, identified by its id.
 *
 * @param[in]  type       Entity type.
 * @param[in]  id         Id of the entity that is modified.
 * @param[in]  m          Model to store the response.
 * @param[in]  parameter  Parameter for modification process.
 */
void
gsd_control::modify (int type,
                     QString id,
                     model_omp_entity *m,
                     QMap<QString, QString>parameter)
{
  this->connector->modifyEntity (type, id, m, parameter);
}


/**
 * @brief SLOT that deletes an override.
 *
 * Deletes a override, identified by its id.
 *
 * @param[in]  index   Index in override model/table.
 */
void
gsd_control::override_delete (int index)
{
  QDomElement ent = overrideModel->getEntity (index);
  QString id = overrideModel->getAttr (ent, "override id");

  int ret = QMessageBox::warning (NULL,
              tr ("Delete?"),
              QString (tr ("Do you really want delete the Override:<b>%1</b>")
                        .arg (overrideModel->getValue (ent, "name"))),
              QMessageBox::Yes,
              QMessageBox::No);
  switch (ret)
    {
      case QMessageBox::Yes:
        {
          if (this->connector->deleteEntity (omp_utilities::OVERRIDE,
                                             id) != 0)
            {
              dialogs->request_failed (omp_utilities::OVERRIDE,
                                       omp_utilities::OMP_DELETE,
                                       -1);
              return;
            }
          overrideModel->removeEntity (index);
          return;
        }
      case QMessageBox::No:
        {
          return;
        }
      default:
        {
          return;
        }
    }
}


/**
 * @brief SLOT that deletes a slave.
 *
 * Deletes a slave, identified by its id.
 *
 * @param[in]  index   Index in slave model/table.
 */
void
gsd_control::slave_delete (int index)
{
  QDomElement ent = slaveModel->getEntity (index);
  QString id = slaveModel->getAttr (ent, "slave id");

  int ret = QMessageBox::warning (NULL,
              tr ("Delete?"),
              QString (tr ("Do you really want delete the Slave:<b>%1</b>")
                        .arg (slaveModel->getValue (ent, "name"))),
              QMessageBox::Yes,
              QMessageBox::No);
  switch (ret)
    {
      case QMessageBox::Yes:
        {
          if (this->connector->deleteEntity (omp_utilities::SLAVE,
                                             id) != 0)
            {
              dialogs->request_failed (omp_utilities::SLAVE,
                                       omp_utilities::OMP_DELETE,
                                       -1);
              return;
            }
          slaveModel->removeEntity (index);
          return;
        }
      case QMessageBox::No:
        {
          return;
        }
      default:
        {
          return;
        }
    }
}

/**
 * @brief SLOT that deletes a port_list.
 *
 * Deletes a port_list, identified by its id.
 *
 * @param[in]  index   Index in port_list model/table.
 */
void
gsd_control::port_list_delete (int index)
{
  QDomElement ent = portListModel->getEntity (index);
  QString id = portListModel->getAttr (ent, "port_list id");

  int ret = QMessageBox::warning (NULL,
              tr ("Delete?"),
              QString (tr ("Do you really want delete the List: <b>%1</b>")
                        .arg (slaveModel->getValue (ent, "name"))),
              QMessageBox::Yes,
              QMessageBox::No);
  switch (ret)
    {
      case QMessageBox::Yes:
        {
          if (this->connector->deleteEntity (omp_utilities::PORT_LIST,
                                             id) != 0)
            {
              dialogs->request_failed (omp_utilities::PORT_LIST,
                                       omp_utilities::OMP_DELETE,
                                       -1);
              return;
            }
          portListModel->removeEntity (index);
          return;
        }
      case QMessageBox::No:
        {
          return;
        }
      default:
        {
          return;
        }
    }
}

/**
 * @brief SLOT that requests note details.
 *
 * @param[in]  id    Note id.
 * @param[in]  m     Model to store the response.
 */
void
gsd_control::request_note_details (QString id)
{
  QMap <QString, QString> parameter;
  parameter.insert ("note_id", id);
  parameter.insert ("details", "1");

  this->connector->getEntity (omp_utilities::NOTE,
                              0,
                              noteDetailsModel,
                              parameter);
}


/**
 * @brief SLOT that requests override details.
 *
 * @param[in]   id    Override id.
 * @param[in]   m     Model to store the response.
 */
void
gsd_control::request_override_details (QString id)
{
  QMap<QString, QString> parameter;
  parameter.insert ("override_id", id);
  parameter.insert ("details", "1");

  this->connector->getEntity (omp_utilities::OVERRIDE,
                              0,
                              overrideDetailsModel,
                              parameter);
}


/**
 * @brief SLOT that requests config details.
 *
 * @param[in]  index   Row in config model/table.
 */
void
gsd_control::request_config_details (int index)
{
  QString id = configModel->getAttr (configModel->getEntity (index),
                                     "config id");
  QMap<QString, QString> parameter;
  parameter.insert ("config_id", id);
  parameter.insert ("families", "1");
  parameter.insert ("preferences", "1");

  this->connector->getEntity (omp_utilities::CONFIG,
                              0,
                              configDetailsModel,
                              parameter);
}


/**
 * @brief SLOT that requests nvt families.
 */
void
gsd_control::request_families ()
{
  QMap<QString, QString> parameter;
  this->connector->getEntity (omp_utilities::NVT_FAMILY, 0, familyModel, parameter);
}


/**
 * @brief SLOT that request family details.
 *
 * @param[in]  config_id   Config, the nvt family belongs to.
 * @param[in]  name        Family name.
 */
void
gsd_control::request_family_details (QString config_id, QString name)
{
  QMap<QString, QString> parameter;
  parameter.insert ("config_id", config_id);
  parameter.insert ("family", name);
  parameter.insert ("preferences", "1");
  parameter.insert ("preference_count", "1");
  parameter.insert ("details", "1");
  familyDetailsModel->removeEntities ();
  this->connector->getEntity (omp_utilities::NVT, 0, familyDetailsModel, parameter);
}

/**
 * @brief SLOT that request port list details.
 *
 * @param[in]  port_list_id Id of the port list.
 */
void
gsd_control::request_port_list_details (QString id)
{
  QMap<QString, QString> parameter;
  parameter.insert ("port_list_id", id);
  parameter.insert ("details", "1");
  portListDetailsModel->removeEntities ();
  this->connector->getEntity (omp_utilities::PORT_LIST, 0,
                            portListDetailsModel, parameter);
}

/**
 * @brief SLOT that requests nvts.
 *
 * @param[in]  family    Family the nvts belong to.
 */
void
gsd_control::request_nvts (QString family)
{
  QMap<QString, QString> parameter;
  parameter.insert ("family", family);
  parameter.insert ("details", "1");
  nvtModel->removeEntities ();
  this->connector->getEntity (omp_utilities::NVT, 0, nvtModel, parameter);
}


/**
 * @brief SLOT that requests report formats.
 */
void
gsd_control::request_report_formats ()
{
  QMap<QString, QString> parameter;
  this->connector->getEntity (omp_utilities::REPORT_FORMAT,
                              0,
                              reportFormatModel,
                              parameter);
}


/**
 * @brief SLOT that requests reports.
 *
 * @param[in]  parameter   Requestparameter.
 */
void
gsd_control::request_report (QMap<QString, QString> parameter)
{
  QDomElement element_html, element_xml;
  QString html_format_id, xml_format_id;
  for (int i = 0; i < reportFormatModel->rowCount (); i++)
    {
      if (reportFormatModel->getValue (reportFormatModel->getEntity (i),
                                       "name").compare ("HTML") == 0)
        {
          element_html = reportFormatModel->getEntity (i);
          html_format_id = reportFormatModel->getAttr (element_html,
                                                   "report_format id");
          break;
        }
    }
  for (int i = 0; i < reportFormatModel->rowCount (); i++)
    {
      if (reportFormatModel->getValue (reportFormatModel->getEntity (i),
                                       "name").compare ("XML") == 0)
        {
          element_xml = reportFormatModel->getEntity (i);
          xml_format_id = reportFormatModel->getAttr (element_xml,
                                               "report_format id");
          break;
        }
    }
  if ((reportFormatModel->getValue (element_html, "active").compare ("0")
      == 0 ||
      reportFormatModel->getValue (element_xml, "active").compare ("0")
      == 0) &&
      parameter["format_id"] == html_format_id)
    {
      QMessageBox::information (NULL, tr ("Report format"),
                                tr ("HTML or XML format not active!"));
      return;
    }
  if (reportFormatModel->getValue (element_xml, "active").compare ("0")
      == 0)
    {
      return;
    }

  if (parameter["format_id"] == html_format_id)
    {
      if (htmlReportModel->rowCount () > 0)
        htmlReportModel->removeEntities ();

      this->connector->getEntity (omp_utilities::REPORT,
                                  0,
                                  htmlReportModel,
                                  parameter);
    }
  if (parameter["format_id"] == xml_format_id)
    {
      parameter.remove ("format");
      if (xmlReportModel->rowCount () > 0)
        xmlReportModel->removeEntities ();

      this->connector->getEntity (omp_utilities::REPORT,
                                  0,
                                  xmlReportModel,
                                  parameter);
    }
}


/**
 * @brief SLOT that deletes a report.
 *
 * Deletes a report, identified by its id.
 *
 * @param[in]  id   Report id.
 */
void
gsd_control::report_delete (QString id)
{
  int ret = QMessageBox::warning (NULL,
              tr ("Delete?"),
              QString (tr ("Do you really want delete this Report?")),
              QMessageBox::Yes,
              QMessageBox::No);
  switch (ret)
    {
      case QMessageBox::Yes:
        {
          if (this->connector->deleteEntity (omp_utilities::REPORT,
                                             id) != 0)
            {
              dialogs->request_failed (omp_utilities::REPORT,
                                       omp_utilities::OMP_DELETE,
                                       -1);
              return;
            }
          return;
        }
      case QMessageBox::No:
        {
          return;
        }
      default:
        {
          return;
        }
    }
}


void
gsd_control::report_download (QMap<QString, QString> parameter)
{
  if (protocol_version >= 2)
    {
      if (tmpReportModel->rowCount () > 0)
        tmpReportModel->removeEntities ();

      QDomElement element;
      for (int i = 0; i < reportFormatModel->rowCount (); i++)
        {
          const QString curId = reportFormatModel->getAttr (
                                 reportFormatModel->getEntity (i),
                                  "report_format id");

          if ( curId == parameter["format_id"])
            {
              element = reportFormatModel->getEntity (i);
              break;
            }
        }
      if (reportFormatModel->getValue (element, "active").compare ("0")
          == 0)
        {
          QMessageBox::information (NULL, tr ("Report format"),
                                    tr ("Report format not active!"));
          return;
        }

      this->connector->getEntity (omp_utilities::REPORT,
                                  0,
                                  tmpReportModel,
                                  parameter);

    }
  else if (protocol_version == 1)
    {
      if (tmpReportModel->rowCount () > 0)
        tmpReportModel->removeEntities ();
      this->connector->getEntity (omp_utilities::REPORT,
                                  0,
                                  tmpReportModel,
                                  parameter);
    }
}

/**
 * @brief Slot that requests nvt details.
 *
 * @param[in]  config_id   Config, the nvt belongs to.
 * @param[in]  oid         OID of the nvt.
 */
void
gsd_control::request_nvt_details (QString config_id, QString oid)
{
  QMap<QString, QString> parameter;
  parameter.insert ("config_id", config_id);
  parameter.insert ("nvt_oid", oid);
  parameter.insert ("preference_count", "1");
  parameter.insert ("preferences", "1");
  parameter.insert ("details", "1");
  nvtDetailsModel->removeEntities ();

  this->connector->getEntity (omp_utilities::NVT,
                              0,
                              nvtDetailsModel,
                              parameter);
}


/**
 * @brief SLOT that requests system reports.
 */
void
gsd_control::system_report_download ()
{
  if (this->connector == NULL)
    {
      // if no connector is set, set an error-information
      QMessageBox::information (NULL, tr ("Login Error"),
                                tr ("Please login first!"));
    }
  else
    {
    }
}


/**
 * @brief SLOT that requests system reports.
 *
 * @param[in]  interval   Time for the system report.
 */
void
gsd_control::request_system_reports (int interval)
{
  QMap<QString, QString> parameter;
  parameter.insert ("brief", "1");
  this->connector->getEntity (omp_utilities::SYSTEM_REPORT,
                              0,
                              systemReportModel,
                              parameter);
}


/**
 * @brief SLOT that requests system reports.
 *
 * @param[in]  name      System report name.
 * @param[in]  interval  Time for the systemreport.
 */
void
gsd_control::request_system_reports (QString name, long interval)
{
  QMap<QString, QString> parameter;
  parameter.insert ("duration", QString ("%1").arg (interval));
  this->connector->getEntity (omp_utilities::SYSTEM_REPORT,
                              0,
                              performanceModel,
                              parameter);
  QMap<QString, QString> parameter_2;
  parameter.insert ("brief", "1");
  parameter.insert ("duration", QString ("%1").arg (interval));
  this->connector->getEntity (omp_utilities::SYSTEM_REPORT,
                              0,
                              systemReportModel,
                              parameter_2);

}


/**
 * @brief SLOT to display login faults
 *
 * Displays errors that occured while trying to login.
 *
 * @param[in]  err   Error code
 */
void
gsd_control::update_failed (int err)
{
  mainwindow->logout ();
  switch (err)
    {
      case 1:
        {
          QMessageBox::information (NULL, tr ("Login Error"),
                                    tr ("Server closed connection!"));
          return;
        }
      case 2:
        {
          QMessageBox::information (NULL, tr ("Login Error"),
                                    tr ("Authentication failed!")); 
          return;
        }
      case 3:
        {
          QMessageBox::information (NULL, tr ("Login Error"),
                                    tr ("Failed to get server address!"));
          return;
        }
      case -1:
        {
          QMessageBox::information (NULL, tr ("Login Error"),
                                    tr ("Internal Error!"));
          return;
        }
      default:
        {
          QMessageBox::information (NULL, tr ("Login Error"),
                                    tr ("Internal Error!"));
          return;
        }
    }
}


#ifndef WIN32
/**
 * @brief Setup logging.
 */
void
gsd_control::setupLogging ()
{
  gchar *filename;
  filename = g_build_filename (OPENVAS_SYSCONF_DIR, "gsd_log.conf", NULL);
  if (g_file_test (filename, G_FILE_TEST_EXISTS))
    {
      log_config = load_log_configuration (filename);
    }

  g_free (filename);
  setup_log_handlers (log_config);
  return;
}
#endif


/**
 * @brief Getter
 *
 * @return The model containing the tasks
 */
model_omp_entity*
gsd_control::getTaskModel ()
{
  return taskModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the schedules
 */
model_omp_entity*
gsd_control::getScheduleModel ()
{
  return scheduleModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the configs
 */
model_omp_entity*
gsd_control::getConfigModel ()
{
  return configModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the targets.
 */
model_omp_entity*
gsd_control::getTargetModel ()
{
  return targetModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the escalators.
 */
model_omp_entity*
gsd_control::getEscalatorModel ()
{
  return escalatorModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the lsc_credentials.
 */
model_omp_entity*
gsd_control::getCredentialModel ()
{
  return credentialModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the agents.
 */
model_omp_entity*
gsd_control::getAgentModel ()
{
  return agentModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the notes.
 */
model_omp_entity*
gsd_control::getNoteModel ()
{
  return noteModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the overrides.
 */
model_omp_entity*
gsd_control::getOverrideModel ()
{
  return overrideModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the system reports.
 */
model_omp_entity*
gsd_control::getSystemDiagramModel ()
{
  return performanceModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the system report graphics.
 */
model_omp_entity*
gsd_control::getSystemReportModel ()
{
  return systemReportModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the nvt families.
 */
model_omp_entity*
gsd_control::getFamilyModel ()
{
  return familyModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the nvt families.
 */
model_omp_entity*
gsd_control::getNVTModel ()
{
  return nvtModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the nvt families details.
 */
model_omp_entity*
gsd_control::getFamilyDetailsModel ()
{
  return familyDetailsModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the config details.
 */
model_omp_entity*
gsd_control::getConfigDetailsModel ()
{
  return configDetailsModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the nvt details.
 */
model_omp_entity*
gsd_control::getNVTDetailsModel ()
{
  return nvtDetailsModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the note details.
 */
model_omp_entity*
gsd_control::getNoteDetailsModel ()
{
  return noteDetailsModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the override details.
 */
model_omp_entity*
gsd_control::getOverrideDetailsModel ()
{
  return overrideDetailsModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the slaves.
 */
model_omp_entity*
gsd_control::getSlaveModel ()
{
  return slaveModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the report in html format.
 */
model_omp_entity*
gsd_control::getHtmlReportModel ()
{
  return htmlReportModel;
}


/**
 * @brief Getter
 *
 * @return The model containing a report in xml format.
 */
model_omp_entity*
gsd_control::getXmlReportModel ()
{
  return xmlReportModel;
}


/**
 * @brief Getter
 *
 * @return The model containing a config prepared to be saved on local
 *         filesystem.
 */
model_omp_entity*
gsd_control::getConfigDownloadModel ()
{
  return configDownloadModel;
}


/**
 * @brief Getter
 *
 * @return The model containing a report prepared to be saved on local
 *         filesystem.
 */
model_omp_entity*
gsd_control::getReportDownloadModel ()
{
  return tmpReportModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the port lists
 */
model_omp_entity*
gsd_control::getPortListModel()
{
  return portListModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the port lists details
 */
model_omp_entity*
gsd_control::getPortListDetailsModel()
{
  return portListDetailsModel;
}


/**
 * @brief Getter
 *
 * @return The model containing the report formats.
 */
model_omp_entity*
gsd_control::getReportFormatModel ()
{
  return reportFormatModel;
}


void
gsd_control::note_modify(int)
{}


void
gsd_control::override_modify(int)
{}


/**
 * @brief Requests a config download.
 *
 * @param[in]  ndx   Index of the config.
 */
void
gsd_control::config_download(int ndx)
{
  QString config_id = configModel->getAttr (configModel->getEntity (ndx),
                                            "config id");
  QMap<QString, QString> parameter;
  parameter.insert ("config_id", config_id);
  parameter.insert ("export", "1");

  this->connector->getEntity (omp_utilities::CONFIG,
                              0,
                              configDownloadModel,
                              parameter);
}


void
gsd_control::system_report_download(QString, int)
{}


/**
 * @brief Creates an entity.
 *
 * @param[in]  type       Entity type.
 * @param[in]  parameter  Parameter for the creation process.
 */
void
gsd_control::create (int type, QMap<QString, QString> parameter)
{
  model_omp_entity *m = new model_omp_entity();
  connector->createEntity(type, m, parameter);
}

/**
 * @brief Creates an entity.
 *
 * @param[in]  type       Entity type.
 * @param[in]  parameter  Parameter for the creation process.
 */
void
gsd_control::modify (int type, QMap<QString, QString> parameter)
{
  model_omp_entity *m = new model_omp_entity();
  if (type == omp_utilities::TASK)
    modify (type, parameter["task_id"], taskModel, parameter);
}

/**
 * @brief Notifies the config details widgets about changes in the model.
 */
void
gsd_control::notifyConfigDetails ()
{
  QString id = configDetailsModel->getAttr (configDetailsModel->getEntity (0),
                                            "config id");
  emit sig_config_details_finished (id);
}

/**
 * @brief Notifies the port list details widgets about changes in the model.
 *
 * @param[in]  id  Id of the portlist.
 */
void
gsd_control::notifyPortListDetails (QString id)
{
  emit sig_port_list_details_finished (id);
}

/**
 * @brief Notifies the family details widgets about changes in the model.
 *
 * @param[in]  config   Id, config the family belongs to.
 */
void
gsd_control::notifyFamilyDetails (QString config)
{
  QString name = familyDetailsModel->getValue (familyDetailsModel->getEntity (0),
                                               "family");
  emit sig_family_details_finished (name, config);
}


/**
 * @brief Notifies the nvt details widgets about changes in the model.
 *
 * @param[in]  config   Id, config the nvt belongs to.
 */
void
gsd_control::notifyNVTDetails (QString config)
{
  QString oid = nvtDetailsModel->getAttr (nvtDetailsModel->getEntity (0),
                                          "nvt oid");
  emit sig_nvt_details_finished (config, oid);
}


/**
 * @brief Notifies the note details widgets about changes in the model.
 *
 * @param[in]  id   Note id.
 */
void
gsd_control::notifyNoteDetails (QString id)
{
  emit sig_note_details_finished (id);
}


/**
 * @brief Notifies the override details widget about changes in the model.
 *
 * @param[in]  id   Override id.
 */
void
gsd_control::notifyOverrideDetails (QString id)
{
  emit sig_override_details_finished (id);
}


/**
 * @brief Notifies the Report widget, that the request has finished.
 *
 * @param[in]  id   Report id.
 */
void
gsd_control::notifyReport (QString id)
{
  emit sig_report_finished (id);
}

/**
 * @brief Notifies the task widget about status changes in the model.
 */
void
gsd_control::notifyTasks ()
{
  mainwindow->focus_tasks (false);
  mainwindow->updateDashboard ();
}


/**
 * @brief Indicates that the config download has finished.
 */
void
gsd_control::notifyConfigDownload ()
{
  mainwindow->save_config ();
}


void
gsd_control::notifyVersion (QString version)
{
  bool ok;
  float version_f = version.toFloat(&ok);
  if (!dialogs->loginOpen ())
    {
      if (version_f <= 1.0)
        {
          protocol_version = 1;
          int ret = QMessageBox::warning (NULL,
                                          tr ("Wrong OMP Version"),
              QString (tr ("You are connected to an OMP %1 service!\n"
                           "Since version 1.1.0 GSD supports OMP > 2.0 only.\n"
                           "Use GSD Version 1.0 instead.").arg(version)),
              QMessageBox::Ok);
          mainwindow->logout ();
          logout ();
          dialogs->login_dlg ();
          return;
        }
      else if (version_f == 2.0)
        protocol_version = 2;
      else if (version_f == 3.0)
          protocol_version = 3;
      else if (version_f > 3.0)
        protocol_version = (int) version_f;
      qApp->setProperty("protocol_version", 3);
      loadData ();
    }
  else
    {
      int p = 0;
      if (version_f == 1.0)
        p = 2;
      else if (version_f == 2.0)
        p = 3;
      else if (version_f == 3.0)
        p = 4;
      else if (version_f > 3.0)
        p = (int) version_f;
      emit sig_check_result (p);
    }
}


void
gsd_control::notifyTaskDetails ()
{
  emit sig_task_details_finished ();
}

/**
 * @brief Listens to the connectors signals to notify the ui about finished
 *        requests.
 *
 * @param[in]  type      Entity type.
 * @param[in]  com       Command type.
 * @param[in]  userData  User data, used for ids and details about the finished
 *                       request.
 */
void
gsd_control::request_finished_listener(int type, int com, QString userData)
{
  if (type == omp_utilities::TASK && (com == omp_utilities::OMP_CREATE ||
                                      com == omp_utilities::OMP_DELETE ||
                                      com >= omp_utilities::START ||
                                      com == omp_utilities::OMP_MODIFY))
    {
      request_tasks (0);
    }
  if (type == omp_utilities::TASK && com == omp_utilities::OMP_DETAILS)
    {
      notifyTasks ();
      notifyTaskDetails ();
    }
  if (type == omp_utilities::CONFIG && (com == omp_utilities::OMP_CREATE ||
                                        com == omp_utilities::OMP_DELETE))
    {
      request_configs (0);
    }
  if (type == omp_utilities::CONFIG && com == omp_utilities::OMP_MODIFY)
    {
      QStringList data;
      if (userData.contains("__"))
        {
          data = userData.split ("__");
          request_family_details (data[0], data[1]);
        }
      else if (userData.contains ("##"))
        {
          data = userData.split ("##");
          request_nvt_details (data[0], data[1]);
        }
      else
        {
          data.insert (0, userData);
        }
      int ndx = 0;
      for (int i = 0; i < configModel->rowCount (); i++)
        {
          if (configModel->getAttr (configModel->getEntity (i),
                                    "config id"). compare (data[0]) == 0)
            {
              ndx = i;
              break;
            }
        }
      request_config_details (ndx);
    }
  if (type == omp_utilities::CONFIG && com == omp_utilities::OMP_PREFERENCES )
    {
      notifyConfigDetails ();
    }
  if (type == omp_utilities::CONFIG && com == omp_utilities::OMP_EXPORT)
    {
      notifyConfigDownload ();
    }
  if (type == omp_utilities::TARGET && (com == omp_utilities::OMP_CREATE ||
                                        com == omp_utilities::OMP_DELETE))
    {
      request_targets (0);
    }
  if (type == omp_utilities::SCHEDULE && (com == omp_utilities::OMP_CREATE ||
                                          com == omp_utilities::OMP_DELETE))
    {
      request_schedules (0);
    }
  if (type == omp_utilities::ESCALATOR && (com == omp_utilities::OMP_CREATE ||
                                           com == omp_utilities::OMP_DELETE))
    {
      request_escalators (0);
    }
  if (type == omp_utilities::LSC_CREDENTIAL && (com == omp_utilities::OMP_CREATE ||
                                                com == omp_utilities::OMP_DELETE))
    {
      request_lsc_credentials (0);
    }
  if (type == omp_utilities::AGENT && (com == omp_utilities::OMP_CREATE ||
                                       com == omp_utilities::OMP_DELETE))
    {
      request_agents (0);
    }
  if (type == omp_utilities::NOTE && (com == omp_utilities::OMP_CREATE ||
                                      com == omp_utilities::OMP_DELETE))
    {
      request_notes (0);
    }
  if (type == omp_utilities::OVERRIDE && (com == omp_utilities::OMP_CREATE ||
                                          com == omp_utilities::OMP_DELETE))
    {
      request_overrides (0);
    }
  if (type == omp_utilities::SLAVE && (com == omp_utilities::OMP_CREATE ||
                                       com == omp_utilities::OMP_DELETE))
    {
      request_slaves (0);
    }
  if (type == omp_utilities::NVT_FAMILY && com == omp_utilities::OMP_PREFERENCES )
    {
      notifyFamilyDetails (userData);
    }
  if (type == omp_utilities::NVT_FAMILY && com == omp_utilities::OMP_GET)
    {
      mainwindow->updateDashboard ();
    }
  if (type == omp_utilities::NVT && com == omp_utilities::OMP_PREFERENCES)
    {
      notifyNVTDetails (userData);
    }
  if (type == omp_utilities::NOTE && com == omp_utilities::OMP_DETAILS)
    {
      notifyNoteDetails (userData);
    }
  if (type == omp_utilities::OVERRIDE && com == omp_utilities::OMP_DETAILS)
    {
      notifyOverrideDetails (userData);
    }
  if (type == omp_utilities::CONFIG && com == omp_utilities::OMP_MODIFY)
    {
    }
  if (type == omp_utilities::NOTE && com == omp_utilities::OMP_MODIFY)
    {
      request_note_details (userData);
      request_notes (0);
    }
  if (type == omp_utilities::OVERRIDE && com == omp_utilities::OMP_MODIFY)
    {
      request_override_details (userData);
      request_overrides (0);
    }
  if (type == omp_utilities::LSC_CREDENTIAL && com == omp_utilities::OMP_MODIFY)
    {
      request_lsc_credentials (0);
    }
  if (type == omp_utilities::REPORT)
    {
      notifyReport (userData);
    }
  if (type == omp_utilities::NOTE && com == omp_utilities::OMP_GET)
    {
      emit sig_note_finished ();
    }
  if (type == omp_utilities::OVERRIDE && com == omp_utilities::OMP_GET)
    {
      emit sig_override_finished ();
    }
  if (type == omp_utilities::LSC_CREDENTIAL && com == omp_utilities::OMP_GET)
    {
      mainwindow->focus_credentials (false);
      emit sig_lsc_credential_finished ();
    }
  if (type == omp_utilities::VERSION)
    {
      notifyVersion (userData);
    }
  if (type == omp_utilities::SYSTEM_REPORT)
    {
      mainwindow->focus_performance(true);
    }
  if (type == omp_utilities::REPORT)
    {
      request_tasks (0);
    }
  if ((type == omp_utilities::NOTE || type == omp_utilities::OVERRIDE) &&
      (com == omp_utilities::OMP_CREATE || com == omp_utilities::OMP_DELETE))
    {
      emit sig_request_report ();
    }
  if (type == omp_utilities::PORT_LIST && com == omp_utilities::OMP_DETAILS)
    {
      notifyPortListDetails (userData);
    }
  if (type == omp_utilities::PORT_LIST && com == omp_utilities::OMP_GET)
    {
      emit sig_port_list_finished ();
    }
  if (type == omp_utilities::PORT_LIST && ( com == omp_utilities::OMP_MODIFY ||
      com == omp_utilities::OMP_CREATE || com == omp_utilities::OMP_DELETE ))
    {
      request_portlists (0);
    }
  if (type == omp_utilities::REPORT_FORMAT) 
    {
      emit sig_report_format_finished ();
    }
}

void
gsd_control::request_failed_listener(int type, int com, int err)
{
}


/**
 * @brief omp protocol version.
 *
 * return omp protocol version of the currently connected manager.
 */
int
gsd_control::getProtocolVersion ()
{
  return protocol_version;
}
