<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr" sourcelanguage="en">
<context>
    <name>MainWindow</name>
    <message>
        <source>Greenbone Security Desktop</source>
        <translation></translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>&amp;Task</source>
        <translation>&amp;Tâche</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>&amp;Paramètres</translation>
    </message>
    <message>
        <source>&amp;Language</source>
        <translation>&amp;Langue</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Voir</translation>
    </message>
    <message>
        <source>&amp;Extras</source>
        <translation>&amp;Extras</translation>
    </message>
    <message>
        <source>New Connection</source>
        <translation>Nouvelle Connexion</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>Close GSD</source>
        <translation>Fermer GSD</translation>
    </message>
    <message>
        <source>&amp;Login</source>
        <translation>&amp;Connexion</translation>
    </message>
    <message>
        <source>L&amp;ogout</source>
        <translation>&amp;Déconnexion</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;A propos</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Statut</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>&amp;Effacer</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <source>&amp;Start</source>
        <translation>&amp;Démarrer</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <source>S&amp;top</source>
        <translation>&amp;Arrêter</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Arrêter</translation>
    </message>
    <message>
        <source>Break</source>
        <translation>Interrompre</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Editer</translation>
    </message>
    <message>
        <source>&amp;Refresh</source>
        <translation>&amp;Rafraîchir</translation>
    </message>
    <message>
        <source>Contents</source>
        <translation>Contenus</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>Afficher</translation>
    </message>
    <message>
        <source>Users</source>
        <translation>Utilisateurs</translation>
    </message>
    <message>
        <source>NVT Feed</source>
        <translation>Flux NVT</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Logging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Schedules</source>
        <translation>&amp;Plannifications</translation>
    </message>
    <message>
        <source>&amp;Targets</source>
        <translation>&amp;Cibles</translation>
    </message>
    <message>
        <source>S&amp;can Configs</source>
        <translation>&amp;Configs de scan</translation>
    </message>
    <message>
        <source>&amp;Escalators</source>
        <translation>&amp;Avertisseurs</translation>
    </message>
    <message>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <source>&amp;Resume</source>
        <translation>&amp;Reprendre</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <source>&amp;Credentials</source>
        <translation>&amp;Identifiants</translation>
    </message>
    <message>
        <source>&amp;Agents</source>
        <translation>&amp;Agent</translation>
    </message>
    <message>
        <source>Download Installer</source>
        <translation>Télécharger l&apos;installateur</translation>
    </message>
    <message>
        <source>Export XML</source>
        <translation>Exporter en XML</translation>
    </message>
    <message>
        <source>Test</source>
        <translation>Test</translation>
    </message>
    <message>
        <source>&amp;Notes</source>
        <translation>&amp;Notes</translation>
    </message>
    <message>
        <source>&amp;Overrides</source>
        <translation>&amp;Substituts</translation>
    </message>
    <message>
        <source>&amp;Performance</source>
        <translation>&amp;Performance</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <source>&amp;Clear Dock Settings</source>
        <translation>&amp;Effacer la configuration du panneau</translation>
    </message>
    <message>
        <source>Import Config</source>
        <translation>Importer la configuration</translation>
    </message>
    <message>
        <source>&amp;German</source>
        <translation>&amp;Allemand</translation>
    </message>
    <message>
        <source>&amp;English</source>
        <translation>&amp;Anglais</translation>
    </message>
    <message>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <source>&amp;Default</source>
        <translation>&amp;Défaut</translation>
    </message>
    <message>
        <source>&amp;Start Greenbone Security Assistant</source>
        <translation>&amp;Démarrer Greenbone Security Assistant</translation>
    </message>
    <message>
        <source>Refresh Interval:</source>
        <translation>Interval de rafraîchissement:</translation>
    </message>
    <message>
        <source> sec</source>
        <translation> sec</translation>
    </message>
    <message>
        <source>Next Refresh: </source>
        <translation>Prochain rafraîchissement: </translation>
    </message>
    <message>
        <source>Apply Interval</source>
        <translation>Appliquer l&apos;interval</translation>
    </message>
    <message>
        <source>Stop Interval</source>
        <translation>Arrêter l&apos;interval</translation>
    </message>
    <message>
        <source>manual</source>
        <translation>manuel</translation>
    </message>
    <message>
        <source>Refresh Progress</source>
        <translation>Rafraîchier la progression</translation>
    </message>
    <message>
        <source>Refresh Settings</source>
        <translation>Rafraîchir les paramètres</translation>
    </message>
    <message>
        <source>Refresh Interval: &lt;b&gt;manual&lt;/b&gt;</source>
        <translation>Interval de rafraîchissement: &lt;b&gt;manuel&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Refresh Interval: &lt;b&gt;%1 second&lt;/b&gt;</source>
        <translation>Interval de rafraîchissement: &lt;b&gt;%1 seconde&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Refresh Interval: &lt;b&gt;%1 seconds&lt;/b&gt;</source>
        <translation>Interval de rafraîchissement: &lt;b&gt;%1 secondes&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&amp;French</source>
        <translation>&amp;Français</translation>
    </message>
    <message>
        <source>Dashboard</source>
        <translation>Tableau de bord</translation>
    </message>
    <message>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
    <message>
        <source>Slaves</source>
        <translation>Esclaves</translation>
    </message>
    <message>
        <source>System Reports</source>
        <translation>Rapports système</translation>
    </message>
</context>
<context>
    <name>delegate_date_time</name>
    <message>
        <source> hour</source>
        <translation> heure</translation>
    </message>
    <message>
        <source> hours</source>
        <translation> heures</translation>
    </message>
    <message>
        <source> day</source>
        <translation> jour</translation>
    </message>
    <message>
        <source> days</source>
        <translation> jours</translation>
    </message>
    <message>
        <source> week</source>
        <translation> semaine</translation>
    </message>
    <message>
        <source> weeks</source>
        <translation> semaines</translation>
    </message>
    <message>
        <source> month</source>
        <translation> mois</translation>
    </message>
    <message>
        <source> months</source>
        <translation> mois</translation>
    </message>
</context>
<context>
    <name>delegate_progress</name>
    <message>
        <source> at </source>
        <translation> à </translation>
    </message>
    <message>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
</context>
<context>
    <name>dlg_about</name>
    <message>
        <source>About</source>
        <translation>A propos</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Greenbone Security Desktop</source>
        <translation>Greenbone Security Desktop</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <source>Greenbone Security Desktop is a graphical user interface&lt;br&gt;to access a Greenbone Security Manager, OpenVAS Manager&lt;br&gt;or any other service that offers the OMP protocol for&lt;br&gt;comprehensive vulnerability management.&lt;br&gt;&lt;br&gt;Copyright 2009-2011 by Greenbone Networks GmbH,&lt;br&gt;&lt;a href=www.greenbone.net&gt; www.greenbone.net&lt;/a&gt;</source>
        <translation>Greenbone Security Desktop est une interface graphique&lt;br&gt;pour accéder à Greenbone Security Manager, OpenVAS Manager&lt;br&gt;ou n&apos;importe quel service offrant le protocole OMP pour&lt;br&gt;la gestion complète des vulnérabilités.&lt;br&gt;&lt;br&gt; Copyright 2009-2011 Greenbone Networks GmbH,&lt;br&gt;&lt;a href=&quot;http://www.greenbone.net&quot;&gt; www.greenbone.net&lt;/a&gt;</translation>
    </message>
    <message>
        <source>License: GNU General Public License version 2 or any later&lt;br&gt;version (&lt;a href=&quot;license&quot;&gt;full license text&lt;a&gt;).</source>
        <translation>Licence: GNU General Public License version 2 ou postérieure&lt;br&gt;(&lt;a href=&quot;license&quot;&gt;texte complet de la licence&lt;a&gt;).</translation>
    </message>
    <message>
        <source>Contact: For updates, feature proposals and bug reports please&lt;br&gt;contact the  &lt;a href=&quot;http://www.greenbone.net/company/contact.html&quot;&gt;Greenbone team&lt;/a&gt; or visit the &lt;a href=&quot;http://www.openvas.org&quot;&gt;OpenVAS homepage&lt;/a&gt;.</source>
        <translation>Contact: Pour les mises à jour, améliorations, propositions et rapport de bugs merci&lt;br&gt; de contacter  &lt;a href=&quot;http://www.greenbone.net/company/contact.html&quot;&gt;l&apos;équipe de Greenbone&lt;/a&gt; ou rendez-vous sur &lt;a href=&quot;http://www.openvas.org&quot;&gt; la page d&apos;accueil d&apos;OpenVAS&lt;/a&gt;.</translation>
    </message>
</context>
<context>
    <name>dlg_detailsNote</name>
    <message>
        <source>Note Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>NVT Name:</source>
        <translation>Nom NVT:</translation>
    </message>
    <message>
        <source>NVT OID:</source>
        <translation>NVT OID:</translation>
    </message>
    <message>
        <source>Created:</source>
        <translation>Créé le:</translation>
    </message>
    <message>
        <source>Last Modified:</source>
        <translation>Dernière Modification:</translation>
    </message>
    <message>
        <source>Application</source>
        <translation>Application</translation>
    </message>
    <message>
        <source>Hosts:</source>
        <translation>Hôtes:</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <source>Threat:</source>
        <translation>Menace:</translation>
    </message>
    <message>
        <source>Task:</source>
        <translation>Tâche:</translation>
    </message>
    <message>
        <source>Result:</source>
        <translation>Résultat:</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>All</source>
        <translatorcomment>(ou tous?)</translatorcomment>
        <translation>Tout</translation>
    </message>
</context>
<context>
    <name>dlg_detailsOverride</name>
    <message>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>NVT Name:</source>
        <translation>Nom NVT:</translation>
    </message>
    <message>
        <source>NVT OID:</source>
        <translation>NVT OID:</translation>
    </message>
    <message>
        <source>Created:</source>
        <translation>Créé le:</translation>
    </message>
    <message>
        <source>Last Modified:</source>
        <translation>Dernière Modification:</translation>
    </message>
    <message>
        <source>Application</source>
        <translation>Application</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <source>Threat:</source>
        <translation>Menace:</translation>
    </message>
    <message>
        <source>New Threat:</source>
        <translation>Nouvelle Menace:</translation>
    </message>
    <message>
        <source>Hosts:</source>
        <translation>Hôtes:</translation>
    </message>
    <message>
        <source>Task:</source>
        <translation>Tâche:</translation>
    </message>
    <message>
        <source>Result:</source>
        <translation>Résultat:</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Any</source>
        <translation>N&apos;importe lequel</translation>
    </message>
</context>
<context>
    <name>dlg_license</name>
    <message>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;		    GNU GENERAL PUBLIC LICENSE&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;		       Version 2, June 1991&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; Copyright (C) 1989, 1991 Free Software Foundation, Inc.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;                       51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; Everyone is permitted to copy and distribute verbatim copies&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; of this license document, but changing it is not allowed.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;			    Preamble&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  The licenses for most software are designed to take away your&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;freedom to share and change it.  By contrast, the GNU General Public&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;License is intended to guarantee your freedom to share and change free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;software--to make sure the software is free for all its users.  This&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;General Public License applies to most of the Free Software&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Foundation&apos;s software and to any other program whose authors commit to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;using it.  (Some other Free Software Foundation software is covered by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the GNU Library General Public License instead.)  You can apply it to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;your programs, too.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  When we speak of free software, we are referring to freedom, not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;price.  Our General Public Licenses are designed to make sure that you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;have the freedom to distribute copies of free software (and charge for&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this service if you wish), that you receive source code or can get it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;if you want it, that you can change the software or use pieces of it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;in new free programs; and that you know you can do these things.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  To protect your rights, we need to make restrictions that forbid&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;anyone to deny you these rights or to ask you to surrender the rights.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;These restrictions translate to certain responsibilities for you if you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute copies of the software, or if you modify it.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  For example, if you distribute copies of such a program, whether&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;gratis or for a fee, you must give the recipients all the rights that&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;you have.  You must make sure that they, too, receive or can get the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;source code.  And you must show them these terms so they know their&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;rights.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  We protect your rights with two steps: (1) copyright the software, and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;(2) offer you this license which gives you legal permission to copy,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute and/or modify the software.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  Also, for each author&apos;s protection and ours, we want to make certain&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;that everyone understands that there is no warranty for this free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;software.  If the software is modified by someone else and passed on, we&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;want its recipients to know that what they have is not the original, so&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;that any problems introduced by others will not reflect on the original&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;authors&apos; reputations.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  Finally, any free program is threatened constantly by software&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;patents.  We wish to avoid the danger that redistributors of a free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;program will individually obtain patent licenses, in effect making the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;program proprietary.  To prevent this, we have made it clear that any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;patent must be licensed for everyone&apos;s free use or not licensed at all.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  The precise terms and conditions for copying, distribution and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;modification follow.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;		    GNU GENERAL PUBLIC LICENSE&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  0. This License applies to any program or other work which contains&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;a notice placed by the copyright holder saying it may be distributed&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;under the terms of this General Public License.  The &quot;Program&quot;, below,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;refers to any such program or work, and a &quot;work based on the Program&quot;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;means either the Program or any derivative work under copyright law:&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;that is to say, a work containing the Program or a portion of it,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;either verbatim or with modifications and/or translated into another&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;language.  (Hereinafter, translation is included without limitation in&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the term &quot;modification&quot;.)  Each licensee is addressed as &quot;you&quot;.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Activities other than copying, distribution and modification are not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;covered by this License; they are outside its scope.  The act of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;running the Program is not restricted, and the output from the Program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;is covered only if its contents constitute a work based on the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Program (independent of having been made by running the Program).&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Whether that is true depends on what the Program does.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  1. You may copy and distribute verbatim copies of the Program&apos;s&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;source code as you receive it, in any medium, provided that you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;conspicuously and appropriately publish on each copy an appropriate&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;copyright notice and disclaimer of warranty; keep intact all the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;notices that refer to this License and to the absence of any warranty;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;and give any other recipients of the Program a copy of this License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;along with the Program.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You may charge a fee for the physical act of transferring a copy, and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;you may at your option offer warranty protection in exchange for a fee.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  2. You may modify your copy or copies of the Program or any portion&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;of it, thus forming a work based on the Program, and copy and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute such modifications or work under the terms of Section 1&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;above, provided that you also meet all of these conditions:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    a) You must cause the modified files to carry prominent notices&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    stating that you changed the files and the date of any change.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    b) You must cause any work that you distribute or publish, that in&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    whole or in part contains or is derived from the Program or any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    part thereof, to be licensed as a whole at no charge to all third&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    parties under the terms of this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    c) If the modified program normally reads commands interactively&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    when run, you must cause it, when started running for such&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    interactive use in the most ordinary way, to print or display an&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    announcement including an appropriate copyright notice and a&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    notice that there is no warranty (or else, saying that you provide&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    a warranty) and that users may redistribute the program under&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    these conditions, and telling the user how to view a copy of this&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    License.  (Exception: if the Program itself is interactive but&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    does not normally print such an announcement, your work based on&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    the Program is not required to print an announcement.)&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;These requirements apply to the modified work as a whole.  If&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;identifiable sections of that work are not derived from the Program,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;and can be reasonably considered independent and separate works in&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;themselves, then this License, and its terms, do not apply to those&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;sections when you distribute them as separate works.  But when you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute the same sections as part of a whole which is a work based&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;on the Program, the distribution of the whole must be on the terms of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this License, whose permissions for other licensees extend to the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;entire whole, and thus to each and every part regardless of who wrote it.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Thus, it is not the intent of this section to claim rights or contest&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;your rights to work written entirely by you; rather, the intent is to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;exercise the right to control the distribution of derivative or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;collective works based on the Program.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;In addition, mere aggregation of another work not based on the Program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;with the Program (or with a work based on the Program) on a volume of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;a storage or distribution medium does not bring the other work under&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the scope of this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  3. You may copy and distribute the Program (or a work based on it,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;under Section 2) in object code or executable form under the terms of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sections 1 and 2 above provided that you also do one of the following:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    a) Accompany it with the complete corresponding machine-readable&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    source code, which must be distributed under the terms of Sections&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    1 and 2 above on a medium customarily used for software interchange; or,&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    b) Accompany it with a written offer, valid for at least three&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    years, to give any third party, for a charge no more than your&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    cost of physically performing source distribution, a complete&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    machine-readable copy of the corresponding source code, to be&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    distributed under the terms of Sections 1 and 2 above on a medium&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    customarily used for software interchange; or,&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    c) Accompany it with the information you received as to the offer&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    to distribute corresponding source code.  (This alternative is&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    allowed only for noncommercial distribution and only if you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    received the program in object code or executable form with such&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    an offer, in accord with Subsection b above.)&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The source code for a work means the preferred form of the work for&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;making modifications to it.  For an executable work, complete source&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;code means all the source code for all modules it contains, plus any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;associated interface definition files, plus the scripts used to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;control compilation and installation of the executable.  However, as a&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;special exception, the source code distributed need not include&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;anything that is normally distributed (in either source or binary&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;form) with the major components (compiler, kernel, and so on) of the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;operating system on which the executable runs, unless that component&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;itself accompanies the executable.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If distribution of executable or object code is made by offering&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;access to copy from a designated place, then offering equivalent&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;access to copy the source code from the same place counts as&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribution of the source code, even though third parties are not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;compelled to copy the source along with the object code.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  4. You may not copy, modify, sublicense, or distribute the Program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;except as expressly provided under this License.  Any attempt&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;otherwise to copy, modify, sublicense or distribute the Program is&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;void, and will automatically terminate your rights under this License.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;However, parties who have received copies, or rights, from you under&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this License will not have their licenses terminated so long as such&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;parties remain in full compliance.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  5. You are not required to accept this License, since you have not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;signed it.  However, nothing else grants you permission to modify or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute the Program or its derivative works.  These actions are&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;prohibited by law if you do not accept this License.  Therefore, by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;modifying or distributing the Program (or any work based on the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Program), you indicate your acceptance of this License to do so, and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;all its terms and conditions for copying, distributing or modifying&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the Program or works based on it.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  6. Each time you redistribute the Program (or any work based on the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Program), the recipient automatically receives a license from the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;original licensor to copy, distribute or modify the Program subject to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;these terms and conditions.  You may not impose any further&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;restrictions on the recipients&apos; exercise of the rights granted herein.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You are not responsible for enforcing compliance by third parties to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  7. If, as a consequence of a court judgment or allegation of patent&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;infringement or for any other reason (not limited to patent issues),&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;conditions are imposed on you (whether by court order, agreement or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;otherwise) that contradict the conditions of this License, they do not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;excuse you from the conditions of this License.  If you cannot&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute so as to satisfy simultaneously your obligations under this&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;License and any other pertinent obligations, then as a consequence you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;may not distribute the Program at all.  For example, if a patent&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;license would not permit royalty-free redistribution of the Program by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;all those who receive copies directly or indirectly through you, then&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the only way you could satisfy both it and this License would be to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;refrain entirely from distribution of the Program.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If any portion of this section is held invalid or unenforceable under&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;any particular circumstance, the balance of the section is intended to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;apply and the section as a whole is intended to apply in other&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;circumstances.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;It is not the purpose of this section to induce you to infringe any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;patents or other property right claims or to contest validity of any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;such claims; this section has the sole purpose of protecting the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;integrity of the free software distribution system, which is&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;implemented by public license practices.  Many people have made&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;generous contributions to the wide range of software distributed&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;through that system in reliance on consistent application of that&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;system; it is up to the author/donor to decide if he or she is willing&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;to distribute software through any other system and a licensee cannot&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;impose that choice.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This section is intended to make thoroughly clear what is believed to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;be a consequence of the rest of this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  8. If the distribution and/or use of the Program is restricted in&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;certain countries either by patents or by copyrighted interfaces, the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;original copyright holder who places the Program under this License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;may add an explicit geographical distribution limitation excluding&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;those countries, so that distribution is permitted only in or among&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;countries not thus excluded.  In such case, this License incorporates&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the limitation as if written in the body of this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  9. The Free Software Foundation may publish revised and/or new versions&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;of the General Public License from time to time.  Such new versions will&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;be similar in spirit to the present version, but may differ in detail to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;address new problems or concerns.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Each version is given a distinguishing version number.  If the Program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;specifies a version number of this License which applies to it and &quot;any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;later version&quot;, you have the option of following the terms and conditions&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;either of that version or of any later version published by the Free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Software Foundation.  If the Program does not specify a version number of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this License, you may choose any version ever published by the Free Software&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Foundation.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  10. If you wish to incorporate parts of the Program into other free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;programs whose distribution conditions are different, write to the author&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;to ask for permission.  For software which is copyrighted by the Free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Software Foundation, write to the Free Software Foundation; we sometimes&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;make exceptions for this.  Our decision will be guided by the two goals&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;of preserving the free status of all derivatives of our free software and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;of promoting the sharing and reuse of software generally.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;			    NO WARRANTY&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;PROVIDE THE PROGRAM &quot;AS IS&quot; WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;REPAIR OR CORRECTION.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;POSSIBILITY OF SUCH DAMAGES.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;		     END OF TERMS AND CONDITIONS&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;	    How to Apply These Terms to Your New Programs&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  If you develop a new program, and you want it to be of the greatest&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;possible use to the public, the best way to achieve this is to make it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;free software which everyone can redistribute and change under these terms.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  To do so, attach the following notices to the program.  It is safest&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;to attach them to the start of each source file to most effectively&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;convey the exclusion of warranty; and each file should have at least&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the &quot;copyright&quot; line and a pointer to where the full notice is found.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    &amp;lt;one line to give the program&apos;s name and a brief idea of what it does.&amp;gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Copyright (C) &amp;lt;year&amp;gt;  &amp;lt;name of author&amp;gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    This program is free software; you can redistribute it and/or modify&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    it under the terms of the GNU General Public License as published by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    the Free Software Foundation; either version 2 of the License, or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    (at your option) any later version.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    This program is distributed in the hope that it will be useful,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    but WITHOUT ANY WARRANTY; without even the implied warranty of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    GNU General Public License for more details.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    You should have received a copy of the GNU General Public License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    along with this program; if not, write to the Free Software&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Also add information on how to contact you by electronic and paper mail.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If the program is interactive, make it output a short notice like this&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;when it starts in an interactive mode:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Gnomovision version 69, Copyright (C) year name of author&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type `show w&apos;.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    This is free software, and you are welcome to redistribute it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    under certain conditions; type `show c&apos; for details.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The hypothetical commands `show w&apos; and `show c&apos; should show the appropriate&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;parts of the General Public License.  Of course, the commands you use may&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;be called something other than `show w&apos; and `show c&apos;; they could even be&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;mouse-clicks or menu items--whatever suits your program.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You should also get your employer (if you work as a programmer) or your&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;school, if any, to sign a &quot;copyright disclaimer&quot; for the program, if&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;necessary.  Here is a sample; alter the names:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  Yoyodyne, Inc., hereby disclaims all copyright interest in the program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  `Gnomovision&apos; (which makes passes at compilers) written by James Hacker.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  &amp;lt;signature of Ty Coon&amp;gt;, 1 April 1989&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  Ty Coon, President of Vice&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This General Public License does not permit incorporating your program into&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;proprietary programs.  If your program is a subroutine library, you may&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;consider it more useful to permit linking proprietary applications with the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;library.  If this is what you want to do, use the GNU Library General&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Public License instead of this License.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>dlg_login</name>
    <message>
        <source>Login</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Serveraddress</source>
        <translation>Adresse serveur</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>9390</source>
        <translation>9390</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <source>Please enter address and
user account for your scan
engine.

If you select one of the
profiles, you only need to
enter the password.

Before you press the login
button you may store the
access profile.

Note, that the scan engine
must have OMP support
enabled for the given port
for a successful connection.</source>
        <translation>Merci d&apos;entrer l&apos;adresse et
le compte utilisateur de
votre moteur de scan.

Si vous sélectionnez un des
profils vous n&apos;avez besoin que
d&apos;entrer le mot de passe.

Avant d&apos;appuyer sur le bouton
de connexion vous pouvez
enregistrer le profil.

Veillez à ce que le moteur de
scan ait le support d&apos;OMP activé
pour le port spécifié afin que la
connexion réussisse.</translation>
    </message>
    <message>
        <source>Please enter: 
</source>
        <translation>Entrez: 
</translation>
    </message>
    <message>
        <source>   - Serveraddress                 
</source>
        <translation>   - Adresse serveur                 
</translation>
    </message>
    <message>
        <source>   - Serverport                    
</source>
        <translation>   - Port serveur                    
</translation>
    </message>
    <message>
        <source>   - Username                      
</source>
        <translation>   - Nom d&apos;utilisateur                      
</translation>
    </message>
    <message>
        <source>   - Password                      
</source>
        <translation>   - Mot de passe                      
</translation>
    </message>
    <message>
        <source>Connection failed...</source>
        <translation>La connexion a échoué...</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
</context>
<context>
    <name>dlg_modify_config</name>
    <message>
        <source>Modify Scan Config</source>
        <translation>Modifier la configuration du scan</translation>
    </message>
    <message>
        <source>Select all NVTs:</source>
        <translation>Sélectionner tous les NVTs:</translation>
    </message>
    <message>
        <source>Trend:</source>
        <translation>Tendance:</translation>
    </message>
    <message>
        <source>Add Family:</source>
        <translation>Ajouter une famille:</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>dlg_modify_credential</name>
    <message>
        <source>Modify Credential</source>
        <translation>Modifier l&apos;identifiant</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Comment:</source>
        <translation>Commentaire:</translation>
    </message>
    <message>
        <source>Login:</source>
        <translation>Login:</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Mot de passe:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
</context>
<context>
    <name>dlg_modify_family</name>
    <message>
        <source>Modify Family</source>
        <translation>Modifier une famille</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Family:</source>
        <translation>Famille:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Selected</source>
        <translation>Sélectionné</translation>
    </message>
</context>
<context>
    <name>dlg_modify_note</name>
    <message>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <source>NVT OID:</source>
        <translation>NVT OID:</translation>
    </message>
    <message>
        <source>NVT Name:</source>
        <translation>Nom NVT:</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Hosts:</source>
        <translation>Hôtes:</translation>
    </message>
    <message>
        <source>Any</source>
        <translation>N&apos;importe lequel</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <source>Threat:</source>
        <translation>Menace:</translation>
    </message>
    <message>
        <source>Task:</source>
        <translation>Tpache:</translation>
    </message>
    <message>
        <source>Result:</source>
        <translation>Résultat:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>dlg_modify_nvt</name>
    <message>
        <source>Modify NVT Preferences</source>
        <translation>Modifier les préférences NVT</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Preference:</source>
        <translation>Préférence:</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Browse...</source>
        <translation>Parcourir...</translation>
    </message>
    <message>
        <source>Open File ...</source>
        <translation>Ouvrir un fichier ...</translation>
    </message>
    <message>
        <source>All files *.*</source>
        <translation>Tous les fichiers *.*</translation>
    </message>
</context>
<context>
    <name>dlg_modify_override</name>
    <message>
        <source>Modify Override</source>
        <translation>Modifier le substitut</translation>
    </message>
    <message>
        <source>Any</source>
        <translation>N&apos;importe lequel</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <source>Task:</source>
        <translation>Tâche:</translation>
    </message>
    <message>
        <source>New Threat:</source>
        <translation>Nouvelle Menace:</translation>
    </message>
    <message>
        <source>Threat:</source>
        <translation>Menace:</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <source>Hosts:</source>
        <translation>Hôtes:</translation>
    </message>
    <message>
        <source>NVT OID:</source>
        <translation>NVT OID:</translation>
    </message>
    <message>
        <source>NVT Name:</source>
        <translation>Nom NVT:</translation>
    </message>
</context>
<context>
    <name>dlg_modify_scan_pref</name>
    <message>
        <source>Modify Scanner Preferences</source>
        <translation>Modifier les préférences du scanner</translation>
    </message>
    <message>
        <source>Preference:</source>
        <translation>Préférence:</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
</context>
<context>
    <name>dlg_new_agent</name>
    <message>
        <source>Open File ...</source>
        <translation>Ouvrir un fichier ...</translation>
    </message>
    <message>
        <source>All files *.*</source>
        <translation>Tous les fichiers *.*</translation>
    </message>
</context>
<context>
    <name>dlg_new_escalator</name>
    <message>
        <source>Data Error.</source>
        <translation>Erreur de données.</translation>
    </message>
    <message>
        <source>Error while parsing Email addresses!
Please check!</source>
        <translation>Erreur lors de l&apos;analyse de l&apos;adresse Email!
Merci de la vérifier!</translation>
    </message>
</context>
<context>
    <name>dlg_new_note</name>
    <message>
        <source>New Note</source>
        <translation>Nouvelle note</translation>
    </message>
    <message>
        <source>Any</source>
        <translation>N&apos;importe lequel</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <source>Hosts:</source>
        <translation>Hôtes:</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <source>Threat:</source>
        <translation>Menace:</translation>
    </message>
    <message>
        <source>Task:</source>
        <translation>Tâche:</translation>
    </message>
    <message>
        <source>Result:</source>
        <translation>Résultat:</translation>
    </message>
    <message>
        <source>Text:</source>
        <translation>Texte:</translation>
    </message>
</context>
<context>
    <name>dlg_new_override</name>
    <message>
        <source>New Override</source>
        <translation>Nouveau substitut</translation>
    </message>
    <message>
        <source>Hosts:</source>
        <translation>Hôte:</translation>
    </message>
    <message>
        <source>Any</source>
        <translation>N&apos;importe lequel</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <source>Threat:</source>
        <translation>Menace:</translation>
    </message>
    <message>
        <source>Task:</source>
        <translation>Tâche:</translation>
    </message>
    <message>
        <source>Result:</source>
        <translation>Résultat:</translation>
    </message>
    <message>
        <source>Text:</source>
        <translation>Texte:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <source>New Threat:</source>
        <translation>Nouvelle Menace:</translation>
    </message>
</context>
<context>
    <name>dlg_new_slave</name>
    <message>
        <source>New Slave</source>
        <translation>Nouvel esclave</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Comment (optional):</source>
        <translation>Commentaire (optionnel):</translation>
    </message>
    <message>
        <source>Host:</source>
        <translation>Hôte:</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <source>Login:</source>
        <translation>Login:</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Mot de passe:</translation>
    </message>
</context>
<context>
    <name>dlg_start_gsa</name>
    <message>
        <source>Start Greenbone Security Assistant</source>
        <translation>Démarrer Greenbone Security Assistant</translation>
    </message>
    <message>
        <source>Greenbone Security Assistant (GSA) is an alternative, web-based client.
It offers extended features for management and administration.
</source>
        <translation>Greenbone Security Assistant (GSA) est un autre client, orienté web.
Il offre des fonctionnalités étendues pour la gestion et l&apos;administration.
</translation>
    </message>
    <message>
        <source>GSA service typically runs SSL-secured on the same server you are currently
connected to, either at port 443 (default for HTTPS) or at port 9392.
But you can also configure another URL which will be stored for this desktop
profile.</source>
        <translation>Le service GSA tourne généralement en SSL sécurisé sur le même serveur que
celui sur lequel vous êtes connectés, soit sur le port 443 (défaut pour HTTPS) soit
sur le port 9392. Cependant vous pouvez également configurer une autre URL qui
sera enregistrée pour ce profil.</translation>
    </message>
    <message>
        <source>Open GSA in default web browser</source>
        <translation>Ouvrir GSA dans le navigateur web par défaut</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
</context>
<context>
    <name>dock_details_config</name>
    <message>
        <source>Config</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <source>Comment:</source>
        <translation>Commentaire:</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>NVT Families</source>
        <translation>Familles NVT</translation>
    </message>
    <message>
        <source>Scanner Preferences</source>
        <translation>Préférences du scanner</translation>
    </message>
    <message>
        <source>NVT Preferences</source>
        <translation>Préférences NVT</translation>
    </message>
    <message>
        <source>Tasks</source>
        <translation>Tâche</translation>
    </message>
    <message>
        <source>Modify Family</source>
        <translation>Modifier une famille</translation>
    </message>
    <message>
        <source>Modify Scan Preferences</source>
        <translation>Modifier les préférences de scan</translation>
    </message>
    <message>
        <source>Details Family</source>
        <translation>Détails famille</translation>
    </message>
    <message>
        <source>Modify NVT Preferences</source>
        <translation>Modifier les préférences NVT</translation>
    </message>
</context>
<context>
    <name>dock_details_credential</name>
    <message>
        <source>Credential</source>
        <translation>Identifiant</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Comment:</source>
        <translation>Commentaire:</translation>
    </message>
    <message>
        <source>Login:</source>
        <translation>Login:</translation>
    </message>
    <message>
        <source>Targets using this Credential</source>
        <translation>Cibles utilisant ces identifiants</translation>
    </message>
</context>
<context>
    <name>dock_details_escalator</name>
    <message>
        <source>Escalator</source>
        <translation>Avertisseur</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Résumé</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Comment:</source>
        <translation>Commentaire:</translation>
    </message>
    <message>
        <source>Condition:</source>
        <translation>Condition:</translation>
    </message>
    <message>
        <source>Event:</source>
        <translation>Evènement:</translation>
    </message>
    <message>
        <source>Method:</source>
        <translation>Méthode:</translation>
    </message>
    <message>
        <source>Tasks using this Escalator</source>
        <translation>Tâches utilisant cet avertisseur</translation>
    </message>
    <message>
        <source> (to </source>
        <translation> (à </translation>
    </message>
    <message>
        <source>To address: %1</source>
        <translation>Adresse de destination: %1</translation>
    </message>
    <message>
        <source>From address: %1</source>
        <translation>Adresse source: %1</translation>
    </message>
    <message>
        <source>Format: Summary (can include vulnerability details)</source>
        <translation>Format: Résumé (peut inclure le détail de vulnérabilités)</translation>
    </message>
    <message>
        <source>Format: Simple Notice</source>
        <translation>Format: Simple note</translation>
    </message>
</context>
<context>
    <name>dock_details_family</name>
    <message>
        <source>Details NVT Family</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Config:</source>
        <translation>Configuration:</translation>
    </message>
    <message>
        <source>Family:</source>
        <translation>Famille:</translation>
    </message>
    <message>
        <source>Network Vulnerability Tests</source>
        <translation>Tests de vulnérabilités réseau</translation>
    </message>
    <message>
        <source>Details NVT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Modify Family</source>
        <translation>Modifier une famille</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>OID</source>
        <translation>OID</translation>
    </message>
    <message>
        <source>Risk</source>
        <translation>Risque</translation>
    </message>
    <message>
        <source>CVSS</source>
        <translation>CVSS</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Délai d&apos;expiration</translation>
    </message>
    <message>
        <source>Prefs</source>
        <translation>Préférences</translation>
    </message>
</context>
<context>
    <name>dock_details_note</name>
    <message>
        <source>Details Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>Result:</source>
        <translation>Résultat:</translation>
    </message>
    <message>
        <source>Threat:</source>
        <translation>Menace:</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <source>Application</source>
        <translation>Application</translation>
    </message>
    <message>
        <source>Task:</source>
        <translation>Tâche:</translation>
    </message>
    <message>
        <source>NVT Name:</source>
        <translation>Nom NVT:</translation>
    </message>
    <message>
        <source>NVT OID:</source>
        <translation>NVT OID:</translation>
    </message>
    <message>
        <source>Created:</source>
        <translation>Créé le:</translation>
    </message>
    <message>
        <source>Last Modified:</source>
        <translation>Dernière Modification:</translation>
    </message>
    <message>
        <source>Hosts:</source>
        <translation>Hôtes:</translation>
    </message>
</context>
<context>
    <name>dock_details_nvt</name>
    <message>
        <source>NVT</source>
        <translation>NVT</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Résumé</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Summary:</source>
        <translation>Résumé:</translation>
    </message>
    <message>
        <source>Family:</source>
        <translation>Famille:</translation>
    </message>
    <message>
        <source>OID:</source>
        <translation>OID:</translation>
    </message>
    <message>
        <source>CVE:</source>
        <translation>CVE:</translation>
    </message>
    <message>
        <source>Bugtraq ID:</source>
        <translation>Identifiant bugtraq:</translation>
    </message>
    <message>
        <source>Other References:</source>
        <translation>Autres références:</translation>
    </message>
    <message>
        <source>Tags:</source>
        <translation type="unfinished">Marqueurs:</translation>
    </message>
    <message>
        <source>CVSS Base:</source>
        <translation>Base CVSS:</translation>
    </message>
    <message>
        <source>Risk factor</source>
        <translation>Facteur de risque</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>Modify NVT</source>
        <translation>Modifier NVT</translation>
    </message>
</context>
<context>
    <name>dock_details_override</name>
    <message>
        <source>Details Override</source>
        <translation>Nouveau substitut</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>Task:</source>
        <translation>Tâche:</translation>
    </message>
    <message>
        <source>Hosts:</source>
        <translation>Hôtes:</translation>
    </message>
    <message>
        <source>NVT Name:</source>
        <translation>Nom NVT:</translation>
    </message>
    <message>
        <source>NVT OID:</source>
        <translation>NVT OID:</translation>
    </message>
    <message>
        <source>Created:</source>
        <translation>Créé le:</translation>
    </message>
    <message>
        <source>Last Modified:</source>
        <translation>Dernière Modification:</translation>
    </message>
    <message>
        <source>Application</source>
        <translation>Application</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <source>Threat:</source>
        <translation>Menace:</translation>
    </message>
    <message>
        <source>New Threat:</source>
        <translation>Nouvelle Menace:</translation>
    </message>
    <message>
        <source>Result:</source>
        <translation>Résultat:</translation>
    </message>
</context>
<context>
    <name>dock_details_schedule</name>
    <message>
        <source>Details Schedule</source>
        <translation>Détails plannification</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Résumé</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Comment:</source>
        <translation>Commentaire:</translation>
    </message>
    <message>
        <source>First Time:</source>
        <translation>Première fois:</translation>
    </message>
    <message>
        <source>Next Time:</source>
        <translation>Prochaine fois:</translation>
    </message>
    <message>
        <source>Period:</source>
        <translation>Période:</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation>Durée:</translation>
    </message>
    <message>
        <source>Tasks using this Schedule</source>
        <translation>Tâches utilisant cette plannification</translation>
    </message>
</context>
<context>
    <name>dock_details_slave</name>
    <message>
        <source>Details Slave</source>
        <translation>Détails esclave</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Résumé</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Comment:</source>
        <translation>Commentaire:</translation>
    </message>
    <message>
        <source>Host:</source>
        <translation>Hôte:</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <source>Login:</source>
        <translation>Login:</translation>
    </message>
    <message>
        <source>Tasks using this Slave</source>
        <translation>Tâches utilisant cet esclave</translation>
    </message>
</context>
<context>
    <name>dock_details_target</name>
    <message>
        <source>Details Target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Résumé</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Comment:</source>
        <translation>Commentaire:</translation>
    </message>
    <message>
        <source>Hosts:</source>
        <translation>Hôtes:</translation>
    </message>
    <message>
        <source>Maximum number of Hosts:</source>
        <translation>Nombre maximum d&apos;hôtes:</translation>
    </message>
    <message>
        <source>SSH Credential:</source>
        <translation>Identifiant SSH:</translation>
    </message>
    <message>
        <source>SMB Credential:</source>
        <translation>Identifiant SMB:</translation>
    </message>
    <message>
        <source>Tasks using this Target</source>
        <translation>Tâches utilisant cette cible</translation>
    </message>
</context>
<context>
    <name>dock_details_task</name>
    <message>
        <source>Task Details</source>
        <translation>Détails de la tâche</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Résumé</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>Comment:</source>
        <translation>Commentaire:</translation>
    </message>
    <message>
        <source>Config:</source>
        <translation>Config:</translation>
    </message>
    <message>
        <source>Escalator:</source>
        <translation>Avertisseur:
</translation>
    </message>
    <message>
        <source>Schedule:</source>
        <translation>Plannification:</translation>
    </message>
    <message>
        <source>Target:</source>
        <translation>Cible:</translation>
    </message>
    <message>
        <source>Status:</source>
        <translation>Statut:</translation>
    </message>
    <message>
        <source>Reports:</source>
        <translation>Rapports:</translation>
    </message>
    <message>
        <source>Reports</source>
        <translation>Rapports</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>Overrides</source>
        <translation>Substituts</translation>
    </message>
    <message>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <source>Status Error</source>
        <translation type="unfinished">Erreur de statut</translation>
    </message>
    <message>
        <source>No report selected!</source>
        <translation>Aucun rapport sélectionné!</translation>
    </message>
    <message>
        <source>Report</source>
        <translation>Rapport</translation>
    </message>
    <message>
        <source>Threat</source>
        <translation>Menace</translation>
    </message>
    <message>
        <source>High</source>
        <translation>Haut</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>Moyen</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>Faible</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <source>false Pos.</source>
        <translation>faux pos.</translation>
    </message>
    <message>
        <source>Slave:</source>
        <translation>Esclave:</translation>
    </message>
</context>
<context>
    <name>dock_logging</name>
    <message>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <source>clear</source>
        <translation>effacer</translation>
    </message>
    <message>
        <source>Log level:</source>
        <translation>Niveau de log:</translation>
    </message>
</context>
<context>
    <name>dock_performance</name>
    <message>
        <source>Performance</source>
        <translation>Performance</translation>
    </message>
    <message>
        <source>Save image as</source>
        <translation>Enregistrer l&apos;image sous</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation>Taille:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>Largeur:</translation>
    </message>
    <message>
        <source>refresh</source>
        <translation>rafraîchir</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>Hauteur:</translation>
    </message>
    <message>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <source>hour(s)</source>
        <translation>heure(s)</translation>
    </message>
    <message>
        <source>day(s)</source>
        <translation>jour(s)</translation>
    </message>
    <message>
        <source>month(s)</source>
        <translation>mois</translation>
    </message>
    <message>
        <source>No system report available.</source>
        <translation>Pas de rapport système disponible.</translation>
    </message>
    <message>
        <source>Save File ...</source>
        <translation>Enregistrer le fichier ...</translation>
    </message>
    <message>
        <source>*.png</source>
        <translation>*.png</translation>
    </message>
</context>
<context>
    <name>dock_reports</name>
    <message>
        <source>Report</source>
        <translation>Rapport</translation>
    </message>
    <message>
        <source>CVSS &gt;=</source>
        <translation>CVSS &gt;=</translation>
    </message>
    <message>
        <source>Apply Overrides</source>
        <translation>Appliquer les substituts</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <source>Results per page:</source>
        <translation>Résultats par page:</translation>
    </message>
    <message>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>&gt;&gt;</source>
        <translation>&gt;&gt;</translation>
    </message>
    <message>
        <source>&lt;&lt;</source>
        <translation>&lt;&lt;</translation>
    </message>
    <message>
        <source>as</source>
        <translation>en</translation>
    </message>
    <message>
        <source>Results %1 - %2 of %3</source>
        <translation>Resultats %1 - %2 sur %3</translation>
    </message>
    <message>
        <source>Filtered results %1-%2</source>
        <translation>Résultats filtrés %1-%2</translation>
    </message>
    <message>
        <source>All filtered results</source>
        <translation>Tous les résultats filtrés</translation>
    </message>
    <message>
        <source>Full report</source>
        <translation>Rapport complet</translation>
    </message>
    <message>
        <source>Unable to open file</source>
        <translation>Impossible d&apos;ouvrir le fichier</translation>
    </message>
    <message>
        <source>Report </source>
        <translation>Rapport </translation>
    </message>
    <message>
        <source>%1 of %2</source>
        <translation>%1 sur %2</translation>
    </message>
    <message>
        <source>port ascending</source>
        <translation>port croissant</translation>
    </message>
    <message>
        <source>port descending</source>
        <translation>port décroissant</translation>
    </message>
    <message>
        <source>threat ascending</source>
        <translation>menace croissante</translation>
    </message>
    <message>
        <source>threat descending</source>
        <translation>menace décroissante</translation>
    </message>
    <message>
        <source>Save File ...</source>
        <translation>Enregistrer le fichier ...</translation>
    </message>
    <message>
        <source>%1 files *.%1</source>
        <translation>%1 fichiers *.%1</translation>
    </message>
    <message>
        <source>File Error</source>
        <translation>Erreur de fichier</translation>
    </message>
    <message>
        <source>Could not open file!</source>
        <translation>Impossible d&apos;ouvrir le fichier!</translation>
    </message>
</context>
<context>
    <name>dock_table</name>
    <message>
        <source>  </source>
        <translation>  </translation>
    </message>
    <message>
        <source>Name: </source>
        <translation>Nom:</translation>
    </message>
    <message>
        <source>
Comment: </source>
        <translation>Commentaire:</translation>
    </message>
</context>
<context>
    <name>gsd_config</name>
    <message>
        <source>DOM Error</source>
        <translation>Erreur DOM</translation>
    </message>
    <message>
        <source>Error in line %1, Column %2: %3</source>
        <translation>Erreur ligne %1, Colonne %2: %3</translation>
    </message>
    <message>
        <source>Not a valid openvas-config file!</source>
        <translation>Ceci n&apos;est pas un fichier de configuration d&apos;openvas valide!</translation>
    </message>
    <message>
        <source>ProfileManager</source>
        <translation>Gestionnaire de profils</translation>
    </message>
    <message>
        <source>Profile allready exists!</source>
        <translation>Le profil existe déjà!</translation>
    </message>
</context>
<context>
    <name>gsd_control</name>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Statut</translation>
    </message>
    <message>
        <source>Reports</source>
        <translation>Rapports</translation>
    </message>
    <message>
        <source>First</source>
        <translation>Premier</translation>
    </message>
    <message>
        <source>Last</source>
        <translation>Dernier</translation>
    </message>
    <message>
        <source>Threat</source>
        <translation>Menace</translation>
    </message>
    <message>
        <source>Trend</source>
        <translation>Tendance</translation>
    </message>
    <message>
        <source>Hosts</source>
        <translation>Hôtes</translation>
    </message>
    <message>
        <source>IPs</source>
        <translation>IPs</translation>
    </message>
    <message>
        <source>SSH Credential</source>
        <translation>Identifiant SSH</translation>
    </message>
    <message>
        <source>SMB Credential</source>
        <translation>Identifiant SMB</translation>
    </message>
    <message>
        <source>Total Families</source>
        <translation>Total familles</translation>
    </message>
    <message>
        <source>Total NVTs</source>
        <translation>Total NVTs</translation>
    </message>
    <message>
        <source>First Run</source>
        <translation>Premier lancement</translation>
    </message>
    <message>
        <source>Next Run</source>
        <translation>Prochain lancement</translation>
    </message>
    <message>
        <source>Period</source>
        <translation>Période</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Durée</translation>
    </message>
    <message>
        <source>Event</source>
        <translation>Evènement</translation>
    </message>
    <message>
        <source>Condition</source>
        <translation>Condition</translation>
    </message>
    <message>
        <source>Method</source>
        <translation>Méthode</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <source>Trust</source>
        <translation>Confiance</translation>
    </message>
    <message>
        <source>NVT</source>
        <translation>NVT</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>From</source>
        <translation>Depuis</translation>
    </message>
    <message>
        <source>To</source>
        <translation>à</translation>
    </message>
    <message>
        <source>Delete?</source>
        <translation>Supprimer?</translation>
    </message>
    <message>
        <source>Do you really want delete the Task :&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Souhaitez-vous vraiment supprimer la tâche :&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Do you really want delete the Config:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Souhaitez-vous vraiment supprimer la configuration:&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Do you really want delete the Target:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Souhaitez-vous vraiment supprimer la cible :&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Do you really want delete the Schedule:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Souhaitez-vous vraiment supprimer la plannification :&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Do you really want delete the Escalator:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Souhaitez-vous vraiment supprimer l&apos;avertisseur :&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Do you really want delete the Credential:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Souhaitez-vous vraiment supprimer l&apos;identifiant :&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Do you really want delete the Agent:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Souhaitez-vous vraiment supprimer l&apos;agent:&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Do you really want delete the Note:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Souhaitez-vous vraiment supprimer la note :&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Do you really want delete the Override:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Souhaitez-vous vraiment supprimer le substitut :&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Report format</source>
        <translation>Format de rapport</translation>
    </message>
    <message>
        <source>HTML or XML format not active!</source>
        <translation>Formats HTML ou XML inactifs!</translation>
    </message>
    <message>
        <source>Do you really want delete this Report?</source>
        <translation>Souhaitez-vous réèllement supprimer ce rapport?</translation>
    </message>
    <message>
        <source>Report format not active!</source>
        <translation>Format de rapport inactif!</translation>
    </message>
    <message>
        <source>Login Error</source>
        <translation>Erreur d&apos;authentification</translation>
    </message>
    <message>
        <source>Please login first!</source>
        <translation>Merci de vous authentifier au préalable!</translation>
    </message>
    <message>
        <source>Server closed connection!</source>
        <translation>Le serveur a fermé la connexion!</translation>
    </message>
    <message>
        <source>Authentication failed!</source>
        <translation>Echec d&apos;authentification!</translation>
    </message>
    <message>
        <source>Failed to get server address!</source>
        <translation>Echec de la récupération de l&apos;adresse du serveur!</translation>
    </message>
    <message>
        <source>Internal Error!</source>
        <translation>Erreur interne!</translation>
    </message>
    <message>
        <source>Wrong OMP Version</source>
        <translation>Mauvaise version d&apos;OMP</translation>
    </message>
    <message>
        <source>You are connected to an OMP 1.0 service!
Since version 1.1.0 GSD supports OMP 2.0 only.
Use GSD Version 1.0 instead.</source>
        <translation>Vous êtes connecté à un service OMP 1.0!
Depuis la version 1.1.0 GSD ne supporte que OMP 2.0.
Utilisez GSD version 1.0 à la place.</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Extension</source>
        <translation>Extension</translation>
    </message>
    <message>
        <source>Content Type</source>
        <translation>Type de contenu</translation>
    </message>
    <message>
        <source>Last verified</source>
        <translation>Dernière vérification</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Actif</translation>
    </message>
    <message>
        <source>Do you really want delete the Slave:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Souhaitez-vous vraiment supprimer l&apos;esclave:&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>gsd_mw</name>
    <message>
        <source>Tasks</source>
        <translation>Tâches</translation>
    </message>
    <message>
        <source>Targets</source>
        <translation>Cibles</translation>
    </message>
    <message>
        <source>Schedules</source>
        <translation>Plannifications</translation>
    </message>
    <message>
        <source>Scan Configs</source>
        <translation>Configs de scan</translation>
    </message>
    <message>
        <source>Escalators</source>
        <translation>Avertisseurs</translation>
    </message>
    <message>
        <source>Credentials</source>
        <translation>Identifiants</translation>
    </message>
    <message>
        <source>Agents</source>
        <translation>Agents</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <source>Overrides</source>
        <translation>Substituts</translation>
    </message>
    <message>
        <source>Ctrl+q</source>
        <translation>Ctrl+q</translation>
    </message>
    <message>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <source>Logged in as: &lt;b&gt;%1&lt;/b&gt; at &lt;b&gt;%2:%3&lt;/b&gt;</source>
        <translation>Conncté en tant que: &lt;b&gt;%1&lt;/b&gt; à &lt;b&gt;%2:%3&lt;/b&gt;</translation>
    </message>
    <message>
        <source>Status Error</source>
        <translation>Erreur de statut</translation>
    </message>
    <message>
        <source>No Task selected!</source>
        <translation>Aucune tâche sélectionnée!</translation>
    </message>
    <message>
        <source>No scan config selected!</source>
        <translation>Aucune configuration de scan sélectionnée!</translation>
    </message>
    <message>
        <source>No target selected!</source>
        <translation>Aucune cible sélectionnée!</translation>
    </message>
    <message>
        <source>No schedule selected!</source>
        <translation>Aucune plannification sélectionnée!</translation>
    </message>
    <message>
        <source>No escalator selected!</source>
        <translation>Aucun avertisseur sélectionné!</translation>
    </message>
    <message>
        <source>No credential selected!</source>
        <translation>Aucun identifiant sélectionné!</translation>
    </message>
    <message>
        <source>No agent selected!</source>
        <translation>Aucun agent sélectionné!</translation>
    </message>
    <message>
        <source>No note selected!</source>
        <translation>Aucune note sélectionnée!</translation>
    </message>
    <message>
        <source>No override selected!</source>
        <translation>Aucun substitut sélectionné!</translation>
    </message>
    <message>
        <source>No Scan configuration selected!</source>
        <translation>Aucune configuration de scan sélectionnée!</translation>
    </message>
    <message>
        <source>No task selected!</source>
        <translation>Aucune tâche sélectionnée!</translation>
    </message>
    <message>
        <source>No config selected!</source>
        <translation>Aucune config sélectionnée!</translation>
    </message>
    <message>
        <source>Open File ...</source>
        <translation>Ouvrir un fichier ...</translation>
    </message>
    <message>
        <source>XML files *.xml</source>
        <translation>Fichiers XML *.xml</translation>
    </message>
    <message>
        <source>Save File ...</source>
        <translation>Enregistrer le fichier ...</translation>
    </message>
    <message>
        <source>File Error</source>
        <translation>Erreur de fichier</translation>
    </message>
    <message>
        <source>Could not open file!</source>
        <translation>Impossible d&apos;ouvrir le fichier!</translation>
    </message>
    <message>
        <source>Login Error</source>
        <translation>Erreur d&apos;authentification</translation>
    </message>
    <message>
        <source>Error while connecting to manager!
Please check server address and port!</source>
        <translation>Erreur lors de la connexion au manager!
Vérifiez l&apos;adresse et le port du serveur!</translation>
    </message>
    <message>
        <source>Manager closed connection!</source>
        <translation>Le manager a interrompu la connexion!</translation>
    </message>
    <message>
        <source>Authentication failed!
Please check username and password!</source>
        <translation>Erreur d&apos;authentification!
Vérifiez le nom d&apos;utilisateur et le mot de passe!</translation>
    </message>
    <message>
        <source> sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <source>Language Selection</source>
        <translation>Sélection de la langue</translation>
    </message>
    <message>
        <source>This setting takes effect after restarting the application.</source>
        <translation>Ce paramètre prend effet au redémarrage de l&apos;application.</translation>
    </message>
    <message>
        <source>Slaves</source>
        <translation>Esclaves</translation>
    </message>
    <message>
        <source>Report Formats</source>
        <translation>Formats de rapport</translation>
    </message>
    <message>
        <source>No slave selected!</source>
        <translation>Aucun esclave sélectionné!</translation>
    </message>
    <message>
        <source>Scan Tasks</source>
        <translation>Tâches de scan</translation>
    </message>
    <message>
        <source>Trends</source>
        <translation>Tendances</translation>
    </message>
    <message>
        <source>Task Overview</source>
        <translation>Vue d&apos;ensemble des tâches</translation>
    </message>
    <message>
        <source>Total</source>
        <translation>Total</translation>
    </message>
    <message>
        <source>Running</source>
        <translation>En cours</translation>
    </message>
    <message>
        <source>Progress</source>
        <translation>Progression</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Effectué</translation>
    </message>
    <message>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Resources Overview</source>
        <translation>Vue d&apos;ensemble des ressources</translation>
    </message>
    <message>
        <source>Vulnerabilities</source>
        <translation>Vulnérabilités</translation>
    </message>
    <message>
        <source>Top 5 Tasks</source>
        <translation>Top 5 des tâches</translation>
    </message>
</context>
<context>
    <name>newAgent</name>
    <message>
        <source>New Agent</source>
        <translation>Nouvel agent</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Comment (optional)</source>
        <translation>Commentaire (optionnel)</translation>
    </message>
    <message>
        <source>Installer</source>
        <translation>Installateur</translation>
    </message>
    <message>
        <source>Installer signature</source>
        <translation>Signature de l&apos;installateur</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>&amp;Create</source>
        <translation>&amp;Créer</translation>
    </message>
    <message>
        <source>Brow&amp;se ...</source>
        <translation>&amp;Parcourir ...</translation>
    </message>
    <message>
        <source>Bro&amp;wse ...</source>
        <translation>&amp;Parcourir ...</translation>
    </message>
    <message>
        <source>(optional)</source>
        <translation>(optionnel)</translation>
    </message>
</context>
<context>
    <name>newConfig</name>
    <message>
        <source>New Scan Config</source>
        <translation>Nouvelle configuration de scan</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Comment (optional)</source>
        <translation>Commentaire (optionnel)</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <source>Empty, static and fast</source>
        <translation>Vide, statique et rapide</translation>
    </message>
    <message>
        <source>Base</source>
        <translation>Base</translation>
    </message>
    <message>
        <source>Full and fast</source>
        <translation>Complet et rapide</translation>
    </message>
</context>
<context>
    <name>newCredential</name>
    <message>
        <source>New Credential</source>
        <translation>Nouvel identifiant</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Comment (optional)</source>
        <translation>Commentaire (optionnel)</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <source>Autogenerate credential</source>
        <translation>Autogénérer l&apos;identifiant</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
</context>
<context>
    <name>newEscalator</name>
    <message>
        <source>New Escalator</source>
        <translation>Nouvel avertisseur</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Comment (optional)</source>
        <translation>Commentaire (optionnel)</translation>
    </message>
    <message>
        <source>Event</source>
        <translation>Evènement</translation>
    </message>
    <message>
        <source>Condition</source>
        <translation>Condition</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Toujours</translation>
    </message>
    <message>
        <source>Threat level is at least</source>
        <translation>Niveau de menace est au moins</translation>
    </message>
    <message>
        <source>Threat level</source>
        <translation>Niveau de menace</translation>
    </message>
    <message>
        <source>Method</source>
        <translation>Méthode</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>simple Notice</source>
        <translation>Simple note</translation>
    </message>
    <message>
        <source>Summary (can include vulnerability details)</source>
        <translation>Format: Résumé (peut inclure le détail de vulnérabilités)</translation>
    </message>
    <message>
        <source>System Logger (Syslog)</source>
        <translation>Journaux systèmes (syslog)</translation>
    </message>
    <message>
        <source>SNMP </source>
        <translation>SNMP</translation>
    </message>
    <message>
        <source>To Address</source>
        <translation>Adresse de destination</translation>
    </message>
    <message>
        <source>FromAddress</source>
        <translation>Adresse source</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Task run status changed to:</source>
        <translation>Statut d&apos;exécution de la tâche devient:</translation>
    </message>
</context>
<context>
    <name>newSchedule</name>
    <message>
        <source>New Schedule</source>
        <translation>Nouvelle plannification</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Comment (optional)</source>
        <translation>Commentaire (optionnel)</translation>
    </message>
    <message>
        <source>First Time</source>
        <translation>Première fois</translation>
    </message>
    <message>
        <source>hh:mm </source>
        <translation>hh:mm</translation>
    </message>
    <message>
        <source>dd.MM.yyyy</source>
        <translation>jj:MM:aaaa</translation>
    </message>
    <message>
        <source>Period (optional)</source>
        <translation>Période (optionnel)</translation>
    </message>
    <message>
        <source>Duration (optional)</source>
        <translation>Durée (optionnel)</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>newTarget</name>
    <message>
        <source>New Target</source>
        <translation>Nouvelle cible</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Comment (optional)</source>
        <translation>Commentaire (optionnel)</translation>
    </message>
    <message>
        <source>Hosts</source>
        <translation>Hôtes</translation>
    </message>
    <message>
        <source>SSH Credential (optional)</source>
        <translation>Identifiant SSH (optionnel)</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <source>SMB Credential (optional)</source>
        <translation>Identifiant SMB (optionnel)</translation>
    </message>
</context>
<context>
    <name>newTask</name>
    <message>
        <source>New Task</source>
        <translation>Nouvelle tâche</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Comment (optional)</source>
        <translation>Commentaire (optionnel)</translation>
    </message>
    <message>
        <source>Scan Config</source>
        <translation>Configuration du scan</translation>
    </message>
    <message>
        <source>Schedule (optional)</source>
        <translation>Plannification (optionnel)</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <source>Escalator (optional)</source>
        <translation>Avertisseur (optionnel)</translation>
    </message>
    <message>
        <source>Scan Targets</source>
        <translation>Cibles du scan</translation>
    </message>
    <message>
        <source>Slave (optional)</source>
        <translation>Esclave (optionnel)</translation>
    </message>
</context>
<context>
    <name>omp_command</name>
    <message>
        <source>Conversion to QDomElement failed!
 %1 at: %2 , %3</source>
        <translation>Echec de la conversion en QDomElement!
 %1 à: %2 , %3</translation>
    </message>
</context>
</TS>
