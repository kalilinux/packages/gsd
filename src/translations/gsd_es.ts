<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_UY">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../gsd_mainwindow.ui" line="26"/>
        <location filename="../ui_gsd_mainwindow.h" line="623"/>
        <source>Greenbone Security Desktop</source>
        <translation>Greenbone Security Desktop</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="52"/>
        <location filename="../ui_gsd_mainwindow.h" line="720"/>
        <source>Dashboard</source>
        <translation>Tablero</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="116"/>
        <location filename="../gsd_mainwindow.ui" line="155"/>
        <location filename="../gsd_mainwindow.ui" line="194"/>
        <location filename="../gsd_mainwindow.ui" line="233"/>
        <location filename="../gsd_mainwindow.ui" line="278"/>
        <location filename="../gsd_mainwindow.ui" line="311"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="339"/>
        <location filename="../ui_gsd_mainwindow.h" line="721"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="350"/>
        <location filename="../ui_gsd_mainwindow.h" line="722"/>
        <source>&amp;Help</source>
        <translation>A&amp;yuda</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="356"/>
        <location filename="../ui_gsd_mainwindow.h" line="723"/>
        <source>&amp;Task</source>
        <translation>&amp;Tarea</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="369"/>
        <location filename="../ui_gsd_mainwindow.h" line="724"/>
        <source>&amp;Settings</source>
        <translation>&amp;Seteos</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="373"/>
        <location filename="../ui_gsd_mainwindow.h" line="725"/>
        <source>&amp;Language</source>
        <translation>&amp;Lenguaje</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="388"/>
        <location filename="../ui_gsd_mainwindow.h" line="726"/>
        <source>&amp;View</source>
        <translation>&amp;Vista</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="403"/>
        <location filename="../ui_gsd_mainwindow.h" line="727"/>
        <source>&amp;Extras</source>
        <translation>&amp;Extras</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="417"/>
        <location filename="../ui_gsd_mainwindow.h" line="624"/>
        <source>New Connection</source>
        <translation>Nueva conexión</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="422"/>
        <location filename="../ui_gsd_mainwindow.h" line="625"/>
        <source>&amp;Quit</source>
        <translation>Sa&amp;lir</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="425"/>
        <location filename="../ui_gsd_mainwindow.h" line="627"/>
        <source>Close GSD</source>
        <translation>Cerrar GSD</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="433"/>
        <location filename="../ui_gsd_mainwindow.h" line="629"/>
        <source>&amp;Login</source>
        <translation>&amp;Iniciar sesión</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="438"/>
        <location filename="../ui_gsd_mainwindow.h" line="630"/>
        <source>L&amp;ogout</source>
        <translation>&amp;Cerrar sesión</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="443"/>
        <location filename="../ui_gsd_mainwindow.h" line="631"/>
        <source>&amp;About</source>
        <translation>&amp;Acerca de</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="452"/>
        <location filename="../ui_gsd_mainwindow.h" line="632"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="464"/>
        <location filename="../ui_gsd_mainwindow.h" line="633"/>
        <source>&amp;New</source>
        <translation>&amp;Nuevo</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="467"/>
        <location filename="../gsd_mainwindow.ui" line="1035"/>
        <location filename="../ui_gsd_mainwindow.h" line="635"/>
        <location filename="../ui_gsd_mainwindow.h" line="716"/>
        <source>New</source>
        <translation>Nuevo</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="476"/>
        <location filename="../ui_gsd_mainwindow.h" line="637"/>
        <source>&amp;Delete</source>
        <translation>&amp;Borrar</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="479"/>
        <location filename="../gsd_mainwindow.ui" line="666"/>
        <location filename="../gsd_mainwindow.ui" line="712"/>
        <location filename="../gsd_mainwindow.ui" line="739"/>
        <location filename="../gsd_mainwindow.ui" line="784"/>
        <location filename="../gsd_mainwindow.ui" line="811"/>
        <location filename="../gsd_mainwindow.ui" line="838"/>
        <location filename="../gsd_mainwindow.ui" line="870"/>
        <location filename="../gsd_mainwindow.ui" line="902"/>
        <location filename="../gsd_mainwindow.ui" line="1026"/>
        <location filename="../ui_gsd_mainwindow.h" line="639"/>
        <location filename="../ui_gsd_mainwindow.h" line="671"/>
        <location filename="../ui_gsd_mainwindow.h" line="677"/>
        <location filename="../ui_gsd_mainwindow.h" line="680"/>
        <location filename="../ui_gsd_mainwindow.h" line="685"/>
        <location filename="../ui_gsd_mainwindow.h" line="688"/>
        <location filename="../ui_gsd_mainwindow.h" line="691"/>
        <location filename="../ui_gsd_mainwindow.h" line="695"/>
        <location filename="../ui_gsd_mainwindow.h" line="699"/>
        <location filename="../ui_gsd_mainwindow.h" line="715"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="488"/>
        <location filename="../ui_gsd_mainwindow.h" line="641"/>
        <source>&amp;Start</source>
        <translation>&amp;Iniciar</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="491"/>
        <location filename="../gsd_mainwindow.ui" line="494"/>
        <location filename="../ui_gsd_mainwindow.h" line="642"/>
        <location filename="../ui_gsd_mainwindow.h" line="644"/>
        <source>Start</source>
        <translation>Iniciar</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="503"/>
        <location filename="../ui_gsd_mainwindow.h" line="646"/>
        <source>S&amp;top</source>
        <translation>&amp;Detener</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="506"/>
        <location filename="../gsd_mainwindow.ui" line="509"/>
        <location filename="../ui_gsd_mainwindow.h" line="647"/>
        <location filename="../ui_gsd_mainwindow.h" line="649"/>
        <source>Stop</source>
        <translation>Detener</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="514"/>
        <location filename="../ui_gsd_mainwindow.h" line="651"/>
        <source>Break</source>
        <translation>Interrumpir</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="519"/>
        <location filename="../gsd_mainwindow.ui" line="766"/>
        <location filename="../gsd_mainwindow.ui" line="879"/>
        <location filename="../gsd_mainwindow.ui" line="911"/>
        <location filename="../ui_gsd_mainwindow.h" line="652"/>
        <location filename="../ui_gsd_mainwindow.h" line="683"/>
        <location filename="../ui_gsd_mainwindow.h" line="696"/>
        <location filename="../ui_gsd_mainwindow.h" line="700"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="528"/>
        <location filename="../gsd_mainwindow.ui" line="614"/>
        <location filename="../ui_gsd_mainwindow.h" line="653"/>
        <location filename="../ui_gsd_mainwindow.h" line="663"/>
        <source>&amp;Refresh</source>
        <translation>&amp;Actualizar</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="537"/>
        <location filename="../ui_gsd_mainwindow.h" line="654"/>
        <source>Contents</source>
        <translation>Contenido</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="546"/>
        <location filename="../gsd_mainwindow.ui" line="555"/>
        <location filename="../gsd_mainwindow.ui" line="564"/>
        <location filename="../gsd_mainwindow.ui" line="573"/>
        <location filename="../gsd_mainwindow.ui" line="582"/>
        <location filename="../ui_gsd_mainwindow.h" line="655"/>
        <location filename="../ui_gsd_mainwindow.h" line="656"/>
        <location filename="../ui_gsd_mainwindow.h" line="657"/>
        <location filename="../ui_gsd_mainwindow.h" line="658"/>
        <location filename="../ui_gsd_mainwindow.h" line="659"/>
        <source>Show</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="591"/>
        <location filename="../ui_gsd_mainwindow.h" line="660"/>
        <source>Users</source>
        <translation>Usuarios</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="600"/>
        <location filename="../ui_gsd_mainwindow.h" line="661"/>
        <source>NVT Feed</source>
        <translation>Flujo NVT</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="609"/>
        <location filename="../ui_gsd_mainwindow.h" line="662"/>
        <source>Settings</source>
        <translation>Seteos</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="619"/>
        <location filename="../ui_gsd_mainwindow.h" line="664"/>
        <source>Logging</source>
        <translation>Registros</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="624"/>
        <location filename="../ui_gsd_mainwindow.h" line="665"/>
        <source>&amp;Schedules</source>
        <translation>&amp;Agendas</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="629"/>
        <location filename="../ui_gsd_mainwindow.h" line="666"/>
        <source>&amp;Targets</source>
        <translation>&amp;Objetivos</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="634"/>
        <location filename="../ui_gsd_mainwindow.h" line="667"/>
        <source>S&amp;can Configs</source>
        <translation>Configuración E&amp;scaneos</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="639"/>
        <location filename="../ui_gsd_mainwindow.h" line="668"/>
        <source>&amp;Escalators</source>
        <translation>&amp;Escaladores</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="648"/>
        <location filename="../ui_gsd_mainwindow.h" line="669"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pausar</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="657"/>
        <location filename="../ui_gsd_mainwindow.h" line="670"/>
        <source>&amp;Resume</source>
        <translation>&amp;Continuar</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="675"/>
        <location filename="../gsd_mainwindow.ui" line="748"/>
        <location filename="../gsd_mainwindow.ui" line="793"/>
        <location filename="../gsd_mainwindow.ui" line="820"/>
        <location filename="../gsd_mainwindow.ui" line="847"/>
        <location filename="../gsd_mainwindow.ui" line="888"/>
        <location filename="../gsd_mainwindow.ui" line="920"/>
        <location filename="../gsd_mainwindow.ui" line="929"/>
        <location filename="../gsd_mainwindow.ui" line="1017"/>
        <location filename="../ui_gsd_mainwindow.h" line="672"/>
        <location filename="../ui_gsd_mainwindow.h" line="681"/>
        <location filename="../ui_gsd_mainwindow.h" line="686"/>
        <location filename="../ui_gsd_mainwindow.h" line="689"/>
        <location filename="../ui_gsd_mainwindow.h" line="692"/>
        <location filename="../ui_gsd_mainwindow.h" line="697"/>
        <location filename="../ui_gsd_mainwindow.h" line="701"/>
        <location filename="../ui_gsd_mainwindow.h" line="702"/>
        <location filename="../ui_gsd_mainwindow.h" line="714"/>
        <source>Details</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="684"/>
        <location filename="../gsd_mainwindow.ui" line="703"/>
        <location filename="../gsd_mainwindow.ui" line="730"/>
        <location filename="../gsd_mainwindow.ui" line="775"/>
        <location filename="../gsd_mainwindow.ui" line="802"/>
        <location filename="../gsd_mainwindow.ui" line="829"/>
        <location filename="../ui_gsd_mainwindow.h" line="673"/>
        <location filename="../ui_gsd_mainwindow.h" line="676"/>
        <location filename="../ui_gsd_mainwindow.h" line="679"/>
        <location filename="../ui_gsd_mainwindow.h" line="684"/>
        <location filename="../ui_gsd_mainwindow.h" line="687"/>
        <location filename="../ui_gsd_mainwindow.h" line="690"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="689"/>
        <location filename="../ui_gsd_mainwindow.h" line="674"/>
        <source>&amp;Credentials</source>
        <translation>&amp;Credenciales</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="694"/>
        <location filename="../ui_gsd_mainwindow.h" line="675"/>
        <source>&amp;Agents</source>
        <translation>&amp;Agentes</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="721"/>
        <location filename="../ui_gsd_mainwindow.h" line="678"/>
        <source>Download Installer</source>
        <translation>Bajar Instalador</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="757"/>
        <location filename="../ui_gsd_mainwindow.h" line="682"/>
        <source>Export XML</source>
        <translation>Exportar XML</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="856"/>
        <location filename="../ui_gsd_mainwindow.h" line="693"/>
        <source>Test</source>
        <translation>Prueba</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="861"/>
        <location filename="../ui_gsd_mainwindow.h" line="694"/>
        <source>&amp;Notes</source>
        <translation>&amp;Notas</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="893"/>
        <location filename="../ui_gsd_mainwindow.h" line="698"/>
        <source>&amp;Overrides</source>
        <translation>&amp;Sobrepasos</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="934"/>
        <location filename="../ui_gsd_mainwindow.h" line="703"/>
        <source>&amp;Performance</source>
        <translation>&amp;Performance</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="943"/>
        <location filename="../ui_gsd_mainwindow.h" line="704"/>
        <source>Result</source>
        <translation>Resultado</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="948"/>
        <location filename="../ui_gsd_mainwindow.h" line="705"/>
        <source>&amp;Clear Dock Settings</source>
        <translation>&amp;Limpiar Seteos del tablero</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="957"/>
        <location filename="../ui_gsd_mainwindow.h" line="706"/>
        <source>Import Config</source>
        <translation>Importar configuración</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="965"/>
        <location filename="../ui_gsd_mainwindow.h" line="707"/>
        <source>&amp;Indonesian</source>
        <translation>&amp;Indonesio</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="973"/>
        <location filename="../ui_gsd_mainwindow.h" line="708"/>
        <source>&amp;French</source>
        <translation>&amp;Francés</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="981"/>
        <location filename="../ui_gsd_mainwindow.h" line="709"/>
        <source>&amp;German</source>
        <translation>&amp;Alemán</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="989"/>
        <location filename="../ui_gsd_mainwindow.h" line="710"/>
        <source>&amp;English</source>
        <translation>I&amp;nglés</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="992"/>
        <location filename="../ui_gsd_mainwindow.h" line="711"/>
        <source>English</source>
        <translation>Inglés</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="1003"/>
        <location filename="../ui_gsd_mainwindow.h" line="712"/>
        <source>&amp;Default</source>
        <translation>&amp;Por defecto</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="1008"/>
        <location filename="../ui_gsd_mainwindow.h" line="713"/>
        <source>&amp;Start Greenbone Security Assistant</source>
        <translation>&amp;Iniciar Greenbone Security Assistant</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="1040"/>
        <location filename="../ui_gsd_mainwindow.h" line="717"/>
        <source>Slaves</source>
        <translation>Esclavos</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="1045"/>
        <location filename="../ui_gsd_mainwindow.h" line="718"/>
        <source>System Reports</source>
        <translation>Reportes del sistema</translation>
    </message>
    <message>
        <location filename="../gsd_mainwindow.ui" line="1054"/>
        <location filename="../ui_gsd_mainwindow.h" line="719"/>
        <source>Modify Task</source>
        <translation>Modificar tarea</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="589"/>
        <source>Refresh Interval:</source>
        <translation>Intervalo de actualización:</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="595"/>
        <source> sec</source>
        <translation> seg</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="601"/>
        <source>Next Refresh: </source>
        <translation>Próxima actualización: </translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="619"/>
        <source>Apply Interval</source>
        <translation>Aplicar intervalo</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="626"/>
        <source>Stop Interval</source>
        <translation>Detener intervalo</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="640"/>
        <location filename="../gsd_mw.cpp" line="1187"/>
        <location filename="../gsd_mw.cpp" line="1226"/>
        <source>manual</source>
        <translation>manual</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="643"/>
        <source>Refresh Progress</source>
        <translation>Actualizar progreso</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="663"/>
        <source>Refresh Settings</source>
        <translation>Seteos de Actualización</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="709"/>
        <location filename="../gsd_mw.cpp" line="1184"/>
        <location filename="../gsd_mw.cpp" line="1223"/>
        <source>Refresh Interval: &lt;b&gt;manual&lt;/b&gt;</source>
        <translation>Intervalo de actualización: &lt;b&gt;manual&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1196"/>
        <source>Refresh Interval: &lt;b&gt;%1 second&lt;/b&gt;</source>
        <translation>Intervalo de actualización: &lt;b&gt;%1 segundo&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1201"/>
        <source>Refresh Interval: &lt;b&gt;%1 seconds&lt;/b&gt;</source>
        <translation>Intervalo de actualización: &lt;b&gt;%1 segundos&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>delegate_date_time</name>
    <message>
        <location filename="../delegate_date_time.cpp" line="111"/>
        <location filename="../delegate_date_time.cpp" line="203"/>
        <source> hour</source>
        <translation> hora</translation>
    </message>
    <message>
        <location filename="../delegate_date_time.cpp" line="124"/>
        <location filename="../delegate_date_time.cpp" line="216"/>
        <source> hours</source>
        <translation> horas</translation>
    </message>
    <message>
        <location filename="../delegate_date_time.cpp" line="137"/>
        <location filename="../delegate_date_time.cpp" line="229"/>
        <source> day</source>
        <translation> día</translation>
    </message>
    <message>
        <location filename="../delegate_date_time.cpp" line="150"/>
        <location filename="../delegate_date_time.cpp" line="242"/>
        <source> days</source>
        <translation> días</translation>
    </message>
    <message>
        <location filename="../delegate_date_time.cpp" line="163"/>
        <location filename="../delegate_date_time.cpp" line="255"/>
        <source> week</source>
        <translation> semana</translation>
    </message>
    <message>
        <location filename="../delegate_date_time.cpp" line="176"/>
        <location filename="../delegate_date_time.cpp" line="268"/>
        <source> weeks</source>
        <translation> semanas</translation>
    </message>
    <message>
        <location filename="../delegate_date_time.cpp" line="287"/>
        <source> month</source>
        <translation> mes</translation>
    </message>
    <message>
        <location filename="../delegate_date_time.cpp" line="300"/>
        <source> months</source>
        <translation> meses</translation>
    </message>
</context>
<context>
    <name>delegate_progress</name>
    <message>
        <location filename="../delegate_progress.cpp" line="124"/>
        <location filename="../delegate_progress.cpp" line="137"/>
        <source> at </source>
        <translation> en </translation>
    </message>
    <message>
        <location filename="../delegate_progress.cpp" line="172"/>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
</context>
<context>
    <name>dlg_about</name>
    <message>
        <location filename="../dlg_about.ui" line="22"/>
        <location filename="../ui_dlg_about.h" line="123"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../dlg_about.ui" line="78"/>
        <location filename="../ui_dlg_about.h" line="125"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="410"/>
        <source>Greenbone Security Desktop</source>
        <translation>Greenbone security Desktop</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="411"/>
        <source>Version %1</source>
        <translation>Versión %1</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="415"/>
        <source>Greenbone Security Desktop is a graphical user interface&lt;br&gt;to access a Greenbone Security Manager, OpenVAS Manager&lt;br&gt;or any other service that offers the OMP protocol for&lt;br&gt;comprehensive vulnerability management.&lt;br&gt;&lt;br&gt;Copyright 2009-2011 by Greenbone Networks GmbH,&lt;br&gt;&lt;a href=www.greenbone.net&gt; www.greenbone.net&lt;/a&gt;</source>
        <translation>El Greenbone Security Desktop es una interface gráfica de usuario&lt;br&gt;para acceder al Manejador de Seguridad Greenbone, Manejador OpenVAS&lt;br&gt;o cualquier otro servicio que ofrezca el protocolo OMP para&lt;br&gt;el manejo comprensivo de vulnerabilidades.&lt;br&gt;&lt;br&gt;Copyright 2008-2011 por Greenbone Networks GmbH,&lt;br&gt;&lt;a href=www.greenbone.net&gt; www.greenbone.net&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="428"/>
        <source>License: GNU General Public License version 2 or any later&lt;br&gt;version (&lt;a href=&quot;license&quot;&gt;full license text&lt;a&gt;).</source>
        <translatorcomment>Anyway, License is in english, not spanish, due to the fact that it&apos;s the english versión the official one.</translatorcomment>
        <translation>Licencia: GNU Licencia Pública General versión 2 o cualquier &lt;br&gt;versión posterior (&lt;a href=&quot;license&quot;&gt;texto completo de la licencia&lt;a&gt;).</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="432"/>
        <source>Contact: For updates, feature proposals and bug reports please&lt;br&gt;contact the  &lt;a href=&quot;http://www.greenbone.net/company/contact.html&quot;&gt;Greenbone team&lt;/a&gt; or visit the &lt;a href=&quot;http://www.openvas.org&quot;&gt;OpenVAS homepage&lt;/a&gt;.</source>
        <translation>Contacto: Para actualizaciones y reporte de fallas, por favor &lt;br&gt;contacte al  &lt;a href=&quot;http://www.greenbone.net/company/contact.html&quot;&gt;equipo de Greenbone&lt;/a&gt; o visite la &lt;a href=&quot;http://www.openvas.org&quot;&gt;página de OpenVAS&lt;/a&gt;.</translation>
    </message>
</context>
<context>
    <name>dlg_detailsNote</name>
    <message>
        <location filename="../dlg_details_note.ui" line="17"/>
        <source>Note Details</source>
        <translation>Detalles de la nota</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="40"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="47"/>
        <source>NVT Name:</source>
        <translation>Nombre NVT:</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="54"/>
        <source>NVT OID:</source>
        <translation>OID de NVT:</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="61"/>
        <source>Created:</source>
        <translation>Creado:</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="68"/>
        <source>Last Modified:</source>
        <translation>Última modificación:</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="82"/>
        <source>Application</source>
        <translation>Aplicación</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="124"/>
        <location filename="../dlg_details_note.ui" line="271"/>
        <source>Hosts:</source>
        <translation>Hosts:</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="144"/>
        <location filename="../dlg_details_note.ui" line="281"/>
        <source>Port:</source>
        <translation>Puerto:</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="164"/>
        <location filename="../dlg_details_note.ui" line="291"/>
        <source>Threat:</source>
        <translation>Amenaza:</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="184"/>
        <location filename="../dlg_details_note.ui" line="304"/>
        <source>Task:</source>
        <translation>Tarea:</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="207"/>
        <location filename="../dlg_details_note.ui" line="314"/>
        <source>Result:</source>
        <translation>Resultado:</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="237"/>
        <location filename="../dlg_details_note.ui" line="364"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="324"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../dlg_details_note.ui" line="384"/>
        <location filename="../dlg_details_note.ui" line="411"/>
        <location filename="../dlg_details_note.ui" line="431"/>
        <location filename="../dlg_details_note.ui" line="451"/>
        <location filename="../dlg_details_note.ui" line="471"/>
        <source>All</source>
        <translation>Todo</translation>
    </message>
</context>
<context>
    <name>dlg_detailsOverride</name>
    <message>
        <location filename="../dlg_details_override.ui" line="16"/>
        <source>Details</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="39"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="46"/>
        <source>NVT Name:</source>
        <translation>Nombre NVT:</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="53"/>
        <source>NVT OID:</source>
        <translation>OID de NVT:</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="60"/>
        <source>Created:</source>
        <translation>Creado:</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="67"/>
        <source>Last Modified:</source>
        <translation>Última modificación:</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="81"/>
        <source>Application</source>
        <translation>Aplicación</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="123"/>
        <location filename="../dlg_details_override.ui" line="258"/>
        <source>Port:</source>
        <translation>Puerto:</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="130"/>
        <location filename="../dlg_details_override.ui" line="265"/>
        <source>Threat:</source>
        <translation>Amenaza:</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="143"/>
        <location filename="../dlg_details_override.ui" line="285"/>
        <source>New Threat:</source>
        <translation>Nueva amenaza:</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="150"/>
        <location filename="../dlg_details_override.ui" line="251"/>
        <source>Hosts:</source>
        <translation>Hosts:</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="157"/>
        <location filename="../dlg_details_override.ui" line="272"/>
        <source>Task:</source>
        <translation>Tarea:</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="164"/>
        <source>Result:</source>
        <translation>Resultado:</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="178"/>
        <location filename="../dlg_details_override.ui" line="305"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="292"/>
        <source>Result</source>
        <translation>Resultado</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="315"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../dlg_details_override.ui" line="351"/>
        <location filename="../dlg_details_override.ui" line="378"/>
        <location filename="../dlg_details_override.ui" line="391"/>
        <location filename="../dlg_details_override.ui" line="427"/>
        <location filename="../dlg_details_override.ui" line="454"/>
        <source>Any</source>
        <translation>Todo</translation>
    </message>
</context>
<context>
    <name>dlg_license</name>
    <message>
        <location filename="../dlg_license.ui" line="16"/>
        <location filename="../ui_dlg_license.h" line="69"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="../dlg_license.ui" line="29"/>
        <location filename="../ui_dlg_license.h" line="70"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;		    GNU GENERAL PUBLIC LICENSE&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;		       Version 2, June 1991&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; Copyright (C) 1989, 1991 Free Software Foundation, Inc.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;                       51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; Everyone is permitted to copy and distribute verbatim copies&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; of this license document, but changing it is not allowed.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;			    Preamble&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  The licenses for most software are designed to take away your&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;freedom to share and change it.  By contrast, the GNU General Public&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;License is intended to guarantee your freedom to share and change free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;software--to make sure the software is free for all its users.  This&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;General Public License applies to most of the Free Software&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Foundation&apos;s software and to any other program whose authors commit to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;using it.  (Some other Free Software Foundation software is covered by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the GNU Library General Public License instead.)  You can apply it to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;your programs, too.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  When we speak of free software, we are referring to freedom, not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;price.  Our General Public Licenses are designed to make sure that you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;have the freedom to distribute copies of free software (and charge for&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this service if you wish), that you receive source code or can get it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;if you want it, that you can change the software or use pieces of it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;in new free programs; and that you know you can do these things.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  To protect your rights, we need to make restrictions that forbid&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;anyone to deny you these rights or to ask you to surrender the rights.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;These restrictions translate to certain responsibilities for you if you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute copies of the software, or if you modify it.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  For example, if you distribute copies of such a program, whether&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;gratis or for a fee, you must give the recipients all the rights that&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;you have.  You must make sure that they, too, receive or can get the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;source code.  And you must show them these terms so they know their&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;rights.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  We protect your rights with two steps: (1) copyright the software, and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;(2) offer you this license which gives you legal permission to copy,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute and/or modify the software.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  Also, for each author&apos;s protection and ours, we want to make certain&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;that everyone understands that there is no warranty for this free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;software.  If the software is modified by someone else and passed on, we&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;want its recipients to know that what they have is not the original, so&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;that any problems introduced by others will not reflect on the original&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;authors&apos; reputations.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  Finally, any free program is threatened constantly by software&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;patents.  We wish to avoid the danger that redistributors of a free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;program will individually obtain patent licenses, in effect making the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;program proprietary.  To prevent this, we have made it clear that any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;patent must be licensed for everyone&apos;s free use or not licensed at all.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  The precise terms and conditions for copying, distribution and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;modification follow.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;		    GNU GENERAL PUBLIC LICENSE&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  0. This License applies to any program or other work which contains&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;a notice placed by the copyright holder saying it may be distributed&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;under the terms of this General Public License.  The &quot;Program&quot;, below,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;refers to any such program or work, and a &quot;work based on the Program&quot;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;means either the Program or any derivative work under copyright law:&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;that is to say, a work containing the Program or a portion of it,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;either verbatim or with modifications and/or translated into another&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;language.  (Hereinafter, translation is included without limitation in&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the term &quot;modification&quot;.)  Each licensee is addressed as &quot;you&quot;.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Activities other than copying, distribution and modification are not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;covered by this License; they are outside its scope.  The act of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;running the Program is not restricted, and the output from the Program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;is covered only if its contents constitute a work based on the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Program (independent of having been made by running the Program).&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Whether that is true depends on what the Program does.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  1. You may copy and distribute verbatim copies of the Program&apos;s&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;source code as you receive it, in any medium, provided that you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;conspicuously and appropriately publish on each copy an appropriate&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;copyright notice and disclaimer of warranty; keep intact all the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;notices that refer to this License and to the absence of any warranty;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;and give any other recipients of the Program a copy of this License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;along with the Program.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You may charge a fee for the physical act of transferring a copy, and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;you may at your option offer warranty protection in exchange for a fee.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  2. You may modify your copy or copies of the Program or any portion&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;of it, thus forming a work based on the Program, and copy and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute such modifications or work under the terms of Section 1&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;above, provided that you also meet all of these conditions:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    a) You must cause the modified files to carry prominent notices&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    stating that you changed the files and the date of any change.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    b) You must cause any work that you distribute or publish, that in&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    whole or in part contains or is derived from the Program or any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    part thereof, to be licensed as a whole at no charge to all third&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    parties under the terms of this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    c) If the modified program normally reads commands interactively&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    when run, you must cause it, when started running for such&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    interactive use in the most ordinary way, to print or display an&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    announcement including an appropriate copyright notice and a&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    notice that there is no warranty (or else, saying that you provide&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    a warranty) and that users may redistribute the program under&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    these conditions, and telling the user how to view a copy of this&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    License.  (Exception: if the Program itself is interactive but&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    does not normally print such an announcement, your work based on&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    the Program is not required to print an announcement.)&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;These requirements apply to the modified work as a whole.  If&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;identifiable sections of that work are not derived from the Program,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;and can be reasonably considered independent and separate works in&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;themselves, then this License, and its terms, do not apply to those&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;sections when you distribute them as separate works.  But when you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute the same sections as part of a whole which is a work based&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;on the Program, the distribution of the whole must be on the terms of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this License, whose permissions for other licensees extend to the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;entire whole, and thus to each and every part regardless of who wrote it.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Thus, it is not the intent of this section to claim rights or contest&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;your rights to work written entirely by you; rather, the intent is to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;exercise the right to control the distribution of derivative or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;collective works based on the Program.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;In addition, mere aggregation of another work not based on the Program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;with the Program (or with a work based on the Program) on a volume of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;a storage or distribution medium does not bring the other work under&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the scope of this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  3. You may copy and distribute the Program (or a work based on it,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;under Section 2) in object code or executable form under the terms of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sections 1 and 2 above provided that you also do one of the following:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    a) Accompany it with the complete corresponding machine-readable&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    source code, which must be distributed under the terms of Sections&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    1 and 2 above on a medium customarily used for software interchange; or,&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    b) Accompany it with a written offer, valid for at least three&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    years, to give any third party, for a charge no more than your&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    cost of physically performing source distribution, a complete&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    machine-readable copy of the corresponding source code, to be&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    distributed under the terms of Sections 1 and 2 above on a medium&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    customarily used for software interchange; or,&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    c) Accompany it with the information you received as to the offer&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    to distribute corresponding source code.  (This alternative is&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    allowed only for noncommercial distribution and only if you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    received the program in object code or executable form with such&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    an offer, in accord with Subsection b above.)&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The source code for a work means the preferred form of the work for&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;making modifications to it.  For an executable work, complete source&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;code means all the source code for all modules it contains, plus any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;associated interface definition files, plus the scripts used to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;control compilation and installation of the executable.  However, as a&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;special exception, the source code distributed need not include&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;anything that is normally distributed (in either source or binary&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;form) with the major components (compiler, kernel, and so on) of the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;operating system on which the executable runs, unless that component&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;itself accompanies the executable.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If distribution of executable or object code is made by offering&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;access to copy from a designated place, then offering equivalent&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;access to copy the source code from the same place counts as&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribution of the source code, even though third parties are not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;compelled to copy the source along with the object code.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  4. You may not copy, modify, sublicense, or distribute the Program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;except as expressly provided under this License.  Any attempt&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;otherwise to copy, modify, sublicense or distribute the Program is&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;void, and will automatically terminate your rights under this License.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;However, parties who have received copies, or rights, from you under&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this License will not have their licenses terminated so long as such&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;parties remain in full compliance.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  5. You are not required to accept this License, since you have not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;signed it.  However, nothing else grants you permission to modify or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute the Program or its derivative works.  These actions are&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;prohibited by law if you do not accept this License.  Therefore, by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;modifying or distributing the Program (or any work based on the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Program), you indicate your acceptance of this License to do so, and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;all its terms and conditions for copying, distributing or modifying&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the Program or works based on it.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  6. Each time you redistribute the Program (or any work based on the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Program), the recipient automatically receives a license from the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;original licensor to copy, distribute or modify the Program subject to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;these terms and conditions.  You may not impose any further&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;restrictions on the recipients&apos; exercise of the rights granted herein.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You are not responsible for enforcing compliance by third parties to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  7. If, as a consequence of a court judgment or allegation of patent&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;infringement or for any other reason (not limited to patent issues),&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;conditions are imposed on you (whether by court order, agreement or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;otherwise) that contradict the conditions of this License, they do not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;excuse you from the conditions of this License.  If you cannot&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute so as to satisfy simultaneously your obligations under this&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;License and any other pertinent obligations, then as a consequence you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;may not distribute the Program at all.  For example, if a patent&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;license would not permit royalty-free redistribution of the Program by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;all those who receive copies directly or indirectly through you, then&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the only way you could satisfy both it and this License would be to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;refrain entirely from distribution of the Program.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If any portion of this section is held invalid or unenforceable under&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;any particular circumstance, the balance of the section is intended to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;apply and the section as a whole is intended to apply in other&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;circumstances.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;It is not the purpose of this section to induce you to infringe any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;patents or other property right claims or to contest validity of any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;such claims; this section has the sole purpose of protecting the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;integrity of the free software distribution system, which is&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;implemented by public license practices.  Many people have made&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;generous contributions to the wide range of software distributed&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;through that system in reliance on consistent application of that&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;system; it is up to the author/donor to decide if he or she is willing&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;to distribute software through any other system and a licensee cannot&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;impose that choice.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This section is intended to make thoroughly clear what is believed to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;be a consequence of the rest of this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  8. If the distribution and/or use of the Program is restricted in&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;certain countries either by patents or by copyrighted interfaces, the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;original copyright holder who places the Program under this License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;may add an explicit geographical distribution limitation excluding&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;those countries, so that distribution is permitted only in or among&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;countries not thus excluded.  In such case, this License incorporates&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the limitation as if written in the body of this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  9. The Free Software Foundation may publish revised and/or new versions&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;of the General Public License from time to time.  Such new versions will&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;be similar in spirit to the present version, but may differ in detail to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;address new problems or concerns.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Each version is given a distinguishing version number.  If the Program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;specifies a version number of this License which applies to it and &quot;any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;later version&quot;, you have the option of following the terms and conditions&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;either of that version or of any later version published by the Free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Software Foundation.  If the Program does not specify a version number of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this License, you may choose any version ever published by the Free Software&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Foundation.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  10. If you wish to incorporate parts of the Program into other free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;programs whose distribution conditions are different, write to the author&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;to ask for permission.  For software which is copyrighted by the Free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Software Foundation, write to the Free Software Foundation; we sometimes&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;make exceptions for this.  Our decision will be guided by the two goals&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;of preserving the free status of all derivatives of our free software and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;of promoting the sharing and reuse of software generally.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;			    NO WARRANTY&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;PROVIDE THE PROGRAM &quot;AS IS&quot; WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;REPAIR OR CORRECTION.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;POSSIBILITY OF SUCH DAMAGES.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;		     END OF TERMS AND CONDITIONS&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;	    How to Apply These Terms to Your New Programs&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  If you develop a new program, and you want it to be of the greatest&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;possible use to the public, the best way to achieve this is to make it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;free software which everyone can redistribute and change under these terms.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  To do so, attach the following notices to the program.  It is safest&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;to attach them to the start of each source file to most effectively&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;convey the exclusion of warranty; and each file should have at least&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the &quot;copyright&quot; line and a pointer to where the full notice is found.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    &amp;lt;one line to give the program&apos;s name and a brief idea of what it does.&amp;gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Copyright (C) &amp;lt;year&amp;gt;  &amp;lt;name of author&amp;gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    This program is free software; you can redistribute it and/or modify&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    it under the terms of the GNU General Public License as published by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    the Free Software Foundation; either version 2 of the License, or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    (at your option) any later version.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    This program is distributed in the hope that it will be useful,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    but WITHOUT ANY WARRANTY; without even the implied warranty of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    GNU General Public License for more details.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    You should have received a copy of the GNU General Public License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    along with this program; if not, write to the Free Software&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Also add information on how to contact you by electronic and paper mail.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If the program is interactive, make it output a short notice like this&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;when it starts in an interactive mode:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Gnomovision version 69, Copyright (C) year name of author&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type `show w&apos;.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    This is free software, and you are welcome to redistribute it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    under certain conditions; type `show c&apos; for details.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The hypothetical commands `show w&apos; and `show c&apos; should show the appropriate&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;parts of the General Public License.  Of course, the commands you use may&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;be called something other than `show w&apos; and `show c&apos;; they could even be&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;mouse-clicks or menu items--whatever suits your program.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You should also get your employer (if you work as a programmer) or your&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;school, if any, to sign a &quot;copyright disclaimer&quot; for the program, if&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;necessary.  Here is a sample; alter the names:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  Yoyodyne, Inc., hereby disclaims all copyright interest in the program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  `Gnomovision&apos; (which makes passes at compilers) written by James Hacker.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  &amp;lt;signature of Ty Coon&amp;gt;, 1 April 1989&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  Ty Coon, President of Vice&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This General Public License does not permit incorporating your program into&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;proprietary programs.  If your program is a subroutine library, you may&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;consider it more useful to permit linking proprietary applications with the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;library.  If this is what you want to do, use the GNU Library General&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Public License instead of this License.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;		    GNU GENERAL PUBLIC LICENSE&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;		       Version 2, June 1991&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; Copyright (C) 1989, 1991 Free Software Foundation, Inc.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;                       51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; Everyone is permitted to copy and distribute verbatim copies&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt; of this license document, but changing it is not allowed.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;			    Preamble&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  The licenses for most software are designed to take away your&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;freedom to share and change it.  By contrast, the GNU General Public&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;License is intended to guarantee your freedom to share and change free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;software--to make sure the software is free for all its users.  This&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;General Public License applies to most of the Free Software&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Foundation&apos;s software and to any other program whose authors commit to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;using it.  (Some other Free Software Foundation software is covered by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the GNU Library General Public License instead.)  You can apply it to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;your programs, too.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  When we speak of free software, we are referring to freedom, not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;price.  Our General Public Licenses are designed to make sure that you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;have the freedom to distribute copies of free software (and charge for&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this service if you wish), that you receive source code or can get it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;if you want it, that you can change the software or use pieces of it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;in new free programs; and that you know you can do these things.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  To protect your rights, we need to make restrictions that forbid&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;anyone to deny you these rights or to ask you to surrender the rights.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;These restrictions translate to certain responsibilities for you if you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute copies of the software, or if you modify it.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  For example, if you distribute copies of such a program, whether&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;gratis or for a fee, you must give the recipients all the rights that&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;you have.  You must make sure that they, too, receive or can get the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;source code.  And you must show them these terms so they know their&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;rights.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  We protect your rights with two steps: (1) copyright the software, and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;(2) offer you this license which gives you legal permission to copy,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute and/or modify the software.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  Also, for each author&apos;s protection and ours, we want to make certain&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;that everyone understands that there is no warranty for this free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;software.  If the software is modified by someone else and passed on, we&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;want its recipients to know that what they have is not the original, so&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;that any problems introduced by others will not reflect on the original&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;authors&apos; reputations.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  Finally, any free program is threatened constantly by software&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;patents.  We wish to avoid the danger that redistributors of a free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;program will individually obtain patent licenses, in effect making the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;program proprietary.  To prevent this, we have made it clear that any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;patent must be licensed for everyone&apos;s free use or not licensed at all.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  The precise terms and conditions for copying, distribution and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;modification follow.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;		    GNU GENERAL PUBLIC LICENSE&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  0. This License applies to any program or other work which contains&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;a notice placed by the copyright holder saying it may be distributed&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;under the terms of this General Public License.  The &quot;Program&quot;, below,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;refers to any such program or work, and a &quot;work based on the Program&quot;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;means either the Program or any derivative work under copyright law:&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;that is to say, a work containing the Program or a portion of it,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;either verbatim or with modifications and/or translated into another&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;language.  (Hereinafter, translation is included without limitation in&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the term &quot;modification&quot;.)  Each licensee is addressed as &quot;you&quot;.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Activities other than copying, distribution and modification are not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;covered by this License; they are outside its scope.  The act of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;running the Program is not restricted, and the output from the Program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;is covered only if its contents constitute a work based on the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Program (independent of having been made by running the Program).&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Whether that is true depends on what the Program does.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  1. You may copy and distribute verbatim copies of the Program&apos;s&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;source code as you receive it, in any medium, provided that you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;conspicuously and appropriately publish on each copy an appropriate&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;copyright notice and disclaimer of warranty; keep intact all the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;notices that refer to this License and to the absence of any warranty;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;and give any other recipients of the Program a copy of this License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;along with the Program.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You may charge a fee for the physical act of transferring a copy, and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;you may at your option offer warranty protection in exchange for a fee.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  2. You may modify your copy or copies of the Program or any portion&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;of it, thus forming a work based on the Program, and copy and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute such modifications or work under the terms of Section 1&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;above, provided that you also meet all of these conditions:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    a) You must cause the modified files to carry prominent notices&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    stating that you changed the files and the date of any change.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    b) You must cause any work that you distribute or publish, that in&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    whole or in part contains or is derived from the Program or any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    part thereof, to be licensed as a whole at no charge to all third&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    parties under the terms of this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    c) If the modified program normally reads commands interactively&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    when run, you must cause it, when started running for such&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    interactive use in the most ordinary way, to print or display an&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    announcement including an appropriate copyright notice and a&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    notice that there is no warranty (or else, saying that you provide&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    a warranty) and that users may redistribute the program under&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    these conditions, and telling the user how to view a copy of this&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    License.  (Exception: if the Program itself is interactive but&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    does not normally print such an announcement, your work based on&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    the Program is not required to print an announcement.)&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;These requirements apply to the modified work as a whole.  If&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;identifiable sections of that work are not derived from the Program,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;and can be reasonably considered independent and separate works in&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;themselves, then this License, and its terms, do not apply to those&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;sections when you distribute them as separate works.  But when you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute the same sections as part of a whole which is a work based&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;on the Program, the distribution of the whole must be on the terms of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this License, whose permissions for other licensees extend to the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;entire whole, and thus to each and every part regardless of who wrote it.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Thus, it is not the intent of this section to claim rights or contest&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;your rights to work written entirely by you; rather, the intent is to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;exercise the right to control the distribution of derivative or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;collective works based on the Program.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;In addition, mere aggregation of another work not based on the Program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;with the Program (or with a work based on the Program) on a volume of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;a storage or distribution medium does not bring the other work under&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the scope of this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  3. You may copy and distribute the Program (or a work based on it,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;under Section 2) in object code or executable form under the terms of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sections 1 and 2 above provided that you also do one of the following:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    a) Accompany it with the complete corresponding machine-readable&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    source code, which must be distributed under the terms of Sections&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    1 and 2 above on a medium customarily used for software interchange; or,&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    b) Accompany it with a written offer, valid for at least three&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    years, to give any third party, for a charge no more than your&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    cost of physically performing source distribution, a complete&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    machine-readable copy of the corresponding source code, to be&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    distributed under the terms of Sections 1 and 2 above on a medium&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    customarily used for software interchange; or,&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    c) Accompany it with the information you received as to the offer&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    to distribute corresponding source code.  (This alternative is&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    allowed only for noncommercial distribution and only if you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    received the program in object code or executable form with such&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    an offer, in accord with Subsection b above.)&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The source code for a work means the preferred form of the work for&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;making modifications to it.  For an executable work, complete source&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;code means all the source code for all modules it contains, plus any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;associated interface definition files, plus the scripts used to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;control compilation and installation of the executable.  However, as a&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;special exception, the source code distributed need not include&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;anything that is normally distributed (in either source or binary&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;form) with the major components (compiler, kernel, and so on) of the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;operating system on which the executable runs, unless that component&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;itself accompanies the executable.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If distribution of executable or object code is made by offering&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;access to copy from a designated place, then offering equivalent&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;access to copy the source code from the same place counts as&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribution of the source code, even though third parties are not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;compelled to copy the source along with the object code.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  4. You may not copy, modify, sublicense, or distribute the Program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;except as expressly provided under this License.  Any attempt&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;otherwise to copy, modify, sublicense or distribute the Program is&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;void, and will automatically terminate your rights under this License.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;However, parties who have received copies, or rights, from you under&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this License will not have their licenses terminated so long as such&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;parties remain in full compliance.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  5. You are not required to accept this License, since you have not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;signed it.  However, nothing else grants you permission to modify or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute the Program or its derivative works.  These actions are&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;prohibited by law if you do not accept this License.  Therefore, by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;modifying or distributing the Program (or any work based on the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Program), you indicate your acceptance of this License to do so, and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;all its terms and conditions for copying, distributing or modifying&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the Program or works based on it.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  6. Each time you redistribute the Program (or any work based on the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Program), the recipient automatically receives a license from the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;original licensor to copy, distribute or modify the Program subject to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;these terms and conditions.  You may not impose any further&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;restrictions on the recipients&apos; exercise of the rights granted herein.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You are not responsible for enforcing compliance by third parties to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  7. If, as a consequence of a court judgment or allegation of patent&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;infringement or for any other reason (not limited to patent issues),&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;conditions are imposed on you (whether by court order, agreement or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;otherwise) that contradict the conditions of this License, they do not&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;excuse you from the conditions of this License.  If you cannot&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;distribute so as to satisfy simultaneously your obligations under this&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;License and any other pertinent obligations, then as a consequence you&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;may not distribute the Program at all.  For example, if a patent&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;license would not permit royalty-free redistribution of the Program by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;all those who receive copies directly or indirectly through you, then&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the only way you could satisfy both it and this License would be to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;refrain entirely from distribution of the Program.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If any portion of this section is held invalid or unenforceable under&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;any particular circumstance, the balance of the section is intended to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;apply and the section as a whole is intended to apply in other&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;circumstances.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;It is not the purpose of this section to induce you to infringe any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;patents or other property right claims or to contest validity of any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;such claims; this section has the sole purpose of protecting the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;integrity of the free software distribution system, which is&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;implemented by public license practices.  Many people have made&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;generous contributions to the wide range of software distributed&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;through that system in reliance on consistent application of that&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;system; it is up to the author/donor to decide if he or she is willing&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;to distribute software through any other system and a licensee cannot&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;impose that choice.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This section is intended to make thoroughly clear what is believed to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;be a consequence of the rest of this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  8. If the distribution and/or use of the Program is restricted in&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;certain countries either by patents or by copyrighted interfaces, the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;original copyright holder who places the Program under this License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;may add an explicit geographical distribution limitation excluding&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;those countries, so that distribution is permitted only in or among&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;countries not thus excluded.  In such case, this License incorporates&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the limitation as if written in the body of this License.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  9. The Free Software Foundation may publish revised and/or new versions&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;of the General Public License from time to time.  Such new versions will&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;be similar in spirit to the present version, but may differ in detail to&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;address new problems or concerns.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Each version is given a distinguishing version number.  If the Program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;specifies a version number of this License which applies to it and &quot;any&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;later version&quot;, you have the option of following the terms and conditions&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;either of that version or of any later version published by the Free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Software Foundation.  If the Program does not specify a version number of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;this License, you may choose any version ever published by the Free Software&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Foundation.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  10. If you wish to incorporate parts of the Program into other free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;programs whose distribution conditions are different, write to the author&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;to ask for permission.  For software which is copyrighted by the Free&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Software Foundation, write to the Free Software Foundation; we sometimes&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;make exceptions for this.  Our decision will be guided by the two goals&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;of preserving the free status of all derivatives of our free software and&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;of promoting the sharing and reuse of software generally.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;			    NO WARRANTY&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;PROVIDE THE PROGRAM &quot;AS IS&quot; WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;REPAIR OR CORRECTION.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;POSSIBILITY OF SUCH DAMAGES.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;		     END OF TERMS AND CONDITIONS&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;	    How to Apply These Terms to Your New Programs&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  If you develop a new program, and you want it to be of the greatest&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;possible use to the public, the best way to achieve this is to make it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;free software which everyone can redistribute and change under these terms.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  To do so, attach the following notices to the program.  It is safest&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;to attach them to the start of each source file to most effectively&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;convey the exclusion of warranty; and each file should have at least&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the &quot;copyright&quot; line and a pointer to where the full notice is found.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    &amp;lt;one line to give the program&apos;s name and a brief idea of what it does.&amp;gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Copyright (C) &amp;lt;year&amp;gt;  &amp;lt;name of author&amp;gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    This program is free software; you can redistribute it and/or modify&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    it under the terms of the GNU General Public License as published by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    the Free Software Foundation; either version 2 of the License, or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    (at your option) any later version.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    This program is distributed in the hope that it will be useful,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    but WITHOUT ANY WARRANTY; without even the implied warranty of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    GNU General Public License for more details.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    You should have received a copy of the GNU General Public License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    along with this program; if not, write to the Free Software&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Also add information on how to contact you by electronic and paper mail.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If the program is interactive, make it output a short notice like this&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;when it starts in an interactive mode:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Gnomovision version 69, Copyright (C) year name of author&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type `show w&apos;.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    This is free software, and you are welcome to redistribute it&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    under certain conditions; type `show c&apos; for details.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The hypothetical commands `show w&apos; and `show c&apos; should show the appropriate&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;parts of the General Public License.  Of course, the commands you use may&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;be called something other than `show w&apos; and `show c&apos;; they could even be&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;mouse-clicks or menu items--whatever suits your program.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You should also get your employer (if you work as a programmer) or your&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;school, if any, to sign a &quot;copyright disclaimer&quot; for the program, if&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;necessary.  Here is a sample; alter the names:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  Yoyodyne, Inc., hereby disclaims all copyright interest in the program&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  `Gnomovision&apos; (which makes passes at compilers) written by James Hacker.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  &amp;lt;signature of Ty Coon&amp;gt;, 1 April 1989&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  Ty Coon, President of Vice&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This General Public License does not permit incorporating your program into&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;proprietary programs.  If your program is a subroutine library, you may&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;consider it more useful to permit linking proprietary applications with the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;library.  If this is what you want to do, use the GNU Library General&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Public License instead of this License.&lt;/p&gt;&lt;/body&gt;&lt;/html</translation>
    </message>
    <message>
        <location filename="../dlg_license.ui" line="379"/>
        <location filename="../ui_dlg_license.h" line="476"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>dlg_login</name>
    <message>
        <location filename="../dlg_login.ui" line="28"/>
        <location filename="../dlg_login.ui" line="368"/>
        <location filename="../ui_dlg_login.h" line="238"/>
        <location filename="../ui_dlg_login.h" line="267"/>
        <source>Log in</source>
        <translation>Entrar</translation>
    </message>
    <message>
        <location filename="../dlg_login.ui" line="77"/>
        <location filename="../ui_dlg_login.h" line="239"/>
        <source>Serveraddress</source>
        <translation>Dirección del Servidor</translation>
    </message>
    <message>
        <location filename="../dlg_login.ui" line="87"/>
        <location filename="../ui_dlg_login.h" line="240"/>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <location filename="../dlg_login.ui" line="97"/>
        <location filename="../ui_dlg_login.h" line="241"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../dlg_login.ui" line="114"/>
        <location filename="../ui_dlg_login.h" line="242"/>
        <source>Port</source>
        <translation>Puerto</translation>
    </message>
    <message>
        <location filename="../dlg_login.ui" line="121"/>
        <location filename="../ui_dlg_login.h" line="243"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../dlg_login.ui" line="264"/>
        <location filename="../ui_dlg_login.h" line="245"/>
        <source>Please enter address and
user account for your scan
engine.

If you select one of the
profiles, you only need to
enter the password.

Before you press the log in
button you may store the
access profile.

Note, that the scan engine
must have OMP support
enabled for the given port
for a successful connection.</source>
        <translation>Por favor ingrese dirección
y cuenta de usuario de su
motor de escaneos.

Si selecciona uno de esots
perfiles, solo necesitará
ingresar la password.

Antes de presiona el botón
Entrar, puede guardar su
perfil de acceso.

Note, que el motor de escaneo
tiene que tener soporte OMP
habilitado para el puerto en cuestión
para una conexión exitosa.</translation>
    </message>
    <message>
        <location filename="../dlg_login.ui" line="314"/>
        <location filename="../ui_dlg_login.h" line="261"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
    <message>
        <location filename="../dlg_login.ui" line="324"/>
        <location filename="../ui_dlg_login.h" line="262"/>
        <source>9390</source>
        <translation>9390</translation>
    </message>
    <message>
        <location filename="../dlg_login.ui" line="338"/>
        <location filename="../ui_dlg_login.h" line="264"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../dlg_login.ui" line="351"/>
        <location filename="../ui_dlg_login.h" line="265"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="309"/>
        <source>Please enter: 
</source>
        <translation>Favor ingrese:
</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="313"/>
        <source>   - Serveraddress                 
</source>
        <translation>-Dirección del servidor              
</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="318"/>
        <source>   - Serverport                    
</source>
        <translation>   - Puerto del servidor                 
</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="323"/>
        <source>   - Username                      
</source>
        <translation>   - Usuario                   
</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="328"/>
        <source>   - Password                      
</source>
        <translation>   - Contraseña               
</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="333"/>
        <source>Connection failed...</source>
        <translation>Falló la conexión...</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="334"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
</context>
<context>
    <name>dlg_modify_config</name>
    <message>
        <location filename="../dlg_modify_config.ui" line="16"/>
        <location filename="../ui_dlg_modify_config.h" line="143"/>
        <source>Modify Scan Config</source>
        <translation>Modificar configuración de escaneos</translation>
    </message>
    <message>
        <location filename="../dlg_modify_config.ui" line="26"/>
        <location filename="../ui_dlg_modify_config.h" line="144"/>
        <source>Select all NVTs:</source>
        <translation>Seleccionar todos los NVTs:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_config.ui" line="56"/>
        <location filename="../ui_dlg_modify_config.h" line="146"/>
        <source>Trend:</source>
        <translation>Tendencia:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_config.ui" line="98"/>
        <location filename="../ui_dlg_modify_config.h" line="149"/>
        <source>Add Family:</source>
        <translation>Agregar familia:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_config.ui" line="105"/>
        <location filename="../ui_dlg_modify_config.h" line="150"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../dlg_modify_config.ui" line="112"/>
        <location filename="../ui_dlg_modify_config.h" line="151"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_config.ui" line="126"/>
        <location filename="../ui_dlg_modify_config.h" line="153"/>
        <source>Add</source>
        <translation>Agregar</translation>
    </message>
    <message>
        <location filename="../dlg_modify_config.ui" line="136"/>
        <location filename="../ui_dlg_modify_config.h" line="154"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
</context>
<context>
    <name>dlg_modify_credential</name>
    <message>
        <location filename="../dlg_modify_credential.ui" line="16"/>
        <location filename="../ui_dlg_modify_credential.h" line="123"/>
        <source>Modify Credential</source>
        <translation>Modificar Credencial</translation>
    </message>
    <message>
        <location filename="../dlg_modify_credential.ui" line="26"/>
        <location filename="../ui_dlg_modify_credential.h" line="124"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_credential.ui" line="36"/>
        <location filename="../ui_dlg_modify_credential.h" line="125"/>
        <source>Comment:</source>
        <translation>Comentario:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_credential.ui" line="46"/>
        <location filename="../ui_dlg_modify_credential.h" line="126"/>
        <source>Login:</source>
        <translation>Entrar:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_credential.ui" line="56"/>
        <location filename="../ui_dlg_modify_credential.h" line="127"/>
        <source>Password:</source>
        <translation>Password:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_credential.ui" line="83"/>
        <location filename="../ui_dlg_modify_credential.h" line="128"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
    <message>
        <location filename="../dlg_modify_credential.ui" line="90"/>
        <location filename="../ui_dlg_modify_credential.h" line="129"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
</context>
<context>
    <name>dlg_modify_family</name>
    <message>
        <location filename="../dlg_modify_family.ui" line="13"/>
        <location filename="../ui_dlg_modify_family.h" line="116"/>
        <source>Modify Family</source>
        <translation>Modificar Familia</translation>
    </message>
    <message>
        <location filename="../dlg_modify_family.ui" line="26"/>
        <location filename="../ui_dlg_modify_family.h" line="117"/>
        <source>Add</source>
        <translation>Agregar</translation>
    </message>
    <message>
        <location filename="../dlg_modify_family.ui" line="33"/>
        <location filename="../ui_dlg_modify_family.h" line="118"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../dlg_modify_family.ui" line="40"/>
        <location filename="../ui_dlg_modify_family.h" line="119"/>
        <source>Family:</source>
        <translation>Familia:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_family.ui" line="54"/>
        <location filename="../ui_dlg_modify_family.h" line="121"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
    <message>
        <location filename="../dlg_modify_family.ui" line="87"/>
        <location filename="../ui_dlg_modify_family.h" line="122"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_family.ui" line="94"/>
        <location filename="../ui_dlg_modify_family.h" line="123"/>
        <source>Selected</source>
        <translation>Seleccionado</translation>
    </message>
</context>
<context>
    <name>dlg_modify_note</name>
    <message>
        <location filename="../dlg_modify_note.ui" line="13"/>
        <location filename="../ui_dlg_modify_note.h" line="296"/>
        <source>Dialog</source>
        <translation>Dialogo</translation>
    </message>
    <message>
        <location filename="../dlg_modify_note.ui" line="19"/>
        <location filename="../ui_dlg_modify_note.h" line="297"/>
        <source>NVT OID:</source>
        <translation>OID de NVT:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_note.ui" line="26"/>
        <location filename="../ui_dlg_modify_note.h" line="298"/>
        <source>NVT Name:</source>
        <translation>Nombre NVT:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_note.ui" line="40"/>
        <location filename="../ui_dlg_modify_note.h" line="299"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../dlg_modify_note.ui" line="57"/>
        <location filename="../ui_dlg_modify_note.h" line="300"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../dlg_modify_note.ui" line="81"/>
        <location filename="../ui_dlg_modify_note.h" line="303"/>
        <source>Hosts:</source>
        <translation>Hosts:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_note.ui" line="98"/>
        <location filename="../dlg_modify_note.ui" line="154"/>
        <location filename="../dlg_modify_note.ui" line="239"/>
        <location filename="../dlg_modify_note.ui" line="269"/>
        <location filename="../dlg_modify_note.ui" line="325"/>
        <location filename="../ui_dlg_modify_note.h" line="305"/>
        <location filename="../ui_dlg_modify_note.h" line="308"/>
        <location filename="../ui_dlg_modify_note.h" line="311"/>
        <location filename="../ui_dlg_modify_note.h" line="314"/>
        <location filename="../ui_dlg_modify_note.h" line="317"/>
        <source>Any</source>
        <translation>Todo</translation>
    </message>
    <message>
        <location filename="../dlg_modify_note.ui" line="137"/>
        <location filename="../ui_dlg_modify_note.h" line="306"/>
        <source>Port:</source>
        <translation>Puerto:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_note.ui" line="193"/>
        <location filename="../ui_dlg_modify_note.h" line="309"/>
        <source>Threat:</source>
        <translation>Amenaza:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_note.ui" line="252"/>
        <location filename="../ui_dlg_modify_note.h" line="312"/>
        <source>Task:</source>
        <translation>Tarea:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_note.ui" line="308"/>
        <location filename="../ui_dlg_modify_note.h" line="315"/>
        <source>Result:</source>
        <translation>Resultado:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_note.ui" line="361"/>
        <location filename="../ui_dlg_modify_note.h" line="318"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
</context>
<context>
    <name>dlg_modify_nvt</name>
    <message>
        <location filename="../dlg_modify_nvt.ui" line="13"/>
        <location filename="../ui_dlg_modify_nvt.h" line="159"/>
        <source>Modify NVT Preferences</source>
        <translation>Modificar preferencias de NVT</translation>
    </message>
    <message>
        <location filename="../dlg_modify_nvt.ui" line="23"/>
        <location filename="../ui_dlg_modify_nvt.h" line="160"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../dlg_modify_nvt.ui" line="46"/>
        <location filename="../ui_dlg_modify_nvt.h" line="162"/>
        <source>Preference:</source>
        <translation>Preferencia:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_nvt.ui" line="53"/>
        <location filename="../ui_dlg_modify_nvt.h" line="163"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_nvt.ui" line="73"/>
        <location filename="../ui_dlg_modify_nvt.h" line="164"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
    <message>
        <location filename="../dlg_modify_nvt.ui" line="105"/>
        <location filename="../ui_dlg_modify_nvt.h" line="165"/>
        <source>Browse...</source>
        <translation>Explorar...</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.h" line="720"/>
        <source>Open File ...</source>
        <translation>Abrir Archivo ...</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.h" line="722"/>
        <source>All files *.*</source>
        <translation>Todos los archivos *.*</translation>
    </message>
</context>
<context>
    <name>dlg_modify_override</name>
    <message>
        <location filename="../dlg_modify_override.ui" line="16"/>
        <location filename="../ui_dlg_modify_override.h" line="285"/>
        <source>Modify Override</source>
        <translation>Modificar Sobrepasos</translation>
    </message>
    <message>
        <location filename="../dlg_modify_override.ui" line="36"/>
        <location filename="../dlg_modify_override.ui" line="49"/>
        <location filename="../dlg_modify_override.ui" line="69"/>
        <location filename="../dlg_modify_override.ui" line="105"/>
        <location filename="../dlg_modify_override.ui" line="132"/>
        <location filename="../ui_dlg_modify_override.h" line="287"/>
        <location filename="../ui_dlg_modify_override.h" line="288"/>
        <location filename="../ui_dlg_modify_override.h" line="290"/>
        <location filename="../ui_dlg_modify_override.h" line="293"/>
        <location filename="../ui_dlg_modify_override.h" line="296"/>
        <source>Any</source>
        <translation>Todo</translation>
    </message>
    <message>
        <location filename="../dlg_modify_override.ui" line="172"/>
        <location filename="../ui_dlg_modify_override.h" line="299"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../dlg_modify_override.ui" line="179"/>
        <location filename="../ui_dlg_modify_override.h" line="300"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
    <message>
        <location filename="../dlg_modify_override.ui" line="205"/>
        <location filename="../ui_dlg_modify_override.h" line="301"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../dlg_modify_override.ui" line="212"/>
        <location filename="../ui_dlg_modify_override.h" line="302"/>
        <source>Result</source>
        <translation>Resultado</translation>
    </message>
    <message>
        <location filename="../dlg_modify_override.ui" line="219"/>
        <location filename="../ui_dlg_modify_override.h" line="303"/>
        <source>Task:</source>
        <translation>Tarea:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_override.ui" line="232"/>
        <location filename="../ui_dlg_modify_override.h" line="304"/>
        <source>New Threat:</source>
        <translation>Nueva amenaza:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_override.ui" line="239"/>
        <location filename="../ui_dlg_modify_override.h" line="305"/>
        <source>Threat:</source>
        <translation>Amenaza:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_override.ui" line="246"/>
        <location filename="../ui_dlg_modify_override.h" line="306"/>
        <source>Port:</source>
        <translation>Puerto:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_override.ui" line="253"/>
        <location filename="../ui_dlg_modify_override.h" line="307"/>
        <source>Hosts:</source>
        <translation>Hosts:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_override.ui" line="260"/>
        <location filename="../ui_dlg_modify_override.h" line="308"/>
        <source>NVT OID:</source>
        <translation>OID de NVT:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_override.ui" line="267"/>
        <location filename="../ui_dlg_modify_override.h" line="309"/>
        <source>NVT Name:</source>
        <translation>Nombre NVT:</translation>
    </message>
</context>
<context>
    <name>dlg_modify_scan_pref</name>
    <message>
        <location filename="../dlg_modify_scan_pref.ui" line="13"/>
        <location filename="../ui_dlg_modify_scan_pref.h" line="131"/>
        <source>Modify Scanner Preferences</source>
        <translation>Modificar preferencias del escaner</translation>
    </message>
    <message>
        <location filename="../dlg_modify_scan_pref.ui" line="23"/>
        <location filename="../ui_dlg_modify_scan_pref.h" line="132"/>
        <source>Preference:</source>
        <translation>Preferencia:</translation>
    </message>
    <message>
        <location filename="../dlg_modify_scan_pref.ui" line="39"/>
        <location filename="../ui_dlg_modify_scan_pref.h" line="133"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../dlg_modify_scan_pref.ui" line="46"/>
        <location filename="../ui_dlg_modify_scan_pref.h" line="134"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
    <message>
        <location filename="../dlg_modify_scan_pref.ui" line="79"/>
        <location filename="../ui_dlg_modify_scan_pref.h" line="135"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
</context>
<context>
    <name>dlg_modify_task</name>
    <message>
        <location filename="../dlg_modify_task.ui" line="16"/>
        <location filename="../ui_dlg_modify_task.h" line="216"/>
        <source>Modify Task</source>
        <translation>Modificar tarea</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="29"/>
        <location filename="../ui_dlg_modify_task.h" line="217"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="39"/>
        <location filename="../ui_dlg_modify_task.h" line="218"/>
        <source>Comment (optional)</source>
        <translation>Comentario (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="49"/>
        <location filename="../ui_dlg_modify_task.h" line="219"/>
        <source>Scan Config</source>
        <translation>Configuración Escaneos</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="76"/>
        <location filename="../ui_dlg_modify_task.h" line="220"/>
        <source>Schedule (optional)</source>
        <translation>Agenda (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="99"/>
        <location filename="../ui_dlg_modify_task.h" line="221"/>
        <source>Escalator (optional)</source>
        <translation>Escalador (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="119"/>
        <location filename="../ui_dlg_modify_task.h" line="222"/>
        <source>Scan Targets</source>
        <translation>Objetivos del escaneo</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="142"/>
        <location filename="../ui_dlg_modify_task.h" line="223"/>
        <source>Slave (optional)</source>
        <translation>Esclavo (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="152"/>
        <location filename="../ui_dlg_modify_task.h" line="225"/>
        <source>New Scan Config</source>
        <translation>Nueva configuración de escaneo</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="169"/>
        <location filename="../ui_dlg_modify_task.h" line="229"/>
        <source>New Target</source>
        <translation>Nuevo objetivo</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="183"/>
        <location filename="../ui_dlg_modify_task.h" line="233"/>
        <source>New Escalator</source>
        <translation>Nuevo escalador</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="197"/>
        <location filename="../ui_dlg_modify_task.h" line="237"/>
        <source>New Schedule</source>
        <translation>Nueva Agenda</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="211"/>
        <location filename="../ui_dlg_modify_task.h" line="241"/>
        <source>New Slave</source>
        <translation>Nuevo esclavo</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="225"/>
        <location filename="../ui_dlg_modify_task.h" line="244"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../dlg_modify_task.ui" line="232"/>
        <location filename="../ui_dlg_modify_task.h" line="245"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
</context>
<context>
    <name>dlg_new_agent</name>
    <message>
        <location filename="../gsd_dialogs.cpp" line="1128"/>
        <location filename="../gsd_dialogs.cpp" line="1141"/>
        <source>Open File ...</source>
        <translation>Abrir Archivo ...</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="1130"/>
        <location filename="../gsd_dialogs.cpp" line="1143"/>
        <source>All files *.*</source>
        <translation>Todos los archivos *.*</translation>
    </message>
</context>
<context>
    <name>dlg_new_escalator</name>
    <message>
        <location filename="../gsd_dialogs.cpp" line="1002"/>
        <source>Data Error.</source>
        <translation>Error de datos.</translation>
    </message>
    <message>
        <location filename="../gsd_dialogs.cpp" line="1003"/>
        <source>Error while parsing Email addresses!
Please check!</source>
        <translation>Error al parsear dirección de correo!
Por favor revíselo!</translation>
    </message>
</context>
<context>
    <name>dlg_new_note</name>
    <message>
        <location filename="../dlg_new_note.ui" line="13"/>
        <location filename="../ui_dlg_new_note.h" line="247"/>
        <source>New Note</source>
        <translation>Nueva Nota</translation>
    </message>
    <message>
        <location filename="../dlg_new_note.ui" line="26"/>
        <location filename="../dlg_new_note.ui" line="59"/>
        <location filename="../dlg_new_note.ui" line="92"/>
        <location filename="../dlg_new_note.ui" line="125"/>
        <location filename="../dlg_new_note.ui" line="158"/>
        <location filename="../ui_dlg_new_note.h" line="248"/>
        <location filename="../ui_dlg_new_note.h" line="250"/>
        <location filename="../ui_dlg_new_note.h" line="252"/>
        <location filename="../ui_dlg_new_note.h" line="254"/>
        <location filename="../ui_dlg_new_note.h" line="256"/>
        <source>Any</source>
        <translation>Todo</translation>
    </message>
    <message>
        <location filename="../dlg_new_note.ui" line="191"/>
        <location filename="../ui_dlg_new_note.h" line="258"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
    <message>
        <location filename="../dlg_new_note.ui" line="198"/>
        <location filename="../ui_dlg_new_note.h" line="259"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="../dlg_new_note.ui" line="218"/>
        <location filename="../ui_dlg_new_note.h" line="260"/>
        <source>Hosts:</source>
        <translation>Hosts:</translation>
    </message>
    <message>
        <location filename="../dlg_new_note.ui" line="225"/>
        <location filename="../ui_dlg_new_note.h" line="261"/>
        <source>Port:</source>
        <translation>Puerto:</translation>
    </message>
    <message>
        <location filename="../dlg_new_note.ui" line="232"/>
        <location filename="../ui_dlg_new_note.h" line="262"/>
        <source>Threat:</source>
        <translation>Amenaza:</translation>
    </message>
    <message>
        <location filename="../dlg_new_note.ui" line="239"/>
        <location filename="../ui_dlg_new_note.h" line="263"/>
        <source>Task:</source>
        <translation>Tarea:</translation>
    </message>
    <message>
        <location filename="../dlg_new_note.ui" line="246"/>
        <location filename="../ui_dlg_new_note.h" line="264"/>
        <source>Result:</source>
        <translation>Resultado:</translation>
    </message>
    <message>
        <location filename="../dlg_new_note.ui" line="253"/>
        <location filename="../ui_dlg_new_note.h" line="265"/>
        <source>Text:</source>
        <translation>Texto:</translation>
    </message>
</context>
<context>
    <name>dlg_new_override</name>
    <message>
        <location filename="../dlg_new_override.ui" line="13"/>
        <location filename="../ui_dlg_new_override.h" line="278"/>
        <source>New Override</source>
        <translation>Nuevo Sobrepaso</translation>
    </message>
    <message>
        <location filename="../dlg_new_override.ui" line="23"/>
        <location filename="../ui_dlg_new_override.h" line="279"/>
        <source>Hosts:</source>
        <translation>Hosts:</translation>
    </message>
    <message>
        <location filename="../dlg_new_override.ui" line="33"/>
        <location filename="../dlg_new_override.ui" line="73"/>
        <location filename="../dlg_new_override.ui" line="113"/>
        <location filename="../dlg_new_override.ui" line="153"/>
        <location filename="../dlg_new_override.ui" line="193"/>
        <location filename="../ui_dlg_new_override.h" line="280"/>
        <location filename="../ui_dlg_new_override.h" line="283"/>
        <location filename="../ui_dlg_new_override.h" line="286"/>
        <location filename="../ui_dlg_new_override.h" line="289"/>
        <location filename="../ui_dlg_new_override.h" line="292"/>
        <source>Any</source>
        <translation>Todo</translation>
    </message>
    <message>
        <location filename="../dlg_new_override.ui" line="63"/>
        <location filename="../ui_dlg_new_override.h" line="282"/>
        <source>Port:</source>
        <translation>Puerto:</translation>
    </message>
    <message>
        <location filename="../dlg_new_override.ui" line="103"/>
        <location filename="../ui_dlg_new_override.h" line="285"/>
        <source>Threat:</source>
        <translation>Amenaza:</translation>
    </message>
    <message>
        <location filename="../dlg_new_override.ui" line="143"/>
        <location filename="../ui_dlg_new_override.h" line="288"/>
        <source>Task:</source>
        <translation>Tarea:</translation>
    </message>
    <message>
        <location filename="../dlg_new_override.ui" line="183"/>
        <location filename="../ui_dlg_new_override.h" line="291"/>
        <source>Result:</source>
        <translation>Resultado:</translation>
    </message>
    <message>
        <location filename="../dlg_new_override.ui" line="223"/>
        <location filename="../ui_dlg_new_override.h" line="294"/>
        <source>Text:</source>
        <translation>Texto:</translation>
    </message>
    <message>
        <location filename="../dlg_new_override.ui" line="259"/>
        <location filename="../ui_dlg_new_override.h" line="295"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
    <message>
        <location filename="../dlg_new_override.ui" line="266"/>
        <location filename="../ui_dlg_new_override.h" line="296"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="../dlg_new_override.ui" line="301"/>
        <location filename="../ui_dlg_new_override.h" line="297"/>
        <source>New Threat:</source>
        <translation>Nueva amenaza:</translation>
    </message>
</context>
<context>
    <name>dlg_new_slave</name>
    <message>
        <location filename="../dlg_new_slave.ui" line="13"/>
        <location filename="../ui_dlg_new_slave.h" line="149"/>
        <source>New Slave</source>
        <translation>Nuevo esclavo</translation>
    </message>
    <message>
        <location filename="../dlg_new_slave.ui" line="26"/>
        <location filename="../ui_dlg_new_slave.h" line="150"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dlg_new_slave.ui" line="36"/>
        <location filename="../ui_dlg_new_slave.h" line="151"/>
        <source>Comment (optional):</source>
        <translation>Comentario (opcional):</translation>
    </message>
    <message>
        <location filename="../dlg_new_slave.ui" line="46"/>
        <location filename="../ui_dlg_new_slave.h" line="152"/>
        <source>Host:</source>
        <translation>Hosts:</translation>
    </message>
    <message>
        <location filename="../dlg_new_slave.ui" line="56"/>
        <location filename="../ui_dlg_new_slave.h" line="153"/>
        <source>Port:</source>
        <translation>Puerto:</translation>
    </message>
    <message>
        <location filename="../dlg_new_slave.ui" line="63"/>
        <location filename="../ui_dlg_new_slave.h" line="154"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
    <message>
        <location filename="../dlg_new_slave.ui" line="70"/>
        <location filename="../ui_dlg_new_slave.h" line="155"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="../dlg_new_slave.ui" line="90"/>
        <location filename="../ui_dlg_new_slave.h" line="156"/>
        <source>Login:</source>
        <translation>Entrar:</translation>
    </message>
    <message>
        <location filename="../dlg_new_slave.ui" line="97"/>
        <location filename="../ui_dlg_new_slave.h" line="157"/>
        <source>Password:</source>
        <translation>Password:</translation>
    </message>
</context>
<context>
    <name>dlg_start_gsa</name>
    <message>
        <location filename="../dlg_start_gsa.ui" line="16"/>
        <location filename="../ui_dlg_start_gsa.h" line="104"/>
        <source>Start Greenbone Security Assistant</source>
        <translation>Iniciar Greenbone Security Assistant</translation>
    </message>
    <message>
        <location filename="../dlg_start_gsa.ui" line="26"/>
        <location filename="../ui_dlg_start_gsa.h" line="105"/>
        <source>Greenbone Security Assistant (GSA) is an alternative, web-based client.
It offers extended features for management and administration.
</source>
        <translation>El Greenbone Security Assistant es cliente alternativo basado en web.
Ofrece funciones adicionales para el gerenciamiento y administración.
</translation>
    </message>
    <message>
        <location filename="../dlg_start_gsa.ui" line="35"/>
        <location filename="../ui_dlg_start_gsa.h" line="108"/>
        <source>GSA service typically runs SSL-secured on the same server you are currently
connected to, either at port 443 (default for HTTPS) or at port 9392.
But you can also configure another URL which will be stored for this desktop
profile.</source>
        <translation>El servicio GSA habitualmente corre securitizado con SSL en el mismo servidor 
al cual se encuentra conectado ahora, ya sea por el puerto 443 (Default para https)
o en el puerto 9392. También puede configurar otra URL que sera habilitada para
este perfil de escritorio.</translation>
    </message>
    <message>
        <location filename="../dlg_start_gsa.ui" line="45"/>
        <location filename="../ui_dlg_start_gsa.h" line="112"/>
        <source>Open GSA in default web browser</source>
        <translation>Abrir GSA en el explorador web por defecto</translation>
    </message>
    <message>
        <location filename="../dlg_start_gsa.ui" line="52"/>
        <location filename="../ui_dlg_start_gsa.h" line="113"/>
        <source>Default</source>
        <translation>Por defecto</translation>
    </message>
</context>
<context>
    <name>dock_details_config</name>
    <message>
        <location filename="../dock_details_config.ui" line="17"/>
        <location filename="../ui_dock_details_config.h" line="189"/>
        <source>Config</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <location filename="../dock_details_config.ui" line="24"/>
        <location filename="../ui_dock_details_config.h" line="194"/>
        <source>Comment:</source>
        <translation>Comentario:</translation>
    </message>
    <message>
        <location filename="../dock_details_config.ui" line="38"/>
        <location filename="../ui_dock_details_config.h" line="196"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dock_details_config.ui" line="56"/>
        <location filename="../ui_dock_details_config.h" line="198"/>
        <source>NVT Families</source>
        <translation>Familias NVT</translation>
    </message>
    <message>
        <location filename="../dock_details_config.ui" line="85"/>
        <location filename="../ui_dock_details_config.h" line="199"/>
        <source>Scanner Preferences</source>
        <translation>Preferencias de Escaner</translation>
    </message>
    <message>
        <location filename="../dock_details_config.ui" line="111"/>
        <location filename="../ui_dock_details_config.h" line="200"/>
        <source>NVT Preferences</source>
        <translation>Preferencias de NVT</translation>
    </message>
    <message>
        <location filename="../dock_details_config.ui" line="143"/>
        <location filename="../ui_dock_details_config.h" line="201"/>
        <source>Tasks</source>
        <translation>Tareas</translation>
    </message>
    <message>
        <location filename="../dock_details_config.ui" line="193"/>
        <location filename="../ui_dock_details_config.h" line="190"/>
        <source>Modify Family</source>
        <translation>Modificar Familia</translation>
    </message>
    <message>
        <location filename="../dock_details_config.ui" line="202"/>
        <location filename="../ui_dock_details_config.h" line="191"/>
        <source>Modify Scan Preferences</source>
        <translation>Modificar preferencias del escaneo</translation>
    </message>
    <message>
        <location filename="../dock_details_config.ui" line="211"/>
        <location filename="../ui_dock_details_config.h" line="192"/>
        <source>Details Family</source>
        <translation>Familia de Detalles</translation>
    </message>
    <message>
        <location filename="../dock_details_config.ui" line="220"/>
        <location filename="../ui_dock_details_config.h" line="193"/>
        <source>Modify NVT Preferences</source>
        <translation>Modificar preferencias de NVT</translation>
    </message>
</context>
<context>
    <name>dock_details_credential</name>
    <message>
        <location filename="../dock_details_credential.ui" line="17"/>
        <location filename="../ui_dock_details_credential.h" line="150"/>
        <source>Credential</source>
        <translation>Credencial</translation>
    </message>
    <message>
        <location filename="../dock_details_credential.ui" line="39"/>
        <location filename="../ui_dock_details_credential.h" line="151"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dock_details_credential.ui" line="59"/>
        <location filename="../ui_dock_details_credential.h" line="153"/>
        <source>Comment:</source>
        <translation>Comentario:</translation>
    </message>
    <message>
        <location filename="../dock_details_credential.ui" line="79"/>
        <location filename="../ui_dock_details_credential.h" line="155"/>
        <source>Login:</source>
        <translation>Entrar:</translation>
    </message>
    <message>
        <location filename="../dock_details_credential.ui" line="105"/>
        <location filename="../ui_dock_details_credential.h" line="157"/>
        <source>Targets using this Credential</source>
        <translation>Objetivos usando esta Credencial</translation>
    </message>
</context>
<context>
    <name>dock_details_escalator</name>
    <message>
        <location filename="../dock_details_escalator.ui" line="17"/>
        <location filename="../ui_dock_details_escalator.h" line="224"/>
        <source>Escalator</source>
        <translation>Escalador</translation>
    </message>
    <message>
        <location filename="../dock_details_escalator.ui" line="28"/>
        <location filename="../ui_dock_details_escalator.h" line="238"/>
        <source>Summary</source>
        <translation>Resumen</translation>
    </message>
    <message>
        <location filename="../dock_details_escalator.ui" line="49"/>
        <location filename="../ui_dock_details_escalator.h" line="225"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dock_details_escalator.ui" line="69"/>
        <location filename="../ui_dock_details_escalator.h" line="227"/>
        <source>Comment:</source>
        <translation>Comentario:</translation>
    </message>
    <message>
        <location filename="../dock_details_escalator.ui" line="86"/>
        <location filename="../ui_dock_details_escalator.h" line="229"/>
        <source>Condition:</source>
        <translation>Condición:</translation>
    </message>
    <message>
        <location filename="../dock_details_escalator.ui" line="106"/>
        <location filename="../ui_dock_details_escalator.h" line="231"/>
        <source>Event:</source>
        <translation>Evento:</translation>
    </message>
    <message>
        <location filename="../dock_details_escalator.ui" line="126"/>
        <location filename="../ui_dock_details_escalator.h" line="233"/>
        <source>Method:</source>
        <translation>Método:</translation>
    </message>
    <message>
        <location filename="../dock_details_escalator.ui" line="231"/>
        <location filename="../ui_dock_details_escalator.h" line="239"/>
        <source>Tasks using this Escalator</source>
        <translation>Tareas usando este Escalador</translation>
    </message>
    <message>
        <location filename="../dock_details.cpp" line="566"/>
        <source> (to </source>
        <translation>(a </translation>
    </message>
    <message>
        <location filename="../dock_details.cpp" line="579"/>
        <source>To address: %1</source>
        <translation>dirección Para: %1</translation>
    </message>
    <message>
        <location filename="../dock_details.cpp" line="582"/>
        <source>From address: %1</source>
        <translation>Dirección Desde: %1</translation>
    </message>
    <message>
        <location filename="../dock_details.cpp" line="586"/>
        <source>Format: Summary (can include vulnerability details)</source>
        <translation>Formato: Resumen (puede incluir detalles de vulnerabilidades)</translation>
    </message>
    <message>
        <location filename="../dock_details.cpp" line="592"/>
        <source>Format: Simple Notice</source>
        <translation>Formato: Noticia Simple</translation>
    </message>
</context>
<context>
    <name>dock_details_family</name>
    <message>
        <location filename="../dock_details_family.ui" line="17"/>
        <location filename="../ui_dock_details_family.h" line="135"/>
        <source>Details NVT Family</source>
        <translation>Familia NVT de Detalles</translation>
    </message>
    <message>
        <location filename="../dock_details_family.ui" line="39"/>
        <location filename="../ui_dock_details_family.h" line="138"/>
        <source>Config:</source>
        <translation>Configuración:</translation>
    </message>
    <message>
        <location filename="../dock_details_family.ui" line="59"/>
        <location filename="../ui_dock_details_family.h" line="140"/>
        <source>Family:</source>
        <translation>Familia:</translation>
    </message>
    <message>
        <location filename="../dock_details_family.ui" line="86"/>
        <location filename="../ui_dock_details_family.h" line="142"/>
        <source>Network Vulnerability Tests</source>
        <translation>Pruebas de vulnerabilidad de la red</translation>
    </message>
    <message>
        <location filename="../dock_details_family.ui" line="133"/>
        <location filename="../ui_dock_details_family.h" line="136"/>
        <source>Details NVT</source>
        <translation>Detalles NVT</translation>
    </message>
    <message>
        <location filename="../dock_details_family.ui" line="142"/>
        <location filename="../ui_dock_details_family.h" line="137"/>
        <source>Modify Family</source>
        <translation>Modificar Familia</translation>
    </message>
    <message>
        <location filename="../dock_details.cpp" line="1250"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../dock_details.cpp" line="1251"/>
        <source>OID</source>
        <translation>OID</translation>
    </message>
    <message>
        <location filename="../dock_details.cpp" line="1252"/>
        <source>Risk</source>
        <translation>Riesgo</translation>
    </message>
    <message>
        <location filename="../dock_details.cpp" line="1253"/>
        <source>CVSS</source>
        <translation>CVSS</translation>
    </message>
    <message>
        <location filename="../dock_details.cpp" line="1254"/>
        <source>Timeout</source>
        <translation>Tiempo excedido</translation>
    </message>
    <message>
        <location filename="../dock_details.cpp" line="1255"/>
        <source>Prefs</source>
        <translation>Prefs</translation>
    </message>
</context>
<context>
    <name>dock_details_note</name>
    <message>
        <location filename="../dock_details_note.ui" line="17"/>
        <location filename="../ui_dock_details_note.h" line="259"/>
        <source>Details Note</source>
        <translation>Nota de Detalles</translation>
    </message>
    <message>
        <location filename="../dock_details_note.ui" line="53"/>
        <location filename="../ui_dock_details_note.h" line="260"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../dock_details_note.ui" line="63"/>
        <location filename="../ui_dock_details_note.h" line="261"/>
        <source>Result:</source>
        <translation>Resultado:</translation>
    </message>
    <message>
        <location filename="../dock_details_note.ui" line="76"/>
        <location filename="../ui_dock_details_note.h" line="262"/>
        <source>Threat:</source>
        <translation>Amenaza:</translation>
    </message>
    <message>
        <location filename="../dock_details_note.ui" line="89"/>
        <location filename="../ui_dock_details_note.h" line="263"/>
        <source>Port:</source>
        <translation>Puerto:</translation>
    </message>
    <message>
        <location filename="../dock_details_note.ui" line="109"/>
        <location filename="../ui_dock_details_note.h" line="264"/>
        <source>Application</source>
        <translation>Aplicación</translation>
    </message>
    <message>
        <location filename="../dock_details_note.ui" line="154"/>
        <location filename="../ui_dock_details_note.h" line="269"/>
        <source>Task:</source>
        <translation>Tarea:</translation>
    </message>
    <message>
        <location filename="../dock_details_note.ui" line="190"/>
        <location filename="../ui_dock_details_note.h" line="271"/>
        <source>NVT Name:</source>
        <translation>Nombre NVT:</translation>
    </message>
    <message>
        <location filename="../dock_details_note.ui" line="207"/>
        <location filename="../ui_dock_details_note.h" line="273"/>
        <source>NVT OID:</source>
        <translation>OID de NVT:</translation>
    </message>
    <message>
        <location filename="../dock_details_note.ui" line="224"/>
        <location filename="../ui_dock_details_note.h" line="275"/>
        <source>Created:</source>
        <translation>Creado:</translation>
    </message>
    <message>
        <location filename="../dock_details_note.ui" line="241"/>
        <location filename="../ui_dock_details_note.h" line="277"/>
        <source>Last Modified:</source>
        <translation>Última modificación:</translation>
    </message>
    <message>
        <location filename="../dock_details_note.ui" line="258"/>
        <location filename="../ui_dock_details_note.h" line="279"/>
        <source>Hosts:</source>
        <translation>Hosts:</translation>
    </message>
</context>
<context>
    <name>dock_details_nvt</name>
    <message>
        <location filename="../dock_details_nvt.ui" line="17"/>
        <location filename="../ui_dock_details_nvt.h" line="266"/>
        <source>NVT</source>
        <translation>NVT</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="28"/>
        <location filename="../ui_dock_details_nvt.h" line="288"/>
        <source>Summary</source>
        <translation>Resumen</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="55"/>
        <location filename="../ui_dock_details_nvt.h" line="268"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="75"/>
        <location filename="../ui_dock_details_nvt.h" line="270"/>
        <source>Summary:</source>
        <translation>Resumen:</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="89"/>
        <location filename="../ui_dock_details_nvt.h" line="272"/>
        <source>Family:</source>
        <translation>Familia:</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="103"/>
        <location filename="../ui_dock_details_nvt.h" line="274"/>
        <source>OID:</source>
        <translation>OID:</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="117"/>
        <location filename="../ui_dock_details_nvt.h" line="276"/>
        <source>CVE:</source>
        <translation>CVE:</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="134"/>
        <location filename="../ui_dock_details_nvt.h" line="278"/>
        <source>Bugtraq ID:</source>
        <translation>ID en Bugtraq:</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="148"/>
        <location filename="../ui_dock_details_nvt.h" line="280"/>
        <source>Other References:</source>
        <translation>Otras referencias:</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="162"/>
        <location filename="../ui_dock_details_nvt.h" line="282"/>
        <source>Tags:</source>
        <translation>Tags:</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="176"/>
        <location filename="../ui_dock_details_nvt.h" line="284"/>
        <source>CVSS Base:</source>
        <translation>Base CVSS:</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="190"/>
        <location filename="../ui_dock_details_nvt.h" line="286"/>
        <source>Risk factor</source>
        <translation>Factor de riesgo</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="235"/>
        <location filename="../ui_dock_details_nvt.h" line="289"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="249"/>
        <location filename="../ui_dock_details_nvt.h" line="290"/>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="../dock_details_nvt.ui" line="286"/>
        <location filename="../ui_dock_details_nvt.h" line="267"/>
        <source>Modify NVT</source>
        <translation>Modificar NVT</translation>
    </message>
</context>
<context>
    <name>dock_details_override</name>
    <message>
        <location filename="../dock_details_override.ui" line="17"/>
        <location filename="../ui_dock_details_override.h" line="235"/>
        <source>Details Override</source>
        <translation>Sobrepaso Detalles</translation>
    </message>
    <message>
        <location filename="../dock_details_override.ui" line="53"/>
        <location filename="../ui_dock_details_override.h" line="236"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../dock_details_override.ui" line="60"/>
        <location filename="../ui_dock_details_override.h" line="237"/>
        <source>Task:</source>
        <translation>Tarea:</translation>
    </message>
    <message>
        <location filename="../dock_details_override.ui" line="67"/>
        <location filename="../ui_dock_details_override.h" line="238"/>
        <source>Hosts:</source>
        <translation>Hosts:</translation>
    </message>
    <message>
        <location filename="../dock_details_override.ui" line="74"/>
        <location filename="../ui_dock_details_override.h" line="239"/>
        <source>NVT Name:</source>
        <translation>Nombre NVT:</translation>
    </message>
    <message>
        <location filename="../dock_details_override.ui" line="88"/>
        <location filename="../ui_dock_details_override.h" line="241"/>
        <source>NVT OID:</source>
        <translation>OID de NVT:</translation>
    </message>
    <message>
        <location filename="../dock_details_override.ui" line="102"/>
        <location filename="../ui_dock_details_override.h" line="243"/>
        <source>Created:</source>
        <translation>Creado:</translation>
    </message>
    <message>
        <location filename="../dock_details_override.ui" line="116"/>
        <location filename="../ui_dock_details_override.h" line="245"/>
        <source>Last Modified:</source>
        <translation>Última modificación:</translation>
    </message>
    <message>
        <location filename="../dock_details_override.ui" line="137"/>
        <location filename="../ui_dock_details_override.h" line="247"/>
        <source>Application</source>
        <translation>Aplicación</translation>
    </message>
    <message>
        <location filename="../dock_details_override.ui" line="151"/>
        <location filename="../ui_dock_details_override.h" line="249"/>
        <source>Port:</source>
        <translation>Puerto:</translation>
    </message>
    <message>
        <location filename="../dock_details_override.ui" line="165"/>
        <location filename="../ui_dock_details_override.h" line="251"/>
        <source>Threat:</source>
        <translation>Amenaza:</translation>
    </message>
    <message>
        <location filename="../dock_details_override.ui" line="185"/>
        <location filename="../ui_dock_details_override.h" line="253"/>
        <source>New Threat:</source>
        <translation>Nueva amenaza:</translation>
    </message>
    <message>
        <location filename="../dock_details_override.ui" line="199"/>
        <location filename="../ui_dock_details_override.h" line="255"/>
        <source>Result:</source>
        <translation>Resultado:</translation>
    </message>
</context>
<context>
    <name>dock_details_schedule</name>
    <message>
        <location filename="../dock_details_schedule.ui" line="17"/>
        <location filename="../ui_dock_details_schedule.h" line="199"/>
        <source>Details Schedule</source>
        <translation>Detalles Agenda</translation>
    </message>
    <message>
        <location filename="../dock_details_schedule.ui" line="28"/>
        <location filename="../ui_dock_details_schedule.h" line="212"/>
        <source>Summary</source>
        <translation>Resumen</translation>
    </message>
    <message>
        <location filename="../dock_details_schedule.ui" line="49"/>
        <location filename="../ui_dock_details_schedule.h" line="200"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dock_details_schedule.ui" line="69"/>
        <location filename="../ui_dock_details_schedule.h" line="202"/>
        <source>Comment:</source>
        <translation>Comentario:</translation>
    </message>
    <message>
        <location filename="../dock_details_schedule.ui" line="89"/>
        <location filename="../ui_dock_details_schedule.h" line="204"/>
        <source>First Time:</source>
        <translation>Primera vez:</translation>
    </message>
    <message>
        <location filename="../dock_details_schedule.ui" line="109"/>
        <location filename="../ui_dock_details_schedule.h" line="206"/>
        <source>Next Time:</source>
        <translation>Próxima vez:</translation>
    </message>
    <message>
        <location filename="../dock_details_schedule.ui" line="129"/>
        <location filename="../ui_dock_details_schedule.h" line="208"/>
        <source>Period:</source>
        <translation>Período:</translation>
    </message>
    <message>
        <location filename="../dock_details_schedule.ui" line="149"/>
        <location filename="../ui_dock_details_schedule.h" line="210"/>
        <source>Duration:</source>
        <translation>Duración:</translation>
    </message>
    <message>
        <location filename="../dock_details_schedule.ui" line="200"/>
        <location filename="../ui_dock_details_schedule.h" line="213"/>
        <source>Tasks using this Schedule</source>
        <translation>Tareas usando esta Agenda</translation>
    </message>
</context>
<context>
    <name>dock_details_slave</name>
    <message>
        <location filename="../dock_details_slave.ui" line="17"/>
        <location filename="../ui_dock_details_slave.h" line="186"/>
        <source>Details Slave</source>
        <translation>Detalles de Esclavo</translation>
    </message>
    <message>
        <location filename="../dock_details_slave.ui" line="28"/>
        <location filename="../ui_dock_details_slave.h" line="197"/>
        <source>Summary</source>
        <translation>Resumen</translation>
    </message>
    <message>
        <location filename="../dock_details_slave.ui" line="49"/>
        <location filename="../ui_dock_details_slave.h" line="187"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dock_details_slave.ui" line="69"/>
        <location filename="../ui_dock_details_slave.h" line="189"/>
        <source>Comment:</source>
        <translation>Comentario:</translation>
    </message>
    <message>
        <location filename="../dock_details_slave.ui" line="89"/>
        <location filename="../ui_dock_details_slave.h" line="191"/>
        <source>Host:</source>
        <translation>Host:</translation>
    </message>
    <message>
        <location filename="../dock_details_slave.ui" line="109"/>
        <location filename="../ui_dock_details_slave.h" line="193"/>
        <source>Port:</source>
        <translation>Puerto:</translation>
    </message>
    <message>
        <location filename="../dock_details_slave.ui" line="129"/>
        <location filename="../ui_dock_details_slave.h" line="195"/>
        <source>Login:</source>
        <translation>Entrar:</translation>
    </message>
    <message>
        <location filename="../dock_details_slave.ui" line="180"/>
        <location filename="../ui_dock_details_slave.h" line="198"/>
        <source>Tasks using this Slave</source>
        <translation>Tareas usando este esclavo</translation>
    </message>
</context>
<context>
    <name>dock_details_target</name>
    <message>
        <location filename="../dock_details_target.ui" line="17"/>
        <location filename="../ui_dock_details_target.h" line="198"/>
        <source>Details Target</source>
        <translation>Detalles Objetivo</translation>
    </message>
    <message>
        <location filename="../dock_details_target.ui" line="28"/>
        <location filename="../ui_dock_details_target.h" line="211"/>
        <source>Summary</source>
        <translation>Resumen</translation>
    </message>
    <message>
        <location filename="../dock_details_target.ui" line="49"/>
        <location filename="../ui_dock_details_target.h" line="199"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dock_details_target.ui" line="69"/>
        <location filename="../ui_dock_details_target.h" line="201"/>
        <source>Comment:</source>
        <translation>Comentario:</translation>
    </message>
    <message>
        <location filename="../dock_details_target.ui" line="89"/>
        <location filename="../ui_dock_details_target.h" line="203"/>
        <source>Hosts:</source>
        <translation>Hosts:</translation>
    </message>
    <message>
        <location filename="../dock_details_target.ui" line="109"/>
        <location filename="../ui_dock_details_target.h" line="205"/>
        <source>Maximum number of Hosts:</source>
        <translation>Número máximo de Hosts:</translation>
    </message>
    <message>
        <location filename="../dock_details_target.ui" line="129"/>
        <location filename="../ui_dock_details_target.h" line="207"/>
        <source>SSH Credential:</source>
        <translation>Credencial SSH:</translation>
    </message>
    <message>
        <location filename="../dock_details_target.ui" line="175"/>
        <location filename="../ui_dock_details_target.h" line="209"/>
        <source>SMB Credential:</source>
        <translation>Credencial SMB:</translation>
    </message>
    <message>
        <location filename="../dock_details_target.ui" line="194"/>
        <location filename="../ui_dock_details_target.h" line="212"/>
        <source>Tasks using this Target</source>
        <translation>Tareas usando este objetivo</translation>
    </message>
</context>
<context>
    <name>dock_details_task</name>
    <message>
        <location filename="../dock_details_task.ui" line="17"/>
        <location filename="../ui_dock_details_task.h" line="275"/>
        <source>Task Details</source>
        <translation>Detalles de la Tarea</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="28"/>
        <location filename="../ui_dock_details_task.h" line="296"/>
        <source>Summary</source>
        <translation>Resumen</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="49"/>
        <location filename="../ui_dock_details_task.h" line="278"/>
        <source>Name:</source>
        <translation>Nombre:</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="63"/>
        <location filename="../ui_dock_details_task.h" line="280"/>
        <source>Comment:</source>
        <translation>Comentario:</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="77"/>
        <location filename="../ui_dock_details_task.h" line="282"/>
        <source>Config:</source>
        <translation>Configuración:</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="91"/>
        <location filename="../ui_dock_details_task.h" line="284"/>
        <source>Escalator:</source>
        <translation>Escalador:</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="105"/>
        <location filename="../ui_dock_details_task.h" line="286"/>
        <source>Schedule:</source>
        <translation>Agenda:</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="119"/>
        <location filename="../ui_dock_details_task.h" line="288"/>
        <source>Target:</source>
        <translation>Objetivo:</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="133"/>
        <location filename="../ui_dock_details_task.h" line="290"/>
        <source>Status:</source>
        <translation>Estado:</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="147"/>
        <location filename="../ui_dock_details_task.h" line="292"/>
        <source>Reports:</source>
        <translation>Reportes:</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="187"/>
        <location filename="../ui_dock_details_task.h" line="294"/>
        <source>Slave:</source>
        <translation>Esclavo:</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="206"/>
        <location filename="../ui_dock_details_task.h" line="297"/>
        <source>Reports</source>
        <translation>Reportes</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="235"/>
        <location filename="../ui_dock_details_task.h" line="298"/>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="264"/>
        <location filename="../ui_dock_details_task.h" line="299"/>
        <source>Overrides</source>
        <translation>Sobrepasos</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="304"/>
        <location filename="../ui_dock_details_task.h" line="276"/>
        <source>Results</source>
        <translation>Resultados</translation>
    </message>
    <message>
        <location filename="../dock_details_task.ui" line="313"/>
        <location filename="../ui_dock_details_task.h" line="277"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../dock_details.cpp" line="241"/>
        <source>Status Error</source>
        <translation>Estado del error</translation>
    </message>
    <message>
        <location filename="../dock_details.cpp" line="242"/>
        <source>No report selected!</source>
        <translation>No se seleccionaron Reportes!</translation>
    </message>
    <message>
        <location filename="../dock_details.h" line="112"/>
        <source>Report</source>
        <translation>Reporte</translation>
    </message>
    <message>
        <location filename="../dock_details.h" line="113"/>
        <source>Threat</source>
        <translation>Amenaza</translation>
    </message>
    <message>
        <location filename="../dock_details.h" line="114"/>
        <source>High</source>
        <translation>Alto</translation>
    </message>
    <message>
        <location filename="../dock_details.h" line="115"/>
        <source>Medium</source>
        <translation>Medio</translation>
    </message>
    <message>
        <location filename="../dock_details.h" line="116"/>
        <source>Low</source>
        <translation>Bajo</translation>
    </message>
    <message>
        <location filename="../dock_details.h" line="117"/>
        <source>Log</source>
        <translation>Registro</translation>
    </message>
    <message>
        <location filename="../dock_details.h" line="118"/>
        <source>false Pos.</source>
        <translation>falso Pos.</translation>
    </message>
</context>
<context>
    <name>dock_logging</name>
    <message>
        <location filename="../dock_logging.ui" line="25"/>
        <location filename="../ui_dock_logging.h" line="93"/>
        <source>Log</source>
        <translation>Registro</translation>
    </message>
    <message>
        <location filename="../dock_logging.ui" line="44"/>
        <location filename="../ui_dock_logging.h" line="94"/>
        <source>clear</source>
        <translation>limpiar</translation>
    </message>
    <message>
        <location filename="../dock_logging.ui" line="74"/>
        <location filename="../ui_dock_logging.h" line="95"/>
        <source>Log level:</source>
        <translation>Nivel de registro:</translation>
    </message>
</context>
<context>
    <name>dock_performance</name>
    <message>
        <location filename="../dock_performance.ui" line="13"/>
        <location filename="../ui_dock_performance.h" line="169"/>
        <source>Performance</source>
        <translation>Performance</translation>
    </message>
    <message>
        <location filename="../dock_performance.ui" line="20"/>
        <location filename="../ui_dock_performance.h" line="170"/>
        <source>Save image as</source>
        <translation>Guardar imagen como</translation>
    </message>
    <message>
        <location filename="../dock_performance.ui" line="40"/>
        <location filename="../ui_dock_performance.h" line="171"/>
        <source>Size:</source>
        <translation>Tamaño:</translation>
    </message>
    <message>
        <location filename="../dock_performance.ui" line="110"/>
        <location filename="../ui_dock_performance.h" line="173"/>
        <source>Width:</source>
        <translation>Ancho:</translation>
    </message>
    <message>
        <location filename="../dock_performance.ui" line="123"/>
        <location filename="../ui_dock_performance.h" line="174"/>
        <source>refresh</source>
        <translation>actualizar</translation>
    </message>
    <message>
        <location filename="../dock_performance.ui" line="130"/>
        <location filename="../ui_dock_performance.h" line="175"/>
        <source>Height:</source>
        <translation>Altura:</translation>
    </message>
    <message>
        <location filename="../dock_performance.ui" line="143"/>
        <location filename="../dock_performance.ui" line="150"/>
        <location filename="../ui_dock_performance.h" line="176"/>
        <location filename="../ui_dock_performance.h" line="177"/>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <location filename="../dock_performance.cpp" line="41"/>
        <location filename="../dock_performance.cpp" line="178"/>
        <location filename="../dock_performance.cpp" line="196"/>
        <source>hour(s)</source>
        <translation>Hora(s)</translation>
    </message>
    <message>
        <location filename="../dock_performance.cpp" line="42"/>
        <location filename="../dock_performance.cpp" line="180"/>
        <location filename="../dock_performance.cpp" line="198"/>
        <source>day(s)</source>
        <translation>dia(s)</translation>
    </message>
    <message>
        <location filename="../dock_performance.cpp" line="43"/>
        <location filename="../dock_performance.cpp" line="182"/>
        <location filename="../dock_performance.cpp" line="200"/>
        <source>month(s)</source>
        <translation>mes(es)</translation>
    </message>
    <message>
        <location filename="../dock_performance.cpp" line="69"/>
        <source>No system report available.</source>
        <translation>No hay disponibilidad de reportes del sistema.</translation>
    </message>
    <message>
        <location filename="../dock_performance.cpp" line="233"/>
        <source>Save File ...</source>
        <translation>Grabar archivo ...</translation>
    </message>
    <message>
        <location filename="../dock_performance.cpp" line="236"/>
        <source>*.png</source>
        <translation>*.png</translation>
    </message>
</context>
<context>
    <name>dock_reports</name>
    <message>
        <location filename="../dock_reports.ui" line="17"/>
        <location filename="../ui_dock_reports.h" line="358"/>
        <source>Report</source>
        <translation>Reporte</translation>
    </message>
    <message>
        <location filename="../dock_reports.ui" line="226"/>
        <location filename="../ui_dock_reports.h" line="369"/>
        <source>CVSS &gt;=</source>
        <translation>CVSS &gt;=</translation>
    </message>
    <message>
        <location filename="../dock_reports.ui" line="266"/>
        <location filename="../ui_dock_reports.h" line="371"/>
        <source>Apply Overrides</source>
        <translation>Aplicar Sobrepasos</translation>
    </message>
    <message>
        <location filename="../dock_reports.ui" line="273"/>
        <location filename="../ui_dock_reports.h" line="372"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../dock_reports.ui" line="280"/>
        <location filename="../ui_dock_reports.h" line="373"/>
        <source>Results per page:</source>
        <translation>Resultados por página:</translation>
    </message>
    <message>
        <location filename="../dock_reports.ui" line="329"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
    <message>
        <location filename="../dock_reports.ui" line="337"/>
        <location filename="../ui_dock_reports.h" line="375"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../dock_reports.ui" line="354"/>
        <location filename="../ui_dock_reports.h" line="377"/>
        <source>&gt;&gt;</source>
        <translation>&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../dock_reports.ui" line="361"/>
        <location filename="../ui_dock_reports.h" line="378"/>
        <source>&lt;&lt;</source>
        <translation>&lt;&lt;</translation>
    </message>
    <message>
        <location filename="../dock_reports.ui" line="401"/>
        <location filename="../ui_dock_reports.h" line="381"/>
        <source>as</source>
        <translation>como</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="100"/>
        <source>Results %1 - %2 of %3</source>
        <translation>Resultados %1 - %2 de %3</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="118"/>
        <location filename="../dock_reports.cpp" line="271"/>
        <source>Filtered results %1-%2</source>
        <translation>Resultados filtrado %1 - %2</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="120"/>
        <location filename="../dock_reports.cpp" line="273"/>
        <source>All filtered results</source>
        <translation>Todos los resultados filtrados</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="121"/>
        <location filename="../dock_reports.cpp" line="274"/>
        <source>Full report</source>
        <translation>Reporte íntegro</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="131"/>
        <source>Add Note</source>
        <translation>Agregar nota</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="132"/>
        <source>Add Override</source>
        <translation>Agregar Sobrepaso</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="166"/>
        <source>Unable to open file</source>
        <translation>Imposible abrir el archivo</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="230"/>
        <source>Report </source>
        <translation>Reporte</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="231"/>
        <location filename="../dock_reports.cpp" line="235"/>
        <location filename="../dock_reports.cpp" line="239"/>
        <location filename="../dock_reports.cpp" line="243"/>
        <location filename="../dock_reports.cpp" line="247"/>
        <source>%1 of %2</source>
        <translation>%1 de %2</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="283"/>
        <source>port ascending</source>
        <translation>puertos ascendientes</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="284"/>
        <source>port descending</source>
        <translation>Puertos descendente</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="285"/>
        <source>threat ascending</source>
        <translation>riesgo aumentandi</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="286"/>
        <source>threat descending</source>
        <translation>riesgo desceniendo</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="597"/>
        <source>Save File ...</source>
        <translation>Guardar achivo ...</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="600"/>
        <source>%1 files *.%1</source>
        <translation>%1 archivos *.%1</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="662"/>
        <location filename="../dock_reports.cpp" line="680"/>
        <source>File Error</source>
        <translation>Error de archivo</translation>
    </message>
    <message>
        <location filename="../dock_reports.cpp" line="663"/>
        <location filename="../dock_reports.cpp" line="681"/>
        <source>Could not open file!</source>
        <translation>No se pudo abrir el archivo!</translation>
    </message>
</context>
<context>
    <name>dock_table</name>
    <message>
        <location filename="../dock_table.cpp" line="169"/>
        <source>  </source>
        <translation>  </translation>
    </message>
    <message>
        <location filename="../dock_table.cpp" line="196"/>
        <source>Name: </source>
        <translation>Nombre: </translation>
    </message>
    <message>
        <location filename="../dock_table.cpp" line="198"/>
        <source>
Comment: </source>
        <translation>
Comentario: </translation>
    </message>
</context>
<context>
    <name>gsd_config</name>
    <message>
        <location filename="../gsd_config.cpp" line="96"/>
        <location filename="../gsd_config.cpp" line="108"/>
        <source>DOM Error</source>
        <translation>Error DOM</translation>
    </message>
    <message>
        <location filename="../gsd_config.cpp" line="97"/>
        <source>Error in line %1, Column %2: %3</source>
        <translation>Error en líne %1, Columna %2: %3</translation>
    </message>
    <message>
        <location filename="../gsd_config.cpp" line="109"/>
        <source>Not a valid openvas-config file!</source>
        <translation>No es un archivo de configuración de openvas!</translation>
    </message>
    <message>
        <location filename="../gsd_config.cpp" line="177"/>
        <source>ProfileManager</source>
        <translation>ManejadordePerfil</translation>
    </message>
    <message>
        <location filename="../gsd_config.cpp" line="178"/>
        <source>Profile allready exists!</source>
        <translation>Ya existe Perfil!</translation>
    </message>
</context>
<context>
    <name>gsd_control</name>
    <message>
        <location filename="../gsd_control.cpp" line="51"/>
        <location filename="../gsd_control.cpp" line="61"/>
        <location filename="../gsd_control.cpp" line="68"/>
        <location filename="../gsd_control.cpp" line="75"/>
        <location filename="../gsd_control.cpp" line="82"/>
        <location filename="../gsd_control.cpp" line="88"/>
        <location filename="../gsd_control.cpp" line="93"/>
        <location filename="../gsd_control.cpp" line="108"/>
        <location filename="../gsd_control.cpp" line="127"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="53"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="54"/>
        <source>Reports</source>
        <translation>Reportes</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="55"/>
        <source>First</source>
        <translation>Primera</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="56"/>
        <source>Last</source>
        <translation>Última</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="57"/>
        <source>Threat</source>
        <translation>Riesgo</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="58"/>
        <location filename="../gsd_control.cpp" line="70"/>
        <location filename="../gsd_control.cpp" line="72"/>
        <source>Trend</source>
        <translation>Tendencia</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="62"/>
        <source>Hosts</source>
        <translation>Hosts</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="63"/>
        <source>IPs</source>
        <translation>IPs</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="64"/>
        <source>SSH Credential</source>
        <translation>Credencial SSH</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="65"/>
        <source>SMB Credential</source>
        <translation>Credencial SMB</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="69"/>
        <source>Total Families</source>
        <translation>Total Familias</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="71"/>
        <source>Total NVTs</source>
        <translation>Total NVTs</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="76"/>
        <source>First Run</source>
        <translation>Primera corrida</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="77"/>
        <source>Next Run</source>
        <translation>Próxima ejecución</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="78"/>
        <source>Period</source>
        <translation>Período</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="79"/>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="83"/>
        <source>Event</source>
        <translation>Evento</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="84"/>
        <source>Condition</source>
        <translation>Condición</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="85"/>
        <source>Method</source>
        <translation>Método</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="89"/>
        <location filename="../gsd_control.cpp" line="111"/>
        <source>Login</source>
        <translation>Entrar</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="90"/>
        <location filename="../gsd_control.cpp" line="95"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="94"/>
        <location filename="../gsd_control.cpp" line="130"/>
        <source>Trust</source>
        <translation>Confianza</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="98"/>
        <location filename="../gsd_control.cpp" line="102"/>
        <source>NVT</source>
        <translation>NVT</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="99"/>
        <location filename="../gsd_control.cpp" line="105"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="103"/>
        <source>From</source>
        <translation>De</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="104"/>
        <source>To</source>
        <translation>Para</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="109"/>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="110"/>
        <source>Port</source>
        <translation>Puerto</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="128"/>
        <source>Extension</source>
        <translation>Extensión</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="129"/>
        <source>Content Type</source>
        <translation>Tipo de contenido</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="131"/>
        <source>Last verified</source>
        <translation>Última verificación</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="132"/>
        <source>Active</source>
        <translation>Activo</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="983"/>
        <location filename="../gsd_control.cpp" line="1028"/>
        <location filename="../gsd_control.cpp" line="1073"/>
        <location filename="../gsd_control.cpp" line="1118"/>
        <location filename="../gsd_control.cpp" line="1163"/>
        <location filename="../gsd_control.cpp" line="1208"/>
        <location filename="../gsd_control.cpp" line="1254"/>
        <location filename="../gsd_control.cpp" line="1300"/>
        <location filename="../gsd_control.cpp" line="1366"/>
        <location filename="../gsd_control.cpp" line="1412"/>
        <location filename="../gsd_control.cpp" line="1651"/>
        <source>Delete?</source>
        <translation>Borrar?</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="984"/>
        <source>Do you really want delete the Task :&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Realmente quiere eliminar la Tarea : &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1029"/>
        <source>Do you really want delete the Config:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Realmente quiere borrar la Configuración:&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1074"/>
        <source>Do you really want delete the Target:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Realmente quiere eliminar el objetivo: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1119"/>
        <source>Do you really want delete the Schedule:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Realmente quiere eliminar esta Agenda:&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1164"/>
        <source>Do you really want delete the Escalator:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Realmente quiere borrar el Escalador:&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1209"/>
        <source>Do you really want delete the Credential:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Realmente quiere borrar la Credencial:&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1255"/>
        <source>Do you really want delete the Agent:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Realmente quiere borrar el Agente:&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1301"/>
        <source>Do you really want delete the Note:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Realmente quiere borrar la Nota:&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1367"/>
        <source>Do you really want delete the Override:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Realmente quiere borrar el Sobrepaso: &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1413"/>
        <source>Do you really want delete the Slave:&lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Realmente quiere eliminar el Esclavo:&lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1599"/>
        <location filename="../gsd_control.cpp" line="1704"/>
        <source>Report format</source>
        <translation>Formato de Reporte</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1600"/>
        <source>HTML or XML format not active!</source>
        <translation>Formato HTML o XML inactivo!</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1652"/>
        <source>Do you really want delete this Report?</source>
        <translation>Realmente quiere eliminar este Reporte?</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1705"/>
        <source>Report format not active!</source>
        <translation>Formato de Reportes no activo!</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1765"/>
        <location filename="../gsd_control.cpp" line="1832"/>
        <location filename="../gsd_control.cpp" line="1838"/>
        <location filename="../gsd_control.cpp" line="1844"/>
        <location filename="../gsd_control.cpp" line="1850"/>
        <location filename="../gsd_control.cpp" line="1856"/>
        <source>Login Error</source>
        <translation>Error de ingreso</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1766"/>
        <source>Please login first!</source>
        <translation>Por favor, ingrese primero!</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1833"/>
        <source>Server closed connection!</source>
        <translation>El servidor cerró la conexión!</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1839"/>
        <source>Authentication failed!</source>
        <translation>Falló la autenticación!</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1845"/>
        <source>Failed to get server address!</source>
        <translation>Falla al obtener dirección del servidor!</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="1851"/>
        <location filename="../gsd_control.cpp" line="1857"/>
        <source>Internal Error!</source>
        <translation>Error interno!</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="2344"/>
        <source>Wrong OMP Version</source>
        <translation>Versión OMP errónea</translation>
    </message>
    <message>
        <location filename="../gsd_control.cpp" line="2345"/>
        <source>You are connected to an OMP 1.0 service!
Since version 1.1.0 GSD supports OMP 2.0 only.
Use GSD Version 1.0 instead.</source>
        <translation>Está conectado a un servicio OMP 1.0
Desde la versión 1.1.0 GSD soporta solo OMP 2.0
Use GSD Versión 1.0 en su lugar.</translation>
    </message>
</context>
<context>
    <name>gsd_mw</name>
    <message>
        <location filename="../gsd_mw.cpp" line="271"/>
        <source>Tasks</source>
        <translation>Tareas</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="303"/>
        <location filename="../gsd_mw.cpp" line="4581"/>
        <source>Targets</source>
        <translation>Objetivos</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="332"/>
        <location filename="../gsd_mw.cpp" line="4583"/>
        <source>Schedules</source>
        <translation>Agendas</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="361"/>
        <location filename="../gsd_mw.cpp" line="4582"/>
        <source>Scan Configs</source>
        <translation>Conf. Escaneos</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="391"/>
        <location filename="../gsd_mw.cpp" line="4584"/>
        <source>Escalators</source>
        <translation>Escaladores</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="420"/>
        <location filename="../gsd_mw.cpp" line="4585"/>
        <source>Credentials</source>
        <translation>Credenciales</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="445"/>
        <location filename="../gsd_mw.cpp" line="4586"/>
        <source>Agents</source>
        <translation>Agentes</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="469"/>
        <location filename="../gsd_mw.cpp" line="4588"/>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="494"/>
        <location filename="../gsd_mw.cpp" line="4587"/>
        <source>Overrides</source>
        <translation>Sobrepasos</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="520"/>
        <source>Slaves</source>
        <translation>Esclavos</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="548"/>
        <source>Report Formats</source>
        <translation>Formatos de Reporte</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="727"/>
        <source>Ctrl+q</source>
        <translation>Ctrl-q</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="748"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1078"/>
        <source>Logged in as: &lt;b&gt;%1&lt;/b&gt; at &lt;b&gt;%2:%3&lt;/b&gt;</source>
        <translation>Registrado como: &lt;b&gt;%1&lt;/b&gt;  en &lt;/b&gt;%2:%3&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1245"/>
        <location filename="../gsd_mw.cpp" line="1261"/>
        <location filename="../gsd_mw.cpp" line="1277"/>
        <location filename="../gsd_mw.cpp" line="1293"/>
        <location filename="../gsd_mw.cpp" line="1351"/>
        <location filename="../gsd_mw.cpp" line="1369"/>
        <location filename="../gsd_mw.cpp" line="1387"/>
        <location filename="../gsd_mw.cpp" line="1405"/>
        <location filename="../gsd_mw.cpp" line="1423"/>
        <location filename="../gsd_mw.cpp" line="1441"/>
        <location filename="../gsd_mw.cpp" line="1459"/>
        <location filename="../gsd_mw.cpp" line="1477"/>
        <location filename="../gsd_mw.cpp" line="1495"/>
        <location filename="../gsd_mw.cpp" line="1698"/>
        <location filename="../gsd_mw.cpp" line="2016"/>
        <location filename="../gsd_mw.cpp" line="2100"/>
        <location filename="../gsd_mw.cpp" line="2184"/>
        <location filename="../gsd_mw.cpp" line="2267"/>
        <location filename="../gsd_mw.cpp" line="2362"/>
        <location filename="../gsd_mw.cpp" line="2464"/>
        <location filename="../gsd_mw.cpp" line="2567"/>
        <location filename="../gsd_mw.cpp" line="2650"/>
        <location filename="../gsd_mw.cpp" line="2666"/>
        <location filename="../gsd_mw.cpp" line="3947"/>
        <source>Status Error</source>
        <translation>Estado del error</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1246"/>
        <location filename="../gsd_mw.cpp" line="1262"/>
        <location filename="../gsd_mw.cpp" line="1278"/>
        <location filename="../gsd_mw.cpp" line="1294"/>
        <source>No Task selected!</source>
        <translation>No hay tarea seleccionada!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1352"/>
        <source>No scan config selected!</source>
        <translation>No hay configuración de Escaneo seleccionada!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1370"/>
        <location filename="../gsd_mw.cpp" line="2268"/>
        <source>No target selected!</source>
        <translation>No hay Objetivos seleccionados!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1388"/>
        <location filename="../gsd_mw.cpp" line="2185"/>
        <source>No schedule selected!</source>
        <translation>No hay agenda seleccionada!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1406"/>
        <location filename="../gsd_mw.cpp" line="2101"/>
        <source>No escalator selected!</source>
        <translation>No hay Escaladores seleccionados!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1424"/>
        <location filename="../gsd_mw.cpp" line="2017"/>
        <source>No credential selected!</source>
        <translation>No se seleccionaron Credenciales!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1442"/>
        <source>No agent selected!</source>
        <translation>No se seleccionó Agente!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1460"/>
        <location filename="../gsd_mw.cpp" line="2465"/>
        <source>No note selected!</source>
        <translation>No se seleccionaron notas!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1478"/>
        <location filename="../gsd_mw.cpp" line="2568"/>
        <source>No override selected!</source>
        <translation>No se eligieron Sobrepasos!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1496"/>
        <location filename="../gsd_mw.cpp" line="2651"/>
        <source>No slave selected!</source>
        <translation>No hya esclavos seleccionados!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="1699"/>
        <source>No Scan configuration selected!</source>
        <translation>No hay configuración de Escaneo seleccionada!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="2363"/>
        <location filename="../gsd_mw.cpp" line="3948"/>
        <source>No task selected!</source>
        <translation>No hay tareas seleccionadas!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="2667"/>
        <source>No config selected!</source>
        <translation>No se seleccionó configuración!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="2678"/>
        <source>Open File ...</source>
        <translation>Abrir archivos...</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="2680"/>
        <location filename="../gsd_mw.cpp" line="2711"/>
        <source>XML files *.xml</source>
        <translation>Archivos XML *.xml</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="2708"/>
        <source>Save File ...</source>
        <translation>Guardar archivos ...</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="2721"/>
        <source>File Error</source>
        <translation>Error de archivo</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="2722"/>
        <source>Could not open file!</source>
        <translation>No se pudo abrir archivo!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="3802"/>
        <location filename="../gsd_mw.cpp" line="3810"/>
        <location filename="../gsd_mw.cpp" line="3816"/>
        <source>Login Error</source>
        <translation>Error de ingreso</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="3803"/>
        <source>Error while connecting to manager!
Please check server address and port!</source>
        <translation>Error cuando conectando al manager!
Por favor revise dirección del servidor y puerto!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="3811"/>
        <source>Manager closed connection!</source>
        <translation>El manejador cerró la conexión!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="3817"/>
        <source>Authentication failed!
Please check username and password!</source>
        <translation>Falló la autenticación!
Por favor revise usuario y password!</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="3878"/>
        <location filename="../gsd_mw.cpp" line="3930"/>
        <source> sec</source>
        <translation> seg</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4198"/>
        <source>Language Selection</source>
        <translation>Selección de idioma</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4198"/>
        <source>This setting takes effect after restarting the application.</source>
        <translation>Este seteo tomará efecto luego de reiniciar la aplicación.</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4568"/>
        <source>Scan Tasks</source>
        <translation>Tareas de escaneo</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4570"/>
        <source>Trends</source>
        <translation>Tendencias</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4572"/>
        <source>Task Overview</source>
        <translation>Tareas actuales</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4573"/>
        <source>Total</source>
        <translation>Total</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4574"/>
        <source>Running</source>
        <translation>Corriendo</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4575"/>
        <source>Progress</source>
        <translation>Progreso</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4576"/>
        <source>Done</source>
        <translation>Finalizados</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4577"/>
        <source>New</source>
        <translation>Nuevos</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4578"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4580"/>
        <source>Resources Overview</source>
        <translation>Recursos</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4590"/>
        <source>Vulnerabilities</source>
        <translation>Vulnerabilidades</translation>
    </message>
    <message>
        <location filename="../gsd_mw.cpp" line="4592"/>
        <source>Top 5 Tasks</source>
        <translation>Top 5 Tareas</translation>
    </message>
</context>
<context>
    <name>newAgent</name>
    <message>
        <location filename="../dlg_newAgent.ui" line="14"/>
        <location filename="../ui_dlg_newAgent.h" line="136"/>
        <source>New Agent</source>
        <translation>Nuevo Agente</translation>
    </message>
    <message>
        <location filename="../dlg_newAgent.ui" line="24"/>
        <location filename="../ui_dlg_newAgent.h" line="137"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../dlg_newAgent.ui" line="34"/>
        <location filename="../ui_dlg_newAgent.h" line="138"/>
        <source>Comment (optional)</source>
        <translation>Comentario (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newAgent.ui" line="44"/>
        <location filename="../ui_dlg_newAgent.h" line="139"/>
        <source>Installer</source>
        <translation>Instalador</translation>
    </message>
    <message>
        <location filename="../dlg_newAgent.ui" line="51"/>
        <location filename="../ui_dlg_newAgent.h" line="140"/>
        <source>Installer signature</source>
        <translation>Firma de instalador</translation>
    </message>
    <message>
        <location filename="../dlg_newAgent.ui" line="58"/>
        <location filename="../ui_dlg_newAgent.h" line="141"/>
        <source>Ca&amp;ncel</source>
        <translation>Ca&amp;ncela</translation>
    </message>
    <message>
        <location filename="../dlg_newAgent.ui" line="65"/>
        <location filename="../ui_dlg_newAgent.h" line="142"/>
        <source>&amp;Create</source>
        <translation>&amp;Crear</translation>
    </message>
    <message>
        <location filename="../dlg_newAgent.ui" line="79"/>
        <location filename="../ui_dlg_newAgent.h" line="143"/>
        <source>Brow&amp;se ...</source>
        <translation>Explo&amp;rador ...</translation>
    </message>
    <message>
        <location filename="../dlg_newAgent.ui" line="93"/>
        <location filename="../ui_dlg_newAgent.h" line="144"/>
        <source>Bro&amp;wse ...</source>
        <translation>Explor&amp;ador ...</translation>
    </message>
    <message>
        <location filename="../dlg_newAgent.ui" line="100"/>
        <location filename="../ui_dlg_newAgent.h" line="145"/>
        <source>(optional)</source>
        <translation>(opcional)</translation>
    </message>
</context>
<context>
    <name>newConfig</name>
    <message>
        <location filename="../dlg_newConfig.ui" line="16"/>
        <location filename="../ui_dlg_newConfig.h" line="118"/>
        <source>New Scan Config</source>
        <translation>Nueva configuración de escaneo</translation>
    </message>
    <message>
        <location filename="../dlg_newConfig.ui" line="26"/>
        <location filename="../ui_dlg_newConfig.h" line="119"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../dlg_newConfig.ui" line="36"/>
        <location filename="../ui_dlg_newConfig.h" line="120"/>
        <source>Comment (optional)</source>
        <translation>Comentario (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newConfig.ui" line="46"/>
        <location filename="../ui_dlg_newConfig.h" line="121"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
    <message>
        <location filename="../dlg_newConfig.ui" line="53"/>
        <location filename="../ui_dlg_newConfig.h" line="122"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="../dlg_newConfig.ui" line="60"/>
        <location filename="../ui_dlg_newConfig.h" line="123"/>
        <source>Empty, static and fast</source>
        <translation>Vacío, estático y rápido</translation>
    </message>
    <message>
        <location filename="../dlg_newConfig.ui" line="70"/>
        <location filename="../ui_dlg_newConfig.h" line="124"/>
        <source>Base</source>
        <translation>Base</translation>
    </message>
    <message>
        <location filename="../dlg_newConfig.ui" line="90"/>
        <location filename="../ui_dlg_newConfig.h" line="125"/>
        <source>Full and fast</source>
        <translation>Completo y rápido</translation>
    </message>
</context>
<context>
    <name>newCredential</name>
    <message>
        <location filename="../dlg_newCredential.ui" line="16"/>
        <location filename="../ui_dlg_newCredential.h" line="133"/>
        <source>New Credential</source>
        <translation>Nueva Credencial</translation>
    </message>
    <message>
        <location filename="../dlg_newCredential.ui" line="26"/>
        <location filename="../ui_dlg_newCredential.h" line="134"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../dlg_newCredential.ui" line="36"/>
        <location filename="../ui_dlg_newCredential.h" line="135"/>
        <source>Comment (optional)</source>
        <translation>Comentario (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newCredential.ui" line="46"/>
        <location filename="../ui_dlg_newCredential.h" line="136"/>
        <source>Login</source>
        <translation>Entrar</translation>
    </message>
    <message>
        <location filename="../dlg_newCredential.ui" line="56"/>
        <location filename="../ui_dlg_newCredential.h" line="137"/>
        <source>Autogenerate credential</source>
        <translation>Autogenerar Credencial</translation>
    </message>
    <message>
        <location filename="../dlg_newCredential.ui" line="66"/>
        <location filename="../ui_dlg_newCredential.h" line="138"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../dlg_newCredential.ui" line="80"/>
        <location filename="../ui_dlg_newCredential.h" line="139"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
    <message>
        <location filename="../dlg_newCredential.ui" line="87"/>
        <location filename="../ui_dlg_newCredential.h" line="140"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
</context>
<context>
    <name>newEscalator</name>
    <message>
        <location filename="../dlg_newEscalator.ui" line="17"/>
        <location filename="../ui_dlg_newEscalator.h" line="270"/>
        <source>New Escalator</source>
        <translation>Nuevo escalador</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="27"/>
        <location filename="../ui_dlg_newEscalator.h" line="271"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="37"/>
        <location filename="../ui_dlg_newEscalator.h" line="272"/>
        <source>Comment (optional)</source>
        <translation>Comentario (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="47"/>
        <location filename="../ui_dlg_newEscalator.h" line="273"/>
        <source>Event</source>
        <translation>Evento</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="54"/>
        <location filename="../ui_dlg_newEscalator.h" line="274"/>
        <source>Condition</source>
        <translation>Condición</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="64"/>
        <location filename="../ui_dlg_newEscalator.h" line="275"/>
        <source>Always</source>
        <translation>Siempre</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="74"/>
        <location filename="../ui_dlg_newEscalator.h" line="276"/>
        <source>Threat level is at least</source>
        <translation>Nivel de Riesgo es por lo menos</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="84"/>
        <location filename="../ui_dlg_newEscalator.h" line="277"/>
        <source>Threat level</source>
        <translation>Nivel de riesgo</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="116"/>
        <location filename="../ui_dlg_newEscalator.h" line="278"/>
        <source>Method</source>
        <translation>Método</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="126"/>
        <location filename="../ui_dlg_newEscalator.h" line="279"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="145"/>
        <location filename="../ui_dlg_newEscalator.h" line="280"/>
        <source>simple Notice</source>
        <translation>Noticia simple</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="155"/>
        <location filename="../ui_dlg_newEscalator.h" line="281"/>
        <source>Summary (can include vulnerability details)</source>
        <translation> Resumen (puede incluir detalles de vulnerabilidades)</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="168"/>
        <location filename="../ui_dlg_newEscalator.h" line="282"/>
        <source>System Logger (Syslog)</source>
        <translation>Registro del Sistema (Syslog)</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="178"/>
        <location filename="../ui_dlg_newEscalator.h" line="283"/>
        <source>SNMP </source>
        <translation>SNMP</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="185"/>
        <location filename="../ui_dlg_newEscalator.h" line="284"/>
        <source>To Address</source>
        <translation>Dirección Para</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="192"/>
        <location filename="../ui_dlg_newEscalator.h" line="285"/>
        <source>FromAddress</source>
        <translation>DirecciónDesde</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="199"/>
        <location filename="../ui_dlg_newEscalator.h" line="286"/>
        <source>Format</source>
        <translation>Formato</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="264"/>
        <location filename="../ui_dlg_newEscalator.h" line="287"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="271"/>
        <location filename="../ui_dlg_newEscalator.h" line="288"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
    <message>
        <location filename="../dlg_newEscalator.ui" line="278"/>
        <location filename="../ui_dlg_newEscalator.h" line="289"/>
        <source>Task run status changed to:</source>
        <translation>Estado de ejecución de la Tarea cambió a:</translation>
    </message>
</context>
<context>
    <name>newSchedule</name>
    <message>
        <location filename="../dlg_newSchedule.ui" line="17"/>
        <location filename="../ui_dlg_newSchedule.h" line="163"/>
        <source>New Schedule</source>
        <translation>Nueva Agenda</translation>
    </message>
    <message>
        <location filename="../dlg_newSchedule.ui" line="27"/>
        <location filename="../ui_dlg_newSchedule.h" line="164"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../dlg_newSchedule.ui" line="37"/>
        <location filename="../ui_dlg_newSchedule.h" line="165"/>
        <source>Comment (optional)</source>
        <translation>Comentario (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newSchedule.ui" line="47"/>
        <location filename="../ui_dlg_newSchedule.h" line="166"/>
        <source>First Time</source>
        <translation>Primera vez</translation>
    </message>
    <message>
        <location filename="../dlg_newSchedule.ui" line="57"/>
        <location filename="../ui_dlg_newSchedule.h" line="167"/>
        <source>hh:mm </source>
        <translation>hh:mm</translation>
    </message>
    <message>
        <location filename="../dlg_newSchedule.ui" line="74"/>
        <location filename="../ui_dlg_newSchedule.h" line="168"/>
        <source>dd.MM.yyyy</source>
        <translation>dd.MM.aaaa</translation>
    </message>
    <message>
        <location filename="../dlg_newSchedule.ui" line="88"/>
        <location filename="../ui_dlg_newSchedule.h" line="169"/>
        <source>Period (optional)</source>
        <translation>Período (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newSchedule.ui" line="105"/>
        <location filename="../ui_dlg_newSchedule.h" line="170"/>
        <source>Duration (optional)</source>
        <translation>Duración (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newSchedule.ui" line="122"/>
        <location filename="../ui_dlg_newSchedule.h" line="171"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="../dlg_newSchedule.ui" line="142"/>
        <location filename="../ui_dlg_newSchedule.h" line="172"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
</context>
<context>
    <name>newTarget</name>
    <message>
        <location filename="../dlg_newTarget.ui" line="13"/>
        <location filename="../ui_dlg_newTarget.h" line="160"/>
        <source>New Target</source>
        <translation>Nuevo Objetivo</translation>
    </message>
    <message>
        <location filename="../dlg_newTarget.ui" line="26"/>
        <location filename="../ui_dlg_newTarget.h" line="161"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../dlg_newTarget.ui" line="36"/>
        <location filename="../ui_dlg_newTarget.h" line="162"/>
        <source>Comment (optional)</source>
        <translation>Comentario (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newTarget.ui" line="46"/>
        <location filename="../ui_dlg_newTarget.h" line="163"/>
        <source>Hosts</source>
        <translation>Hosts</translation>
    </message>
    <message>
        <location filename="../dlg_newTarget.ui" line="56"/>
        <location filename="../ui_dlg_newTarget.h" line="164"/>
        <source>SSH Credential (optional)</source>
        <translation>Credencial SSH (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newTarget.ui" line="66"/>
        <location filename="../ui_dlg_newTarget.h" line="165"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../dlg_newTarget.ui" line="89"/>
        <location filename="../ui_dlg_newTarget.h" line="166"/>
        <source>SMB Credential (optional)</source>
        <translation>Credencial SMB (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newTarget.ui" line="99"/>
        <location filename="../ui_dlg_newTarget.h" line="170"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="../dlg_newTarget.ui" line="113"/>
        <location filename="../dlg_newTarget.ui" line="127"/>
        <location filename="../ui_dlg_newTarget.h" line="172"/>
        <location filename="../ui_dlg_newTarget.h" line="176"/>
        <source>New Credential</source>
        <translation>Nueva credencial</translation>
    </message>
</context>
<context>
    <name>newTask</name>
    <message>
        <location filename="../dlg_newTask.ui" line="16"/>
        <location filename="../ui_dlg_newTask.h" line="212"/>
        <source>New Task</source>
        <translation>Nueva Tarea</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="29"/>
        <location filename="../ui_dlg_newTask.h" line="213"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="39"/>
        <location filename="../ui_dlg_newTask.h" line="214"/>
        <source>Comment (optional)</source>
        <translation>Comentario (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="49"/>
        <location filename="../ui_dlg_newTask.h" line="215"/>
        <source>Scan Config</source>
        <translation>Configuración Escaneos</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="69"/>
        <location filename="../ui_dlg_newTask.h" line="216"/>
        <source>Schedule (optional)</source>
        <translation>Agenda (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="92"/>
        <location filename="../ui_dlg_newTask.h" line="217"/>
        <source>Escalator (optional)</source>
        <translation>Escalador (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="112"/>
        <location filename="../ui_dlg_newTask.h" line="218"/>
        <source>Scan Targets</source>
        <translation>Objetivos del escaneo</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="135"/>
        <location filename="../ui_dlg_newTask.h" line="219"/>
        <source>Slave (optional)</source>
        <translation>Esclavo (opcional)</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="142"/>
        <location filename="../ui_dlg_newTask.h" line="221"/>
        <source>New Scan Config</source>
        <translation>Nueva configuración de escaneo</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="156"/>
        <location filename="../ui_dlg_newTask.h" line="225"/>
        <source>New Target</source>
        <translation>Nuevo objetivo</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="170"/>
        <location filename="../ui_dlg_newTask.h" line="229"/>
        <source>New Escalator</source>
        <translation>Nuevo escalador</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="184"/>
        <location filename="../ui_dlg_newTask.h" line="233"/>
        <source>New Schedule</source>
        <translation>Nueva Agenda</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="198"/>
        <location filename="../ui_dlg_newTask.h" line="237"/>
        <source>New Slave</source>
        <translation>Nuevo esclavo</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="212"/>
        <location filename="../ui_dlg_newTask.h" line="240"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="../dlg_newTask.ui" line="219"/>
        <location filename="../ui_dlg_newTask.h" line="241"/>
        <source>Cancel</source>
        <translation>Cancela</translation>
    </message>
</context>
<context>
    <name>omp_command</name>
    <message>
        <location filename="../omp_command.cpp" line="651"/>
        <source>Conversion to QDomElement failed!
 %1 at: %2 , %3</source>
        <translation>Conversión a QDomElement falló!
 %1 en; %2, %3</translation>
    </message>
</context>
</TS>
