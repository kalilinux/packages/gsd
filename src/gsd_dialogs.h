/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file gsd_dialogs.h
 */

#ifndef GSD_DIALOGS_H
#define GSD_DIALOGS_H

#include <QtGui>

#include "ui_dlg_login.h"
#include "ui_dlg_about.h"
#include "ui_dlg_license.h"
#include "ui_dlg_start_gsa.h"

#include "ui_dlg_newTask.h"
#include "ui_dlg_newTarget.h"
#include "ui_dlg_newSchedule.h"
#include "ui_dlg_newEscalator.h"
#include "ui_dlg_newCredential.h"
#include "ui_dlg_newAgent.h"
#include "ui_dlg_newConfig.h"
#include "ui_dlg_newPortList.h"
#include "ui_dlg_new_note.h"
#include "ui_dlg_new_override.h"
#include "ui_dlg_new_slave.h"
#include "model_omp_entity.h"

#include "ui_dlg_modify_config.h"
#include "ui_dlg_modify_family.h"
#include "ui_dlg_modify_scan_pref.h"
#include "ui_dlg_modify_nvt.h"
#include "ui_dlg_modify_note.h"
#include "ui_dlg_modify_override.h"
#include "ui_dlg_modify_credential.h"
#include "ui_dlg_modify_task.h"
#include "delegate_trend.h"
#include "delegate_text.h"
#include "delegate_threat.h"
#include "delegate_date_time.h"
#include "delegate_icon.h"
#include "delegate_progress.h"
#include "omp_utilities.h"
#include "omp_credentials.h"


class gsd_control;
/**
 * @brief Protos and data structures for about dialog.
 */
class dlg_about : public QDialog, private Ui::dlg_about
{
  Q_OBJECT

  private slots:
    void link_clicked (QString);

  public:
    dlg_about () {setupUi (this);};
    ~dlg_about () {};

    void load ();
};


/**
 * @brief Protos and data structures for license dialog.
 */
class dlg_license : public QDialog, private Ui::dlg_license
{
  Q_OBJECT

  public:
    dlg_license () {setupUi (this);};
    ~dlg_license () {};
};


/**
 * @brief Protos and data structures for login dialog.
 */
class dlg_login :public QDialog, private Ui::dlg_login
{
  Q_OBJECT

  signals:
    void sig_login (QString, int, QString, QString);
    void sig_profile_selected (int);
    void sig_save_profile (QString, omp_credentials&);
    void sig_remove_profile (int);
    void sig_check_server (QString, int, QString, QString);

  public:
    dlg_login ()
      {
        setupUi (this);
        connect (cb_profile,
                 SIGNAL (currentIndexChanged (int)),
                 this,
                 SLOT (profile_changed (int)));
        connect (pb_login,
                 SIGNAL (released ()),
                 this,
                 SLOT (accept_login ()));
        connect (pb_saveprofile,
                 SIGNAL (released ()),
                 this,
                 SLOT (profile_save ()));
        connect (pb_del,
                 SIGNAL (released ()),
                 this,
                 SLOT (profile_remove ()));
        connect (le_srvaddr,
                 SIGNAL (textChanged (QString)),
                 this,
                 SLOT (checkServer ()));
        connect (le_srvport,
                 SIGNAL (textChanged (QString)),
                 this,
                 SLOT (checkServer ()));
        connect (le_uname,
                 SIGNAL (editingFinished ()),
                 this,
                 SLOT (checkServer ()));
        connect (le_upass,
                 SIGNAL (editingFinished ()),
                 this,
                 SLOT (checkServer ()));


        QIntValidator *val = new QIntValidator (0, 65535, le_srvport);
        le_srvport->setValidator(val);
      };
    ~dlg_login (){};

    QString getAddress ();
    QString getPort ();
    QString getUsername ();
    QString getPassword ();
    QString getProfile ();

    void setAddress (QString addr);
    void setPort (int port);
    void setUsername (QString name);
    void setProfile (QString name, int pos);
    void setUrlValid (int res);
    void selectProfile (int ndx);
    bool isOpen ();

  public slots:
    void checkServer ();

  private slots:
    void profile_changed (int);
    void profile_save ();
    void profile_remove ();
    void accept_login ();

  private:
    QString address;
    QString port;
    QString user;
    QString password;
};


/**
 * @brief Protos and data structures for license dialog.
 */
class dlg_start_gsa : public QDialog, private Ui::dlg_start_gsa
{
  Q_OBJECT

  private:
    QString gsd_url;

  private slots:
    void start_gsa ();
    void reset ();

  public:
    dlg_start_gsa ()
      {
        setupUi (this);
        connect (pb_open,
                 SIGNAL (released ()),
                 this,
                 SLOT (start_gsa ()));
        connect (pb_default,
                 SIGNAL (released ()),
                 this,
                 SLOT (reset ()));
      };
    ~dlg_start_gsa () {};

    void load ();
    void setDefaultURL (QString url);
};


/**
 * @brief Superclass for "new" dialogs.
 */
class create_dialog : public QDialog
{
  Q_OBJECT

  signals:
    void sig_create (int, QMap<QString, QString>);

  public:
    create_dialog (){};
    ~create_dialog (){};

};


/**
 * @brief Superclass for "details" dialogs.
 */
class details_dialog : public QDialog
{
  Q_OBJECT

  signals:
    void sig_modify (int, QString, QMap<QString, QString>);

  private slots:
    //virtual void modify ();
    //virtual void toggle_modify_widget ();

  protected:
    model_omp_entity *model;
    QString id;
    omp_utilities::omp_type type;

  public:
    details_dialog () {};
    ~details_dialog () {};

    void setModel (model_omp_entity *);
    void setId (QString id);
    void load ();
    void update ();
};

/**
 * @brief Protos and data structures for new port list.
 */
class dlg_new_port_list : public create_dialog, private Ui::newPortList
{
  Q_OBJECT

  public:
    dlg_new_port_list ()
      {
        setupUi (this);
        connect (pb_create,
                 SIGNAL (released ()),
                 this,
                 SLOT (create ()));
      };
    ~dlg_new_port_list () {};

  private slots:
    void create ();
};

/**
 * @brief Protos and data structures for new task.
 */
class dlg_new_task : public create_dialog, private Ui::newTask
{

  Q_OBJECT

  signals:
    void sig_new_target ();
    void sig_new_config ();
    void sig_new_escalator ();
    void sig_new_schedule ();
    void sig_new_slave ();

  public:
    dlg_new_task ()
      {
        setupUi (this);
        connect (pb_create,
                 SIGNAL (released ()),
                 this,
                 SLOT (create ()));
        connect (pb_new_target,
                 SIGNAL (released ()),
                 this,
                 SLOT (new_target ()));
        connect (pb_new_config,
                 SIGNAL (released ()),
                 this,
                 SLOT (new_config ()));
        connect (pb_new_escalator,
                 SIGNAL (released ()),
                 this,
                 SLOT (new_escalator ()));
        connect (pb_new_schedule,
                 SIGNAL (released ()),
                 this,
                 SLOT (new_schedule ()));
        connect (pb_new_slave,
                 SIGNAL (released ()),
                 this,
                 SLOT (new_slave ()));
      };
    ~dlg_new_task () {};

    void load ();
    void addConfig (QString name, QString id);
    void addTarget (QString name, QString id);
    void addSchedule (QString name, QString id);
    void addEscalator (QString name, QString id);
    void addSlave (QString name, QString id);

    void selectConfig (int pos);
    void selectTarget (int pos);
    void selectSchedule (int pos);
    void selectEscalator (int pos);
    void selectSlave (int pos);
  private slots:
    void create ();
    void new_target ();
    void new_config ();
    void new_escalator ();
    void new_schedule ();
    void new_slave ();
};


/**
 * @brief Protos and data structures for new target dialog.
 */
class dlg_new_target : public create_dialog, private Ui::newTarget
{
  Q_OBJECT

  signals:
    void sig_new_credential ();
    void sig_new_port_list ();

  private:
    QString type;

  public:
    dlg_new_target ()
     {
       setupUi (this);
       connect (pb_create,
                SIGNAL (released ()),
                this,
                SLOT (create ()));
       connect (pb_new_smbcredential,
                SIGNAL (released ()),
                this,
                SLOT (new_smb_credential ()));
       connect (pb_new_sshcredential,
                SIGNAL (released ()),
                this,
                SLOT (new_ssh_credential ()));
       connect (pb_new_plist,
                SIGNAL (released ()),
                this,
                SLOT (new_port_list ()));
     };
    ~dlg_new_target () {};

    void load ();
    void addCredential (QString name, QString id);
    void addPortList (QString name, QString id);
    void selectPortList (int pos);
    void selectCredential (int pos);

  private slots:
    void create ();
    void new_ssh_credential ();
    void new_smb_credential ();
    void new_port_list();
};


/**
 * @brief Protos and Data structures for new schedule dialog.
 */
class dlg_new_schedule : public create_dialog, private Ui::newSchedule
{
  Q_OBJECT

  public:
    dlg_new_schedule ()
      {
        setupUi (this);
        connect (pb_create,
                 SIGNAL (released ()),
                 this,
                 SLOT (create ()));
        this->de_first->setDate (QDate::currentDate ());
        this->te_first->setTime (QTime::currentTime ());

        this->cb_dura_unit->addItem ("hour(s)", "hour");
        this->cb_dura_unit->addItem ("day(s)", "day");
        this->cb_dura_unit->addItem ("week(s)", "week");

        this->cb_peri_unit->addItem ("hour(s)", "hour");
        this->cb_peri_unit->addItem ("day(s)", "day");
        this->cb_peri_unit->addItem ("weeks(s)", "week");
        this->cb_peri_unit->addItem ("month(s)", "month");
      };
    ~dlg_new_schedule () {};

  private slots:
    void create ();
};


/**
 * @brief Protos and data structures for new escalator dialog.
 */
class dlg_new_escalator : public create_dialog, private Ui::newEscalator
{
  Q_OBJECT

  public:
    dlg_new_escalator ()
      {
        setupUi (this);
        connect (pb_create,
                 SIGNAL (released ()),
                 this,
                 SLOT (create ()));
        this->cb_event->addItem ("Done", "Done");
        this->cb_event->addItem ("New", "New");
        this->cb_event->addItem ("Running", "Running");
        this->cb_event->addItem ("Stopped", "Stopped");
        this->cb_event->addItem ("Requested", "Requested");
        this->cb_event->addItem ("Stop Requested", "Stop Requested");
        this->cb_event->addItem ("Delete Requested", "Delete Requested");

        this->cb_cond1->addItem ("High", "High");
        this->cb_cond1->addItem ("Medium", "Medium");
        this->cb_cond1->addItem ("Low", "Low");
        this->cb_cond1->addItem ("Log", "Log");
        this->cb_cond2->addItem ("changed", "changed");
        this->cb_cond2->addItem ("increased", "increased");
        this->cb_cond2->addItem ("decreased", "decreased");

        this->rb_cond1->setChecked (true);
        this->rb_method1->setChecked (true);
        this->rb_mail1->setChecked (true);

      };
    ~dlg_new_escalator () {};

  private slots:
    void create ();

  private:
    bool checkAddr (QString address);
};


/**
 * @brief Protos and data structures for new credential dialog.
 */
class dlg_new_credential : public create_dialog, private Ui::newCredential
{
  Q_OBJECT

  public:
    dlg_new_credential ()
      {
        setupUi (this);
        connect (pb_create,
                 SIGNAL (released ()),
                 this,
                 SLOT (create ()));
      };
    ~dlg_new_credential () {};

  private slots:
    void create ();
};


/**
 * @brief Protos and data structures for new config dialog.
 */
class dlg_new_config : public create_dialog, private Ui::newConfig
{
  Q_OBJECT

  public:
    dlg_new_config ()
      {
        setupUi (this);
        connect (pb_create,
                 SIGNAL (released ()),
                 this,
                 SLOT (create ()));
      };
    ~dlg_new_config () {};

  private slots:
    void create ();
};


/**
 * @brief Protos and data structures for new agent dialog.
 */
class dlg_new_agent : public create_dialog, private Ui::newAgent
{
  Q_OBJECT

  public:
    dlg_new_agent ()
      {
        setupUi (this);
        connect (pb_create,
                 SIGNAL (released ()),
                 this,
                 SLOT (create ()));
        connect (pb_installer,
                 SIGNAL (released ()),
                 this,
                 SLOT (open_installer ()));
        connect (pb_signature,
                 SIGNAL (released ()),
                 this,
                 SLOT (open_signature ()));

      };
    ~dlg_new_agent () {};

  private slots:
    void open_installer ();
    void open_signature ();
    void create ();
};


/**
 * @brief Protos and data structures for modify config dialog.
 */
class dlg_modify_config : public QDialog,
                          private Ui::dlg_modify_config
{
  Q_OBJECT

  signals:
    void sig_modify (bool, bool);
    void sig_add_family (QString name);

  private slots:
    void modify ()
      {
        emit sig_modify (rb_trend_grow->isChecked (),
                         cb_selected->isChecked ());
        this->close ();
      };
    void add_family ()
      {
        emit sig_add_family (cb_families->currentText ());
        cb_families->removeItem (cb_families->currentIndex ());
      };

  public:
    dlg_modify_config ()
      {
        setupUi (this);
        cb_families->clear ();
        connect (pb_save,
                 SIGNAL (released ()),
                 this,
                 SLOT (modify ()));
        connect (pb_add,
                 SIGNAL (released ()),
                 this,
                 SLOT (add_family ()));
      };
    ~dlg_modify_config () {};

    void setName (QString name) {this->la_name_v->setText (name);};
    void setSelected (bool selected) {this->cb_selected->setChecked (selected);};
    void setTrend (bool growing) {if (growing) rb_trend_grow->setChecked (true);
                                  else rb_trend_same->setChecked (true);};
    void addFamily (QString name) {this->cb_families->addItem (name);};
};


/**
 * @brief Protos and data structures for modify scan preferences dialog.
 */
class dlg_modify_scan_pref : public QDialog,
                             private Ui::dlg_modify_scan_pref
{
  Q_OBJECT

  signals:
    void sig_modify (QString);

  private slots:
    void modify ()
      {
        if (wi_cb_value->isVisible ())
          emit sig_modify (comboBox->currentText ());
        else
          emit sig_modify (le_value->text ());
        this->close ();
      };

  public:
    dlg_modify_scan_pref ()
      {
        setupUi (this);
        wi_cb_value->setVisible (false);
        wi_le_value->setVisible (false);
        connect (pb_save,
                 SIGNAL (released ()),
                 this,
                 SLOT (modify ()));
      };
    ~dlg_modify_scan_pref () {};
    void setName (QString name) {this->la_name_v->setText (name);};
    void setComboBox (QStringList list)
      {
        this->comboBox->insertItems (0, list);
        this->wi_cb_value->setVisible (true);
      };
    void setText (QString text)
      {
        this->wi_le_value->setVisible (true);
        this->le_value->setText (text);
      };
};


/**
 * @brief Protos and data structure for modify family dialog.
 */
class dlg_modify_family : public QDialog,
                          private Ui::dlg_modify_family
{
  Q_OBJECT

  signals:
    void sig_modify (bool);
    void sig_add_nvt (QString oid, QString name);

  private slots:
    void modify ()
      {
        emit sig_modify (cb_selected->isChecked ());
        this->close ();
      };
    void add_nvt ()
      {
        emit sig_add_nvt (cb_add->itemData (cb_add->currentIndex ())
                                             .toString (),
                          la_name_v->text ());
        cb_add->removeItem (cb_add->currentIndex ());
      };

  public:
    dlg_modify_family ()
      {
        setupUi (this);
        cb_add->clear ();
        connect (pb_save,
                 SIGNAL (released ()),
                 this,
                 SLOT (modify ()));
        connect (pb_add,
                 SIGNAL (released ()),
                 this,
                 SLOT (add_nvt ()));
      };
    ~dlg_modify_family () {};
    void setName (QString name) {this->la_name_v->setText (name);};
    void setNVT (QString name) {this->la_nvt_v->setText (name);};
    void setSelected (bool selected)
      {
        this->cb_selected->setChecked (selected);
      };
    void addNVT (QString name, QString oid) {this->cb_add->addItem (name, oid);};
};


/**
 * @brief Protos and data structures for modify nvt dialog.
 */
class dlg_modify_nvt : public QDialog,
                       private Ui::dlg_modify_nvt
{
  Q_OBJECT

  signals:
    void sig_modify (QString);

  private slots:
    void modify ()
      {
        if (widget_cb->isVisible ())
          emit sig_modify (comboBox->currentText ());
        else if (widget->isVisible ())
          emit sig_modify (le_value->text ());
        else if (widget_2->isVisible ())
          {
            QFile f (lineEdit->text ());
            QByteArray ba = f.readAll ();
            QString b64 = ba.toBase64 ().data ();
            emit sig_modify (b64);
          }
        else if (widget_3->isVisible ())
          {
            emit sig_modify (lineEdit_2->text ());
          }
        this->close ();
      };
    void openFile ()
      {
        QString filename = QFileDialog::getOpenFileName (this, tr ("Open File ..."),
                                                   QDir::currentPath (),
                                                   tr ("All files *.*"));
        this->lineEdit->setText (filename);
      };

  public:
    dlg_modify_nvt ()
      {
        setupUi (this);
        this->widget->setVisible (false);
        this->widget_cb->setVisible (false);
        lineEdit_2->setEchoMode (QLineEdit::Password);
        connect (pb_save,
                 SIGNAL (released ()),
                 this,
                 SLOT (modify ()));
        connect (pushButton,
                 SIGNAL (released ()),
                 this,
                 SLOT (openFile ()));
      };
    ~dlg_modify_nvt () {};
    void setName (QString name) {this->la_name_v->setText (name);};
    void setText (QString text)
      {
        this->widget->setVisible (true);
        this->widget_cb->setVisible (false);
        this->widget_2->setVisible (false);
        this->widget_3->setVisible (false);
        this->le_value->setText (text);
      }
    void setComboBox (QStringList alt)
      {
        this->widget_cb->setVisible (true);
        this->widget_2->setVisible (false);
        this->widget->setVisible (false);
        this->widget_3->setVisible (false);
        this->comboBox->insertItems (0, alt);
      }
    void setPassword ()
      {
        this->widget_cb->setVisible (false);
        this->widget_2->setVisible (false);
        this->widget->setVisible (false);
        this->widget_3->setVisible (true);
      }
    void setFile ()
      {
        this->widget_cb->setVisible (false);
        this->widget->setVisible (false);
        this->widget_3->setVisible (false);
        this->widget_2->setVisible (true);
      }
};


/**
 * @brief Protos and data structures for dlg_modify_note.
 */
class dlg_modify_note : public QDialog,
                        private Ui::dlg_modify_note
{
  Q_OBJECT

  signals:
    void sig_modify (QString, QString, QString, QString, QString, QString);

  private slots:
    void modify ()
      {
        QString hosts, port, newThreat, threat, task, result, text;
        if (this->rb_hosts_new->isChecked ())
           hosts = "";
        else
          hosts = rb_hosts_old->text ();
        if (this->rb_port_new->isChecked ())
          port = "";
        else
          port = rb_port_old->text ();
        if (this->rb_threat_new->isChecked ())
          threat = "";
        else
          threat = rb_threat_old->text ();
        if (this->rb_task_new->isChecked ())
          task = "";
        else
          task = rb_task_old->text ();
        if (this->rb_result_new->isChecked ())
          result = "";
        else
          result = rb_result_old->text ();

        text = te_text->toPlainText ();

        emit sig_modify (hosts, port, threat, task, result, text);
        close ();
      };

  public:
    dlg_modify_note ()
      {
        setupUi (this);
        connect (pb_save,
                 SIGNAL (released ()),
                 this,
                 SLOT (modify ()));
      };
    ~dlg_modify_note () {};
    void setName (QString name) {this->la_name_v->setText (name);};
    void setOID (QString oid) {this->la_oid_v->setText (oid);};
    void setHosts (QString hosts)
      {
        if (hosts.compare ("any") == 0)
          {
            rb_hosts_new->setChecked (true);
            rb_hosts_old->setVisible (false);
          }
        else
          {
            rb_hosts_old->setText (hosts);
            rb_hosts_old->setChecked (true);
          }
      };
    void setPort (QString port)
      {
        if (port.compare ("any") == 0)
          {
            rb_port_new->setChecked (true);
            rb_port_old->setVisible (false);
          }
        else
          {
            rb_port_old->setText (port);
            rb_port_old->setChecked (true);
          }
      };
    void setThreat (QString threat)
      {
        if (threat.compare ("any") == 0)
          {
            rb_threat_new->setChecked (true);
            rb_threat_old->setVisible (false);
          }
        else
          {
            rb_threat_old->setText (threat);
            rb_threat_old->setChecked (true);
          }
      };
    void setTask (QString task)
      {
        if (task.compare ("any") == 0)
          {
            rb_task_new->setChecked (true);
            rb_task_old->setVisible (false);
          }
        else
          {
            rb_task_old->setText (task);
            rb_task_old->setChecked (true);
          }
      };
    void setResult (QString result)
      {
        if (result.compare ("any") == 0)
          {
            rb_result_new->setChecked (true);
            rb_result_old->setVisible (false);
          }
        else
          {
            rb_result_new->setText (result);
            rb_result_old->setChecked (true);

          }
      };
    void setText (QString text)
      {
        te_text->setText (text);
      };
};


/**
 * @brief Protos and data structures fo dlg_modify_override.
 */
class dlg_modify_override : public QDialog,
                            private Ui::dlg_modify_override
{
  Q_OBJECT

  signals:
    void sig_modify (QString, QString, QString, QString,
                     QString, QString, QString);

  private slots:
    void modify ()
      {
        QString hosts, port, newThreat, threat, task, result, text;
        if (this->rb_hosts_new->isChecked ())
           hosts = "";
        else
          hosts = rb_hosts_old->text ();
        if (this->rb_port_new->isChecked ())
          port = "";
        else
          port = rb_port_old->text ();
        if (rb_threat_new->isChecked ())
          threat = "";
        else
          threat = rb_threat_old->text ();
        if (la_newthreat->text ().compare (cb_threat->currentText ()) != 0)
          newThreat = cb_threat->currentText ();
        else
          newThreat = la_newthreat->text ();
        if (this->rb_task_new->isChecked ())
          task = "";
        else
          task = rb_task_old->text ();
        if (this->rb_result_new->isChecked ())
          result = "";
        else
          result = rb_result_old->text ();

        text = te_text->toPlainText ();

        emit sig_modify (hosts, port, threat, newThreat, task, result, text);
        close ();
      };

  public:
    dlg_modify_override ()
      {
        setupUi (this);
        connect (pb_save,
                 SIGNAL (released ()),
                 this,
                 SLOT (modify ()));
      };
    ~dlg_modify_override () {};
    void setName (QString name) {this->la_name_v->setText (name);};
    void setOID (QString oid) {this->la_oid_v->setText (oid);};
    void setHosts (QString hosts)
      {
        if (hosts.compare ("any") == 0)
          {
            rb_hosts_new->setChecked (true);
            rb_hosts_old->setVisible (false);
          }
        else
          {
            rb_hosts_old->setText (hosts);
            rb_hosts_old->setChecked (true);
          }
      };
    void setPort (QString port)
      {
        if (port.compare ("any") == 0)
          {
            rb_port_new->setChecked (true);
            rb_port_old->setVisible (false);
          }
        else
          {
            rb_port_old->setText (port);
            rb_port_old->setChecked (true);
          }
      };
    void setThreat (QString threat)
      {
        if (threat.compare ("any") == 0)
          {
            rb_threat_new->setChecked (true);
            rb_threat_old->setVisible (false);
          }
        else
          {
            rb_threat_old->setText (threat);
            rb_threat_old->setChecked (true);
          }
      };
    void setNewThreat (QString newThreat)
      {
        la_newthreat->setText (newThreat);
        cb_threat->setCurrentIndex (cb_threat->findText (newThreat));
      };
    void addNewThreat (QString newThreat)
      {
        cb_threat->addItem (newThreat);
      };
    void setTask (QString task)
      {
        if (task.compare ("any") == 0)
          {
            rb_task_new->setChecked (true);
            rb_task_old->setVisible (false);
          }
        else
          {
            rb_task_old->setText (task);
            rb_task_old->setChecked (true);
          }
      };
    void setResult (QString result)
      {
        if (result.compare ("any") == 0)
          {
            rb_result_new->setChecked (true);
            rb_result_old->setVisible (false);
          }
        else
          {
            rb_result_old->setText (result);
            rb_result_old->setChecked (true);
          }
      };
    void setText (QString text)
      {
        te_text->setText (text);
      };
};


/**
 * @brief Protos and data structures fo dlg_modify_credential.
 */
class dlg_modify_credential : public QDialog,
                              private Ui::dlg_modify_credential
{
  Q_OBJECT

  signals:
    void sig_modify (QString, QString, QString, QString);

  private slots:
    void modify ()
      {
        QString login;
        QString password;
        if (generated)
          {
            login = "";
            password = "";
          }
        else
          {
            login = le_login->text ();
            password = le_password->text ();
          }
        emit sig_modify (le_name->text (),
                         le_comment->text (),
                         login,
                         password);
        this->close ();
      };

  private:
    bool generated;

  public:
    dlg_modify_credential ()
      {
        setupUi (this);
        connect (pb_save,
                 SIGNAL (released ()),
                 this,
                 SLOT (modify ()));
      };
    ~dlg_modify_credential () {};
    void setName (QString name) {this->le_name->setText (name);};
    void setComment (QString comment) {this->le_comment->setText (comment);};
    void setLogin (QString login) {this->le_login->setText (login);};
    void setAutogenerated (bool val) {generated = val;
                                      this->le_login->setEnabled (!val);
                                      this->le_password->setEnabled (!val);};
 };


class dlg_modify_task : public QDialog,
                        private Ui::dlg_modify_task
{
  Q_OBJECT

  signals:
    void sig_modify (int, QMap<QString, QString>);
    void sig_new_schedule ();
    void sig_new_escalator ();
    void sig_new_slave ();

  private slots:
    void modify ();
    void new_escalator ();
    void new_schedule ();
    void new_slave ();

  private:
    QString id;

  public:
    dlg_modify_task ()
      {
        setupUi (this);
        connect (pb_save,
                 SIGNAL (released ()),
                 this,
                 SLOT (modify ()));
        connect (pb_new_schedule,
                 SIGNAL (released ()),
                 this,
                 SLOT (new_schedule ()));
        connect (pb_new_escalator,
                 SIGNAL (released ()),
                 this,
                 SLOT (new_escalator ()));
        connect (pb_new_slave,
                 SIGNAL (released ()),
                 this,
                 SLOT (new_slave ()));
      }
    ~dlg_modify_task () {};
    void setId (QString id) {this->id = id;};
    void setName (QString name) {this->le_name->setText (name);};
    void setComment (QString comment) {this->le_comment->setText (comment);};
    void setConfig (QString config) {this->cb_config->addItem (config, "");};
    void setTarget (QString target) {this->cb_targets->addItem (target, "");};
    void addSchedule (QString name, QString id);
    void addEscalator (QString name, QString id);
    void addSlave (QString name, QString id);

    void selectSchedule (int pos);
    void selectEscalator (int pos);
    void selectSlave (int pos);
};


/**
 * @brief Protos and data structures for dlg_new_note.
 */
class dlg_new_note : public create_dialog,
                     private Ui::dlg_new_note
{
  Q_OBJECT

  private slots:
    void create ();

  private:
    QString task_id;
    QString nvt_oid;
    gsd_control *control;

  public:
    dlg_new_note ()
      {
        setupUi (this);
        connect (pb_create,
                 SIGNAL (released ()),
                 this,
                 SLOT (create ()));
      };
    ~dlg_new_note () {};
    void setTask (QString name)
      {
        this->rb_task_value->setText (name);
        this->rb_task_value->setChecked (true);
      };
    void setTaskId (QString id) {this->task_id = id;};
    void setNVT (QString oid) {this->nvt_oid = oid;}
    void setPort (QString port)
      {
        this->rb_port_value->setText (port);
        this->rb_port_value->setChecked (true);
      };
    void setThreat (QString threat)
      {
        this->rb_threat_value->setText (threat);
        this->rb_threat_value->setChecked (true);
      };
    void setHosts (QString hosts)
      {
        this->rb_hosts_value->setText (hosts);
        this->rb_hosts_value ->setChecked (true);
      };
    void setResult (QString result)
      {
        this->rb_result_value->setText (result);
        this->rb_result_any->setChecked (true);
      };
};


/**
 * @brief Protos and data structures for dlg_new_override.
 */
class dlg_new_override : public create_dialog,
                         private Ui::dlg_new_override
{
  Q_OBJECT

  private slots:
    void create ();

  private:
    QString task_id;
    QString nvt_oid;
    gsd_control *control;

  public:
    dlg_new_override ()
      {
        setupUi (this);
        connect (pb_create,
                 SIGNAL (released ()),
                 this,
                 SLOT (create ()));
        cb_new_threat->addItem ("High");
        cb_new_threat->addItem ("Medium");
        cb_new_threat->addItem ("Low");
        cb_new_threat->addItem ("Log");
        cb_new_threat->addItem ("False Positive");
        cb_new_threat->setCurrentIndex (4);
      };
    ~dlg_new_override () {};
    void setTask (QString name)
      {
        this->rb_task_value->setText (name);
        this->rb_task_value->setChecked (true);
      };
    void setTaskId (QString id) {this->task_id = id;};
    void setNVT (QString oid) {this->nvt_oid = oid;}
    void setPort (QString port)
      {
        this->rb_port_value->setText (port);
        this->rb_port_value->setChecked (true);
      };
    void setThreat (QString threat)
      {
        this->rb_threat_value->setText (threat);
        this->rb_threat_value->setChecked (true);
      };
    void setHosts (QString hosts)
      {
        this->rb_hosts_value->setText (hosts);
        this->rb_hosts_value ->setChecked (true);
      };
    void setResult (QString result)
      {
        this->rb_result_value->setText (result);
        this->rb_result_any->setChecked (true);
      };
};


class dlg_new_slave : public create_dialog,
                      private Ui::dlg_new_slave
{
  Q_OBJECT

  private slots:
    void create ();

  public:
    dlg_new_slave ()
      {
        setupUi (this);
        connect (pb_create,
                 SIGNAL (released ()),
                 this,
                 SLOT (create ()));
        QIntValidator *val = new QIntValidator (0, 65535, le_port);
        le_port->setValidator(val);
      }
    ~dlg_new_slave () {};
};
#endif

