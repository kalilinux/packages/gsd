/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "gsd_dlg.h"
#include "gsd_control.h"

/**
 * @file gsd_dlg.cpp
 * @brief Holds the dialogs.
 */

/**
 * @brief Performs initial actions to control the gsd dialogs.
 *
 * @param[in]  ctl    GSD-Controller.
 */
gsd_dlg::gsd_dlg (gsd_control *ctl)
{
  this->control = ctl;

  about = new dlg_about ();
  login = new dlg_login ();
  start_gsa = new dlg_start_gsa ();
  new_task = new dlg_new_task ();
  new_target = new dlg_new_target ();
  new_config = new dlg_new_config ();
  new_schedule = new dlg_new_schedule ();
  new_escalator = new dlg_new_escalator ();
  new_credential = new dlg_new_credential ();
  new_agent = new dlg_new_agent ();
  new_slave = new dlg_new_slave ();
  new_port_list = new dlg_new_port_list ();
  modify_task = new dlg_modify_task ();

  connect (login,
           SIGNAL (sig_save_profile (QString, omp_credentials&)),
           control,
           SLOT (profile_save (QString, omp_credentials&)));
  connect (login,
           SIGNAL (sig_login (QString, int, QString, QString)),
           this,
           SLOT (accept_login (QString, int, QString, QString)));
  connect (login,
           SIGNAL (sig_check_server (QString, int, QString, QString)),
           this,
           SLOT (check_server (QString, int, QString, QString)));
  connect (login,
           SIGNAL (sig_profile_selected (int)),
           this,
           SLOT (login_update (int)));
  connect (login,
           SIGNAL (sig_remove_profile (int)),
           control,
           SLOT (profile_remove (int)));
  connect (new_task,
           SIGNAL (sig_create (int, QMap<QString, QString>)),
           this,
           SLOT (create (int, QMap <QString, QString>)));
  connect (modify_task,
           SIGNAL (sig_modify (int, QMap<QString, QString>)),
           this,
           SLOT (modify (int, QMap<QString, QString>)));
  connect (new_target,
           SIGNAL (sig_create (int, QMap<QString, QString>)),
           this,
           SLOT (create (int, QMap <QString, QString>)));
  connect (new_config,
           SIGNAL (sig_create (int, QMap<QString, QString>)),
           this,
           SLOT (create (int, QMap <QString, QString>)));
  connect (new_schedule,
           SIGNAL (sig_create (int, QMap<QString, QString>)),
           this,
           SLOT (create (int, QMap <QString, QString>)));
  connect (new_escalator,
           SIGNAL (sig_create (int, QMap<QString, QString>)),
           this,
           SLOT (create (int, QMap <QString, QString>)));
  connect (new_credential,
           SIGNAL (sig_create (int, QMap<QString, QString>)),
           this,
           SLOT (create (int, QMap <QString, QString>)));
  connect (new_agent,
           SIGNAL (sig_create (int, QMap<QString, QString>)),
           this,
           SLOT (create (int, QMap <QString, QString>)));
  connect (new_slave,
           SIGNAL (sig_create (int, QMap<QString, QString>)),
           this,
           SLOT (create (int, QMap<QString, QString>)));
  connect (new_port_list,
           SIGNAL (sig_create (int, QMap<QString, QString>)),
           this,
           SLOT (create (int, QMap <QString, QString>)));
  connect (new_task,
           SIGNAL (sig_new_target ()),
           this,
           SLOT (new_target_dlg ()));
  connect (new_task,
           SIGNAL (sig_new_config ()),
           this,
           SLOT (new_config_dlg ()));
  connect (new_task,
           SIGNAL (sig_new_escalator ()),
           this,
           SLOT (new_escalator_dlg ()));
  connect (new_task,
           SIGNAL (sig_new_schedule ()),
           this,
           SLOT (new_schedule_dlg ()));
  connect (new_task,
           SIGNAL (sig_new_slave ()),
           this,
           SLOT (new_slave_dlg ()));
  connect (new_target,
           SIGNAL (sig_new_credential ()),
           this,
           SLOT (new_credential_dlg ()));
  connect (new_target,
           SIGNAL (sig_new_port_list ()),
           this,
           SLOT (new_port_list_dlg ()));
  connect (modify_task,
           SIGNAL (sig_modify (int, QMap<QString, QString>)),
           this,
           SLOT (modify (int, QMap<QString, QString>)));
  connect (modify_task,
           SIGNAL (sig_new_schedule ()),
           this,
           SLOT (new_schedule_dlg ()));
  connect (modify_task,
           SIGNAL (sig_new_escalator ()),
           this,
           SLOT (new_escalator_dlg ()));
  connect (modify_task,
           SIGNAL (sig_new_slave ()),
           this,
           SLOT (new_slave_dlg ()));
}

gsd_dlg::~gsd_dlg ()
{
  free (about);
  free (login);
  free (start_gsa);

  free (new_task);
  free (new_target);
  free (new_config);
  free (new_schedule);
  free (new_escalator);
  free (new_credential);
  free (new_port_list);
  free (new_agent);
}


/**
 * @brief Receives create signal send by dialogs and forwards it to the
 *        controller.
 *
 * @param[in]  type       Entity type.
 * @param[in]  parameter  Parameter for creation process.
 */
void
gsd_dlg::create (int type, QMap<QString, QString> parameter)
{
  emit sig_create (type, parameter);
}


/**
 * @brief Receives modify signal send by dialogs and forwards it to the
 *        controller.
 *
 * @param[in]  type       Entity type.
 * @param[in]  parameter  Parameter for modification.
 */
void
gsd_dlg::modify (int type, QMap<QString, QString> parameter)
{
  emit sig_modify (type, parameter);
}


/**
 * @brief Shows the new agent dialog.
 */
void
gsd_dlg::new_agent_dlg ()
{
  new_agent->show ();
}


/**
 * @brief Shows the new config dialog.
 */
void
gsd_dlg::new_config_dlg ()
{
  new_config->show ();
}


/**
 * @brief Shows the new credential dialog.
 */
void
gsd_dlg::new_credential_dlg ()
{
  new_credential->show ();
}

/**
 * @brief Shows the new port_list dialog.
 */
void
gsd_dlg::new_port_list_dlg ()
{
  new_port_list->show ();
}

/**
 * @brief Shows the new escalator dialog.
 */
void
gsd_dlg::new_escalator_dlg ()
{
  new_escalator->show ();
}


/**
 * @brief Shows the new schedule dialog.
 */
void
gsd_dlg::new_schedule_dlg ()
{
  new_schedule->show ();
}


/**
 * @brief Prepares and shows the new target dialog.
 */
void
gsd_dlg::new_target_dlg ()
{
  new_target->load ();

  connect (control->getCredentialModel (),
           SIGNAL (rowsInserted (const QModelIndex&, int, int)),
           this,
           SLOT (update_target_credentials (const QModelIndex&, int, int)));

  connect (control->getPortListModel(),
           SIGNAL (rowsInserted (const QModelIndex&, int, int)),
           this,
           SLOT (update_target_port_lists (const QModelIndex&, int, int)));

  update_target_credentials (QModelIndex (), -1, -1);
  update_target_port_lists (QModelIndex (), -1, -1);

  new_target->show ();
}

void
gsd_dlg::update_target_port_lists(const QModelIndex &parent, int start, int end)
{
  model_omp_entity *portLists = control->getPortListModel ();

  for (int i = 0; i < portLists->rowCount (); i++)
    {
      QDomElement elem = portLists->getEntity (i);
      QString name = portLists->getValue (elem, "name");
      QString id = portLists->getAttr (elem, "port_list id");
      new_target->addPortList (name, id);
    }

  if (start != -1)
    new_target->selectCredential(start + 1);
}


void
gsd_dlg::update_target_credentials(const QModelIndex &parent, int start, int end)
{
  model_omp_entity *credential = control->getCredentialModel ();

  int i = 0;
  new_target->addCredential ("--", "--");
  while (i < credential->rowCount ())
    {
      QDomElement elem = credential->getEntity (i);
      QString name = credential->getValue (elem, "name");
      QString id = credential->getAttr (elem, "target id");
      new_target->addCredential (name, id);
      i++;
    }
  if (start != -1)
    new_target->selectCredential(start + 1);
}


/**
 * @brief Prepares an shows the new task dialog.
 */
void
gsd_dlg::new_task_dlg ()
{
  new_task->load ();

  connect (control->getConfigModel (),
           SIGNAL (rowsInserted (const QModelIndex&, int, int)),
           this,
           SLOT (update_task_config (const QModelIndex&, int, int)));
  connect (control->getTargetModel (),
           SIGNAL (rowsInserted (const QModelIndex&, int, int)),
           this,
           SLOT (update_task_target (const QModelIndex&, int, int)));
  connect (control->getScheduleModel (),
           SIGNAL (rowsInserted (const QModelIndex&, int, int)),
           this,
           SLOT (update_task_schedule (const QModelIndex&, int, int)));
  connect (control->getEscalatorModel (),
           SIGNAL (rowsInserted (const QModelIndex&, int, int)),
           this,
           SLOT (update_task_escalator (const QModelIndex&, int, int)));
  connect (control->getSlaveModel (),
           SIGNAL (rowsInserted (const QModelIndex&, int, int)),
           this,
           SLOT (update_task_slave (const QModelIndex&, int, int)));

  update_task_config (QModelIndex (), -1, -1);
  update_task_target (QModelIndex (), -1, -1);
  update_task_schedule (QModelIndex (), -1, -1);
  update_task_escalator (QModelIndex (), -1, -1);
  update_task_slave (QModelIndex (), -1, -1);
  new_task->show ();
}


/**
 * @brief Prepares an shows the modify task dialog.
 */
void
gsd_dlg::modify_task_dlg (int pos)
{
  model_omp_entity *tmp = control->getTaskModel ();
  QDomElement elem = tmp->getEntity (pos);
  modify_task->setId (tmp->getAttr (elem, "task id"));
  modify_task->setName (tmp->getValue (elem, "name"));
  modify_task->setComment (tmp->getValue (elem, "comment"));

  connect (control->getScheduleModel (),
           SIGNAL (rowsInserted (const QModelIndex&, int, int)),
           this,
           SLOT (update_task_schedule_modify (const QModelIndex&, int, int)));
  connect (control->getEscalatorModel (),
           SIGNAL (rowsInserted (const QModelIndex&, int, int)),
           this,
           SLOT (update_task_escalator_modify (const QModelIndex&, int, int)));
  connect (control->getSlaveModel (),
           SIGNAL (rowsInserted (const QModelIndex&, int, int)),
           this,
           SLOT (update_task_slave_modify (const QModelIndex&, int, int)));

  modify_task->setConfig (tmp->getValue (elem, "config name"));
  modify_task->setTarget (tmp->getValue (elem, "target name"));
  int selectedSchedule = control->getScheduleModel ()->getEntityIndex ("name",
      tmp->getValue (elem, "schedule name"));
  int selectedEscalator = control->getEscalatorModel ()->getEntityIndex ("name",
      tmp->getValue (elem, "escalator name"));
  int selectedSlave = control->getSlaveModel ()->getEntityIndex ("name",
      tmp->getValue (elem, "slave name"));
  update_task_schedule_modify (QModelIndex (), selectedSchedule, -1);
  update_task_escalator_modify (QModelIndex (), selectedEscalator, -1);
  update_task_slave_modify (QModelIndex (), selectedSlave, -1);

  modify_task->show ();
}


void
gsd_dlg::update_task_config(const QModelIndex &parent, int start, int end)
{
  model_omp_entity *config = control->getConfigModel ();
  int i = 0;
  while (i < config->rowCount ())
    {
      QDomElement elem = config->getEntity (i);
      QString name = config->getValue (elem, "name");
      QString id = config->getAttr (elem, "config id");
      new_task->addConfig (name, id);
      i++;
    }
  if (start != -1)
    new_task->selectConfig (start);
}

void
gsd_dlg::update_task_target (const QModelIndex &parent, int start, int end)
{
  model_omp_entity *target = control->getTargetModel ();
  int i = 0;
  while (i < target->rowCount ())
    {
      QDomElement elem = target->getEntity (i);
      QString name = target->getValue (elem, "name");
      QString id = target->getAttr (elem, "target id");
      new_task->addTarget (name, id);
      i++;
    }
  if (start != -1)
    new_task->selectTarget (start);
}

void
gsd_dlg::update_task_schedule (const QModelIndex &parent, int start, int end)
{
  model_omp_entity *schedule = control->getScheduleModel ();
  int i = 0;
  new_task->addSchedule ("--", "--");
  while (i < schedule->rowCount ())
    {
      QDomElement elem = schedule->getEntity (i);
      QString name = schedule->getValue (elem, "name");
      QString id = schedule->getAttr (elem, "schedule id");
      new_task->addSchedule (name, id);
      i++;
    }
  if (start != -1)
    new_task->selectSchedule (start + 1);
}

void
gsd_dlg::update_task_escalator (const QModelIndex &parent, int start, int end)
{
  model_omp_entity *escalator = control->getEscalatorModel ();
  int i = 0;
  new_task->addEscalator ("--", "--");
  while (i < escalator->rowCount ())
    {
      QDomElement elem = escalator->getEntity (i);
      QString name = escalator->getValue (elem, "name");
      QString id = escalator->getAttr (elem, "escalator id");
      new_task->addEscalator (name, id);
      i++;
    }
  if (start != -1)
    new_task->selectEscalator (start + 1);
}

void
gsd_dlg::update_task_slave (const QModelIndex &parent, int start, int end)
{
  model_omp_entity *slave = control->getSlaveModel ();
  int i = 0;
  new_task->addSlave ("--", "--");
  while (i < slave->rowCount ())
    {
      QDomElement elem = slave->getEntity (i);
      QString name = slave->getValue (elem, "name");
      QString id = slave->getAttr (elem, "slave id");
      new_task->addSlave (name, id);
      i++;
    }
  if (start != -1)
    new_task->selectSlave (start + 1);
}

void
gsd_dlg::update_task_schedule_modify (const QModelIndex &parent,
                                      int start,
                                      int end)
{
  model_omp_entity *schedule = control->getScheduleModel ();
  int i = 0;
  modify_task->addSchedule ("--", "--");
  while (i < schedule->rowCount ())
    {
      QDomElement elem = schedule->getEntity (i);
      QString name = schedule->getValue (elem, "name");
      QString id = schedule->getAttr (elem, "schedule id");
      modify_task->addSchedule (name, id);
      i++;
    }
  if (start != -1)
    modify_task->selectSchedule (start + 1);
}

void
gsd_dlg::update_task_escalator_modify (const QModelIndex &parent,
                                       int start,
                                       int end)
{
  model_omp_entity *escalator = control->getEscalatorModel ();
  int i = 0;
  modify_task->addEscalator ("--", "--");
  while (i < escalator->rowCount ())
    {
      QDomElement elem = escalator->getEntity (i);
      QString name = escalator->getValue (elem, "name");
      QString id = escalator->getAttr (elem, "escalator id");
      modify_task->addEscalator (name, id);
      i++;
    }
  if (start != -1)
    modify_task->selectEscalator (start + 1);
}

void
gsd_dlg::update_task_slave_modify (const QModelIndex &parent,
                                   int start,
                                   int end)
{
  model_omp_entity *slave = control->getSlaveModel ();
  int i = 0;
  modify_task->addSlave ("--", "--");
  while (i < slave->rowCount ())
    {
      QDomElement elem = slave->getEntity (i);
      QString name = slave->getValue (elem, "name");
      QString id = slave->getAttr (elem, "slave id");
      modify_task->addSlave (name, id);
      i++;
    }
  if (start != -1)
    modify_task->selectSlave (start + 1);
}


void
gsd_dlg::new_slave_dlg ()
{
  new_slave->show ();
}

void
gsd_dlg::request_failed (int type, int command, int err)
{}


/**
 * @brief Shows the about dialog.
 */
void
gsd_dlg::about_dlg ()
{
  this->about->load ();
  this->about->show ();
}


/**
 * @brief Shows the start-gsa dialog.
 */
void
gsd_dlg::start_gsa_dlg ()
{
  this->start_gsa->setDefaultURL (this->control->getLoginData ()->getServerAddress ());
  this->start_gsa->load ();
  this->start_gsa->show ();
}


/**
 * @brief Prepares and shows the login dialog.
 */
void
gsd_dlg::login_dlg ()
{
  for (int i = 0; i < control->getProfileCount (); i++)
    {
      login->setProfile (control->getProfile (i), i);
    }
  QSettings settings ("Greenbone", "GSD");
  settings.beginGroup ("Login");
  login->selectProfile (settings.value ("profile").toInt ());
  settings.endGroup ();
  this->login->show ();
  login->checkServer ();
}


/**
 * @brief Updates the login dialog content.
 *
 * @param[in]  index   Profile index.
 */
void
gsd_dlg::login_update (int index)
{
  QString name = control->getProfile (index);
  omp_credentials *crd = control->getProfile (name);

  login->setAddress (crd->getServerAddress ());
  login->setPort (crd->getServerPort ());
  login->setUsername (crd->getUserName ());
  login->checkServer ();
}


/**
 * @brief Receives the login data and forwards the data to the controller.
 *
 * @param[in]  address    Serveradress.
 * @param[in]  port       Serverport.
 * @param[in]  user       Username.
 * @param[in]  password   Userpassword.
 */
void
gsd_dlg::accept_login (QString address,
                               int port,
                               QString user,
                               QString password)
{
  emit sig_login (address, port, user, password);
}


void
gsd_dlg::check_server (QString address,
                       int port,
                       QString user,
                       QString password)
{
  emit sig_check_server (address, port, user, password);
}

/**
 * @brief Closes the login dialog.
 */
void
gsd_dlg::authentication_successful ()
{
  login->close ();
}


void
gsd_dlg::check_result (int res)
{
  login->setUrlValid (res);
}


bool
gsd_dlg::loginOpen ()
{
  return login->isOpen ();
}

