/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file delegate_date_time.h
 * @class delegate_date_time
 * @brief Protos and data structures for delegate_date_time.
 */

#ifndef DELEGATE_DATE_TIME_H
#define DELEGATE_DATE_TIME_H

#include <QtGui>

#include "model_omp_entity.h"

class delegate_date_time : public QStyledItemDelegate
{
  Q_OBJECT

  private:
    QString modifyDate (QString date, bool full) const;

  public:
    delegate_date_time ();
    ~delegate_date_time ();

    /**
     * @brief new implemented sizeHint (..), returning the size of the delegate
     */
    QSize sizeHint (const QStyleOptionViewItem&, const QModelIndex&) const 
      {
        return QSize (16,16);
      }

    void paint (QPainter* painter, const QStyleOptionViewItem& option,
                const QModelIndex& index) const;

    void setFormat (QString);
    QString formatDate (QString date, bool full);
};
#endif

