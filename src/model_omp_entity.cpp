/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "model_omp_entity.h"

/**
 * @file model_omp_entity.cpp
 * @brief Model for a Tableview containing the data in an xml format.
 *
 * Implements the QAbstractTableModel.
 * Provides functionality for adding and removing entities from the model.
 */

model_omp_entity::model_omp_entity ()
{
}


model_omp_entity::~model_omp_entity ()
{
}


/**
 * @brief New implemented flags
 *
 * @param index: Index of the item in the model
 */
Qt::ItemFlags
model_omp_entity::flags (const QModelIndex &index) const
{
  if (!index.isValid ())
    return Qt::ItemIsSelectable;

  return QAbstractTableModel::flags (index) | Qt::ItemIsSelectable;
}


/**
 * @brief new implemented rowCount.
 *
 * @param parent index of the parent in the model.
 *
 * @return size of the contentlist.
 */
int
model_omp_entity::rowCount (const QModelIndex &parent) const
{
  return s_Content.count ();
}


/**
 * @brief new implemented columnCount.
 *
 * @param parent: Index of the parent in the model
 *
 * @return Size of header list.
 */
int
model_omp_entity::columnCount (const QModelIndex &parent) const
{
  return s_HeaderData.count ();
}


/**
 * @brief new implemented data ().
 *
 * @param index index of the item in the model
 * @param role  itemrole, look at the qt-doc for more information
 *
 * @return data of the item selected by index
 */
QVariant
model_omp_entity::data (const QModelIndex &index, int role) const
{
  if (!index.isValid ())
    return QVariant ();

  if (index.row () >= s_Content.size () || index.row () < 0)
     return QVariant();
  if (role == Qt::DisplayRole) 
    {
      int i = 1;
      QStringList list = s_HeaderOmp.at (index.column ()).split (" ");
      QDomElement node = s_Content.at (index.row ());

      QDomNode child = node.firstChildElement (list.at (0));
      while (!child.isNull () && i < list.size ())
        {
          child = child.firstChildElement (list.at (i));
          i++;
        }
      QDomElement child_q = child.toElement ();
      QString temp = child_q.text ();
      if (child_q.hasChildNodes ())
        {
          QString s = child_q.text ();
          QDomNodeList list = child_q.childNodes ();
          int i = 0;
          int length = 0;
          while (i < list.length ())
            {
              length += list.at (i).toElement ().text ().length ();
              i++;
            }
          s.remove (s.length () - length, length );
          return s;
        }
      if (temp != NULL)
        return temp;
      else
        return QVariant ();
    }
  return QVariant ();
}


/**
 * @brief new implemented headerdata.
 *
 * @param section      current position of the header
 * @param orientation  orientation of the header (horizontal/vertical)
 * @param role         headerrole
 *
 * @return title of the header columns
 */
QVariant
model_omp_entity::headerData (int section, Qt::Orientation orientation,
                              int role) const
{
  if (role != Qt::DisplayRole)
    return QVariant ();

  if (orientation == Qt::Horizontal)
    {
          return s_HeaderData.at (section);
    }
  return QVariant ();
}


/**
 * @brief Add an entity to the model.
 *
 * @param entity, containing the data.
 */
int
model_omp_entity::addEntity (QDomElement ent, int pos)
{
  mutex.lock ();
  if (pos >= s_Content.size ())
    {
      beginInsertRows (QModelIndex (), s_Content.size (),
                       s_Content.size ());
      s_Content.insert (pos, ent);
      endInsertRows ();
      mutex.unlock ();
      emit sig_changed (true);
      return 0;
    }
  QDomElement temp = s_Content[pos].toElement () ;
  QDomElement node = ent.toElement ();
  if (!node.isNull () && !temp.isNull ())
    {
      if (node.nodeName ().compare (temp.nodeName ()) == 0)
        {
          if (node.text ().compare (temp.text ()) != 0 ||
              compareAttributes (node, temp) != 0)
            {
              s_Content[pos].clear ();
              s_Content[pos] = node.cloneNode (true).toElement ();
              mutex.unlock ();
              emit sig_changed (true);
              return 0;
            }
          QDomNodeList list1 = node.childNodes ();
          QDomNodeList list2 = temp.childNodes ();
          if (list1.size () != list2.size ())
            {
              s_Content[pos].clear ();
              s_Content[pos] = node.cloneNode (true).toElement ();
              mutex.unlock ();
              emit sig_changed (true);
              return 0;
            }
        }
      else
        {
          mutex.unlock ();
          return 0;
        }
    }
  if (!node.isNull () && temp.isNull ())
    {
      beginInsertRows (QModelIndex (), s_Content.size (),
                       s_Content.size ());
      s_Content.insert (pos, ent);
      endInsertRows ();
    }

  mutex.unlock ();
  emit sig_changed (true);
  return 0;
}


/**
 * @brief Compares XML-Attributes of the given nodes.
 *
 * @param node XML node.
 * @param compare XML node to compare with.
 *
 * @return 0 if attributes are equal, else -1.
 */
int
model_omp_entity::compareAttributes (QDomElement node, QDomElement compare)
{
  if (!node.isNull () && !compare.isNull ())
    {
      if (node.hasAttributes () && compare.hasAttributes ())
        {
          QDomNamedNodeMap nodeList = node.attributes ();
          QDomNamedNodeMap compareList = compare.attributes ();
          for (int i = 0;i < nodeList.size (); i++)
            {
              if (nodeList.item (i).nodeValue ().compare
                  (compareList.item (i).nodeValue ()) != 0)
                {
                  return -1;
                }
            }
        }
      else
      {
        return -1;
      }
    }
  return 0;
}


/**
 * @brief Remove an entity from the model by name and value.
 *
 * @param name Name of the entity that should be deleted.
 * @param value Text of the entity.
 *
 * @return 0 if Entity successfully removed, -1 if entity not found.
 */
int
model_omp_entity::removeEntity (QString name, QString value)
{
  int i = 0;
  while (i < s_Content.size ())
    {
      int ndx = this->getEntityIndex (name, value);

      if (ndx >= 0 )
        {
          removeRows (i, 1, QModelIndex ());
          return 0;
        }
    }
  return -1;
}


/**
 * @brief Remove an entity from the model identified by complete entity.
 *
 * @param ent the entity that should be deleted.
 */
int
model_omp_entity::removeEntity (QDomElement ent)
{
  int i = 0;

  while (i < s_Content.size ())
    {
      QDomElement temp = s_Content.at (i);

      if (temp == ent)
        {
          removeRows (i, 1, QModelIndex ());
          emit sig_changed (true);
          return 0;
        }
      i++;
    }
  return -1;
}


/**
 * @brief Remove an entity by index.
 *
 * @param ndx index of the entity.
 */
int
model_omp_entity::removeEntity (int ndx)
{
  removeRows (ndx, 1, QModelIndex ());
  emit sig_changed (true);
  return 0;
}


/**
 * @brief Remove all tasks from the model.
 */
void
model_omp_entity::removeEntities ()
{
  removeRows (0, s_Content.size (), QModelIndex ());
  emit sig_changed (true);
}


/**
 * @brief Remove multiple rows from the model
 *
 * @param position first row
 * @param rows     rowcount
 * @param index    look at qt-doc for more information
 */
bool
model_omp_entity::removeRows (int position, int rows,
                              const QModelIndex &index)
{
  beginRemoveRows (QModelIndex (), position, position+rows-1);

  for (int row = 0; row < rows; ++row)
    {
      s_Content.removeAt (position);
    }
  endRemoveRows ();

  return true;
}


/**
 * @brief Getter
 *
 * @param index the index of the taskitem.
 *
 * @return if index is valid the selected task, else empty element.
 */
QDomElement
model_omp_entity::getEntity (int index)
{
  if ((index < 0) || (index >= s_Content.size ()))
    {
      return QDomElement();
    }
  return s_Content [index];
}


/**
 * @brief Getter
 *
 * @param ent     OMP Entity
 * @param name    Name of the nodes.
 * @param parent  Name of the parent node.
 *
 * @return Returns the found nodes with parent node as root, if parent node is
 *         NULL, the parent is named "root".
 */
QDomElement
model_omp_entity::getEntities (QDomElement ent, QString name, QString parent)
{
  QDomDocument rootdoc ("doc");
  if (parent.isNull ())
    parent = "root";
  QDomElement root = rootdoc.createElement (parent);


  int i = 1;
  QStringList list = name.split (" ");
  QDomElement node = ent.firstChildElement (list.at (0)).toElement ();
  while (!node.isNull () && i < list.size() - 1)
    {
      node = node.firstChildElement (list.at (i)).toElement ();
      i++;
    }
  QDomElement temp;
  if (list.size () == 1)
    {
      i = 0;
      temp = ent;
    }
  else
    temp = node;
  if (temp.isNull ())
    {
      return root;
    }
  if (temp.hasChildNodes ())
    {
      QDomNodeList nlist = temp.childNodes ();
      int j = 0;
      int size = nlist.size ();
      while (!nlist.isEmpty () && j < size)
        {
          if (nlist.at (j).nodeName ().compare (list.at (i)) == 0)
            {
              root.appendChild (nlist.at (j).cloneNode (true));
            }
          j++;
        }
    }
  return root;

}


/**
* @brief Get Taskindex in contentlist by id.
*
* @param id id of the task.
*
* @return index of the task in contentlist if found, else -1.
*/
int
model_omp_entity::getEntityIndex (QString name, QString value)
{
  for (int i = 0; i < s_Content.size (); i++)
    {
      if (value.compare (getValue (s_Content.at (i), name)) == 0)
        return i;
    }
  return -1;
}


/**
* @brief Get the Value of a XML node.
*
* @param ent XML element.
* @param name name of the entity, identified by a list of the parent element
*             names, seperated by space.
*
* @return Value of the XML node.
*/
QString
model_omp_entity::getValue (QDomElement ent, QString name)
{
  int i = 1;
  QStringList list = name.split (" ");
  QDomElement node = ent.firstChildElement (list.at (0)).toElement ();
  while (!node.isNull () && i < list.size())
    {
      node = node.firstChildElement (list.at (i)).toElement ();
      i++;
    }
  QDomElement temp = node;
  if (temp.isNull ())
    {
      return QString ();
    }
  if (temp.hasChildNodes ())
    {
      QString s = temp.text ();
      QDomNodeList list = temp.childNodes ();
      int i = 0;
      int length = 0;
      while (i < list.length ())
        {
          length += list.at (i).toElement ().text ().length ();
          i++;
        }
      s.remove (s.length () - length, length);
      return s;
    }
  return node.text ();
}


/**
* @brief Get an Attribute from a XML node.
*
* @param ent XML element.
* @param attr name of the attribute, identified by a list of the parent element
*        names and the attribute name, seperated by space.
*
* @return Value of the attribute.
*/
QString
model_omp_entity::getAttr (QDomElement ent, QString attr)
{
  int i = 1;
  QStringList list = attr.split (" ");
  QDomElement node = ent;
  while (!node.isNull () && i < list.size()-1)
    {
      node = node.firstChildElement (list.at (i)).toElement ();
      i++;
    }
  if (node.hasAttributes ())
    {
      QDomNamedNodeMap nodeList = node.attributes ();
      if (nodeList.contains (list.at (i)))
        {
          return nodeList.namedItem (list.at (i)).nodeValue ();
        }
    }
  return NULL;
}


/**
* @brief Add a Header to the table.
*
* @param displayname name that is shown in the table view header.
* @param ompName name of the omp element containing the data for this row.
*
*/
void
model_omp_entity::addHeader (QString displayName, QString ompName)
{
  s_HeaderData.append (displayName);
  s_HeaderOmp.append (ompName);
}


/**
* @brief Get the omp header for a specified row.
*
* @param ndx index of the row.
* 
* @return omp header string.
*/
QString
model_omp_entity::getHeader (int ndx)
{
  return s_HeaderOmp[ndx];
}


/**
 * @brief Execute the filter. Request the result using getFilterIndexList.
 */
void
model_omp_entity::filter ()
{
  if (filterType == NO_FILTER)
    {
      filterList.clear ();
      return;
    }
  int i = 1;
  int j = 0;
  QStringList list = filterText.split (" ");
  QDomElement node;
  while (j < s_Content.size ())
    {
      i = 1;
      node = s_Content[j];
      while (!node.isNull () && i < list.size()-2)
        {
          node = node.firstChildElement (list.at (i)).toElement ();
          i++;
        }
      if (filterType == ATTRIBUTE_FILTER)
        {
          if (node.hasAttributes ())
            {
              QDomNamedNodeMap nodeList = node.attributes ();
              if (nodeList.contains (list.at (i)))
                {
                  if (nodeList.namedItem (list.at (i)).nodeValue ().compare (list.at (i+1)) == 0)
                    {
                        filterList.append (j);
                    }
                }
            }
        }

      else if (filterType == VALUE_FILTER)
        {
          node = node.firstChildElement (list.at (i)).toElement ();
          QDomElement temp = node.toElement ();
          if (temp.isNull ())
            {
              return;
            }
          QString s = temp.text ();
          QDomNodeList nlist = temp.childNodes ();
          int k = 0;
          int length = 0;
          while (k < nlist.length ())
            {
              length += nlist.at (k).toElement ().text ().length ();
              k++;
            }
          s.remove (s.length () - length, length);
          if (s.compare (list.at (i+1)) == 0)
            {
              filterList.append (j);
            }
        }
      j++;
    }
    return;
}


/**
 * @brief Set filter text.
 *
 * @param text Filter text, containing the xml path to the entity, separated
 *             by blank character.
 */
void
model_omp_entity::setFilter (QString text)
{
  this->filterText = text;
  this->filter ();
}


/**
 * @brief Sets filter type. Set to ATTRIBUTE_FILTER if filter text contains
 *        an attribute, VALUE_FILTER if filtertext contains a value and
 *        NO_FILTER to disable filtering.
 *
 * @param type Filter type.
 */
void
model_omp_entity::setFilterType (FilterType type)
{
  this->filterType = type;
}


/**
 * @brief Resets the filter.
 *
 * Sets filtertype to NO_FILTER, clears the filter index list and the filter text.
 */
void
model_omp_entity::resetFilter ()
{
  this->filterType = NO_FILTER;
  this->filterList.clear ();
  this->filterText = "";
}


/**
 * @brief Getter for index list, containing the filtered entities. 
 *        Contains a list of entities that fit to the filter text.
 *
 * @return Index list.
 */
QList <int>
model_omp_entity::getFilterIndexList ()
{
  return filterList;
}
