/**
 *
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file omp_utilities.h
 * @class omp_utilities
 * @brief Protos and data structures for omp_utilities.
 */

#ifndef OMP_UTILITIES_H
#define OMP_UTILITIES_H

#include <QObject>

#include "omp_credentials.h"
#include "entity_defines.h"
#include "model_omp_entity.h"

#include <openvas/omp/xml.h>
#include <openvas/omp/omp.h>
#include <openvas/misc/openvas_server.h>

class omp_utilities : public QObject
{
  Q_OBJECT

  public:
    omp_utilities ();
    ~omp_utilities ();

    enum omp_type_t
      {
        NONE = 0,
        AGENT = 1,
        CONFIG = 2,
        DEPENDENCY = 3,
        ESCALATOR = 4,
        LSC_CREDENTIAL = 5,
        NOTE = 6,
        NVT = 7,
        NVT_FAMILY = 8,
        NVT_FEED_CHECKSUM = 9,
        OVERRIDE = 10,
        PREFERENCE = 11,
        REPORT = 12,
        RESULT = 13,
        SCHEDULE = 14,
        SYSTEM_REPORT = 15,
        TARGET_LOCATOR = 16,
        TARGET = 17,
        TASK = 18,
        VERSION = 19,
        REPORT_FORMAT = 20,
        SLAVE = 21,
        PORT_LIST = 22,
        PORT_RANGE = 23
      };
    typedef int omp_type;

    enum omp_entity_command
      {
        OMP_GET = 30,
        OMP_CREATE = 31,
        OMP_DELETE = 32,
        OMP_MODIFY = 33,
        OMP_COMMAND = 34,
        OMP_DETAILS = 35,
        OMP_PREFERENCES =36,
        OMP_EXPORT = 37,
      };
    typedef int omp_entity_command;

    enum omp_task_command
      {
        START = 40,
        STOP = 41,
        PAUSE = 42,
        RESUME_STOPPED = 43,
        RESUME_PAUSED = 44,
      };
    typedef int omp_task_command;
    int checkResponse (QString response);
    omp_type getType (QString);

  private:
    QString appendCreationParameter (omp_utilities::omp_type,
                                     QMap<QString, QString> parameter);
};


/**
 * @brief Container for an omp command request.
 *
 * Takes a model, a command and an omp type to store a complete request.
 */
class request_container
{
  private:
    model_omp_entity *model;
    QString omp_string;
    QString type;

  public:
    request_container (QString type, QString omp_string,
                       model_omp_entity * model)
      {
        this->model = model;
        this->omp_string = omp_string;
        this->type = type;
      };
    ~request_container (){};

    model_omp_entity *getModel () {return this->model;};
    QString getOMPString () {return this->omp_string;};
    QString getType () {return this->type;};
};

#endif

