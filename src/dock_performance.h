/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file dock_performance.h
 * @class dock_performance
 * @brief Protos and data structures for dock_performance.
 */

#ifndef DOCK_PERFORMANCE_H
#define DOCK_PERFORMANCE_H

#include <QtGui>
#include "ui_dock_performance.h"
#include "model_omp_entity.h"
class dock_performance : public QDockWidget, private Ui::dock_performance
{
  Q_OBJECT

  signals:
    void sig_request_system_reports (QString, long);

  public:
    dock_performance ();
    ~dock_performance ();

  public:
    void setReportModel (model_omp_entity *m);
    void setGraphicsModel (model_omp_entity *m);
    void load ();
  private slots:
    void selectionChanged ();
    void timeChanged ();
    void comboChanged (int index);
    void saveAsPNG ();
    void sizeChangedX (int i);
    void sizeChangedY (int i);
  private:
    model_omp_entity *graphicsModel;
    model_omp_entity *reportModel;
    int sizeX;
    int sizeY;
    QPixmap shownPixmap;
    int currentIndex;
    bool time_changed;
};
#endif

