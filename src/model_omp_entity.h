/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file model_omp_entity.h
 * @class model_omp_entity
 * @brief Protos and data structures for model_omp_entity.
 */

#ifndef MODEL_OMP_ENTITY_H
#define MODEL_OMP_ENTITY_H

#include <QtGui>
#include <qdom.h>
#include <glib.h>
#include <openvas/omp/xml.h>
#include <openvas/omp/omp.h>
#include <openvas/misc/openvas_server.h>

class model_omp_entity : public QAbstractTableModel
{
  Q_OBJECT

  signals:
    void sig_changed (bool);

  public:
    model_omp_entity ();
    ~model_omp_entity ();

    enum filterType
      {
        ATTRIBUTE_FILTER = 2,
        VALUE_FILTER = 1,
        NO_FILTER = 0
      };
    typedef filterType FilterType;

    virtual Qt::ItemFlags flags (const QModelIndex &index) const;

    virtual int rowCount (const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount (const QModelIndex &parent = QModelIndex()) const;

    virtual QVariant data (const QModelIndex &index, 
                           int role = Qt::DisplayRole) const;
    virtual QVariant headerData (int section, Qt::Orientation orientation,
                                 int role = Qt::DisplayRole) const;

    int addEntity (QDomElement ent, int pos);
    int removeEntity (QString name, QString value);
    int removeEntity (QDomElement ent);
    int removeEntity (int ndx);
    void removeEntities ();

    QDomElement getEntity (int index);
    QDomElement getEntities (QDomElement ent, QString name, QString parent);
    int getEntityIndex (QString name, QString value);
    QString getValue (QDomElement ent, QString name);
    QString getAttr (QDomElement ent, QString attr);

    int compareAttributes (QDomElement node, QDomElement comp);
    bool removeRows (int position, int rows, const QModelIndex &index);

    void addHeader (QString displayName, QString ompName);
    QString getHeader (int ndx);

    void setFilter (QString filtertext);
    void setFilterType (FilterType type);
    void resetFilter ();
    QList <int> getFilterIndexList ();

  private:
    QList<QDomElement> s_Content;
    QStringList m_headers;
    QList<QString> s_HeaderData;
    QList<QString> s_HeaderOmp;
    QMutex mutex;
    QString filterText;
    QList <int> filterList;
    FilterType filterType;
    void filter ();
};
#endif

