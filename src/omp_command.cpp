/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "omp_command.h"

#define OMP_DEBUG if (0) qDebug() << __PRETTY_FUNCTION__

/**
 * @file omp_command.cpp
 * @class omp_command
 * @brief Thread to execute omp request.
 *
 * Implements the functionality to omp_command.h
 * Inherits basic omp- and thread functionalty from omp_basic and QThread
 */

omp_command::omp_command ()
{
  util = new omp_utilities ();
  this->running = false;
  this->helper = NULL;
}

omp_command::~omp_command ()
{
  free (util);
}


/**
 * @brief Starting point for the thread
 *
 * Starts an update for an omp entity.
 */
void
omp_command::run ()
{
  while (true)
    {
      mutex.lock ();
      this->running = true;
      int i = 0;
      helper->threadStarted ();
      while (queue.size () > 0)
        {
          if (queue.first ()->getType ().compare ("request") == 0)
            {
              request (queue.first ()->getOMPString (),
                                 queue.first ()->getModel ());
              queue.removeFirst ();
            }
          else if (queue.first ()->getType ().compare ("create") == 0)
            {
              create (queue.first ()->getOMPString (),
                                queue.first ()->getModel ());
              queue.removeFirst ();
            }
          else if (queue.first ()->getType ().compare ("command") == 0)
            {
              command (queue.first ()->getOMPString ());
              queue.removeFirst ();
            }
        }
      this->running = false;
      helper->threadFinished ();
      mutex.unlock ();
      return;
    }
}


/**
 * @brief Takes a request, and starts the thread.
 *
 * @param[in]   omp_string  Containing the omp command.
 * @param[out]  m           model for the requested update.
 *
 * @return 0
 */
int
omp_command::requestEntity (QString omp_string, model_omp_entity *m)
{
  if (this->getCredentials () == NULL)
    return -1;
  request_container *container = new request_container ("request",
                                                       omp_string, m);

  int i = 0;
  while (i < queue.size ())
    {
      if (queue.at (i)->getOMPString ().compare (omp_string) == 0)
        return 0;
      i++;
    }
  queue.append (container);

  if (this->running)
    {
      return 0;
    }
  else
    start ();
  return 0;
}


/**
 * @brief Takes an updaterequest, and starts the thread.
 *
 * @param[in]   omp_string  Containing the omp command.
 * @param[out]  m           Model for the omp entities.
 *
 * @return   0  Success.
 */
int
omp_command::createEntity (QString omp_string, model_omp_entity *m)
{
  if (this->getCredentials () == NULL)
    {
      return -1;
    }
  request_container *container = new request_container ("create",
                                                        omp_string, m);
  queue.append (container);

  if (this->running)
    {
      return 0;
    }
  else
    start ();
  return 0;
}


/**
 * @brief Takes a delete entity command and starts the thread.
 *
 * @param[in]   omp_string  Containing the omp command.
 *
 * @return   0  Success.
 */
int
omp_command::removeEntity (QString omp_string)
{
  if (this->getCredentials () == NULL)
    {
      return -1;
    }

  request_container *container = new request_container ("command", omp_string,
                                                        NULL);
  queue.append (container);
  if (this->running)
    {
      return 0;
    }
  else
    start ();
  return 0;
}


/**
 * @brief Takes a modify entity command and starts the thread.
 *
 * @param[in]   omp_string  Containing the omp command.
 *
 * @return   0  Success.
 */
int
omp_command::modifyEntity (QString omp_string)
{
  if (this->getCredentials () == NULL)
    {
      return -1;
    }

  request_container *container = new request_container ("command", omp_string,
                                                        NULL);
  queue.append (container);

  if (this->running)
    {
      return 0;
    }
  else
    start ();
  return 0;
}


/**
 * @brief Authenticates with a manager.
 */
void
omp_command::authenticate ()
{
  if (this->getCredentials () == NULL)
    {
      return ;
    }

  request_container *container = new request_container ("command",
                                                        "<authenticate/>",
                                                        NULL);
  queue.append (container);

  if (this->running)
    {
      return ;
    }
  else
    start ();
  return ;

}

/**
 * @brief Takes a modify entity command and starts the thread.
 *
 * @param[in]   omp_string  Containing the omp command.
 *
 * @return   0  Success.
 */
int
omp_command::runTaskCommand (QString omp_string)
{
  if (this->getCredentials () == NULL)
    {
      return -1;
    }

  request_container *container = new request_container ("command", omp_string,
                                                        NULL);
  queue.append (container);

  if (this->running)
    {
      return 0;
    }
  else
    start ();
  return 0;
}


/**
 * @brief Executes an entity request command.
 *
 * @param[in]   omp_string  Containing the omp command.
 * @param[out]  m           Model for the omp entities.
 *
 * @return   0  Success.
 *          -1  Internal error (http error 500).
 *          -2  Http error 40X.
 *          -3  Logindata missing.
 *          -4  Connection failed
 *          -5  Message tranfer failed.
 *          -6  Failed to read response.
 *          -7  Response was a NULL node.
 *          -8  QDom conversion failed
 */
int
omp_command::request (QString omp_string, model_omp_entity *m)
{
  OMP_DEBUG << omp_string;
  entity_t entity = NULL;
  int i = 0, ret = 0;
  QString type ("");
  if (this->getCredentials () == NULL)
    ret = -3;

  // Open a conenction to the manager.
  if (ret == 0)
    if (this->openConnection () == -1)
      ret = -4;
  // Send omp_string to manager.
  if (ret == 0)
    if (openvas_server_send (&session, omp_string.toLatin1 ()) == -1)
      ret = -5;
  //read the managers response.
  if (ret == 0)
    if (read_entity (&session, &entity))
      ret = -6;
  if (entity == NULL && ret == 0)
    ret = -7;
  // Use the openvas libraries to print the response to a GSTring type.
  if (ret == 0)
    {
      GString *response_g = new GString ();
      print_entity_to_string (entity, response_g);
      QString response_q (response_g->str);
      QDomElement doc = convertToQDomDoc (*response_g);
      if (doc.isNull ())
        ret = -8;
      QDomNodeList entityList;
      // Find type of entities.
      type = doc.tagName ();
      type = type.remove ("get_");
      type = type.remove ("s_response");
      if (type.endsWith ("ie"))
        type = type.replace (type.length ()-2, 2, "y");
      if (type.contains ("nvt_family"))
        type = type.remove ("nvt_");
      if (type.contains ("version"))
        {
           OMP_DEBUG << "Response for version: " << response_q;
           type = "version";
        }
      entityList = doc.elementsByTagName (type);
      int j = 0;
      while (i < entityList.length ())
        {
          if (type == "nvt")
            {
              if (entityList.item (i).parentNode  ().nodeName ().compare ("preference") != 0)
                {
                  m->addEntity (entityList.item (i).toElement (), j);
                  j++;
                }
              i++;
            }
          else if (omp_string.contains ("export"))
            {
              m->addEntity (doc, 0);
              break;
            }
          else
            {
              m->addEntity (entityList.item (i).toElement (), i);
              i++;
            }
        }
      free (response_g);
      free_entity (entity);
      ret = util->checkResponse (response_q);
    }
  closeConnection ();
  if (omp_string.contains ("get_nvts") && omp_string.contains ("family"))
    type = "family";
  if (ret != 0)
    {
      OMP_DEBUG << "Failed Request of type: " << util->getType (type);
      helper->requestFailed(util->getType (type), omp_utilities::OMP_GET,
                            ret);
      return ret;
    }
  int com = omp_utilities::OMP_GET;
  if (omp_string.contains ("preferences"))
    com = omp_utilities::OMP_PREFERENCES;
  else if (omp_string.contains ("details"))
    com = omp_utilities::OMP_DETAILS;
  else if (omp_string.contains ("export"))
    com = omp_utilities::OMP_EXPORT;
  QString userData;
  if (omp_string.contains ("config_id"))
    userData = omp_string.section ("\"", 1,1);
  if (type.compare ("note") == 0 && omp_string.contains ("note_id"))
    userData = m->getAttr (m->getEntity (0), "note id");
  if (type.compare ("override") == 0 && omp_string.contains ("override_id"))
    userData = m->getAttr (m->getEntity (0), "override id");
  if (type.compare ("report") == 0)
    {
      userData = m->getAttr (m->getEntity (0), "report id");
      if (omp_string.contains ("html") ||
          m->getAttr (m->getEntity (0), "report extension").compare ("html")
            == 0)
        userData.append ("##html");
      else if (omp_string.contains ("xml") ||
               m->getAttr (m->getEntity (0),
                           "report extension").compare ("xml") == 0)
        userData.append ("##xml");
    }
  if (type.compare ("version") == 0)
    userData = m->getEntity (0).text ();
  OMP_DEBUG << "Request of type: " << type << " Finished."
            << "added userData: " << userData;
  helper->requestFinished (util->getType (type), com, userData);
  return ret;
}


/**
 * @brief Creates a new entity.
 *
 * @param[in]   omp_string  Containing the omp command.
 * @param[out]  m           Response with id of the new element.
 *
 * @return   0  Success.
 *          -1  Internal error (http error 500).
 *          -2  Http error 40X.
 *          -3  Logindata missing.
 *          -4  Connection failed
 *          -5  Message tranfer failed.
 *          -6  Failed to read response.
 *          -7  Response was a NULL node.
 *          -8  QDom conversion failed
 */
int
omp_command::create (QString omp_string, model_omp_entity *m)
{
  OMP_DEBUG << omp_string;
  entity_t entity = NULL;
  unsigned int i = 0, ret = 0;
  QString type ("");

  if (this->getCredentials () == NULL)
    ret = -3;

  // Open a conenction to the manager.
  if (this->openConnection () == -1 && ret == 0)
    ret = -4;
  // Send omp_string to manager.
  if (openvas_server_send (&session, omp_string.toUtf8 ()) == -1 && ret == 0)
    ret = -5;

  //read the managers response.
  if (read_entity (&session, &entity) && ret == 0)
    ret = -6;

  if (entity == NULL && ret == 0)
    ret = -7;

  // Use the openvas libraries to print the response to a GSTring type.
  if (ret == 0)
    {
      GString *response_g = new GString ();
      print_entity_to_string (entity, response_g);
      QString response_q (response_g->str);

      QDomElement doc = convertToQDomDoc (*response_g);
      if (doc.isNull ())
        ret = -8;

      type = response_q.remove ("<create_");
      type = type.remove (type.indexOf ("_response"),
                          type.length () - type.indexOf ("_response"));

      m->addEntity (doc.firstChildElement ("type"), 0);

      free_entity (entity);
      free (response_g);
      ret = util->checkResponse (response_q);
    }

  closeConnection ();
  if (ret != 0)
    {
      OMP_DEBUG << "Create request failed for type " << type
                << " with code: " << ret;
      helper->requestFailed(util->getType (type), omp_utilities::OMP_CREATE,
                            ret);
      return ret;
    }
  helper->requestFinished (util->getType (type), omp_utilities::OMP_CREATE, "");
  return ret;
}


/**
 * @brief Executes a delete, modify or task operation.
 *
 * @param[in]   omp_string  Containing the omp command.
 * @param[out]  m           Response with id of the new element.
 *
 * @return   0  Success.
 *          -1  Internal error (http error 500).
 *          -2  Http error 40X.
 *          -3  Logindata missing.
 *          -4  Connection failed
 *          -5  Message tranfer failed.
 *          -6  Failed to read response.
 *          -7  Response was a NULL node.
 */
int
omp_command::command (QString omp_string)
{
  OMP_DEBUG << omp_string;
  entity_t entity = NULL;
  int ret = 0;

  if (this->getCredentials () == NULL)
    ret = -3;

  // Open a conenction to the manager.
  if (ret == 0)
    if (this->openConnection () == -1)
      ret = -4;
  // Send omp_string to manager.
  if (ret == 0)
    if (openvas_server_send (&session, omp_string.toLatin1 ()) == -1)
      ret = -5;

  //read the managers response.
  if (ret == 0)
    if (read_entity (&session, &entity))
      ret = -6;

  if (entity == NULL && ret == 0)
    ret = -7;

  QVariant var_prot_version = qApp->property("protocol_version");
  float protocol_version = -1;
  if (var_prot_version.isValid())
    protocol_version = var_prot_version.toFloat();

  QString str_type ("");
  omp_utilities::omp_type type;
  int com;
  if (omp_string.contains ("start"))
    {
      com = omp_utilities::START;
      type = omp_utilities::TASK;
    }
  else if (omp_string.contains ("stop_"))
    {
      com = omp_utilities::STOP;
      omp_utilities::TASK;
    }
  else if (omp_string.contains ("pause"))
    {
      type = omp_utilities::TASK;
      com = omp_utilities::PAUSE;
    }
  else if (omp_string.contains ("resume_stopped"))
    {
      type = omp_utilities::TASK;
      com = omp_utilities::RESUME_STOPPED;
    }
  else if (omp_string.contains ("resume_paused"))
    {
      type = omp_utilities::TASK;
      com = omp_utilities::RESUME_PAUSED;
    }
  else if (omp_string.contains ("delete"))
    {
      com = omp_utilities::OMP_DELETE;
    }
  else if (omp_string.contains ("create"))
    {
      com = omp_utilities::OMP_CREATE;
    }
  else if (omp_string.contains ("modify"))
    com = omp_utilities::OMP_MODIFY;

  if (omp_string.contains ("task") &&
      omp_string.indexOf ("task") < 10)
    type = omp_utilities::TASK;
  else if (omp_string.contains ("target") &&
           omp_string.indexOf ("target") < 10)
    type = omp_utilities::TARGET;
  else if (omp_string.contains ("config") &&
           omp_string.indexOf ("config") < 10)
    type = omp_utilities::CONFIG;
  else if (omp_string.contains ("schedule") &&
           omp_string.indexOf ("schedule") < 10)
    type = omp_utilities::SCHEDULE;
  else if ((omp_string.contains ("escalator") &&
            omp_string.indexOf ("escalator") < 10) ||
           (protocol_version > 3 &&
           (omp_string.contains ("alert") &&
            omp_string.indexOf ("alert") < 10)))
    type = omp_utilities::ESCALATOR;
  else if (omp_string.contains ("agent") &&
           omp_string.indexOf ("agent") < 10)
    type = omp_utilities::AGENT;
  else if (omp_string.contains ("lsc_credential") &&
           omp_string.indexOf ("lsc_credential") < 10)
    type = omp_utilities::LSC_CREDENTIAL;
  else if (omp_string.contains ("note") &&
           omp_string.indexOf ("note") < 10)
    type = omp_utilities::NOTE;
  else if (omp_string.contains ("override") &&
           omp_string.indexOf ("override") < 10)
    type = omp_utilities::OVERRIDE;
  else if (omp_string.contains ("report") &&
           omp_string.indexOf ("report") < 10)
    type = omp_utilities::REPORT;
  else if (omp_string.contains ("port_list") &&
           omp_string.indexOf ("port_list") < 10)
    type = omp_utilities::PORT_LIST;

  // Use the openvas libraries to print the response to a GSTring type.
  if (ret == 0)
    {
      GString *response_g = new GString ();
      print_entity_to_string (entity, response_g);
      QString response_q (response_g->str);

      free_entity (entity);
      free (response_g);

      closeConnection ();
      ret = util->checkResponse (response_q);
      if (ret != 0)
        {
          helper->requestFailed(type, com, ret);
          return ret;
        }
      QString data;
      if (type == omp_utilities::CONFIG)
        {
          data = omp_string.section ("\"", 1,1);
          if (omp_string.contains ("nvt_selection"))
            data += "__" + omp_string.section ("family>", 1,1);
          else if (omp_string.contains ("<preference>"))
            {
              QString tmp = omp_string.remove (0,
                                               omp_string.indexOf ("oid") + 5);
              tmp = tmp.remove (omp_string.indexOf ("\""),
                                omp_string.length () -
                                omp_string.indexOf ("\""));
              data += "##" + tmp;
            }
          if (data.contains ("</"))
            data.remove ("</");
        }
      if (type == omp_utilities::NOTE)
        {
          data = omp_string.section ("\"", 1, 1);
        }
      if (type == omp_utilities::OVERRIDE)
        {
          data = omp_string.section ("\"", 1, 1);
        }
      helper->requestFinished (type, com, data);
    }
  else
    {
      helper->requestFailed(type, com, ret);
      return ret;
    }
  return ret;
}


/**
 * @brief Converts struct entity_t to QDomDocument.
 *
 * @param ent entity that contains omp data.
 *
 * @return Returns the converted QDomDocument.
 */
QDomElement
omp_command::convertToQDomDoc (GString ent)
{
  int errLine, errCol;
  QString errMsg;

  QString qResponse = QString::fromUtf8 (ent.str);
  QDomDocument *doc = new QDomDocument ();
  if (!doc->setContent (qResponse, true, &errMsg, &errLine, &errCol))
    {
      helper->log (tr ("Conversion to QDomElement failed!"
                       "\n %1 at: %2 , %3").arg (errMsg)
                                           .arg (errLine)
                                           .arg (errCol), 3);
      doc = NULL;
    }
  return doc->documentElement ();
}


/**
 * @brief Setter for thread helper.
 *
 * @param[in]  helper   Thread helper to send signals from  a thread to
 *                      an other.
 */
void
omp_command::setHelper (omp_thread_helper *help)
{
  this->helper = help;
}

