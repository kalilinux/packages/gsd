/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file omp_command.h
 * @brief Protos and data structures for omp_command.
 *
 * TODO Thread save
 */
#ifndef OMP_COMMAND_H
#define OMP_COMMAND_H

#include <QtGui>
#include <qdom.h>

#include "omp_basic.h"
#include "omp_utilities.h"
#include "omp_thread_helper.h"
#include "model_omp_entity.h"

class omp_command : public omp_basic, protected QThread
{
  private:
    virtual void run ();

    bool running;
    QWaitCondition update;
    QList <request_container *> queue;
    omp_utilities *util;

    int request (QString omp_string, model_omp_entity *m);
    int create (QString omp_string, model_omp_entity *m);
    int command (QString omp_string);
    QDomElement convertToQDomDoc (GString string);

  protected:
    omp_thread_helper *helper;

  public:
    omp_command ();
    ~omp_command ();

    QMutex mutex;

    void setHelper (omp_thread_helper *help);

    int requestEntity (QString omp_string, model_omp_entity *m);
    int createEntity (QString omp_string, model_omp_entity *m);
    int runTaskCommand (QString omp_string);
    int removeEntity (QString omp_string);
    int modifyEntity (QString omp_string);
    void authenticate ();
};
#endif

