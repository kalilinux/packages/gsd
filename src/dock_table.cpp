/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "dock_table.h"

/**
 * @file dock_table.cpp
 *
 * @brief Dock widget for tables.
 *
 * This widget uses a QTableView to show data depending on the model.
 */

/**
 * @brief Creates a new dock widget
 *
 * Creates a table dock widget.
 */
dock_table::dock_table()
{
  setupUi (this);
  menu = new QMenu (this);
  menu_title = new QAction (this);
  tw_table->setEditTriggers(QAbstractItemView::NoEditTriggers);
  tw_table->setAlternatingRowColors(true);
  tw_table->setSelectionMode(QAbstractItemView::SingleSelection);
  tw_table->setSelectionBehavior(QAbstractItemView::SelectRows);
  tw_table->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
  tw_table->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
  tw_table->verticalHeader ()->setVisible (false);
  tw_table->horizontalHeader ()->setHighlightSections (false);

  toolBar = new QToolBar ();
  QMainWindow *win = new QMainWindow (this);
  win->setWindowFlags (Qt::Widget);
  win->addToolBar (toolBar);
  this->setWidget (win);
  win->setCentralWidget (tw_table);
  toolBar->setToolButtonStyle (Qt::ToolButtonIconOnly);
  toolBar->setMinimumSize(QSize(0, 0));
  toolBar->setSizeIncrement(QSize(28, 28));
  toolBar->setBaseSize(QSize(28, 28));
  toolBar->setIconSize(QSize(18, 18));
  toolBar->setFloatable (false);
  toolBar->setMovable (false);
  toolBar->toggleViewAction ()->setVisible (false);
  tw_table->setContextMenuPolicy (Qt::CustomContextMenu);
  connect (tw_table, SIGNAL (customContextMenuRequested (const QPoint &)),
           this, SLOT (contextmenu (const QPoint &)));
  menu->addAction (menu_title);
  menu->addSeparator ();


}

dock_table::~dock_table()
{
}


/**
 * @brief Setter
 *
 * Use to set the model containing data and table header.
 */
void
dock_table::setModel (QAbstractTableModel *model)
{
  tw_table->setModel( model);
}


/**
 * @brief Getter
 *
 * @return Tableview shown in the dock widget.
 */
QTableView *
dock_table::getTable ()
{
  return tw_table;
}


/**
 * @brief Adds a new action to toolbar.
 */
void
dock_table::addActionToToolBar (QAction *action)
{
  toolBar->addAction (action);
}


/**
 * @brief Adds a new action to context menu.
 */
void
dock_table::addActionToMenu (QAction *action)
{
  menu->addAction (action);
}


/**
 * @brief Adds a separator to toolbar.
 */
void
dock_table::addSeparator ()
{
  toolBar->addSeparator ();
}


/**
 * @brief Adds a separator to context menu.
 */
void
dock_table::addSeparatorToMenu ()
{
  menu->addSeparator ();
}


/**
 * @brief Hides the toolbar.
 */
void
dock_table::hideToolBar ()
{
  toolBar->hide ();
}


/**
 * @brief SLOT to show the context menu for the table view
 *
 * @param position: Position of the cursor
 */
void
dock_table::contextmenu (const QPoint &position)
{
  QModelIndexList selected = tw_table->selectionModel ()->selectedRows (0);
  if (selected.size() > 0)
    {
      if (tw_table->indexAt (position).isValid ()) 
        {
          QPoint p (position.x (), position.y () + 22);
          QString n (this->objectName () + tr ("  "));
          model_omp_entity *m = (model_omp_entity *) tw_table->model ();
          QDomNode node = m->getEntity (selected. at (0).row ());
          n.append (m->getValue (node.toElement (), "name"));
          QFont f = menu_title->font ();
          f.setBold (true);
          menu_title->setFont (f);
          menu_title->setText (n);
          menu_title->setEnabled (false);

          menu->exec (tw_table->mapToGlobal (p));
        }
    }
}


/**
 * @brief Display tool tip.
 *
 * @param pos_in  cursor position.
 * @param pos_out tool tip position (global position).
 */
void
dock_table::showToolTip (QPoint pos_in, QPoint pos_out)
{
  model_omp_entity *m = (model_omp_entity *) tw_table->model ();
  QDomNode node = m->getEntity (tw_table->indexAt (pos_in).row ());
  QString text (tr ("Name: "));
  text.append (m->getValue (node.toElement (), "name"));
  text.append (tr ("\nComment: "));
  text.append (m->getValue (node.toElement (), "comment"));

  QToolTip::showText (pos_out, text);
}
