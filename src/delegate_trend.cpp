/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "delegate_trend.h"

/**
 * @file delegate_trend.cpp
 * @brief Delegate for trend icons.
 *
 * Used for painting the trendicons in widgets like tableview, treeview, etc.
 */

delegate_trend::delegate_trend()
{
}

delegate_trend::~delegate_trend()
{
}


/**
 * @brief New implemented paintfunction.
 *
 * Implementation to paint a trend icon in a view widget.
 * Paints trend icon depending on the status of a task.
 *
 * @param painter Painter to draw the icon
 * @param option  Styleoptions
 * @param index   viewindex
 */
void
delegate_trend::paint (QPainter* painter,
                       const QStyleOptionViewItem& option,
                       const QModelIndex& index) const
{
  model_omp_entity * model = (model_omp_entity*) index.model ();

  if (model == 0)
    {
      QStyledItemDelegate::paint (painter, option, index);
      return;
    }

  QStyleOptionButton opts;
  QDomElement pItem = model->getEntity (index.row ());

  if (!pItem.isNull ())
    {
      if (option.state & QStyle::State_Selected)
        {
          if (!(option.state & QStyle::State_Active))
            painter->fillRect (option.rect,
                               option.palette.color (QPalette::Inactive,
                                                     QPalette::Highlight));
          else
            painter->fillRect (option.rect,
                               option.palette.color (QPalette::Active,
                                                     QPalette::Highlight));
        }
      opts.rect = option.rect;
      opts.rect.setRight (option.rect.right ()-18);
      opts.rect.setLeft (option.rect.left ()+18);
      opts.rect.setTop (option.rect.top ()+3);
      opts.rect.setHeight (option.rect.height ()-6);
      opts.iconSize = QSize (16, 16);
      opts.state = QStyle::State_Enabled;

      QString s = model->getValue (pItem, "trend");
      if (s.isNull () && index.column () == 2)
        s = model->getValue (pItem, "family_count growing");
      else if (s.isNull () && index.column () == 4)
        s = model->getValue (pItem, "nvt_count growing");

      if (s.isNull ())
        s = model->getValue (pItem, "growing");

      if (s. compare ("up") == 0)
        {
          opts.icon.addPixmap
           (QPixmap (QString::fromUtf8 (":/img/trend_up.png")),
            QIcon::Normal, QIcon::On);
          QApplication::style ()->drawControl
           (QStyle::CE_PushButtonLabel, &opts, painter);
          return;
        }
      else if (s.compare ("more") == 0 || s.compare ("1") == 0)
        {
          opts.icon.addPixmap
           (QPixmap (QString::fromUtf8 (":/img/trend_more.png")),
            QIcon::Normal, QIcon::On);
          QApplication::style ()->drawControl
           (QStyle::CE_PushButtonLabel, &opts, painter);
           return;
        }
      else if (s.compare ("same") == 0 || s.compare ("0") == 0)
        {
          opts.icon.addPixmap
           (QPixmap (QString::fromUtf8 (":/img/trend_nochange.png")),
            QIcon::Normal, QIcon::On);
          QApplication::style ()->drawControl
           (QStyle::CE_PushButtonLabel, &opts, painter);
          return;
        }
      else if (s.compare ("less") == 0)
        {
          opts.icon.addPixmap
           (QPixmap (QString::fromUtf8 (":/img/trend_less.png")),
            QIcon::Normal, QIcon::On);
          QApplication::style ()->drawControl
           (QStyle::CE_PushButtonLabel, &opts, painter);
          return;
        }
      else if (s.compare ("down") == 0)
        {
          opts.icon.addPixmap
           (QPixmap (QString::fromUtf8 (":/img/trend_down.png")),
            QIcon::Active, QIcon::On);
          QApplication::style ()->drawControl
           (QStyle::CE_PushButtonLabel, &opts, painter);
          return;
        }
      else
        {
          return;
        }
    }
  QStyledItemDelegate::paint (painter, option, index);
}

