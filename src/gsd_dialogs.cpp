/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "gsd_dialogs.h"
#include "gsd_control.h"
/**
 * @file gsd_dialogs.cpp
 * @brief Implements the slots and functions for gsd dialogs.
 */

/**
 * @class details_dialog.
 */

/**
 * @brief Setter
 */
void
details_dialog::setModel (model_omp_entity *model)
{
  this->model = model;
}


/**
 * @brief Show details of entity with the given id.
 *
 * @param[in]  id   Entity id. Format: "entitytype idstring", where entitytype
 *                  is e.g. "target" and id string is the id, both separated
 *                  by blank character.
 */
void
details_dialog::setId (QString id)
{
  if (id == NULL || id.compare ("") == 0)
    return;

  this->id = id;
}


/**
 * @class dlg_login
 */

/**
 * @brief Getter
 *
 * Getter to access the data.
 *
 * @return Serveraddress
 */
QString
dlg_login::getAddress ()
{
  return le_srvaddr->text ();
}


/**
 * @brief Getter
 *
 * Getter to access the data.
 *
 * @return Serverport
 */
QString
dlg_login::getPort ()
{
  return le_srvport->text ();
}


/**
 * @brief Getter
 *
 * Getter to access the data.
 *
 * @return Username
 */
QString
dlg_login::getUsername ()
{
  return le_uname->text ();
}


/**
 * @brief Getter
 *
 * Getter to access the data.
 *
 * @return Password
 */
QString
dlg_login::getPassword ()
{
  return le_upass->text ();
}


/**
 * @brief Getter
 *
 * Getter to access the data.
 *
 * @return Profilename
 */
QString
dlg_login::getProfile ()
{
  return cb_profile->currentText ();
}


/**
 * @brief Setter
 *
 * Setter for loaded profiles.
 *
 * @param[in]  addr   Serveraddress.
 */
void
dlg_login::setAddress (QString addr)
{
  le_srvaddr->setText (addr);
}


/**
 * @brief Setter
 *
 * Setter for loaded profiles.
 *
 * @param[in]  port   Serverport.
 */
void
dlg_login::setPort (int port)
{
  disconnect (le_srvaddr,
              SIGNAL (textChanged (QString)),
              this,
              SLOT (checkServer ()));
  disconnect (le_srvport,
              SIGNAL (textChanged (QString)),
              this,
              SLOT (checkServer ()));
  disconnect (le_uname,
              SIGNAL (editingFinished ()),
              this,
              SLOT (checkServer ()));
  QString p;
  le_srvport->setText (p.setNum (port, 10));
  connect (le_srvaddr,
           SIGNAL (textChanged (QString)),
           this,
           SLOT (checkServer ()));
  connect (le_srvport,
           SIGNAL (textChanged (QString)),
           this,
           SLOT (checkServer ()));
  connect (le_uname,
           SIGNAL (editingFinished ()),
           this,
           SLOT (checkServer ()));
}


/**
 * @brief Setter
 *
 * Setter for loaded profiles.
 *
 * @param[in]  name   Username.
 */
void
dlg_login::setUsername (QString name)
{
  le_uname->setText (name);
}


/**
 * @brief Setter
 *
 * Setter for loaded profiles.
 *
 * @param[in]  name   Profilname.
 * @param[in]  pos    Position in combobox.
 */
void
dlg_login::setProfile (QString name, int pos)
{
  if (cb_profile->findText (name) == -1)
    cb_profile->insertItem (pos, name, 0);
}

/**
 * @brief SLOT to inform the controller about profileselection.
 *
 * If profileselection changed, the new profilename is send to the controller.
 *
 * @param[in]  name   new selected profile.
 */
void
dlg_login::profile_changed (int index)
{
  emit sig_profile_selected (index);
}


/**
 * @brief SLOT to save a new profile
 *
 * Sends a signal that informs the controller to save a new profile in the
 * configuration. The signal contains profilename and logindata.
 */
void
dlg_login::profile_save ()
{
  if (cb_profile->currentText ().length () > 0)
    {
      bool *ok = false;
      omp_credentials *c = new omp_credentials ();
      c->setServerAddress (le_srvaddr->text ());
      c->setServerPort (le_srvport->text ().toInt (ok, 10));
      c->setUserName (le_uname->text ());

      emit sig_save_profile (cb_profile->currentText (), *c);

      if (cb_profile->findText(cb_profile->currentText ()) == -1)
        {
          cb_profile->addItem (cb_profile->currentText ());
        }
    }
}


/**
 * @brief SLOT to remove a profile
 *
 * Sends a signal that informs the controller to remove a profile from the
 * configuration.
 */
void dlg_login::profile_remove ()
{
  cb_profile->removeItem (cb_profile->currentIndex ());

  emit sig_remove_profile (cb_profile->currentIndex ());

  QString s (le_srvport->text ());
  if (strcmp (s.toLatin1 ().data (), "0") == 0)
    le_srvport->setText ("9390");

}

void
dlg_login::selectProfile (int ndx)
{
  cb_profile->setCurrentIndex (ndx);
}


bool
dlg_login::isOpen ()
{
  bool vis = isVisible ();
  return vis;
}

/**
 * @brief Checks if login data is complete and sends login signal.
 */
void
dlg_login::accept_login ()
{
  bool *ok;

  QSettings settings ("Greenbone", "GSD");
  settings.beginGroup ("Login");
  settings.setValue ("profile", cb_profile->currentIndex ());
  settings.endGroup ();

  address = le_srvaddr->text ();
  port = le_srvport->text ();
  user = le_uname->text ();
  password = le_upass->text ();

  QString q (tr ("Please enter: \n"));
  bool *test= false, error  = false;
  if (address.compare ("") == 0)
    {
      q.append (tr ("   - Serveraddress                 \n"));
      error = true;
    }
  if (port == 0)
    {
      q.append (tr ("   - Serverport                    \n"));
      error = true;
    }
  if (user.compare ("") == 0)
    {
      q.append (tr ("   - Username                      \n"));
      error = true;
    }
  if (password.compare ("") == 0)
    {
      q.append (tr ("   - Password                      \n"));
      error = true;
    }
  if (error)
    {
      QMessageBox::question (NULL, tr ("Connection failed..."),
                             q, tr ("&Ok"), QString (), 0, 1 );
    }
  else
    emit sig_login (address, port.toInt (), user, password);
}


void
dlg_login::checkServer ()
{
  address = le_srvaddr->text ();
  port = le_srvport->text ();
  user = le_uname->text ();
  password = le_upass->text ();

  if (address.compare ("") == 0 || port == 0)
    return;

  if (user.compare ("") != 0 && password.compare ("") != 0)
    emit sig_check_server (address, port.toInt (), user, password);
  else
    emit sig_check_server (address, port.toInt (), "", "");
}


void
dlg_login::setUrlValid (int res)
{
  if (res == 1)
    {
      la_result->setText ("");
      QImage image (":/img/help.png");
      QPixmap pm = QPixmap::fromImage (image);
      la_result->setPixmap (pm);
    }
  else if (res == 2)
    {
      QImage image (":/img/delete.png");
      QPixmap pm = QPixmap::fromImage (image);
      la_result->setPixmap (pm);
      la_result2->setText ("OMP 1.0");
      la_result2->setStyleSheet ("QLabel {color: red}");
    }
  else if (res == 3)
    {
      QImage image (":/img/favicon.gif");
      QPixmap pm = QPixmap::fromImage (image);
      la_result->setPixmap (pm);
      la_result2->setText ("OMP 2.0");
      la_result2->setStyleSheet ("QLabel {color: green}");
    }
  else if (res == 4)
    {
      QImage image (":/img/favicon.gif");
      QPixmap pm = QPixmap::fromImage (image);
      la_result->setPixmap (pm);
      la_result2->setText ("OMP 3.0");
      la_result2->setStyleSheet ("QLabel {color: green}");
    }
  else if (res > 4)
    {
      QImage image (":/img/favicon.gif");
      QPixmap pm = QPixmap::fromImage (image);
      la_result->setPixmap (pm);
      la_result2->setText (QString::fromLatin1("OMP %1").arg(res -1));
      la_result2->setStyleSheet ("QLabel {color: green}");
    }
  else
    {
      la_result2->setText ("");
      QImage image (":/img/delete.png");
      QPixmap pm = QPixmap::fromImage (image);
      la_result->setPixmap (pm);
      la_result->setStyleSheet ("QLabel {color: red}");
    }
}

/**
 * @class dlg_about
 */

/**
 * @brief Creates the maincontent for the about-dialog
 */
void
dlg_about::load ()
{
  //set background white
  QPalette bg_palette = this->palette ();
  bg_palette.setBrush (QPalette::Background, Qt::white);
  this->setPalette (bg_palette);

  label->setText (tr ("Greenbone Security Desktop"));
  label_1->setText (tr ("Version %1").arg (GSD_VERSION));

  label_2->setOpenExternalLinks (true);
  label_2->setText
   (tr ("Greenbone Security Desktop is a "
                             "graphical user interface<br>"
                             "to access a Greenbone Security Manager, "
                             "OpenVAS Manager<br>"
                             "or any other service that offers the "
                             "OMP protocol for<br>"
                             "comprehensive vulnerability management.<br><br>"
                             "Copyright 2009-2012 by Greenbone Networks GmbH,<br>"
                             "<a href=www.greenbone.net> www.greenbone.net</a>"));
  connect (label_3,
           SIGNAL (linkActivated (QString)),
           this,
           SLOT (link_clicked (QString)));
  label_3->setText (tr ("License: GNU General Public License version 2 or "
                        "any later<br>version (<a href=\"license\">full license text<a>)."));

  label_4->setOpenExternalLinks (true);
  label_4->setText (tr ("Contact: For updates, feature proposals and bug "
                        "reports please<br>"
                        "contact the  <a href=\"http://www.greenbone.net/company/contact.html\">Greenbone team</a> or "
                        "visit the <a href=\"http://www.openvas.org\">OpenVAS homepage</a>."));

  //add images
  QImage image (":/img/gsd-logo.png");
  QPixmap pm = QPixmap::fromImage (image);
  imageLabel->setPixmap (pm.scaled (260, 190, Qt::KeepAspectRatio,
                                    Qt::SmoothTransformation));
}


void
dlg_about::link_clicked (QString link)
{
  if (link.compare ("license") == 0)
    {
      dlg_license *license = new dlg_license ();
      license->show ();
    }
}


void
dlg_start_gsa::load ()
{
  QSettings settings ("Greenbone", "GSD");
  settings.beginGroup ("GSA");
  QString url = settings.value ("url").toString ();
  int port = settings.value ("port").toInt ();
  settings.endGroup ();

  if (url.compare ("") == 0)
    le_url->setText ("https://" + gsd_url);
  else
    le_url->setText (url);

  cb_port->clear ();
  cb_port->addItem ("443");
  cb_port->addItem ("9392");
  if (port != 0)
    {
      cb_port->addItem (QString::number (port));
      cb_port->setCurrentIndex (2);
    }
}


void
dlg_start_gsa::start_gsa ()
{
  QSettings settings ("Greenbone", "GSD");
  settings.beginGroup ("GSA");
  settings.setValue ("url", le_url->text ());
  settings.setValue ("port", cb_port->currentText ());
  settings.endGroup ();

  QDesktopServices::openUrl(QUrl(le_url->text () + ":" + cb_port->currentText ()));
  this->close ();
}


void
dlg_start_gsa::reset ()
{
  le_url->setText ("https://" + gsd_url);
  cb_port->setCurrentIndex (0);
}


void
dlg_start_gsa::setDefaultURL (QString url)
{
  gsd_url = url;
}

/**
 * @class dlg_new_task
 */

/**
 * @brief Prepares new task dialog.
 */
void
dlg_new_task::load ()
{
  cb_config->clear ();
  cb_targets->clear ();
  cb_escalator->clear ();
  cb_schedule->clear ();
}


/**
 * @brief Adds a config name to combobox.
 *
 * @param[in]  name   Config name.
 * @param[in]  id     Config id.
 */
void
dlg_new_task::addConfig (QString name, QString id)
{
  if (cb_config->findData (id) == -1)
    cb_config->addItem (name, id);
}


/**
 * @brief Adds a target name to combobox.
 *
 * @param[in]  name   Target name.
 * @param[in]  id     Target id.
 */
void
dlg_new_task::addTarget (QString name, QString id)
{
  if (cb_targets->findData (id) == -1)
    cb_targets->addItem (name, id);
}


/**
 * @brief Adds a schedule name to combobox.
 *
 * @param[in]  name   Schedule name.
 * @param[in]  id     Schedule id.
 */
void
dlg_new_task::addSchedule (QString name, QString id)
{
  if (cb_schedule->findData (id) == -1)
    cb_schedule->addItem (name, id);
}


/**
 * @brief Adds a escalator name to combobox.
 *
 * @param[in]  name   Escalator name.
 * @param[in]  id     Escalator id.
 */
void
dlg_new_task::addEscalator (QString name, QString id)
{
  if (cb_escalator->findData (id) == -1)
    cb_escalator->addItem (name, id);
}


/**
 * @brief Adds a slave name to combobox.
 *
 * @param[in]  name   Slave name.
 * @param[in]  id     Slave id.
 */
void
dlg_new_task::addSlave (QString name, QString id)
{
  if (cb_slave->findData (id) == -1)
    cb_slave->addItem (name, id);
}


/**
 * @brief Emits a signal to start task creation.
 */
void
dlg_new_task::create ()
{
  QString configId = this->cb_config->itemData (cb_config->currentIndex ())
                                               .toString ();
  QString targetId = this->cb_targets->itemData (cb_targets->currentIndex ())
                                                .toString ();
  QString scheduleId = this->cb_schedule->itemData
                                          (cb_schedule->currentIndex ())
                                          .toString ();
  QString escalatorId = this->cb_escalator->itemData
                                            (cb_escalator->currentIndex ())
                                            .toString ();
  QString slaveId = this->cb_slave->itemData (cb_slave->currentIndex())
                                     .toString ();
  QMap<QString, QString> parameter;

  parameter.insert ("name", this->le_name->text ());
  parameter.insert ("comment", this->le_comment->text ());
  parameter.insert ("config", configId);
  parameter.insert ("target", targetId);
  parameter.insert ("schedule", scheduleId);
  parameter.insert ("escalator", escalatorId);
  parameter.insert ("slave", slaveId);
  emit sig_create (omp_utilities::TASK, parameter);
  this->close ();
}


void
dlg_new_task::new_target ()
{
  emit sig_new_target ();
}


void
dlg_new_task::new_config ()
{
  emit sig_new_config ();
}


void
dlg_new_task::new_escalator ()
{
  emit sig_new_escalator ();
}


void
dlg_new_task::new_schedule ()
{
  emit sig_new_schedule ();
}


void
dlg_new_task::new_slave ()
{
  emit sig_new_slave ();
}

void
dlg_new_task::selectConfig (int pos)
{
  cb_config->setCurrentIndex (pos);
}

void
dlg_new_task::selectTarget (int pos)
{
  cb_targets->setCurrentIndex (pos);
}

void
dlg_new_task::selectSchedule (int pos)
{
  cb_schedule->setCurrentIndex (pos);
}

void
dlg_new_task::selectEscalator (int pos)
{
  cb_escalator->setCurrentIndex (pos);
}

void
dlg_new_task::selectSlave (int pos)
{
  cb_slave->setCurrentIndex (pos);
}


/**
 * @class dlg_modify_task.
 */
void
dlg_modify_task::addSchedule (QString name, QString id)
{
  if (cb_schedule->findData (id) == -1)
    cb_schedule->addItem (name, id);
}

void
dlg_modify_task::addEscalator (QString name, QString id)
{
  if (cb_escalator->findData (id) == -1)
    cb_escalator->addItem (name, id);
}

void
dlg_modify_task::addSlave (QString name, QString id)
{
  if (cb_slave->findData (id) == -1)
    cb_slave->addItem (name, id);
}

void
dlg_modify_task::selectSchedule (int pos)
{
  if (pos != -1)
    cb_schedule->setCurrentIndex (pos);
}

void
dlg_modify_task::selectEscalator (int pos)
{
  if (pos != -1)
    cb_escalator->setCurrentIndex (pos);
}

void
dlg_modify_task::selectSlave (int pos)
{
  if (pos != 1)
    cb_slave->setCurrentIndex (pos);
}

void
dlg_modify_task::new_escalator ()
{
  emit sig_new_escalator ();
}

void
dlg_modify_task::new_schedule ()
{
  emit sig_new_schedule ();
}

void
dlg_modify_task::new_slave ()
{
  emit sig_new_slave ();
}
void
dlg_modify_task::modify ()
{
  QString scheduleId = this->cb_schedule->itemData
                                          (cb_schedule->currentIndex ())
                                           .toString ();
  QString escalatorId = this->cb_escalator->itemData
                                            (cb_escalator->currentIndex ())
                                             .toString ();
  QString slaveId = this->cb_slave->itemData
                                     (cb_slave->currentIndex ())
                                      .toString ();

  QMap<QString, QString> parameter;
  parameter.insert ("task_id", id);
  parameter.insert ("name", le_name->text ());
  parameter.insert ("comment", le_comment->text ());
  parameter.insert ("schedule", scheduleId);
  parameter.insert ("escalator", escalatorId);
  parameter.insert ("slave", slaveId);

  emit sig_modify (omp_utilities::TASK, parameter);
  this->close ();
}


/**
 * @class dlg_new_target.
 */

/**
 * @brief Prepare the new target dialog.
 */
void
dlg_new_target::load ()
{
  cb_sshcredential->clear ();
  cb_smbcredential->clear ();
  cb_plist->clear ();
  cb_plist->setVisible(false);
  label_6->setVisible(false);
  pb_new_plist->setVisible(false);
}


/**
 * @brief Adds a config name to combobox.
 *
 * @param[in]  name   Credential name.
 * @param[in]  id     Credential id.
 */
void
dlg_new_target::addCredential (QString name, QString id)
{
  if (cb_sshcredential->findData (id) == -1 &&
      cb_smbcredential->findData (id) == -1)
    {
      cb_sshcredential->addItem (name, id);
      cb_smbcredential->addItem (name, id);
    }
}


/**
 * @brief Adds a Port List to combobox. And activate GUI for it.
 *
 * @param[in]  name   Port List name.
 * @param[in]  id     Port List id.
 */
void
dlg_new_target::addPortList (QString name, QString id)
{
  if (cb_plist->findData (id) == -1)
    {
      cb_plist->addItem (name, id);
    }
  cb_plist->setVisible(true);
  label_6->setVisible(true);
  pb_new_plist->setVisible(true);
}

/**
 * @brief Emits a signal to start target creation.
 */
void
dlg_new_target::create ()
{
  QMap<QString, QString> parameter;
  parameter.insert ("name", this->le_name->text ());
  parameter.insert ("comment", this->le_comment->text ());
  parameter.insert ("hosts", this->le_hosts->text ());
  if (this->cb_sshcredential->currentText ().compare ("--") != 0)
    parameter.insert ("ssh_lsc_credential", this->cb_sshcredential->itemData
                                            (cb_sshcredential->currentIndex ())
                                            .toString ());
  if (this->cb_smbcredential->currentText ().compare ("--") != 0)
    parameter.insert ("smb_lsc_credential", this->cb_smbcredential->itemData
                                            (cb_smbcredential->currentIndex ())
                                            .toString ());

  QString plistId = this->cb_plist->itemData (cb_plist->currentIndex())
                                             .toString ();
  parameter.insert ("port_list", plistId);


  emit sig_create (omp_utilities::TARGET, parameter);
  this->close ();
}

void
dlg_new_target::new_ssh_credential ()
{
  type = "ssh";
  emit sig_new_credential ();
}

void
dlg_new_target::new_port_list ()
{
  emit sig_new_port_list ();
}

void
dlg_new_target::selectPortList (int pos)
{
  cb_plist->setCurrentIndex (pos);
}

void
dlg_new_target::new_smb_credential ()
{
  type = "smb";
  emit sig_new_credential ();
}

void
dlg_new_target::selectCredential (int pos)
{
  if (type.compare ("ssh") == 0)
    cb_sshcredential->setCurrentIndex (pos);
  else if (type.compare ("smb") == 0)
    cb_smbcredential->setCurrentIndex (pos);
  else
    return;
}


/**
 * @class dlg_new_config.
 */

/**
 * @brief Creates a new scan config.
 */
void
dlg_new_config::create ()
{
  QString base, name, comment;

  if (radioButton->isChecked ())
    {
      base = "085569ce-73ed-11df-83c3-002264764cea";
    }
  else
    {
      base = "daba56c8-73ec-11df-a475-002264764cea";
    }

  QMap<QString, QString>parameter;
  parameter.insert ("name", le_name->text ());
  parameter.insert ("comment", le_comment->text ());
  parameter.insert ("base", base);

  emit sig_create (omp_utilities::CONFIG, parameter);

  this->close ();
}


/**
 * @class dlg_new_schedule.
 */

/**
 * @brief Emits a signal to start task creation.
 */
void
dlg_new_schedule::create ()
{
  if (le_name->text ().length () == 0)
    {
      return;
    }

  QString name = le_name->text ();
  QString comment = le_comment->text ();
  int first_min, first_hour, first_day, first_mon, first_year, dura, period;

  first_min = te_first->time ().minute ();
  first_hour = te_first->time ().hour ();
  first_day = de_first->date ().day ();
  first_mon = de_first->date ().month ();
  first_year = de_first->date ().year ();

  dura = sb_dura->value ();
  period = sb_period->value ();

  QString dura_unit = cb_dura_unit->itemData (cb_dura_unit->currentIndex ())
                                              .toString ();
  QString period_unit = cb_peri_unit->itemData (cb_peri_unit->currentIndex ())
                                                .toString ();

  bool *ok;
  QMap<QString, QString> parameter;
  parameter.insert ("name", name);
  parameter.insert ("comment", comment);
  parameter.insert ("minute", QString ("%1").arg (first_min));
  parameter.insert ("hour", QString ("%1").arg (first_hour));
  parameter.insert ("day", QString ("%1").arg (first_day));
  parameter.insert ("month", QString ("%1").arg (first_mon));
  parameter.insert ("year", QString ("%1").arg (first_year));
  parameter.insert ("duration", QString ("%1").arg (dura));
  parameter.insert ("duration unit", dura_unit);
  parameter.insert ("period", QString ("%1").arg (period));
  parameter.insert ("period unit", period_unit);

  emit sig_create (omp_utilities::SCHEDULE, parameter);
  this->close ();
}


/**
 * @class dlg_new_escalator.
 */

/**
 * @brief Creates a new escalator.
 */
void
dlg_new_escalator::create ()
{
  if (le_name->text ().length () == 0)
    {
      return;
    }

  QString name = le_name->text ();
  QString comment = le_comment->text ();

  QString cond, event, method;
  QString cond_data_1, cond_data_2,
          eve_data_1, eve_data_2,
          meth_data_1, meth_data_2, meth_data_3, meth_data_4;

  if (rb_cond1->isChecked ())
    cond = "Always";
  else if (rb_cond2->isChecked ())
    {
      cond = "Threat level at least";
      cond_data_1 = cb_cond1->currentText ();
      cond_data_2 = "level";
    }
  else if (rb_cond3->isChecked ())
    {
      cond = "Threat level changed";
      cond_data_1 = cb_cond2->currentText ();
      cond_data_2 = "direction";
    }

  event = "Task run status changed";
  eve_data_1 =cb_event->currentText();
  eve_data_2 = "status";

  if (rb_method1->isChecked ())
    {
      method = "Email";
      if (checkAddr (le_mail_from->text ()) &&
          checkAddr (le_mail_to->text ()))
        {
          meth_data_1 = le_mail_to->text ();
          meth_data_2 = "to_address";
          meth_data_3 = le_mail_from->text ();
          meth_data_4 = "from_address";
        }
      else
        {
          QMessageBox::information (NULL,tr ("Data Error."),
                                    tr ("Error while parsing Email addresses!"
                                    "\nPlease check!"));
          return;
        }
    }
  if (rb_method2->isChecked ())
    {
      method = "syslog";
    }
  if (rb_method3->isChecked ())
    {
      method = "SNMP";
    }
  QMap<QString, QString> parameter;
  parameter.insert ("name", name);
  parameter.insert ("comment", comment);
  parameter.insert ("condition", cond);
  parameter.insert ("condition data", cond_data_1);
  parameter.insert ("condition name", cond_data_2);
  parameter.insert ("event", event);
  parameter.insert ("event data", eve_data_1);
  parameter.insert ("event name", eve_data_2);
  parameter.insert ("method", method);
  parameter.insert ("method data", meth_data_1);
  parameter.insert ("method name", meth_data_2);
  parameter.insert ("method data 2", meth_data_3);
  parameter.insert ("method name 2", meth_data_4);
  emit sig_create (omp_utilities::ESCALATOR, parameter);
  this->close ();
}


/**
 * @brief Checks if a string contains "@" and "."
 *
 * @param address Email address.
 */
bool
dlg_new_escalator::checkAddr (QString address)
{
  int cnt = address.count ("@");
  if (cnt != 1)
    return false;
  QStringList l = address.split ("@");
  if (l.size () != 2)
    return false;
  else
    {
      if (!l[1].contains ("."))
        return false;
    }
  return true;
}

/**
 * @class dlg_new_port_list.
 */
/**
 * @brief Emits a signal to start port_list creation.
 */
void
dlg_new_port_list::create ()
{
  if (le_name->text ().length () == 0 ||
      le_port_range->text ().length () == 0)
    {
      return;
    }

  QString type;

  QMap<QString, QString> parameter;
  parameter.insert ("name", le_name->text ());
  parameter.insert ("comment", le_comment->text ());
  parameter.insert ("port_range", le_port_range->text ());

  emit sig_create (omp_utilities::PORT_LIST, parameter);
  this->close ();
}

/**
 * @class dlg_new_credential.
 */

/**
 * @brief Emits a signal to start credential creation.
 */
void
dlg_new_credential::create ()
{
  if (le_name->text ().length () == 0 ||
      le_login->text ().length () == 0)
    {
      return;
    }

  QString type;
  if (rb_auto->isChecked ())
    {
      type = "gen";
    }
  else
    {
      type = "pass";
    }

  QMap<QString, QString> parameter;
  parameter.insert ("name", le_name->text ());
  parameter.insert ("comment", le_comment->text ());
  parameter.insert ("login", le_login->text ());
  parameter.insert ("password", le_passwd->text ());
  parameter.insert ("type", type);

  emit sig_create (omp_utilities::LSC_CREDENTIAL, parameter);
  this->close ();
}


/**
 * @class dlg_new_agent
 */

/**
 * @brief Creates a new scan config.
 */
void
dlg_new_agent::create ()
{
  if (le_name->text ().length () == 0 ||
      le_installer->text ().length () == 0)
    return;

  //TODO load installer and signature file.
  QMap<QString, QString> parameter;
  parameter.insert ("name", le_name->text ());
  parameter.insert ("comment", le_comment->text ());
  parameter.insert ("installer", le_installer->text ());
  parameter.insert ("signature", le_signature->text ());
  emit sig_create (omp_utilities::AGENT, parameter);

  this->close ();
}


/**
 * @brief Opens a file dialog.
 */
void
dlg_new_agent::open_installer ()
{
  QString filename = QFileDialog::getOpenFileName (this, tr ("Open File ..."),
                                                   QDir::currentPath (),
                                                   tr ("All files *.*"));
  this->le_installer->setText (filename);
}


/**
 * @brief Opens a file dialog.
 */
void
dlg_new_agent::open_signature ()
{
  QString filename = QFileDialog::getOpenFileName (this, tr ("Open File ..."),
                                                   QDir::currentPath (),
                                                   tr ("All files *.*"));
  this->le_signature->setText (filename);

}


/**
 * @class dlg_new_note
 */

/**
 * @brief Create a new note.
 */
void
dlg_new_note::create ()
{
  QMap<QString, QString> parameter;
  parameter.insert ("text", te_text->toPlainText ());
  parameter.insert ("nvt", nvt_oid);
  if (rb_hosts_value->isChecked ())
    parameter.insert ("hosts", rb_hosts_value->text ());
  if (rb_port_value->isChecked ())
    parameter.insert ("port", rb_port_value->text ());
  if (rb_threat_value->isChecked ())
    parameter.insert ("threat", rb_threat_value->text ());
  if (rb_task_value->isChecked ())
    parameter.insert ("task", task_id);
  if (rb_result_value->isChecked ())
    parameter.insert ("result", rb_result_value->text ());

  emit sig_create (omp_utilities::NOTE, parameter);

  this->close ();
}


/**
 * @class dlg_new_override
 */

/**
 * @brief Create a new override.
 */
void
dlg_new_override::create ()
{
  QMap<QString, QString> parameter;
  parameter.insert ("text", te_text->toPlainText ());
  parameter.insert ("nvt", nvt_oid);
  if (rb_hosts_value->isChecked ())
    parameter.insert ("hosts", rb_hosts_value->text ());
  if (rb_port_value->isChecked ())
    parameter.insert ("port", rb_port_value->text ());
  if (rb_threat_value->isChecked ())
    parameter.insert ("threat", rb_threat_value->text ());
  if (rb_task_value->isChecked ())
    parameter.insert ("task", task_id);
  if (rb_result_value->isChecked ())
    parameter.insert ("result", rb_result_value->text ());

  parameter.insert ("new_threat", cb_new_threat->currentText ());

  emit sig_create (omp_utilities::OVERRIDE, parameter);

  this->close ();
}


void
dlg_new_slave::create ()
{
  QMap<QString,QString> parameter;
  parameter.insert ("name", le_name->text ());
  parameter.insert ("comment", le_comment->text ());
  parameter.insert ("host", le_host->text ());
  parameter.insert ("port", le_port->text ());
  parameter.insert ("login", le_login->text ());
  parameter.insert ("password", le_password->text ());

  emit sig_create (omp_utilities::SLAVE, parameter);

  this->close ();
}
