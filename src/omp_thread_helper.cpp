/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "omp_thread_helper.h"

/**
 * @file omp_thread_helper.cpp
 * @brief Helperclass for the updaterthread
 *
 * This class is used to send signals from the updaterthread to the mainthread.
 */

omp_thread_helper::omp_thread_helper ()
{
}

omp_thread_helper::~omp_thread_helper ()
{
}


/**
 * @brief Syncronise with the updatethread.
 *
 * Send the sig_triger_update () signal, when an update is done and the thread
 * starts sleeping.
 */
void
omp_thread_helper::triggerUpdate ()
{
  emit sig_update_trigger ();
}


/**
 * @brief Update finished.
 *
 * Sends the sig_finished_update () signal, when the update is done.
 */
void
omp_thread_helper::finishedUpdate ()
{
  emit sig_update_finished ();
}


/**
 * @brief Thread started.
 */
void
omp_thread_helper::threadStarted ()
{
  emit sig_thread_started ();
}


/**
 * @brief Thread finished.
 */
void
omp_thread_helper::threadFinished ()
{
  emit sig_thread_finished ();
}


/**
 * @brief Request finished.
 */
void
omp_thread_helper::requestFinished (int type, int com, QString userData)
{
  emit sig_request_finished (type, com, userData);
}


/**
 * @brief Request failed.
 */
void
omp_thread_helper::requestFailed (int type, int com, int err)
{
  emit sig_request_failed (type, com, err);
}


/**
 * @brief Send log message.
 *
 * Sends the sig_log () signal with log message and priority.
 */
void
omp_thread_helper::log (QString msg, int prio)
{
  emit sig_log (msg, prio);
}
