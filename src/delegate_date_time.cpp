/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "delegate_date_time.h"

/**
 * @file delegate_icon.cpp
 * @brief Delegate for icons.
 *
 * Used for painting the icons in widgets like tableview, treeview, etc.
 */

delegate_date_time::delegate_date_time ()
{
}

delegate_date_time::~delegate_date_time()
{
}


/**
 * @brief New implemented paintfunction.
 *
 * Implementation to paint an icon in a view widget.
 *
 * @param painter Painter to draw the icon
 * @param option  Styleoptions
 * @param index   viewindex
 */
void
delegate_date_time::paint (QPainter* painter,
                           const QStyleOptionViewItem& option,
                           const QModelIndex& index) const
{
  model_omp_entity * model = (model_omp_entity*) index.model ();

  if (model == 0)
    {
      QStyledItemDelegate::paint (painter, option, index);
      return;
    }
  QColor c;
  QStyleOptionViewItem opts (option);
  QDomElement pItem = model->getEntity (index.row ());
  if (!pItem.isNull ())
    {
      if (option.state & QStyle::State_Selected)
        {
          if (!(option.state & QStyle::State_Active))
            {
              painter->fillRect (option.rect,
                                 option.palette.color
                                  (QPalette::Inactive,
                                   QPalette::Highlight));
              c = option.palette.color (QPalette::Inactive,
                                               QPalette::HighlightedText);
            }
          else
            {
              painter->fillRect (option.rect,
                               option.palette.color (QPalette::Active,
                                                     QPalette::Highlight));
              c = option.palette.color (QPalette::Active,
                                               QPalette::HighlightedText);
            }
        }
      opts.palette.setColor (QPalette::HighlightedText, c);
      bool *ok = false;
      QString temp = model->getValue (pItem, model->getHeader (index.column ()));
      if (model->getHeader (index.column ()).compare ("duration") == 0)
        {
          qlonglong nbr = temp.toLongLong (ok, 10);
          long time = nbr;
          time /= 3600;
          long hour = time % 24;
          time /= 24;
          long day = time % 7;
          time /= 7;
          long week = time;
          QString s;
          if (hour == 1)
            {
              QApplication::style ()->drawItemText(painter,
                                                   option.rect,
                                                   Qt::AlignVCenter,
                                                   opts.palette,
                                                   true,
                                                   QString
                                                    (s.setNum (hour, 10)
                                                     + tr (" hour")),
                                               QPalette::HighlightedText);
              return;
            }
          else if (hour > 1)
            {
              QApplication::style ()->drawItemText(painter,
                                                   option.rect,
                                                   Qt::AlignVCenter,
                                                   opts.palette,
                                                   true,
                                                   QString
                                                    (s.setNum (hour, 10)
                                                     + tr (" hours")),
                                               QPalette::HighlightedText);
              return;
            }
          else if (day == 1)
            {
              QApplication::style ()->drawItemText(painter,
                                                   option.rect,
                                                   Qt::AlignVCenter,
                                                   opts.palette,
                                                   true,
                                                   QString
                                                    (s.setNum (day, 10)
                                                     + tr (" day")),
                                               QPalette::HighlightedText);
              return;
            }
          else if (day > 1)
            {
              QApplication::style ()->drawItemText(painter,
                                                   option.rect,
                                                   Qt::AlignVCenter,
                                                   opts.palette,
                                                   true,
                                                   QString
                                                    (s.setNum (day, 10)
                                                     + tr (" days")),
                                               QPalette::HighlightedText);
              return;
            }
          else if (week == 1)
            {
              QApplication::style ()->drawItemText(painter,
                                                   option.rect,
                                                   Qt::AlignVCenter,
                                                   opts.palette,
                                                   true,
                                                   QString
                                                    (s.setNum (week, 10)
                                                     + tr ( " week")),
                                               QPalette::HighlightedText);
              return;
            }
          else if (week > 1)
            {
              QApplication::style ()->drawItemText(painter,
                                                   option.rect,
                                                   Qt::AlignVCenter,
                                                   opts.palette,
                                                   true,
                                                   QString
                                                    (s.setNum (week, 10)
                                                     + tr (" weeks")),
                                               QPalette::HighlightedText);
              return;
            }
        }
      if (model->getHeader (index.column ()).compare ("period") == 0)
        {
          qlonglong nbr = temp.toLongLong (ok, 10);
          if (nbr != 0)
            {
              long time = nbr;
              time /= 3600;
              long hour = time % 24;
              time /= 24;
              long day = time % 7;
              time /= 7;
              long week = time;
              QString s;
              if (hour == 1)
                {
                  QApplication::style ()->drawItemText(painter,
                                                       option.rect,
                                                       Qt::AlignVCenter,
                                                       opts.palette,
                                                       true,
                                                       QString
                                                        (s.setNum (hour, 10)
                                                         + tr (" hour")),
                                               QPalette::HighlightedText);
                  return;
                }
              else if (hour > 1)
                {
                  QApplication::style ()->drawItemText(painter,
                                                       opts.rect,
                                                       Qt::AlignVCenter,
                                                       opts.palette,
                                                       true,
                                                       QString
                                                        (s.setNum (hour, 10)
                                                         + tr (" hours")),
                                               QPalette::HighlightedText);
                  return;
                }
              else if (day == 1)
                {
                  QApplication::style ()->drawItemText(painter,
                                                       option.rect,
                                                       Qt::AlignVCenter,
                                                       opts.palette,
                                                       true,
                                                       QString
                                                        (s.setNum (day, 10)
                                                         + tr (" day")),
                                               QPalette::HighlightedText);
                  return;
                }
              else if (day > 1)
                {
                  QApplication::style ()->drawItemText(painter,
                                                       option.rect,
                                                       Qt::AlignVCenter,
                                                       opts.palette,
                                                       true,
                                                       QString
                                                        (s.setNum (day, 10)
                                                         + tr (" days")),
                                               QPalette::HighlightedText);
                  return;
                }
              else if (week == 1)
                {
                  QApplication::style ()->drawItemText(painter,
                                                       option.rect,
                                                       Qt::AlignVCenter,
                                                       opts.palette,
                                                       true,
                                                       QString
                                                        (s.setNum (week, 10)
                                                         + tr (" week")),
                                               QPalette::HighlightedText);
                  return;
                }
              else if (week > 1)
                {
                  QApplication::style ()->drawItemText(painter,
                                                       option.rect,
                                                       Qt::AlignVCenter,
                                                       opts.palette,
                                                       true,
                                                       QString
                                                        (s.setNum (week, 10)
                                                         + tr (" weeks")),
                                               QPalette::HighlightedText);
                  return;
                }
            }
          else
            {
              temp = model->getValue (pItem, "period_months");
              nbr = temp.toLongLong (ok, 10);
              QString s;
              if (nbr == 1 )
                {
                  QApplication::style ()->drawItemText(painter,
                                                       option.rect,
                                                       Qt::AlignVCenter,
                                                       opts.palette,
                                                       true,
                                                       QString
                                                        (s.setNum (nbr, 10)
                                                         + tr (" month")),
                                               QPalette::HighlightedText);
                  return;
                }
              if (nbr > 1)
                {
                  QApplication::style ()->drawItemText(painter,
                                                       option.rect,
                                                       Qt::AlignVCenter,
                                                       opts.palette,
                                                       true,
                                                       QString
                                                        (s.setNum (nbr, 10)
                                                         + tr (" months")),
                                               QPalette::HighlightedText);
                  return;
                }
            }
        }
      QString header = model->getHeader (index.column ());
      if (header.contains ("time"))
        {
          bool full = false;
          if (header.contains ("first_time") ||
              header.contains ("next_time") ||
              model->getEntity (index.column ()).tagName ().compare ("report")
                == 0)
            {
              full = true;
            }
          QString modDate = modifyDate(temp, full);
          QApplication::style ()->drawItemText(painter,
                                               option.rect,
                                               Qt::AlignVCenter,
                                               opts.palette,
                                               true,
                                               modDate,
                                               QPalette::HighlightedText);
          return;
        }
    }
  QStyledItemDelegate::paint (painter, option, index);
}


/**
 * @brief Date modification
 *
 * Reads the date from the given string and returns month, day and year in a
 * new string
 *
 *  @param date ompdate as string.
 *  @param full print out the full date.
 **/
QString
delegate_date_time::modifyDate (QString date, bool full) const
{
  if (date.length()>0)
    {
      QDateTime qdate;

      /* Try parse ISO format. */
      /* Cut off the timezone information. Qt version < 4.7 is not able to parse
       * the timezone.*/
      qdate = QDateTime::fromString (date.left (19), Qt::ISODate);
      if (qdate.isValid ())
        {
          if (full)
            return qdate.toString ();
          else
            return qdate.date ().toString ("MMM d yyyy");
        }
      else
        {
          if (full)
            {
              QDateTime dt = QDateTime::fromString (date);
              return dt.toString ();
            }
          QStringList cleaned;
          /* Failed, try parse ctime format. */
          cleaned = date.split (" ");
          if (cleaned.size () < 5)
            {
              return "";
            }
          for (int i = 0; i < cleaned.size()-1; i++)
            {
              if (cleaned[i].length() == 0)
                {
                  cleaned[i] = cleaned[i+1];
                  cleaned[i+1] = "";
                }
            }
          if (cleaned.last() == "")
            cleaned.removeLast();
          cleaned [3] = cleaned [4];
          cleaned.removeLast ();
          QDate d = QDate::fromString (cleaned.join(" "));
          QString ret = d.shortMonthName (d.month ()) + " " +
                                          QString::number (d.day ()) + " " +
                                          QString::number ( d.year ());
          return ret;
        }
    }
  else
    {
      return "";
    }
}

/**
 * @brief Modifies the date string equal to the table view delegate.
 *
 * @param date The date string
 * @param full Print out the full date or the short date.
 */
QString
delegate_date_time::formatDate (QString date, bool full)
{
  return modifyDate(date, full);
}

