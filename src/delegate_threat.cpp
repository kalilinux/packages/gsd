/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "delegate_threat.h"

/**
 * @file delegate_threat.cpp
 * @brief Delegate for threat-icons.
 *
 * Used for painting the threat-icons in widgets like tableview, treeview, etc.
 */

delegate_threat::delegate_threat ()
{
}

delegate_threat::~delegate_threat ()
{
}


/**
 * @brief New implemented paintfunction.
 *
 * Implementation to paint a threaticon in a viewwidget.
 * Paints a threat icon depending on the status of a task.
 *
 * @param painter Painter to draw the icon
 * @param option  Styleoptions
 * @param index   viewindex
 */
void
delegate_threat::paint (QPainter* painter,
                        const QStyleOptionViewItem& option,
                        const QModelIndex& index) const
{
  model_omp_entity * model = (model_omp_entity*) index.model ();

  if (model == 0)
    {
      QStyledItemDelegate::paint (painter, option, index);
      return;
    }

  QStyleOptionButton opts;
  QDomElement pItem = model->getEntity (index.row ());

  if (!pItem.isNull ())
    {
      if (option.state & QStyle::State_Selected)
        {
          if (!(option.state & QStyle::State_Active))
            painter->fillRect (option.rect,
                               option.palette.color (QPalette::Inactive,
                                                    QPalette::Highlight));
          else
            painter->fillRect (option.rect,
                               option.palette.color (QPalette::Active,
                                                    QPalette::Highlight));
        }
      opts.rect = option.rect;
      opts.rect.setRight (option.rect.right ()-4);
      opts.rect.setLeft (option.rect.left ()+4);
      opts.rect.setTop (option.rect.top ()+3);
      opts.rect.setHeight (option.rect.height ()-6);
      opts.iconSize = QSize (58, 16);
      opts.state = QStyle::State_Enabled;

      QString res;
      if (pItem.nodeName ().compare ("task") == 0)
        res = "last_report report result_count";
      else
        res = "result_count";
      bool *ok = false;
      QString s = model->getValue (pItem, res + " hole");
      int t_hole = s.toInt (ok, 10);
      s = model->getValue (pItem, res + " warning");
      int t_warn = s.toInt (ok, 10);
      s = model->getValue (pItem, res + " info");
      int t_low = s.toInt (ok, 10);
      int threat = delegate_threat::calcThreat (t_hole, t_warn, t_low);
      if (pItem.nodeName ().compare ("task") == 0 &&
          model->getValue (pItem, "report_count").compare ("0") == 0)
        threat = 3;
      switch (threat)
        {
          case 0:
            {
              opts.icon.addPixmap
               (QPixmap (QString::fromUtf8 (":/img/high_big.png")),
                QIcon::Normal, QIcon::On);
              QApplication::style ()->drawControl
               (QStyle::CE_PushButtonLabel, &opts, painter);
                return;
            }
          case 1:
            {
              opts.icon.addPixmap
               (QPixmap (QString::fromUtf8 (":/img/medium_big.png")),
                QIcon::Normal, QIcon::On);
              QApplication::style ()->drawControl
               (QStyle::CE_PushButtonLabel, &opts, painter);
              return;
            }
          case 2:
            {
              opts.icon.addPixmap
               (QPixmap (QString::fromUtf8 (":/img/low_big.png")),
                QIcon::Normal, QIcon::On);
              QApplication::style ()->drawControl
               (QStyle::CE_PushButtonLabel, &opts, painter);
              return;
            }
          case -1:
            {
              opts.icon.addPixmap 
               (QPixmap (QString::fromUtf8 (":/img/none_big.png")),
                QIcon::Normal, QIcon::On);
              QApplication::style ()->drawControl
               (QStyle::CE_PushButtonLabel, &opts, painter);
              return;
            }
          default:
            break;
        }
    }
  QStyledItemDelegate::paint (painter, option, index);
}


/**
 * @brief Threatcalculation
 *
 * Calculates the enumeration for the threat
 *
 * @param hole: number of holes for a task
 * @param warning: number of warnings for a task
 * @param info: number of infos for a task
 */
int
delegate_threat::calcThreat (int hole, int warning, int info)
{
  if (hole > 0)
    {
      return 0;
    }
  else if (warning > 0)
    {
      return 1;
    }
  else if (info > 0)
    {
      return 2;
    }
  else
    {
      return -1;
    }
}

