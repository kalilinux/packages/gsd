/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file dock_details.h
 * @brief Protos and data structures for details dock widgets.
 */

#ifndef DOCK_DETAILS_H
#define DOCK_DETAILS_H

#include <QtGui>

#include "ui_dock_details_task.h"
#include "ui_dock_details_target.h"
#include "ui_dock_details_credential.h"
#include "ui_dock_details_escalator.h"
#include "ui_dock_details_schedule.h"
#include "ui_dock_details_config.h"
#include "ui_dock_details_family.h"
#include "ui_dock_details_nvt.h"
#include "ui_dock_details_note.h"
#include "ui_dock_details_override.h"
#include "ui_dock_details_slave.h"
#include "ui_dock_details_port_list.h"
#include "delegate_trend.h"
#include "delegate_text.h"
#include "delegate_threat.h"
#include "delegate_date_time.h"
#include "delegate_icon.h"
#include "delegate_progress.h"
#include "omp_utilities.h"
#include "omp_credentials.h"

class gsd_control;

/**
 * @brief Superclass for details dock widgets.
 */
class dock_details : public QDockWidget
{
  Q_OBJECT

  signals:
    void sig_selected (int, QString);

  public slots:
    virtual void update () {};

  protected:
    QString id;
    gsd_control *control;
    void closeEvent (QCloseEvent *event);

  public:
    dock_details () {};
    ~dock_details () {};

    void setId (QString id) {this->id = id;};
    virtual void load () {};
};


/**
 * @brief Protos and data structures for task details widget.
 */
class dock_details_task : public dock_details, private Ui::dock_details_task
{
  Q_OBJECT

  signals:
    void sig_report (QString, QString);
    void sig_delete_report (QString);

  private slots:
    void report ();
    void report_selection_changed (const QModelIndex &sel,
                                   const QModelIndex &desel);
    void delete_report ();

  public slots:
    void update ();

  private:
    model_omp_entity *reports;

  public:
    dock_details_task (gsd_control *ctl)
      {
        setupUi (this);
        this->control = ctl;
        reports = new model_omp_entity ();
        reports->addHeader (tr ("Report"), "timestamp");
        reports->addHeader (tr ("Threat"), "messages");
        reports->addHeader (tr ("High"), "result_count hole");
        reports->addHeader (tr ("Medium"), "result_count warning");
        reports->addHeader (tr ("Low"), "result_count info");
        reports->addHeader (tr ("Log"), "result_count log");
        reports->addHeader (tr ("false Pos."), "result_count false_positive");
      };
    ~dock_details_task () {};
    void load ();
};


/**
 * @brief Protos and data structures for target details widget.
 */
class dock_details_target : public dock_details, private Ui::dock_details_target
{
  Q_OBJECT

  public slots:
    void update ();
  public:
    dock_details_target (gsd_control *ctl)
      {
        setupUi (this);
        this->control = ctl;
      };
    ~dock_details_target () {};
    void load ();
};

/**
 * @brief Protos and data structures for port_list details widget.
 */
class dock_details_port_list : public dock_details,
                               private Ui::dock_details_port_list
{
  Q_OBJECT

  public slots:
    void update (QString id);
    void update ();
  public:
    dock_details_port_list (gsd_control *ctl)
      {
        setupUi (this);
        this->control = ctl;
        portRanges = new model_omp_entity ();
      };
    ~dock_details_port_list () {};
    void load ();

  private:
    model_omp_entity *portRanges;
};

/**
 * @brief Protos and data structures for credentials details widget.
 */
class dock_details_credential : public dock_details,
                                 private Ui::dock_details_credential
{
  Q_OBJECT

  signals:
    void sig_modify (int, QString, model_omp_entity *m, QMap<QString, QString>);

  public slots:
    void update ();
    void modify_credential ();
    void exec_modify_credential (QString, QString, QString, QString);

  public:
    dock_details_credential (gsd_control *ctl)
      {
        setupUi (this);
        this->control = ctl;
        connect (tb_edit,
                 SIGNAL (released ()),
                 this,
                 SLOT (modify_credential ()));
      };
    ~dock_details_credential () {};
    void load ();
};


/**
 * @brief Protos and data structures for escalator details widget.
 */
class dock_details_escalator : public dock_details,
                               private Ui::dock_details_escalator
{
  Q_OBJECT

  public slots:
    void update ();
  public:
    dock_details_escalator (gsd_control *ctl)
      {
        setupUi (this);
        this->control = ctl;
      };
    ~dock_details_escalator () {};
    void load ();
};


/**
 * @brief Protos and data structures for escalator details widget.
 */
class dock_details_schedule : public dock_details,
                              private Ui::dock_details_schedule
{
  Q_OBJECT

  public slots:
    void update ();
  public:
    dock_details_schedule (gsd_control *ctl)
      {
        setupUi (this);
        this->control = ctl;
      };
    ~dock_details_schedule () {};
    void load ();
};


/**
 * @brief Protos and data structures for config details widget.
 */
class dock_details_config : public dock_details,
                            private Ui::dock_details_config
{
  Q_OBJECT
  signals:
    void sig_modify (int, QString, model_omp_entity *m, QMap<QString, QString>);
    void sig_details_family (QString, QString);
    void sig_details_nvt (QString, QString);
  private:
    bool editable;
    model_omp_entity *families;
    model_omp_entity *preferences;
  private slots:
    void selectionChanged_family (const QModelIndex &sel,
                                  const QModelIndex &desel);
    void selectionChanged_scanPref (const QModelIndex &sel,
                                    const QModelIndex &desel);
    void selectionChanged_nvtPref (const QModelIndex &sel,
                                   const QModelIndex &desel);
    void modify_scan_pref ();
    void modify_config ();

    void exec_modify_config (bool trend, bool selected);
    void add_family (QString name);
    void exec_modify_scanpref (QString value);

  public slots:
    void update (QString config_id);
    void details_family ();
    void details_nvt ();

  public:
    dock_details_config (gsd_control *ctl)
      {
        setupUi (this);
        this->control = ctl;
        families = new model_omp_entity ();
        preferences = new model_omp_entity ();
      };
    ~dock_details_config () {};
    void load ();
    bool isEditable () {return editable;};
    void setEditable (bool value) {editable = value;};
};


/**
 * @brief Protos and data structures for nvt family details widget.
 */
class dock_details_family : public dock_details,
                            private Ui::dock_details_family
{
  Q_OBJECT

  signals:
    void sig_details_nvt (QString, QString);
    void sig_modify (int, QString, model_omp_entity*, QMap<QString, QString>);
  private:
    bool editable;
    QString name;
    model_omp_entity *nvtModel;

  private slots:
    void details_nvt ();
    void modify_family ();
    void exec_modify_family (bool value);
    void add_nvt (QString oid, QString name);
    void nvt_selection_changed (const QModelIndex&, const QModelIndex &);

  public slots:
    void update (QString name, QString config_id);

  public:
    dock_details_family (gsd_control *ctl)
      {
        setupUi (this);
        this->control = ctl;
        nvtModel = new model_omp_entity ();
      };
    ~dock_details_family () {};
    void load ();
    void setName (QString family) {name = family;};
    void setEditable (bool value) {editable = value;};
    bool isEditable () {return editable;};
};


/**
 * @brief Protos and data structures for nvt family details widget.
 */
class dock_details_nvt : public dock_details,
                         private Ui::dock_details_nvt
{
  Q_OBJECT

  signals:
    void sig_modify (int, QString, model_omp_entity*, QMap<QString, QString>);
  private:
    bool editable;
    QString oid;
    model_omp_entity *nvtModel;
    model_omp_entity *prefModel;

  public slots:
    void update (QString name, QString config_id);
    void modify_nvt ();
    void exec_modify_nvt (QString);
  public:
    dock_details_nvt (gsd_control *ctl)
      {
        setupUi (this);
        this->control = ctl;
        nvtModel = new model_omp_entity ();
        prefModel = new model_omp_entity ();
        prefModel->addHeader ("Name", "name");
        prefModel->addHeader ("Value", "value");
        connect (actionModify_NVT,
                 SIGNAL (triggered (bool)),
                 this,
                 SLOT (modify_nvt ()));
        connect (tv_preferences,
                 SIGNAL (doubleClicked (const QModelIndex &)),
                 this,
                 SLOT (modify_nvt ()));
      };
    ~dock_details_nvt () {};
    void load ();
    void setOid (QString oid) {this->oid = oid;};
    void setEditable (bool value) {editable = value;};
    bool isEditable () {return editable;};
};


/**
 * @brief Protos and data structures for note details widget.
 */
class dock_details_note : public dock_details,
                          private Ui::dock_details_note
{
  Q_OBJECT

  signals:
    void sig_modify (int, QString, model_omp_entity*, QMap<QString, QString>);

  private:
    model_omp_entity *note;
    QString task_id;
  public slots:
    void update (QString id);
    void modify_note ();
    void exec_modify_note (QString hosts,
                           QString port,
                           QString threat,
                           QString task,
                           QString result,
                           QString text);

  public :
    dock_details_note (gsd_control *ctl)
      {
        setupUi (this);
        this->control = ctl;
        note = new model_omp_entity ();
        connect (tb_edit,
                 SIGNAL (released ()),
                 this,
                 SLOT (modify_note ()));
      };
    ~dock_details_note () {};
    void load ();
};


/**
 * @brief Protos and data structures for override details widget.
 */
class dock_details_override : public dock_details,
                              private Ui::dock_details_override
{
  Q_OBJECT

  signals:
    void sig_modify (int, QString, model_omp_entity*, QMap<QString, QString>);

  private:
    model_omp_entity *override;

  public slots:
    void update (QString id);
    void modify_override ();
    void exec_modify_override (QString hosts,
                               QString port,
                               QString threat,
                               QString newThreat,
                               QString task,
                               QString result,
                               QString text);

  public:
    dock_details_override (gsd_control *ctl)
      {
        setupUi (this);
        this->control = ctl;
        override = new model_omp_entity ();
        connect (tb_edit,
                 SIGNAL (released ()),
                 this,
                 SLOT (modify_override ()));
      };
    ~dock_details_override () {};
    void load ();
};


/**
 * @brief Protos and data structures for target details widget.
 */
class dock_details_slave : public dock_details, private Ui::dock_details_slave
{
  Q_OBJECT

  public slots:
    void update ();
  public:
    dock_details_slave (gsd_control *ctl)
      {
        setupUi (this);
        this->control = ctl;
      };
    ~dock_details_slave () {};
    void load ();
};
#endif

