/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file dock_logging.h
 * @brief Protos and data structures for dock_logging.
 */

#ifndef DOCK_LOGGING_H
#define DOCK_LOGGING_H

#include <QtGui>
#include "ui_dock_logging.h"

class dock_logging : public QDockWidget, private Ui::dock_logging
{
  Q_OBJECT

  signals:
/**
 * @brief SIGNAL to set a new log level
 *
 * @param : log level
 */
    void sig_log_lvl (double);

  public:
    dock_logging();

    ~dock_logging();

  public:
    QListWidget* getList ();

  private slots:
    void level_changed (double lvl);
    void clear_list ();
};
#endif

