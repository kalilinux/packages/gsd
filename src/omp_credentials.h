/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file omp_credentials.h
 * @class omp_credentials
 * @brief Protos and data structures for omp_credentials.
 */

#ifndef OMP_CREDENTIALS_H
#define OMP_CREDENTIALS_H

#include <QtGui>

class omp_credentials
{
  private:
    const QString *userName;
    const QString *password;
    const QString *serverAddress;
    int serverPort;

  public:
    omp_credentials ();
    ~omp_credentials ();

    QString getUserName ();
    QString getPassword ();
    QString getServerAddress ();
    int getServerPort ();

    void setUserName (const QString &name);
    void setPassword (const QString &pass);
    void setServerAddress (const QString &addr);
    void setServerPort (int port);
};
#endif

