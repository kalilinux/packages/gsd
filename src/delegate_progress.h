/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file delegate_progress.h
 * @class delegate_progress
 * @brief Protos and data structures for delegate_progress.
 */

#ifndef DELEGATE_PROGRESS_H
#define DELEGATE_PROGRESS_H

#include <QtGui>

#include "model_omp_entity.h"

class delegate_progress : public QStyledItemDelegate
{
  Q_OBJECT

  public:
    delegate_progress ();
    ~delegate_progress ();

    /**
     * @brief new implemented sizeHint (...), returning the size of the delegate
     */
    QSize sizeHint (const QStyleOptionViewItem&, const QModelIndex&) const 
      {
        return QSize(60,16);
      }

    void paint (QPainter* painter, const QStyleOptionViewItem& option,
                const QModelIndex& index) const;
  };
#endif

