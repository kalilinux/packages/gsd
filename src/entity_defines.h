/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef ENTITIY_DEFINES_H
#define ENTITIY_DEFINES_H

/**
 * @file entity_defines.h
 * @brief Macros for getter and setter construction
 */

/**
 * @brief Macro to generate a virtual abstract classproperty getter/setter pair
 */

#define VIRTUAL_ABSTRACT_CLASSPROPERTY(type,property) \
  virtual void set##property(type val) = 0; \
  virtual type get##property(void) = 0;

/**
 * @brief Macro to generate a vir. abstr. classobjectprop. getter/setter pair
 */

#define VIRTUAL_ABSTRACT_CLASSOBJECTPROPERTY(type,property) \
  virtual void set##property(const type &val) = 0; \
  virtual type get##property(void) = 0;

/**
 * @brief Macro to generate a classproperty getter/setter pair
 */

#define CLASS_PROPERTY(type,property)  \
  void set##property(type val) {s_##property = val; } \
  type get##property(void) { return s_##property; }

/**
 * @brief Macro to generate a classobjectproperty getter/setter pair
 */

#define CLASS_OBJECT_PROPERTY(type,property) \
  void set##property(const type &val) { s_##property = val; } \
  type get##property(void) { return s_##property; }

/**
 * @brief Macro to generate a virtual classproperty getter/setter pair
 */

#define VIRTUAL_CLASS_PROPERTY(type,property) \
  virtual void set##property(type val) { s_##property = val; } \
  virtual type get##property(void) { return s_##property; }

/**
 * @brief Macro to generate a virtual classobjectproperty getter/setter pair
 */

#define VIRTUAL_CLASS_OBJECT_PROPERTY(type,property) \
  virtual void set##property(const type &val) { s_##property = val; } \
  virtual type get##property(void) { return s_##property; }

#endif

