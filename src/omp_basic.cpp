/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "omp_basic.h"

/**
 * @file omp_basic.cpp
 * @class omp_basic
 * @brief Basic omp-functionality
 *
 * This file provides the basic functions to connect to the OpenVas Manager,
 * like establishing an connection, closing a connection, storing the session
 * and logindata.
 */

omp_basic::omp_basic ()
{
  this->credentials = NULL;
}

omp_basic::~omp_basic ()
{
  free (this->credentials);
}


/**
 * @brief Establish a connection to OpenVAS Manager.
 *
 * Connects to the OpenVAS Manager and creates a session.
 *
 * @param addr Serveraddress
 * @param port Serverport
 *
 * @return 0 if a connection is successful established, 1 on error.
 */
int
omp_basic::connectToManager (const QString &addr, int port)
{
  gnutls_session_t session;
  session = NULL;

  // QString to const char
  QByteArray baddr = addr.toLatin1 ();
  const char* caddr = baddr.constData ();
  // open a connection to the manager
  this->socket = openvas_server_open (&session, caddr, port);
  if (this->socket == -1)
    {
      return 1;
    }
  else
    {
      this->session = session;
      return 0;
    }
}


/**
 * @brief Autenticate with the OpenVAS Manager.
 *
 * After connecting to the manager and creating a session, this function is
 * used to login.
 *
 * @param name Login username
 * @param pwd  Login password
 *
 * @return 0 on success, 1 if manager closed connection, 2 if auth failed,
 *         -1 on error.
 */
int
omp_basic::authenticate (const QString &name, const QString &pwd)
{
  // QString to const char
  QByteArray bname = name.toLatin1 ();
  const char* cname = bname.constData ();

  QByteArray bpwd = pwd.toLatin1 ();
  const char* cpwd = bpwd.constData ();

  // authenticate with the manager
  int ret = omp_authenticate (&this->session, cname, cpwd);
  return ret;
}


/**
 * @brief Opens a connection to the OpenVAS Manager.
 *
 * Use this function to connect to the Openvas Manager. Before a connection can
 * be opened, logindata has to be set, using setCredentials (...).
 *
 * @return 0 on success, 1 if manager closed connection, 2 if auth failed,
 *         -1 on error.
 */
int
omp_basic::openConnection ()
{
  if (this->credentials != NULL)
    {
      if (connectToManager (this->credentials->getServerAddress (),
                            this->credentials-> getServerPort ())==0)
        {
          return authenticate (this->credentials->getUserName (),
                              this->credentials->getPassword ());
        }
      else
        {
          return -1;
        }
    }
  return -1;
}


/**
 * @brief Closes the conection to the OpenVAS Manager.
 *
 * @return 0 on success, -1 on error.
 */
int
omp_basic::closeConnection ()
{
  int ret = openvas_server_close (this->socket, this->session);
  return ret;
}


/**
 * @brief Setter for logindata and connectiondetails.
 *
 * Sets the logindata, serveraddress and port used to connect to the OpenVAS
 * Manager.
 *
 * @param addr  Serveraddress
 * @param port  Serverport
 * @param name  Username
 * @param pwd   Password
 */
void
omp_basic::setCredentials (QString addr, int port, QString name,
                           QString pwd)
{
  this->credentials = new omp_credentials ();
  this->credentials->setServerAddress (addr);
  this->credentials->setServerPort (port);
  this->credentials->setUserName (name);
  this->credentials->setPassword (pwd);
}


/**
 * @brief Setter for logindata and connectiondetails.
 *
 * Sets the logindata, serveraddress and port used to connect to the OpenVAS
 * Manager.
 *
 * @param crd logindata encapsulted in the entityobject omp_credentials.
 */
void
omp_basic::setCredentials (omp_credentials &crd)
{
  this->setCredentials (crd.getServerAddress (), crd.getServerPort (),
                        crd.getUserName (), crd.getPassword ());
}


omp_credentials*
omp_basic::getCredentials ()
{
  return this->credentials;
}

