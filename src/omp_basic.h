/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file omp_basic.h
 * @brief Protos and data structures for omp_basic.
 */

#ifndef OMP_BASIC_H
#define OMP_BASIC_H

#include <QObject>

#include "omp_credentials.h"
#include "entity_defines.h"

#include <openvas/omp/xml.h>
#include <openvas/omp/omp.h>
#include <openvas/misc/openvas_server.h>

class omp_basic
{
  private:
    omp_credentials *credentials;
    int connectToManager (const QString &addr, int port);
    int authenticate (const QString &name, const QString &pwd);

  protected:
    gnutls_session_t session;
    int socket;

  public:
    omp_basic ();
    ~omp_basic ();

    int openConnection ();
    int closeConnection ();

    void setCredentials (QString addr, int port, QString name, QString pwd);
    void setCredentials (omp_credentials &crd);
    omp_credentials *getCredentials ();
};
#endif

