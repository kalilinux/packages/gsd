/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file omp_update.h
 * @class omp_update
 * @brief Protos and data structures for omp_update.
 */

#ifndef OMP_UPDATE_H
#define OMP_UPDATE_H

#include <QtGui>

#include "omp_command.h"

#include <glib.h>

#include <openvas/omp/xml.h>
#include <openvas/omp/omp.h>
#include <openvas/misc/openvas_server.h>

class omp_update :public QObject
{
  Q_OBJECT

  signals:
    void sig_update_trigger ();
  private:
    int interval;
    bool running;
    QTimer *intervalTimer;
    QMap <omp_utilities::omp_type, model_omp_entity*> modelList;
    QMap <omp_utilities::omp_type, QString > stringList;
    QList <omp_utilities::omp_type> updateQueue;
    omp_command *worker;
  private slots:
    void update ();

  public:
    omp_update ();
    ~omp_update ();

    bool isRunning ();

    int getInterval ();
    void setInterval (int interval);
    void addModel (omp_utilities::omp_type type, model_omp_entity *m);
    void addCommand (omp_utilities::omp_type type, QString omp_string);
    void removeType (omp_utilities::omp_type type);
    void removeFromQueue (omp_utilities::omp_type type);
    void addToQueue (omp_utilities::omp_type type);

    void start ();
    void stop ();

    void setHelper (omp_thread_helper *helper);
    void setCredentials (QString addr, int port, QString user, QString pwd);
};
#endif

