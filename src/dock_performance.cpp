/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "dock_performance.h"

/**
 * @file dock_performance.cpp
 *
 * @brief Dock widget to show performance statistics as png graphic.
 */

/**
 * @brief Creates a new widget.
 *
 * Creates a new widget and configures signal/slot connections.
 */
dock_performance::dock_performance ()
{
  setupUi (this);
  currentIndex = 0;
  comboBox->addItem (tr ("hour(s)"));
  comboBox->addItem (tr ("day(s)"));
  comboBox->addItem (tr ("month(s)"));
  spinBox->setMinimum (0);
  spinBox->setMaximum (48);
  spinBox_2->setMaximum (1024);
  spinBox_3->setMaximum (768);
  connect (pushButton_2, SIGNAL (released ()), this, SLOT (timeChanged ()));
  connect (comboBox, SIGNAL (currentIndexChanged (int)), this, SLOT (comboChanged (int)));
  connect (pushButton, SIGNAL (released ()), this, SLOT (saveAsPNG ()));
  pushButton->setEnabled (false);
}


dock_performance::~dock_performance ()
{
}

/**
 * @brief Setter
 *
 * @param[in]   m   Model containing the reports list.
 */
void
dock_performance::setReportModel (model_omp_entity *m)
{
  this->reportModel = m;
  if (m->rowCount () == 0)
    label->setText (tr("No system report available."));
  connect (comboBox_2, SIGNAL (currentIndexChanged (int)),
           this, SLOT (selectionChanged ()));
}


/**
 * @brief Setter
 *
 * @param[in]   m   Model containing the graphics.
 */
void
dock_performance::setGraphicsModel (model_omp_entity *m)
{
  this->graphicsModel = m;
  connect (graphicsModel, SIGNAL (sig_changed (bool)),
           this, SLOT (selectionChanged ()));
}


/**
 *  @brief SLOT that shows the graphics selected from list.
 */
void
dock_performance::selectionChanged ()
{
  pushButton_2->setEnabled (true);
  QDomElement e;
  if (time_changed)
    {
      e = reportModel->getEntity (currentIndex);
      time_changed = false;
    }
  else
    e = reportModel->getEntity (comboBox_2->currentIndex ());
  QString name = e.firstChildElement ("name").text ();
  graphicsModel->setFilterType (model_omp_entity::VALUE_FILTER);
  graphicsModel->setFilter ("report name " + name);

  QList<int> list = graphicsModel->getFilterIndexList ();
  graphicsModel->resetFilter ();
  QDomElement graphic;

  if (list.size () == 1)
    graphic = graphicsModel->getEntity (list.at (0));
  else
    return;

  pushButton->setEnabled (true);

  gsize len;
  uchar *file = g_base64_decode (graphic.firstChildElement ("report").text ().toLatin1 (), &len);

  QImage i = QImage::fromData (file, len, "PNG");

  QPixmap pm = QPixmap::fromImage (i);
  shownPixmap = pm;

  label->resize (pm.width (), pm.height ());
  label->setScaledContents (true);
  label->setPixmap (pm);
  label->setAlignment (Qt::AlignCenter);

  QString s = graphicsModel->getAttr (graphicsModel->getEntity (0) , "system_report report duration");
  long time = s.toLongLong ();

  time = time/3600;
  if (time % 24 != 0)
    {
      spinBox->setValue (time);
      comboBox->setCurrentIndex (0);
    }
  else if (time % 24 == 0 && !(time % (24*30) == 0))
    {
      spinBox->setValue (time /24);
      comboBox->setCurrentIndex (1);
    }
  else if (time % (24*30) == 0)
    {
      spinBox->setValue (time/(24*30));
      comboBox->setCurrentIndex (2);
    }

  if (spinBox_2->value () == 0)
    {
      spinBox_2->setValue (label->size ().width ());
      spinBox_3->setValue (label->size ().height ());
      sizeX = label->size ().width ();
    }
  sizeChangedX (sizeX);
  connect (spinBox_2, SIGNAL (valueChanged (int)), this, SLOT (sizeChangedX (int)));
  connect (spinBox_3, SIGNAL (valueChanged (int)), this, SLOT (sizeChangedY (int)));

}


/**
 * @brief SLOT that requests new graphics with the new duration.
 */
void
dock_performance::timeChanged ()
{
  long duration;
  if (spinBox->value () == 0)
    return;

  time_changed = true;
  currentIndex = comboBox_2->currentIndex ();

  if (comboBox->currentText ().compare (tr ("hour(s)")) == 0)
    duration = spinBox->value () * 3600;
  if (comboBox->currentText ().compare (tr ("day(s)")) == 0)
    duration = spinBox->value () * 3600 *24;
  if (comboBox->currentText ().compare (tr ("month(s)")) == 0)
    duration = spinBox->value () * 3600 * 24 *30;

  pushButton_2->setEnabled (false);
  emit sig_request_system_reports ( NULL, duration);
}


/**
 * @brief SLOT that sets the maximum value for duration.
 */
void
dock_performance::comboChanged (int index)
{
  if (comboBox->itemText (index).compare (tr ("hour(s)")) == 0)
    spinBox->setMaximum (48);
  if (comboBox->itemText (index).compare (tr ("day(s)")) == 0)
    spinBox->setMaximum (120);
  if (comboBox->itemText (index).compare (tr ("month(s)")) == 0)
    spinBox->setMaximum (12);
}


/**
 * @brief SLOT that loads the report list.
 */
void
dock_performance::load ()
{
  comboBox_2->clear ();
  for (int i = 0; i < reportModel->rowCount (); i++)
    {
      QString s = reportModel->getValue (reportModel->getEntity (i), "system_report title");
      comboBox_2->addItem (reportModel->getValue (reportModel->getEntity (i), "title"));
    }
  comboBox_2->setCurrentIndex (currentIndex);
}


/**
 * @brief SLOT that saves the current graphic as png.
 */
void
dock_performance::saveAsPNG ()
{
  QDomElement e = reportModel->getEntity(comboBox_2->currentIndex ());
  QString name = e.firstChildElement ("title").text ();
  name = name.replace (" ", "_");
  name = name.remove ("*");
  name = name.remove ("/");
  name = name.remove (":");
  QString filename = QFileDialog::getSaveFileName (this, tr ("Save File ..."),
                                                   QDir::homePath () + "/" +
                                                   name + ".png",
                                                   tr ("*.png"));
  if (filename.compare ("") == 0)
    return;
  label->pixmap ()->save (filename, "PNG", 10);
}


/**
 * @brief SLOT that changes the graphic size.
 *
 * @param[in]   i   New value for graphic width.
 */
void
dock_performance::sizeChangedX (int i)
{
  sizeX = i;
  double ratio = (double)label->pixmap ()->height () / (double)label->pixmap ()->width ();

  if (spinBox_2->value () != label->pixmap ()->width ())
    {
      sizeX = spinBox_2->value ();
      double sizeY = spinBox_2->value () * ratio;
      disconnect (spinBox_3, SIGNAL (valueChanged (int)), this, SLOT (sizeChangedY (int)));
      spinBox_3->setValue (sizeY);
      label->resize (sizeX, sizeY);
      label->setPixmap (shownPixmap.scaledToWidth (sizeX, Qt::SmoothTransformation));
      connect (spinBox_3, SIGNAL (valueChanged (int)), this, SLOT (sizeChangedY (int)));
    }
}


/**
 * @brief SLOT that changes the graphic size
 *
 * @param[in]   i   New value for gaphic height.
 */
void
dock_performance::sizeChangedY (int i)
{
  double ratio = (double)label->pixmap ()->height () / (double)label->pixmap ()->width ();

  if (spinBox_3->value () != label->pixmap ()->height ())
    {
      sizeY = spinBox_3->value ();
      double sizeX = spinBox_3->value () / ratio;
      disconnect (spinBox_2, SIGNAL (valueChanged (int)), this, SLOT (sizeChangedX (int)));
      spinBox_2->setValue (sizeX);
      label->resize (sizeX, sizeY);
      label->setPixmap (shownPixmap.scaledToHeight (sizeY, Qt::SmoothTransformation));
      connect (spinBox_2, SIGNAL (valueChanged (int)), this, SLOT (sizeChangedX (int)));
    }
}
