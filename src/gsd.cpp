/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @mainpage
 * @verbinclude README
 *
 * @section Installation
 * @verbinclude INSTALL
 */

#include <QApplication>
#include <gcrypt.h>
#include <pthread.h>

#include "gsd_control.h"
#include "omp_connector.h"
#include "gsd_mw.h"
#include "gsd_dlg.h"
#include "gsd_config.h"


#include "omp_connector.h"

/**
 * @file gsd.cpp
 * @brief Mainentrypoint for application
 */


/**
 * @brief extern "c" includes
 */
extern "C"
{
#include <errno.h>

GCRY_THREAD_OPTION_PTHREAD_IMPL;
}


/**
 * @brief Init glib and gcrypt for connection to the manager.
 *
 * @return -1 on init error, else 0
 */
int
gsd_init ()
{
 /* Init Glib. */
  if (!g_thread_supported ()) g_thread_init (NULL);

  /* Init GCRYPT. */
  gcry_control (GCRYCTL_SET_THREAD_CBS, &gcry_threads_pthread);

  /* Version check should be the very first call because it makes sure that
   * important subsystems are intialized. */
  if (!gcry_check_version (GCRYPT_VERSION))
    {
      return -1;
    }

  /* We don't want to see any warnings, e.g. because we have not yet parsed
   * program options which might be used to suppress such warnings. */
  gcry_control (GCRYCTL_SUSPEND_SECMEM_WARN);

  /* ... If required, other initialization goes here.  Note that the process
   * might still be running with increased privileges and that the secure
   * memory has not been intialized. */

  /* Allocate a pool of 16k secure memory.  This make the secure memory
   * available and also drops privileges where needed. */
  gcry_control (GCRYCTL_INIT_SECMEM, 16384, 0);

  /* It is now okay to let Libgcrypt complain when there was/is a problem with
   * the secure memory. */
  gcry_control (GCRYCTL_RESUME_SECMEM_WARN);

  /* ... If required, other initialization goes here. */

  /* Tell Libgcrypt that initialization has completed. */
  gcry_control (GCRYCTL_INITIALIZATION_FINISHED, 0);

  /* Init GNUTLS. */
  int ret = gnutls_global_init ();
  if (ret < 0)
    {
      return -1;
    }
  return 0;
}


/**
 * @brief Entry point to the gsd.
 *
 * Setup and launch the gsd.
 *
 * @param[in]  argc  The number of arguments in argv.
 * @param[in]  argv  The list of arguments to the program.
 *
 * @return 0 on normal termination, anything else indicates a failure.
 */
int main(int argc, char *argv[])
  {
    GError *error = NULL;

    static gboolean print_version = FALSE;
    GOptionContext *option_context;
    static GOptionEntry option_entries []
      = {
          { "version", '\0', 0, G_OPTION_ARG_NONE, &print_version, "Print version and exit.", NULL },
          { NULL }
        };

    option_context = g_option_context_new ("- Desktop Client for OpenVAS Manager");
    g_option_context_add_main_entries (option_context, option_entries, NULL);
    if (!g_option_context_parse (option_context, &argc, &argv, &error))
      {
        g_option_context_free (option_context);
        g_critical ("%s: %s\n\n", __FUNCTION__, error->message);
        exit (EXIT_FAILURE);
      }
    g_option_context_free (option_context);

    if (print_version)
      {
        printf ("Greenbone Security Desktop %s\n", GSD_VERSION);
        printf ("Copyright (C) 2011-2012 Greenbone Networks GmbH\n");
        printf ("License GPLv2+: GNU GPL version 2 or later\n");
        printf
          ("This is free software: you are free to change and redistribute it.\n"
           "There is NO WARRANTY, to the extent permitted by law.\n\n");
        exit (EXIT_SUCCESS);
      }

    QApplication app (argc, argv);

    if (gsd_init() != 0)
      {
        QMessageBox::information (NULL,"Init Error",
                                  "Error while initializing gcrypt!");
      }

    app.setOrganizationName ("Greenbone");
    app.setApplicationName ("Greenbone Security Desktop");

    QSettings settings ("Greenbone", "GSD");
    settings.beginGroup ("MainWindow");
    QString language = settings.value ("language").toString ();
    settings.endGroup ();

    // Load translation
    QTranslator appTranslator;
    if (language.compare ("default") == 0 ||
        language.compare ("") == 0)
      {
        language = QLocale::system ().name ();
      }
#if defined(WIN32) || defined(__APPLE__)
    appTranslator.load ("gsd_" + language,
                        QDir::currentPath ());
#else
    appTranslator.load ("gsd_" + language,
                        OPENVAS_TRANSLATIONS_DIR);
#endif
    app.installTranslator (&appTranslator);
    gsd_control *controller = new gsd_control ();
#ifndef WIN32
    controller->setupLogging ();
#endif
    omp_connector *connector = new omp_connector ();
    gsd_mw *mainwindow = new gsd_mw (controller);
    gsd_dlg *dialogs = new gsd_dlg (controller);
    controller->setConnector (connector);
    controller->setMainwindow (mainwindow);
    controller->setDialogs (dialogs);
    controller->start ();
    mainwindow->setLanguage (language);
    return app.exec ();
}

