/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "omp_update.h"

/**
 * @file omp_update.cpp
 * @brief Basic functionality for an updatethread
 *
 * This class provides basic functionality for a thread to connect to the
 * Openvas Manager. It provides the main used functions to repeat functionality,
 * like starting and stoping an update. To use this class inherit it and
 * reimplement the virtual run () to do useful work.
 */

omp_update::omp_update ()
{
  this->interval = 0;
  this->running = false;
  intervalTimer = new QTimer ();
  connect (intervalTimer, SIGNAL (timeout ()), this, SLOT (update ()));
  worker = new omp_command ();
}

omp_update::~omp_update ()
{
  free (intervalTimer);
}


/**
 * @brief Indicates if the update is running in an interval.
 */
bool
omp_update::isRunning ()
{
  return this->running;
}

/**
 * @brief Returns the interval between updates.
 *
 * @return Interval in seconds.
 */
int
omp_update::getInterval ()
{
  return this->interval;
}


/**
 * @brief Sets the interval between requests.
 *
 * Starts repeating an update in the userdefined interval.
 *
 * @param interval: time between two updates.
 */
void
omp_update::setInterval (int interval)
{
  this->interval = interval;
  intervalTimer->setInterval (interval);
}


/**
 * @brief Starts repeating requests.
 */
void
omp_update::start ()
{
  this->running = true;
  intervalTimer->start ();
}
/**
 * @brief Stop repeating requests.
 */
void
omp_update::stop ()
{
  this->running = false;
  intervalTimer->stop ();
}


/**
 * @brief Adds a model - type pair. The model is used to store the manager
 *        response of a request with "type".
 *
 * @param[in]   type    omp entity type.
 * @param[out]  m       Datamodel to store the manager response.
 */
void
omp_update::addModel (omp_utilities::omp_type type, model_omp_entity *m)
{
  if (!modelList.contains (type))
    this->modelList.insert (type, m);
  else
    {
      stringList.remove (type);
      modelList.insert (type, m);
    }
}


/**
 * @brief Adds a omp string - type pair. The string represents the omp command
 *        sent to the manager.
 *
 * @param[in]   type        omp entity type.
 * @param[out]  omp_string  omp string representing the omp command.
 */
void
omp_update::addCommand (omp_utilities::omp_type type, QString omp_string)
{
  if (!stringList.contains (type))
    this->stringList.insert (type, omp_string);
  else
    {
      stringList.remove (type);
      stringList.insert (type, omp_string);
    }
}


/**
 * @brief Removes the model and the omp command to the omp type.
 *
 * @param[in]   type    omp_entity type.
 */
void
omp_update::removeType (omp_utilities::omp_type type)
{
  if (updateQueue.contains (type))
    {
      int position = updateQueue.indexOf (type);
      updateQueue.removeAt (position);
    }

  if (stringList.contains (type))
    stringList.remove (type);
  if (modelList.contains (type))
    modelList.remove (type);
}

/**
 * @brief Adds an omp entity type to the update queue. A type in this list is
 *        requested while the update is running.
 *
 * @param[in]   type    omp entity type.
 */
void
omp_update::addToQueue (omp_utilities::omp_type type)
{
  if (!updateQueue.contains (type))
    this->updateQueue.append (type);
}


/**
 * @brief Removes an omp entity type from the update queue.
 *
 * @param[in]   type   omp entity type
 */
void
omp_update::removeFromQueue (omp_utilities::omp_type type)
{
  if (this->updateQueue.contains (type))
    {
      int position = updateQueue.indexOf (type);
      this->updateQueue.removeAt (position);
    }
}


/**
 * @brief SLOT that request the entities in the queue.
 */
void
omp_update::update ()
{
  emit sig_update_trigger ();

  for (int i = 0; i < updateQueue.size (); i++)
    {
      QString str = stringList.value (updateQueue.at (i));
      model_omp_entity *tmp = modelList.value (updateQueue.at (i));
        worker->requestEntity (str, tmp);
    }

}


void
omp_update::setHelper (omp_thread_helper *helper)
{
  worker->setHelper (helper);
}


void
omp_update::setCredentials (QString addr, int port, QString user, QString pwd)
{
  worker->setCredentials (addr, port, user, pwd);
}
