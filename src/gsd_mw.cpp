/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "gsd_mw.h"
#include "gsd_control.h"
/**
 * @file gsd_mw.cpp
 * @brief The GSD main window
 *
 * This file contains main window for GSD.
 */

/**
 * @brief Set up the mainwindow
 *
 * Creates the mainwindow and the nessesary delegates for the tableviews.
 * Configures the signal/slot connections for userinteraction
 */
gsd_mw::gsd_mw (gsd_control *ctl)
{
  this->control = ctl;
  setupUi (this);

  statuslabel = new QLabel ("");

  tasks = new dock_table ();
  tasks->setObjectName ("Tasks");
  this->addDockWidget (Qt::BottomDockWidgetArea, tasks);
  schedules = new dock_table ();
  schedules->setObjectName ("Schedule");
  this->addDockWidget (Qt::BottomDockWidgetArea, schedules);
  targets = new dock_table ();
  targets->setObjectName ("Target");
  this->addDockWidget (Qt::BottomDockWidgetArea, targets);
  configs = new dock_table ();
  configs->setObjectName ("Scan Config");
  this->addDockWidget (Qt::BottomDockWidgetArea, configs);
  escalators = new dock_table ();
  escalators->setObjectName ("Escalator");
  this->addDockWidget (Qt::BottomDockWidgetArea, escalators);
  credentials = new dock_table ();
  credentials->setObjectName ("Credential");
  this->addDockWidget (Qt::BottomDockWidgetArea, credentials);
  agents = new dock_table ();
  agents->setObjectName ("Agent");
  this->addDockWidget (Qt::BottomDockWidgetArea, agents);
  notes = new dock_table ();
  notes->setObjectName ("Note");
  this->addDockWidget (Qt::BottomDockWidgetArea, notes);
  overrides = new dock_table ();
  overrides->setObjectName ("Override");
  this->addDockWidget (Qt::BottomDockWidgetArea, overrides);
  performance = new dock_performance ();
  this->addDockWidget (Qt::BottomDockWidgetArea, performance);
  slaves = new dock_table ();
  slaves->setObjectName ("Slaves");
  this->addDockWidget (Qt::BottomDockWidgetArea, slaves);
  reportformats = new dock_table ();
  reportformats->setObjectName ("Reportformats");
  this->addDockWidget (Qt::BottomDockWidgetArea, reportformats);
  portlists = new dock_table ();
  portlists->setObjectName ("Portlists");
  this->addDockWidget (Qt::BottomDockWidgetArea, portlists);

  taskProgress = new delegate_progress ();
  trendIcon = new delegate_trend ();
  threatIcon = new delegate_threat ();
  icon = new delegate_icon ();
  date_time = new delegate_date_time ();
  text = new delegate_text ();
  progressIcon = new QMovie(":/img/anibul13.gif");
  progressIcon->start();
  la_status_progress = new QLabel ();
  la_status_progress->setMovie (progressIcon);
  la_status_progress->setVisible (false);
  statusBar ()->addPermanentWidget (la_status_progress);

  //Timer
  progressUpdateTimer = new QTimer ();

  //create and modify UI
  createUpdateWidget ();
  createStatusBar ();
  createConnections ();
  createToolBars ();
  createContextMenu ();
  prepareWidgets ();

  read_settings ();

  updateInterval = 0;
  setLoggedOut ();
}

gsd_mw::~gsd_mw ()
{}


/**
 * @brief Creates and configures Toolbars
 */
void
gsd_mw::createToolBars ()
{
  tasks->addActionToToolBar (actionNewTask);
  tasks->addActionToToolBar (actionModify_Task);
  tasks->addActionToToolBar (actionDelete);
  tasks->addSeparator ();
  tasks->addActionToToolBar (actionRun);
  tasks->addActionToToolBar (actionPause);
  tasks->addActionToToolBar (actionResume);
  tasks->addActionToToolBar (actionStop);
  tasks->addActionToToolBar (actionDetails_Task);
  tasks->addSeparator ();
  tasks->addActionToToolBar (actionRefresh);

  actionPause->setVisible (false);
  actionResume->setVisible (false);
  actionRun->setDisabled (true);
  actionStop->setDisabled (true);
  actionDelete->setDisabled (true);
  actionModify_Task->setDisabled (true);
  actionRefresh->setDisabled (true);
  actionLogout->setDisabled (true);
  actionNewTask->setDisabled (true);
  actionResume->setDisabled (true);
  tb_set_refresh->setDisabled (true);

  configs->addActionToToolBar (actionNew_Config);
  configs->addActionToToolBar (actionDelete_Config);
  configs->addSeparator ();
  configs->addActionToToolBar (actionDetails_Config);
  configs->addActionToToolBar (actionExport_Config);
  configs->addActionToToolBar (actionImport_Config);
  actionDelete_Config->setEnabled (false);

  targets->addActionToToolBar (actionNew_Target);
  targets->addActionToToolBar (actionDelete_Target);
  targets->addSeparator ();
  targets->addActionToToolBar (actionDetails_Target);

  escalators->addActionToToolBar (actionNew_Escalator);
  escalators->addActionToToolBar (actionDelete_Escalator);
  escalators->addSeparator ();
  escalators->addActionToToolBar (actionDetails_Escalator);
  escalators->addActionToToolBar (actionTest_Escalator);
  actionTest_Escalator->setEnabled (false);

  credentials->addActionToToolBar (actionNew_Credential);
  credentials->addActionToToolBar (actionDelete_Credential);
  credentials->addSeparator ();
  credentials->addActionToToolBar (actionDetails_Credential);

  agents->addActionToToolBar (actionNew_Agent);
  agents->addActionToToolBar (actionDelete_Agent);
  agents->addSeparator ();
  agents->addActionToToolBar (actionDownload_Installer);
  actionDownload_Installer->setEnabled (false);

  schedules->addActionToToolBar (actionNew_Schedule);
  schedules->addActionToToolBar (actionDelete_Schedule);
  schedules->addSeparator ();
  schedules->addActionToToolBar (actionDetails_Schedule);

  notes->addActionToToolBar (actionDelete_Note);
  notes->addSeparator ();
  notes->addActionToToolBar (actionDetails_Note);

  overrides->addActionToToolBar (actionDelete_Override);
  overrides->addSeparator ();
  overrides->addActionToToolBar (actionDetails_Override);

  slaves->addActionToToolBar (actionNew_Slave);
  slaves->addActionToToolBar (actionDelete_Slave);
  slaves->addSeparator ();
  slaves->addActionToToolBar (actionDetails_Slave);

  portlists->addActionToToolBar (actionNew_PortList);
  portlists->addActionToToolBar (actionDelete_PortList);
  portlists->addSeparator ();
  portlists->addActionToToolBar (actionDetails_PortList);
}

/**
 * @brief Adds actions to context menus.
 */
void
gsd_mw::createContextMenu ()
{
  tasks->addActionToMenu (actionNewTask);
  tasks->addActionToMenu (actionModify_Task);
  tasks->addActionToMenu (actionDelete);
  tasks->addSeparatorToMenu ();
  tasks->addActionToMenu (actionRun);
  tasks->addActionToMenu (actionPause);
  tasks->addActionToMenu (actionResume);
  tasks->addActionToMenu (actionStop);
  tasks->addActionToMenu (actionDetails_Task);
  tasks->addSeparatorToMenu ();
  tasks->addActionToMenu (actionRefresh);

  configs->addActionToMenu (actionNew_Config);
  configs->addActionToMenu (actionDelete_Config);
  configs->addSeparatorToMenu ();
  configs->addActionToMenu (actionDetails_Config);
  configs->addActionToMenu (actionExport_Config);
  configs->addActionToMenu (actionImport_Config);

  targets->addActionToMenu (actionNew_Target);
  targets->addActionToMenu (actionDelete_Target);
  targets->addSeparatorToMenu ();
  targets->addActionToMenu (actionDetails_Target);

  escalators->addActionToMenu (actionNew_Escalator);
  escalators->addActionToMenu (actionDelete_Escalator);
  escalators->addSeparatorToMenu ();
  escalators->addActionToMenu (actionDetails_Escalator);
  escalators->addActionToMenu (actionTest_Escalator);

  credentials->addActionToMenu (actionNew_Credential);
  credentials->addActionToMenu (actionDelete_Credential);
  credentials->addSeparatorToMenu ();
  credentials->addActionToMenu (actionDetails_Credential);

  agents->addActionToMenu (actionNew_Agent);
  agents->addActionToMenu (actionDelete_Agent);
  agents->addSeparatorToMenu ();
  agents->addActionToMenu (actionDownload_Installer);

  schedules->addActionToMenu (actionNew_Schedule);
  schedules->addActionToMenu (actionDelete_Schedule);
  schedules->addSeparatorToMenu ();
  schedules->addActionToMenu (actionDetails_Schedule);

  notes->addActionToMenu (actionDelete_Note);
  notes->addSeparatorToMenu ();
  notes->addActionToMenu (actionDetails_Note);

  overrides->addActionToMenu (actionDelete_Override);
  overrides->addSeparatorToMenu ();
  overrides->addActionToMenu (actionDetails_Override);

  slaves->addActionToMenu (actionNew_Slave);
  slaves->addActionToMenu (actionDelete_Slave);
  slaves->addSeparatorToMenu ();
  slaves->addActionToMenu (actionDetails_Slave);

  portlists->addActionToMenu (actionNew_PortList);
  portlists->addActionToMenu (actionDelete_PortList);
  portlists->addSeparatorToMenu ();
  portlists->addActionToMenu (actionDetails_PortList);
}


/**
 * @brief Modifies Widgets and configures signal/slot connections
 *
 * Modifies the taskview, to set up the header and connects signal/slots for
 * taskselection and contextmenu
 */
void
gsd_mw::prepareWidgets ()
{
  tasks->setWindowTitle (tr ("Tasks"));
  tasks->setModel (control->getTaskModel ());
  QTableView *tv_tasks = tasks->getTable ();
  tv_tasks->setObjectName ("tv_tasks");
  tv_tasks->setItemDelegateForColumn (1, icon);
  tv_tasks->setItemDelegateForColumn (2, taskProgress);
  tv_tasks->setItemDelegateForColumn (4, date_time);
  tv_tasks->setItemDelegateForColumn (5, date_time);
  tv_tasks->setItemDelegateForColumn (6, threatIcon);
  tv_tasks->setItemDelegateForColumn (7, trendIcon);
  tv_tasks->verticalHeader ()->setVisible (false);
  tv_tasks->horizontalHeader ()->setHighlightSections (false);
  tv_tasks->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_tasks->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_tasks->setColumnWidth (0, 120);
  tv_tasks->setColumnWidth (1, 22);
  tv_tasks->setColumnWidth (2, 120);
  tv_tasks->setColumnWidth (3, 60);
  tv_tasks->setColumnWidth (4, 150);
  tv_tasks->setColumnWidth (5, 150);
  tv_tasks->setColumnWidth (6, 80);
  tv_tasks->setColumnWidth (7, 60);
  tv_tasks->installEventFilter (this);
  connect (tv_tasks, SIGNAL (doubleClicked (const QModelIndex &)), this,
           SLOT (details_task_dlg ()));
  connect (tv_tasks->selectionModel (),
           SIGNAL (selectionChanged (const QItemSelection &,
                                     const QItemSelection &)),
           this,
           SLOT (task_selectionchanged (const QItemSelection &,
                                        const QItemSelection &)));

  targets->setWindowTitle (tr ("Targets"));
  QMainWindow::tabifyDockWidget (tasks, targets);
  targets->setModel (control->getTargetModel ());
  targets->setFeatures (QDockWidget::DockWidgetClosable |
                        QDockWidget::DockWidgetMovable |
                        QDockWidget::DockWidgetFloatable);
  QTableView *tv_targets = targets->getTable ();
  tv_targets->setObjectName ("tv_targets");
  tv_targets->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_targets->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_targets->setColumnWidth (0, 120);
  tv_targets->setColumnWidth (1, 150);
  tv_targets->setColumnWidth (2, 150);
  tv_targets->setColumnWidth (3, 100);
  tv_targets->setColumnWidth (4, 100);
  tv_targets->verticalHeader ()->setVisible (false);
  tv_targets->horizontalHeader ()->setHighlightSections (false);
  tv_targets->horizontalHeader ()->setMinimumSectionSize (75);

  tv_targets->installEventFilter (this);
  connect (tv_targets, SIGNAL (doubleClicked (const QModelIndex &)), this,
           SLOT (details_target_dlg ()));
  connect (tv_targets->selectionModel (),
           SIGNAL (selectionChanged (const QItemSelection&,
                                     const QItemSelection &)),
           this,
           SLOT (target_selectionchanged (const QItemSelection &,
                                          const QItemSelection &)));

  schedules->setWindowTitle (tr ("Schedules"));
  QMainWindow::tabifyDockWidget (targets, schedules);
  schedules->setModel (control->getScheduleModel ());
  schedules->setFeatures (QDockWidget::DockWidgetClosable |
                          QDockWidget::DockWidgetMovable |
                          QDockWidget::DockWidgetFloatable);
  QTableView *tv_schedules = schedules->getTable ();
  tv_schedules->setObjectName ("tv_schedules");
  tv_schedules->setItemDelegateForColumn (4, date_time);
  tv_schedules->setItemDelegateForColumn (3, date_time);
  tv_schedules->setItemDelegateForColumn (2, date_time);
  tv_schedules->setItemDelegateForColumn (1, date_time);
  tv_schedules->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_schedules->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_schedules->setColumnWidth (0, 120);
  tv_schedules->setColumnWidth (1, 150);
  tv_schedules->setColumnWidth (2, 150);
  tv_schedules->setColumnWidth (3, 80);
  tv_schedules->setColumnWidth (4, 80);
  tv_schedules->verticalHeader ()->setVisible (false);
  tv_schedules->horizontalHeader ()->setHighlightSections (false);
  tv_schedules->horizontalHeader ()->setMinimumSectionSize (75);

  tv_schedules->installEventFilter (this);
  connect (tv_schedules, SIGNAL (doubleClicked (const QModelIndex &)), this,
           SLOT (details_schedule_dlg ()));
  connect (tv_schedules->selectionModel (),
           SIGNAL (selectionChanged (const QItemSelection&, const QItemSelection &)),
           this, SLOT (schedule_selectionchanged (const QItemSelection &,
                                                  const QItemSelection &)));

  configs->setWindowTitle (tr ("Scan Configs"));
  QMainWindow::tabifyDockWidget (targets, configs);
  configs->setModel (control->getConfigModel ());
  configs->setFeatures (QDockWidget::DockWidgetClosable |
                        QDockWidget::DockWidgetMovable |
                        QDockWidget::DockWidgetFloatable);
  QTableView *tv_configs = configs->getTable ();
  tv_configs->setObjectName ("tv_configs");
  tv_configs->setItemDelegateForColumn (2, trendIcon);
  tv_configs->setItemDelegateForColumn (4, trendIcon);
  tv_configs->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_configs->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_configs->setColumnWidth (0, 120);
  tv_configs->setColumnWidth (1, 100);
  tv_configs->setColumnWidth (2, 60);
  tv_configs->setColumnWidth (3, 80);
  tv_configs->setColumnWidth (4, 60);
  tv_configs->verticalHeader ()->setVisible (false);
  tv_configs->horizontalHeader ()->setHighlightSections (false);
  tv_configs->horizontalHeader ()->setMinimumSectionSize (75);

  tv_configs->installEventFilter (this);
  connect (tv_configs, SIGNAL (doubleClicked (const QModelIndex &)), this,
           SLOT (details_config_dlg ()));
  connect (tv_configs->selectionModel (),
           SIGNAL (selectionChanged (const QItemSelection&, const QItemSelection &)),
           this, SLOT (config_selectionchanged (const QItemSelection &,
                                                const QItemSelection &)));


  escalators->setWindowTitle (tr ("Escalators"));
  QMainWindow::tabifyDockWidget (targets, escalators);
  escalators->setModel (control->getEscalatorModel ());
  escalators->setFeatures (QDockWidget::DockWidgetClosable |
                           QDockWidget::DockWidgetMovable |
                           QDockWidget::DockWidgetFloatable);
  QTableView *tv_escalators = escalators->getTable ();
  tv_escalators->setObjectName ("tv_escalators");
  tv_escalators->setItemDelegateForColumn (1, text);
  tv_escalators->setItemDelegateForColumn (2, text);
  tv_escalators->setItemDelegateForColumn (3, text);
  tv_escalators->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_escalators->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_escalators->setColumnWidth (0, 120);
  tv_escalators->setColumnWidth (1, 200);
  tv_escalators->setColumnWidth (2, 200);
  tv_escalators->setColumnWidth (3, 200);
  tv_escalators->verticalHeader ()->setVisible (false);
  tv_escalators->horizontalHeader ()->setHighlightSections (false);
  tv_escalators->horizontalHeader ()->setMinimumSectionSize (75);

  tv_escalators->installEventFilter (this);
  connect (tv_escalators, SIGNAL (doubleClicked (const QModelIndex &)), this,
           SLOT (details_escalator_dlg ()));
  connect (tv_escalators->selectionModel (),
           SIGNAL (selectionChanged (const QItemSelection&, const QItemSelection &)),
           this, SLOT (escalator_selectionchanged (const QItemSelection &,
                                                const QItemSelection &)));

  credentials->setWindowTitle (tr ("Credentials"));
  QMainWindow::tabifyDockWidget (targets, credentials);
  credentials->setModel (control->getCredentialModel ());
  credentials->setFeatures (QDockWidget::DockWidgetClosable |
                            QDockWidget::DockWidgetMovable |
                            QDockWidget::DockWidgetFloatable);
  QTableView *tv_credentials = credentials->getTable ();
  tv_credentials->setObjectName ("tv_credentials");
  tv_credentials->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_credentials->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_credentials->setColumnWidth (0, 120);
  tv_credentials->setColumnWidth (1, 200);
  tv_credentials->setColumnWidth (2, 200);
  tv_credentials->verticalHeader ()->setVisible (false);
  tv_credentials->horizontalHeader ()->setHighlightSections (false);
  tv_credentials->horizontalHeader ()->setMinimumSectionSize (75);

  tv_credentials->installEventFilter (this);
  connect (tv_credentials, SIGNAL (doubleClicked (const QModelIndex &)), this,
           SLOT (details_credential_dlg ()));
  connect (tv_credentials->selectionModel (),
           SIGNAL (selectionChanged (const QItemSelection&, const QItemSelection &)),
           this, SLOT (credential_selectionchanged (const QItemSelection &,
                                                    const QItemSelection &)));

  agents->setWindowTitle (tr ("Agents"));
  QMainWindow::tabifyDockWidget (targets, agents);
  agents->setModel (control->getAgentModel ());
  agents->setFeatures (QDockWidget::DockWidgetClosable |
                       QDockWidget::DockWidgetMovable |
                       QDockWidget::DockWidgetFloatable);
  QTableView *tv_agents = agents->getTable ();
  tv_agents->setObjectName ("tv_agents");
  tv_agents->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_agents->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_agents->setColumnWidth (0, 120);
  tv_agents->setColumnWidth (1, 200);
  tv_agents->setColumnWidth (2, 200);
  tv_agents->verticalHeader ()->setVisible (false);
  tv_agents->horizontalHeader ()->setHighlightSections (false);
  tv_agents->horizontalHeader ()->setMinimumSectionSize (75);

  tv_agents->installEventFilter (this);
  connect (tv_agents->selectionModel (),
           SIGNAL (selectionChanged (const QItemSelection&, const QItemSelection &)),
           this, SLOT (agent_selectionchanged (const QItemSelection &,
                                               const QItemSelection &)));


  notes->setWindowTitle (tr ("Notes"));
  QMainWindow::tabifyDockWidget (targets, notes);
  notes->setModel (control->getNoteModel ());
  notes->setFeatures (QDockWidget::DockWidgetClosable |
                      QDockWidget::DockWidgetMovable |
                      QDockWidget::DockWidgetFloatable);
  QTableView *tv_notes = notes->getTable ();
  tv_notes->setObjectName ("tv_notes");
  tv_notes->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_notes->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_notes->setColumnWidth (0, 150);
  tv_notes->setColumnWidth (1, 300);
  tv_notes->verticalHeader ()->setVisible (false);
  tv_notes->horizontalHeader ()->setHighlightSections (false);
  tv_notes->horizontalHeader ()->setMinimumSectionSize (75);

  tv_notes->installEventFilter (this);
  connect (tv_notes, SIGNAL (doubleClicked (const QModelIndex &)), this,
           SLOT (details_note_dlg ()));
  connect (tv_notes->selectionModel (),
           SIGNAL (selectionChanged (const QItemSelection&, const QItemSelection &)),
           this, SLOT (note_selectionchanged (const QItemSelection &,
                                              const QItemSelection &)));


  overrides->setWindowTitle (tr ("Overrides"));
  QMainWindow::tabifyDockWidget (targets, overrides);
  overrides->setModel (control->getOverrideModel ());
  overrides->setFeatures (QDockWidget::DockWidgetClosable |
                          QDockWidget::DockWidgetMovable |
                          QDockWidget::DockWidgetFloatable);
  QTableView *tv_overrides = overrides->getTable ();
  tv_overrides->setObjectName ("tv_overrides");
  tv_overrides->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_overrides->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_overrides->setColumnWidth (0, 150);
  tv_overrides->setColumnWidth (1, 80);
  tv_overrides->setColumnWidth (2, 80);
  tv_overrides->setColumnWidth (3, 250);
  tv_overrides->verticalHeader ()->setVisible (false);
  tv_overrides->horizontalHeader ()->setHighlightSections (false);
  tv_overrides->horizontalHeader ()->setMinimumSectionSize (75);

  tv_overrides->installEventFilter (this);
  connect (tv_overrides, SIGNAL (doubleClicked (const QModelIndex &)), this,
           SLOT (details_override_dlg ()));
  connect (tv_overrides->selectionModel (),
           SIGNAL (selectionChanged (const QItemSelection&, const QItemSelection &)),
           this, SLOT (override_selectionchanged (const QItemSelection &,
                                                  const QItemSelection &)));

  slaves->setWindowTitle (tr ("Slaves"));
  QMainWindow::tabifyDockWidget (targets, slaves);
  slaves->setModel (control->getSlaveModel ());
  slaves->setFeatures (QDockWidget::DockWidgetClosable |
                       QDockWidget::DockWidgetMovable |
                       QDockWidget::DockWidgetFloatable);
  QTableView *tv_slaves = slaves->getTable ();
  tv_slaves->setObjectName ("tv_slaves");
  tv_slaves->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_slaves->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_slaves->setColumnWidth (0, 150);
  tv_slaves->setColumnWidth (1, 130);
  tv_slaves->setColumnWidth (2, 60);
  tv_slaves->setColumnWidth (3, 80);
  tv_slaves->verticalHeader ()->setVisible (false);
  tv_slaves->horizontalHeader ()->setHighlightSections (false);
  tv_slaves->horizontalHeader ()->setMinimumSectionSize (60);

  tv_slaves->installEventFilter (this);
  connect (tv_slaves, SIGNAL (doubleClicked (const QModelIndex&)), this,
           SLOT (details_slave_dlg ()));
  connect (tv_slaves->selectionModel (),
           SIGNAL (selectionChanged (const QItemSelection&,
                                     const QItemSelection& )),
           this,
           SLOT (slave_selectionchanged (const QItemSelection&,
                                         const QItemSelection&)));

  reportformats->setWindowTitle (tr ("Report Formats"));
  QMainWindow::tabifyDockWidget (targets, reportformats);
  reportformats->setModel (control->getReportFormatModel ());
  reportformats->setFeatures (QDockWidget::DockWidgetClosable |
                              QDockWidget::DockWidgetMovable |
                              QDockWidget::DockWidgetFloatable);
  QTableView *tv_reportformats = reportformats->getTable ();
  tv_reportformats->setObjectName ("tv_reportformats");
  tv_reportformats->setItemDelegateForColumn (5, text);
  tv_reportformats->setItemDelegateForColumn (4, date_time);
  tv_reportformats->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_reportformats->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_reportformats->setColumnWidth (0, 150);
  tv_reportformats->setColumnWidth (1, 80);
  tv_reportformats->setColumnWidth (2, 90);
  tv_reportformats->setColumnWidth (3, 90);
  tv_reportformats->setColumnWidth (4, 140);
  tv_reportformats->setColumnWidth (5, 80);
  tv_reportformats->verticalHeader ()->setVisible (false);
  tv_reportformats->horizontalHeader ()->setHighlightSections (false);
  tv_reportformats->horizontalHeader ()->setMinimumSectionSize (60);

  portlists->setWindowTitle (tr ("Port Lists"));
  QMainWindow::tabifyDockWidget (targets, portlists);
  portlists->setModel (control->getPortListModel ());
  portlists->setFeatures (QDockWidget::DockWidgetClosable |
                           QDockWidget::DockWidgetMovable |
                           QDockWidget::DockWidgetFloatable);
  QTableView *tv_portlists = portlists->getTable ();
  tv_portlists->setObjectName ("tv_portlists");
  tv_portlists->setColumnWidth (0, 150);
  tv_portlists->setColumnWidth (1, 150);
  tv_portlists->setColumnWidth (2, 80);
  tv_portlists->setColumnWidth (3, 90);
  tv_portlists->setColumnWidth (4, 90);
  tv_portlists->setColumnWidth (5, 90);
  tv_portlists->setItemDelegateForColumn (0, text);
  tv_portlists->setItemDelegateForColumn (1, text);
  tv_portlists->setItemDelegateForColumn (2, text);
  tv_portlists->setItemDelegateForColumn (3, text);
  tv_portlists->setItemDelegateForColumn (4, text);
  tv_portlists->setItemDelegateForColumn (5, text);
  tv_portlists->horizontalHeader ()->setResizeMode (QHeaderView::Fixed);
  tv_portlists->horizontalHeader ()->setResizeMode (0, QHeaderView::Stretch);
  tv_portlists->verticalHeader ()->setVisible (false);
  tv_portlists->horizontalHeader ()->setHighlightSections (false);
  tv_portlists->horizontalHeader ()->setMinimumSectionSize (75);

  tv_portlists->installEventFilter (this);
  connect (tv_portlists, SIGNAL (doubleClicked (const QModelIndex &)), this,
           SLOT (details_port_list_dlg ()));
  connect (tv_portlists->selectionModel (),
           SIGNAL (selectionChanged (const QItemSelection&, const QItemSelection &)),
           this, SLOT (portlist_selectionchanged (const QItemSelection &,
                                                const QItemSelection &)));

  QMainWindow::tabifyDockWidget (targets, performance);
  performance->setReportModel (control->getSystemReportModel ());
  performance->setGraphicsModel (control->getSystemDiagramModel ());

  tasks->raise ();
}


/**
 * @brief Creates and configures the update widget
 */
void
gsd_mw::createUpdateWidget ()
{
  dockWidgetContents_update = new QWidget ();

  //Label
  la_update = new QLabel (dockWidgetContents_update);
  la_update->setObjectName (QString::fromUtf8 ("la_update"));
  la_update->setGeometry (QRect (10, 0, 88, 30));
  la_update->setText (QApplication::translate ("MainWindow",
                      "Refresh Interval:", 0, QApplication::UnicodeUTF8));

  la_sec = new QLabel (dockWidgetContents_update);
  la_sec->setObjectName (QString::fromUtf8 ("la_sec"));
  la_sec->setGeometry (QRect (184, 2, 48, 26));
  la_sec->setText (QApplication::translate ("MainWindow", " sec", 0,
                   QApplication::UnicodeUTF8));

  la_prog_update = new QLabel ();
  la_prog_update->setObjectName (QString::fromUtf8 ("la_prog_update"));
  la_prog_update->setGeometry (QRect (500, 2, 58, 26));
  la_prog_update->setText (QApplication::translate ("MainWindow",
                           "Next Refresh: " ,0, QApplication::UnicodeUTF8));

  //Spinbox
  sb_update = new QSpinBox (dockWidgetContents_update);
  sb_update->setObjectName (QString::fromUtf8 ("sb_update"));
  sb_update->setGeometry (QRect (110, 2, 63, 26));
  sb_update->setMinimum (0);
  sb_update->setMaximum (60);
  sb_update->setSingleStep (10);
  sb_update->setValue (0);
  sb_update->setMaximumSize (40, 30);

  //Toolbutton
  tb_set_refresh = new QToolButton (dockWidgetContents_update);
  tb_set_refresh->setObjectName (QString::fromUtf8 ("tb_set_refresh"));
  tb_set_refresh->setEnabled (true);
  tb_set_refresh->setMinimumHeight (26);
  tb_set_refresh->setText (QApplication::translate ("MainWindow",
                          "Apply Interval", 0, QApplication::UnicodeUTF8));

  tb_stop_refresh = new QToolButton (dockWidgetContents_update);
  tb_stop_refresh->setObjectName (QString::fromUtf8 ("tb_stop_refresh"));
  tb_stop_refresh->setEnabled (false);
  tb_stop_refresh->setMinimumHeight (26);
  tb_stop_refresh->setText (QApplication::translate ("MainWindow",
                           "Stop Interval", 0, QApplication::UnicodeUTF8));

  //Progressbar
  pbar_update = new QProgressBar ();
  pbar_update->setObjectName (QString::fromUtf8 ("pbar_update"));
  pbar_update->setGeometry (QRect(560, 4, 118, 23));
  pbar_update->setValue (0);
  pbar_update->setAlignment (Qt::AlignCenter);
  pbar_update->setInvertedAppearance (false);
  pbar_update->setTextDirection (QProgressBar::TopToBottom);
  pbar_update->setMaximumHeight (14);
  pbar_update->setMinimumWidth (100);
  pbar_update->setMaximumWidth (140);
  pbar_update->setFormat (QApplication::translate ("MainWindow", "manual", 0,
                          QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
  pbar_update->setToolTip (QApplication::translate ("MainWindow",
                           "Refresh Progress", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP

  //Dockwidget
  dw_update = new QDockWidget (this);
  QSizePolicy sizePolicy (QSizePolicy::Expanding, QSizePolicy::Expanding);
  sizePolicy.setHorizontalStretch (0);
  sizePolicy.setVerticalStretch (0);
  dw_update->setSizePolicy (sizePolicy);
  sizePolicy.setHeightForWidth (dw_update->sizePolicy ().hasHeightForWidth ());
  dw_update->setObjectName (QString::fromUtf8 ("dw_update"));
  dw_update->setAutoFillBackground (true);
  dw_update->setFloating (false);
  dw_update->setFeatures (QDockWidget::DockWidgetFloatable|
                          QDockWidget::DockWidgetMovable|
                          QDockWidget::DockWidgetClosable);
  dw_update->setAllowedAreas (Qt::AllDockWidgetAreas);
  dw_update->setMinimumSize (QSize (40, 50));
  dw_update->setContextMenuPolicy (Qt::NoContextMenu);
  dw_update->setWindowTitle (QApplication::translate ("MainWindow", 
                            "Refresh Settings", 0, QApplication::UnicodeUTF8));
  dw_update->setWidget (dockWidgetContents_update);


  //Dockwidgetcontent
  dockWidgetContents_update->setObjectName (QString::fromUtf8
                                            ("dockWidgetContents_update"));

  //Dockwidgetlayout
  updateLayout = new QGridLayout (dockWidgetContents_update);
  updateLayout->setObjectName (QString::fromUtf8 ("updateLayout"));
  updateLayout->addWidget (la_update,0,0,1,2);
  updateLayout->addWidget (la_sec,0,3,1,1);
  updateLayout->addWidget (pbar_update,3,2,1,2);
  updateLayout->addWidget (sb_update,0,2,1,1);
  updateLayout->addWidget (tb_set_refresh, 1,0,1,2);
  updateLayout->addWidget (la_prog_update, 3,0,1,2);
  updateLayout->addWidget (tb_stop_refresh, 1,2,1,2);
  updateLayout->setRowMinimumHeight (0, 25);
  updateLayout->setRowMinimumHeight (1, 25);
  updateLayout->setRowMinimumHeight (3, 25);
  updateLayout->setRowStretch (0,0);
  updateLayout->setRowStretch (1,0);
  updateLayout->setRowStretch (2,1);
  updateLayout->setRowStretch (3,0);
  updateLayout->setColumnStretch (0,0);
  updateLayout->setColumnStretch (1,0);
  updateLayout->setColumnStretch (2,0);
  updateLayout->setColumnStretch (3,0);
  updateLayout->setColumnStretch (4,1);

  this->addDockWidget (static_cast<Qt::DockWidgetArea> (1), dw_update);
  dw_update->hide ();
}


/**
 * @brief Creates and configures the statusbar
 */
void
gsd_mw::createStatusBar ()
{
  la_stat_update = new QLabel ();
  la_stat_update->setObjectName (QString::fromUtf8 ("la_stat_update"));
  la_stat_update->setGeometry (QRect(500, 2, 58, 26));
  la_stat_update->setText (QApplication::translate ("MainWindow",
                           "Refresh Interval: <b>manual</b>" , 0,
                           QApplication::UnicodeUTF8));

  statusBar ()->addPermanentWidget (la_stat_update);
}


/**
 * @brief Configures signal/slot connections
 */
void
gsd_mw::createConnections ()
{
  connect (actionQuit,
           SIGNAL (triggered (bool)),
           this,
           SLOT (close ()));
  actionQuit->setShortcut (tr ("Ctrl+q"));
  connect (actionLogin,
           SIGNAL (triggered (bool)),
           this,
           SLOT (login_dlg ()));
  connect (actionLogout,
           SIGNAL (triggered (bool)),
           this,
           SLOT (logout ()));
  connect (actionAbout,
           SIGNAL (triggered (bool)),
           this,
           SLOT (about_dlg ()));
  connect (actionStart_GSA,
           SIGNAL (triggered (bool)),
           this,
           SLOT (start_gsa ()));
  connect (actionRefresh,
           SIGNAL (triggered (bool)),
           this,
           SLOT (update ()));
  actionRefresh->setShortcut (tr ("F5"));
  connect (progressUpdateTimer,
           SIGNAL (timeout()),
           this,
           SLOT (progress_timeout ()));
  connect (actionRun,
           SIGNAL (triggered (bool)),
           this,
           SLOT (task_start ()));
  connect (actionStop,
           SIGNAL (triggered (bool)),
           this,
           SLOT (task_stop ()));
  connect (actionPause,
           SIGNAL (triggered (bool)),
           this,
           SLOT (task_pause ()));
  connect (actionResume,
           SIGNAL (triggered (bool)),
           this,
           SLOT (task_resume ()));
  connect (actionDelete,
           SIGNAL (triggered (bool)),
           this,
           SLOT (task_delete ()));
  connect (tb_set_refresh,
           SIGNAL (released ()),
           this,
           SLOT (update_start ()));
  connect (tb_stop_refresh,
           SIGNAL (released ()),
           this,
           SLOT (update_stop ()));
  connect (actionSchedules,
           SIGNAL (triggered (bool)),
           this,
           SLOT (schedule_dw()));
  connect (actionTargets,
           SIGNAL (triggered (bool)),
           this,
           SLOT (target_dw ()));
  connect (actionScan_Configs,
           SIGNAL (triggered (bool)),
           this,
           SLOT (config_dw ()));
  connect (actionEscalators,
           SIGNAL (triggered (bool)),
           this,
           SLOT (escalator_dw ()));
  connect (actionCredentials,
           SIGNAL (triggered (bool)),
           this,
           SLOT (credential_dw ()));
  connect (actionAgents,
           SIGNAL (triggered (bool)),
           this,
           SLOT (agent_dw ()));
  connect (actionNotes,
           SIGNAL (triggered (bool)),
           this,
           SLOT (note_dw ()));
  connect (actionOverrides,
           SIGNAL (triggered (bool)),
           this,
           SLOT (override_dw ()));
  connect (actionSlaves,
           SIGNAL (triggered (bool)),
           this,
           SLOT (slave_dw ()));
  connect (actionPerformance,
           SIGNAL (triggered(bool)),
           this,
           SLOT (performance_dw ()));
  connect (actionPortList,
           SIGNAL (triggered (bool)),
           this,
           SLOT (portlists_dw ()));
  connect (actionRefresh_settings,
           SIGNAL (triggered(bool)),
           this,
           SLOT (update_dw ()));
  connect (actionNewTask,
           SIGNAL (triggered (bool)),
           this,
           SLOT (new_task_dlg ()));
  connect (actionModify_Task,
           SIGNAL (triggered (bool)),
           this,
           SLOT (modify_task_dlg ()));
  connect (actionNew_Config,
           SIGNAL (triggered (bool)),
           this,
           SLOT (new_config_dlg ()));
  connect (actionDelete_Config,
           SIGNAL (triggered (bool)),
           this,
           SLOT (delete_config ()));
  connect (actionNew_Target,
           SIGNAL (triggered (bool)),
           this,
           SLOT (new_target_dlg ()));
  connect (actionDelete_Target,
           SIGNAL (triggered (bool)),
           this,
           SLOT (delete_target ()));
  connect (actionNew_Schedule,
           SIGNAL (triggered (bool)),
           this,
           SLOT (new_schedule_dlg ()));
  connect (actionDelete_Schedule,
           SIGNAL (triggered (bool)),
           this,
           SLOT (delete_schedule ()));
  connect (actionNew_Escalator,
           SIGNAL (triggered (bool)),
           this,
           SLOT (new_escalator_dlg ()));
  connect (actionDelete_Escalator,
           SIGNAL (triggered (bool)),
           this,
           SLOT (delete_escalator ()));
  connect (actionNew_Credential,
           SIGNAL (triggered (bool)),
           this,
           SLOT (new_credential_dlg ()));
  connect (actionNew_PortList,
           SIGNAL (triggered (bool)),
           this,
           SLOT (new_port_list_dlg ()));
  connect (actionDelete_Credential,
           SIGNAL (triggered (bool)),
           this,
           SLOT (delete_credential ()));
  connect (actionNew_Agent,
           SIGNAL (triggered (bool)),
           this,
           SLOT (new_agent_dlg ()));
  connect (actionDelete_Agent,
           SIGNAL (triggered (bool)),
           this,
           SLOT (delete_agent ()));
  connect (actionDelete_Note,
           SIGNAL (triggered (bool)),
           this,
           SLOT (delete_note ()));
  connect (actionDelete_Override,
           SIGNAL (triggered (bool)),
           this,
           SLOT (delete_override ()));
  connect (actionNew_Slave,
           SIGNAL (triggered (bool)),
           this,
           SLOT (new_slave_dlg ()));
  connect (actionDelete_Slave,
           SIGNAL (triggered (bool)),
           this,
           SLOT (delete_slave ()));
  connect (actionDelete_PortList,
           SIGNAL (triggered (bool)),
           this,
           SLOT (delete_port_list ()));
  connect (actionDetails_Target,
           SIGNAL (triggered (bool)),
           this,
           SLOT (details_target_dlg ()));
  connect (actionDetails_PortList,
           SIGNAL (triggered (bool)),
           this,
           SLOT (details_port_list_dlg ()));
  connect (actionDetails_Schedule,
           SIGNAL (triggered (bool)),
           this,
           SLOT (details_schedule_dlg ()));
  connect (actionDetails_Credential,
           SIGNAL (triggered (bool)),
           this,
           SLOT (details_credential_dlg ()));
  connect (actionDetails_Escalator,
           SIGNAL (triggered (bool)),
           this,
           SLOT (details_escalator_dlg ()));
  connect (actionDetails_Note,
           SIGNAL (triggered (bool)),
           this,
           SLOT (details_note_dlg ()));
  connect (actionEdit_Note,
           SIGNAL (triggered (bool)),
           this,
           SLOT (modify_note_dlg ()));
  connect (actionDetails_Override,
           SIGNAL (triggered (bool)),
           this,
           SLOT (details_override_dlg ()));
  connect (actionEdit_Override,
           SIGNAL (triggered (bool)),
           this,
           SLOT (modify_override_dlg ()));
  connect (actionDetails_Slave,
           SIGNAL (triggered (bool)),
           this,
           SLOT (details_slave_dlg ()));
  connect (actionDetails_Task,
           SIGNAL (triggered (bool)),
           this,
           SLOT (details_task_dlg ()));
  connect (actionDetails_Config,
           SIGNAL (triggered (bool)),
           this,
           SLOT (details_config_dlg ()));
  connect (actionExport_Config,
           SIGNAL (triggered (bool)),
           this,
           SLOT (config_download ()));
  connect (actionImport_Config,
           SIGNAL (triggered (bool)),
           this,
           SLOT (config_upload ()));
  connect (this,
           SIGNAL (sig_upload (int, QMap<QString, QString>)),
           control,
           SLOT (create (int, QMap<QString, QString>)));
  connect (actionClear_Dock_Settings,
           SIGNAL (triggered (bool)),
           this,
           SLOT (clear_dock_widget_settings ()));
  connect (actionSpanish,
           SIGNAL (triggered (bool)),
           this,
           SLOT (setLangSpanish ()));
  connect (actionIndonesian,
           SIGNAL (triggered (bool)),
           this,
           SLOT (setLangIndonesian ()));
  connect (actionFrench,
           SIGNAL (triggered (bool)),
           this,
           SLOT (setLangFrench ()));
  connect (actionGerman,
           SIGNAL (triggered (bool)),
           this,
           SLOT (setLangGerman ()));
  connect (actionEnglish,
           SIGNAL (triggered (bool)),
           this,
           SLOT (setLangEnglish ()));
  connect (actionSystem,
           SIGNAL (triggered (bool)),
           this,
           SLOT (setLangDefault ()));
  connect (actionFrench,
           SIGNAL (triggered (bool)),
           this,
           SLOT (languageChanged ()));
  connect (actionGerman,
           SIGNAL (triggered (bool)),
           this,
           SLOT (languageChanged ()));
  connect (actionEnglish,
           SIGNAL (triggered (bool)),
           this,
           SLOT (languageChanged ()));
  connect (actionSystem,
           SIGNAL (triggered (bool)),
           this,
           SLOT (languageChanged ()));
  connect (control,
           SIGNAL (sig_port_list_finished ()),
           this,
           SLOT (activate_port_list_actions ()));
}


/**
 * @brief Performs actions on logout.
 */
void
gsd_mw::logout ()
{
  update_stop ();
  setLoggedOut ();
  closeDockWidgets ();
  emit sig_logout ();
}


/**
 * @brief Disables Actions and buttons and version dependant widgets
 */
void
gsd_mw::setLoggedOut ()
{
  //modify UI
  statuslabel->setText ("");
  actionDetails_Task->setDisabled (true);
  actionLogin->setDisabled (false);
  actionRun->setDisabled (true);
  actionStop->setDisabled (true);
  actionDelete->setDisabled (true);
  actionModify_Task->setDisabled (true);
  actionRefresh->setDisabled (true);
  actionLogout->setDisabled (true);
  actionNewTask->setDisabled (true);
  tb_set_refresh->setDisabled (true);
  actionNew_Config->setDisabled (true);
  actionDelete_Config->setDisabled (true);
  actionDetails_Config->setDisabled (true);
  actionExport_Config->setDisabled (true);
  actionNew_Target->setDisabled (true);
  actionDelete_Target->setDisabled (true);
  actionDetails_Target->setDisabled (true);
  actionNew_Escalator->setDisabled (true);
  actionDelete_Escalator->setDisabled (true);
  actionDetails_Escalator->setDisabled (true);
  actionTest_Escalator->setDisabled (true);
  actionNew_Credential->setDisabled (true);
  actionDelete_Credential->setDisabled (true);
  actionDetails_Credential->setDisabled (true);
  actionNew_Agent->setDisabled (true);
  actionDelete_Agent->setDisabled (true);
  actionDownload_Installer->setDisabled (true);
  actionNew_Schedule->setDisabled (true);
  actionDelete_Schedule->setDisabled (true);
  actionDetails_Schedule->setDisabled (true);
  actionDelete_Note->setDisabled (true);
  actionDetails_Note->setDisabled (true);
  actionEdit_Note->setDisabled (true);
  actionDelete_Override->setDisabled (true);
  actionDetails_Override->setDisabled (true);
  actionEdit_Override->setDisabled (true);
  actionNew_Slave->setDisabled (true);
  actionDelete_Slave->setDisabled (true);
  actionDetails_Slave->setDisabled (true);
  actionNew_PortList->setDisabled (true);
  actionDelete_PortList->setDisabled (true);
  actionDetails_PortList->setDisabled (true);
  clearDashboard ();
}


/**
 * @brief Remove all dock widgets except of the main entity widgets.
 */
void
gsd_mw::closeDockWidgets ()
{
  foreach (dock_details *widget, details_widgets)
    widget->close ();
}

/**
 * @brief SLOT called if authentication with the manager is successful.
 */
void
gsd_mw::authentication_successful ()
{
  omp_credentials *crd = control->getLoginData ();
  QString statustext (tr ("Logged in as: <b>%1</b> at <b>%2:%3</b>")
                          .arg (crd->getUserName ())
                          .arg (crd->getServerAddress ())
                          .arg (crd->getServerPort ()));

  if (statuslabel->text ().compare ("") == 0)
    {
      statusbar->addWidget (statuslabel);
    }

  statuslabel->setText (statustext);

  connect (tasks, SIGNAL (visibilityChanged (bool)), this,
           SLOT (focus_tasks (bool)));
  connect (schedules, SIGNAL (visibilityChanged (bool)), this,
           SLOT (focus_schedules (bool)));
  connect (targets, SIGNAL (visibilityChanged (bool)), this,
           SLOT (focus_targets (bool)));
  connect (configs, SIGNAL (visibilityChanged (bool)), this,
           SLOT (focus_configs (bool)));
  connect (escalators, SIGNAL (visibilityChanged (bool)), this,
           SLOT (focus_escalators (bool)));
  connect (credentials, SIGNAL (visibilityChanged (bool)), this,
           SLOT (focus_credentials (bool)));
  connect (agents, SIGNAL (visibilityChanged (bool)), this,
           SLOT (focus_agents (bool)));
  connect (notes, SIGNAL (visibilityChanged (bool)), this,
           SLOT (focus_notes (bool)));
  connect (overrides, SIGNAL (visibilityChanged (bool)), this,
           SLOT (focus_overrides (bool)));
  connect (slaves, SIGNAL (visibilityChanged (bool)), this,
           SLOT (focus_slaves (bool)));
  connect (performance, SIGNAL (visibilityChanged (bool)), this,
           SLOT (focus_performance (bool)));

  connect (performance, SIGNAL (sig_request_system_reports (QString, long)),
           control, SLOT (request_system_reports (QString, long)));

  actionLogin->setDisabled (true);
  actionRefresh->setDisabled (false);
  actionLogout->setDisabled (false);
  actionNewTask->setDisabled (false);
  tb_set_refresh->setDisabled (false);
  actionNew_Config->setDisabled (false);
  actionNew_Target->setDisabled (false);
  actionNew_Escalator->setDisabled (false);
  actionNew_Credential->setDisabled (false);
  actionNew_Agent->setDisabled (false);
  actionNew_Schedule->setDisabled (false);
  actionNew_Slave->setDisabled (false);
}


/**
 * @brief Requests the login dialog.
 */
void
gsd_mw::login_dlg ()
{
  emit sig_login ();
}


/**
 * @brief Requests the About dialog.
 */
void
gsd_mw::about_dlg ()
{
  emit sig_about ();
}


/**
 * @brief Requests the start-gsa dialog.
 */
void
gsd_mw::start_gsa ()
{
  emit sig_start_gsa ();
}


/**
 * @brief
 *
 * Emits sig_req_tasks () to start an update.
 */
void
gsd_mw::update ()
{
  emit sig_req_tasks (0);
}


/**
 * @brief Starts the update interval.
 *
 * Emits sig_req_tasks () to start an update.
 */
void
gsd_mw::update_start ()
{
  updateInterval = sb_update->value ();
  if (updateInterval == 0)
    {
      la_stat_update->setText (QApplication::translate ("MainWindow",
                               "Refresh Interval: <b>manual</b>" , 0,
                               QApplication::UnicodeUTF8));
      pbar_update->setFormat (QApplication::translate ("MainWindow", "manual",
                              0, QApplication::UnicodeUTF8));
      tb_stop_refresh->setDisabled (true);
      emit sig_stop_update ();
    }
  else
    {
      tb_stop_refresh->setEnabled (true);
      if (updateInterval == 1)
        la_stat_update->setText (QApplication::translate ("MainWindow",
                                 "Refresh Interval: <b>%1 second</b>" , 0,
                                 QApplication::UnicodeUTF8).arg
                                                            (updateInterval));
      else
        la_stat_update->setText (QApplication::translate ("MainWindow",
                                 "Refresh Interval: <b>%1 seconds</b>" , 0,
                                 QApplication::UnicodeUTF8).arg
                                                            (updateInterval));

      tb_stop_refresh->setDisabled (false);
      emit sig_req_tasks (updateInterval*1000);
      pbar_update->setMaximum (updateInterval);
      progressUpdateTimer->start (1000);
    }
}


/**
 * @brief SLOT that manipulates UI if task update is stopped
 */
void
gsd_mw::update_stop ()
{
  progressUpdateTimer->stop ();
  updateInterval = 0;
  pbar_update->setValue (0);
  la_stat_update->setText (QApplication::translate ("MainWindow",
                           "Refresh Interval: <b>manual</b>" , 0,
                           QApplication::UnicodeUTF8));
  pbar_update->setFormat (QApplication::translate ("MainWindow", "manual",
                          0, QApplication::UnicodeUTF8));
  tb_stop_refresh->setDisabled (true);

  emit sig_stop_update ();
}


/**
 * @brief SLOT to start runnig a task
 */
void
gsd_mw::task_start ()
{
  QModelIndexList selected = tasks->getTable ()->selectionModel ()
                                               ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_start_task (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No Task selected!"));
}


/**
 * @brief SLOT to stop a running task
 */
void
gsd_mw::task_stop ()
{
  QModelIndexList selected = tasks->getTable ()->selectionModel ()
                                               ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_stop_task (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No Task selected!"));
}


/**
 * @brief SLOT to pause a running task
 */
void
gsd_mw::task_pause ()
{
  QModelIndexList selected = tasks->getTable ()->selectionModel ()
                                               ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_pause_task (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No Task selected!"));
}


/**
 * @brief SLOT to resume a task
 */
void
gsd_mw::task_resume ()
{
  QModelIndexList selected = tasks->getTable ()->selectionModel ()
                                               ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_resume_task (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No Task selected!"));
}


/**
 * @brief SLOT that shows the modify note dialog.
 */
void
gsd_mw::modify_note_dlg ()
{
  QModelIndexList selected = notes->getTable ()->selectionModel ()
                                               ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_modify_note (selected.at (0).row ());
}


/**
 * @brief SLOT that shows the modify override dialog.
 */
void
gsd_mw::modify_override_dlg ()
{
  QModelIndexList selected = overrides->getTable ()->selectionModel ()
                                                   ->selectedRows (0);
  if (selected.at (0).isValid ())
  emit sig_modify_override (selected.at (0).row ());
}


/**
 * @brief SLOT that shows the modify task dialog.
 */
void
gsd_mw::modify_task_dlg ()
{
  QModelIndexList selected = tasks->getTable ()->selectionModel ()
                                               ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_modify_task (selected.at (0).row ());
}


/**
 * @brief SLOT to delete currently selected scan config.
 *
 * Emits sig_delete_config (...) to delete the config
 */
void
gsd_mw::delete_config ()
{
  QDomNode ent;
  QModelIndexList selected = configs->getTable ()->selectionModel ()
                                                   ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_delete_config (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No scan config selected!"));
}


/**
 * @brief SLOT to delete currently selected target.
 *
 * Emits sig_delete_target (...) to delete the config
 */
void
gsd_mw::delete_target ()
{
  QModelIndexList selected = targets->getTable ()->selectionModel ()
                                                   ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_delete_target (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No target selected!"));
}


/**
 * @brief SLOT to delete currently selected schedule.
 *
 * Emits sig_delete_schedule (...) to delete the schedule.
 */
void
gsd_mw::delete_schedule ()
{
  QModelIndexList selected = schedules->getTable ()->selectionModel ()
                                                     ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_delete_schedule (selected.at (0).row ());
  else
      QMessageBox::information (NULL,tr ("Status Error"),
                                tr ("No schedule selected!"));
}


/**
 * @brief SLOT to delete currently selected escalator.
 *
 * Emits sig_delete_escalator (...) to delete the escalator.
 */
void
gsd_mw::delete_escalator ()
{
  QModelIndexList selected = escalators->getTable ()->selectionModel ()
                                                      ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_delete_escalator (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No escalator selected!"));
}


/**
 * @brief SLOT to delete currently selected credential.
 *
 * Emits sig_delete_credential (...) to delete the credential.
 */
void
gsd_mw::delete_credential ()
{
  QModelIndexList selected = credentials->getTable ()->selectionModel ()
                                                       ->selectedRows (0);
  if (selected.size () != 0)
    emit sig_delete_credential (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No credential selected!"));
}


/**
 * @brief SLOT to delete currently selected agent.
 *
 * Emits sig_delete_agent (...) to delete the agent.
 */
void
gsd_mw ::delete_agent ()
{
  QModelIndexList selected = agents->getTable ()->selectionModel ()
                                                  ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_delete_agent (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No agent selected!"));
}


/**
 * @brief SLOT to delete currently selected note.
 *
 * Emits sig_delete_agent (...) to delete the note.
 */
void
gsd_mw ::delete_note ()
{
  QModelIndexList selected = notes->getTable ()->selectionModel ()
                                                ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_delete_note (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                                tr ("No note selected!"));
}


/**
 * @brief SLOT to delete currently selected override.
 *
 * Emits sig_delete_override (...) to delete the override.
 */
void
gsd_mw::delete_override ()
{
  QModelIndexList selected = overrides->getTable ()->selectionModel ()
                                                     ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_delete_override (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No override selected!"));
}


/**
 * @brief SLOT to delete currently selected slave.
 *
 * Emits sig_delete_slave (...) to delete the slave.
 */
void
gsd_mw::delete_slave ()
{
  QModelIndexList selected = slaves->getTable ()->selectionModel ()
                                                  ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_delete_slave (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No slave selected!"));
}

/**
 * @brief SLOT to delete currently selected port list.
 *
 * Emits sig_delete_port_list (...) to delete the config
 */
void
gsd_mw::delete_port_list ()
{
  QDomNode ent;
  QModelIndexList selected = portlists->getTable ()->selectionModel ()
                                                   ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_delete_port_list (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No port list selected!"));
}

/**
 * @brief Requests the new task dialog.
 */
void
gsd_mw::new_task_dlg ()
{
  emit sig_create_task ();
}


/**
 * @brief Requests the new config dialog.
 */
void
gsd_mw::new_config_dlg ()
{
  emit sig_create_config ();
}


/**
 * @brief Requests the new target dialog.
 */
void
gsd_mw::new_target_dlg ()
{
  emit sig_create_target ();
}


/**
 * @brief Requests the new schedule dialog.
 */
void
gsd_mw::new_schedule_dlg ()
{
  emit sig_create_schedule ();
}


/**
 * @brief Requests the new escalator dialog.
 */
void
gsd_mw::new_escalator_dlg ()
{
  emit sig_create_escalator ();
}


/**
 * @brief Requests the new credential dialog.
 */
void
gsd_mw::new_credential_dlg ()
{
  emit sig_create_credential ();
}

/**
 * @brief Requests the new port_list dialog.
 */
void
gsd_mw::new_port_list_dlg ()
{
  emit sig_create_port_list ();
}

/**
 * @brief Requests the new agent dialog.
 */
void
gsd_mw::new_agent_dlg ()
{
  emit sig_create_agent ();
}


/**
 * @brief Requests the new slave dialog.
 */
void
gsd_mw::new_slave_dlg ()
{
  emit sig_create_slave ();
}


/**
 * @brief Requests config details.
 */
void
gsd_mw::details_config_dlg ()
{
  QModelIndexList selected = configs->getTable ()->selectionModel ()
                                                   ->selectedRows (0);
  if (selected.at (0).isValid ())
    {
      emit sig_details_config (selected.at (0).row ());
      QDomElement config = control->getConfigModel ()
                                        ->getEntity (selected.at (0).row ());
      QString selected_id = control->getConfigModel ()
                                     ->getAttr (config, "config id");
      if (details_widgets.contains (selected_id))
        {
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          details_widgets[selected_id]->restoreGeometry (settings.value ("geometrie").toByteArray ());
          settings.endGroup ();
          int area = this->dockWidgetArea (details_widgets.value (selected_id));
          QMapIterator <QString, dock_details*> i (details_widgets);
          while (i.hasNext ())
            {
              i.next ();
              int usedArea = this->dockWidgetArea (i.value ());
              if (usedArea == area &&
                  i.key ().compare (selected_id) != 0 &&
                  i.value ()->isVisible ())
                {
                  details_widgets.value (selected_id)->show ();
                  details_widgets.value (selected_id)->update ();
                  this->tabifyDockWidget (i.value (),
                                          details_widgets.value (selected_id));
                  details_widgets.value (selected_id)->raise ();
                  return;
                }
            }
          details_widgets.value (selected_id)->show ();
          details_widgets.value (selected_id)->update ();
          details_widgets.value (selected_id)->raise ();
        }
      else
        {
          dock_details_config *details =
            new dock_details_config (this->control);
          connect (control,
                   SIGNAL (sig_config_details_finished (QString)),
                   details,
                   SLOT (update (QString)));
          connect (details,
                   SIGNAL (sig_details_family (QString, QString)),
                   this,
                   SLOT (details_family_dlg (QString, QString)));
          connect (details,
                   SIGNAL (sig_details_nvt (QString, QString)),
                   this,
                   SLOT (details_nvt_dlg (QString, QString)));
          connect (details,
                   SIGNAL (sig_modify (int,
                                       QString,
                                       model_omp_entity*,
                                       QMap<QString, QString>)),
                   this->control,
                   SLOT (modify (int,
                                 QString,
                                 model_omp_entity*,
                                 QMap<QString, QString>)));

          details->setId (selected_id);
          details->load ();
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          if (!this->restoreDockWidget (details))
            {
              int area = this->dockWidgetArea (details_widgets.value (selected_id));
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      details->show ();
                      this->tabifyDockWidget (i.value (), details);
                      details->raise ();
                      details_widgets.insert (selected_id, details);
                      return;
                    }
                 }
              details->raise ();
              this->addDockWidget (Qt::BottomDockWidgetArea, details);
            }

          settings.endGroup ();
          if (this->dockWidgetArea (details) == Qt::BottomDockWidgetArea)
            {
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      this->tabifyDockWidget (i.value (), details);
                    }
                }
            }
          details->show ();
          details->raise ();
          details_widgets.insert (selected_id, details);
        }
    }
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No Scan configuration selected!"));
}


/**
 * @brief Requests family details.
 */
void
gsd_mw::details_family_dlg (QString config_id, QString name)
{
  if (config_id.compare ("") == 0 || name.compare ("") == 0)
    return;

  emit sig_details_family (config_id, name);
  emit sig_nvts (name);
  if (details_widgets.contains (name+config_id))
    {
      QSettings settings ("Greenbone", "GSD");
      settings.beginGroup ("DockWidgets");
      details_widgets[name+config_id]->restoreGeometry (settings.value ("geometrie").toByteArray ());
      settings.endGroup ();
      details_widgets.value (name+config_id)->show ();
      details_widgets.value (name+config_id)->update ();
      details_widgets.value (name+config_id)->raise ();
    }
  else
    {
      dock_details_family *details =
        new dock_details_family (this->control);
      connect (control,
               SIGNAL (sig_family_details_finished (QString, QString)),
               details,
               SLOT (update (QString, QString)));
      connect (details,
               SIGNAL (sig_details_nvt (QString, QString)),
               this,
               SLOT (details_nvt_dlg (QString, QString)));
      connect (details,
               SIGNAL (sig_modify (int,
                                   QString,
                                   model_omp_entity*,
                                   QMap<QString, QString>)),
               this->control,
               SLOT (modify (int,
                             QString,
                             model_omp_entity*,
                             QMap<QString, QString>)));

      details->setId (config_id);
      details->setName (name);
      int i = 0;
      while (i < this->control->getConfigModel ()->rowCount ())
        {
          QDomElement e = this->control->getConfigModel ()->getEntity (i);
          if (this->control
                    ->getConfigModel ()
                      ->getAttr (e, "config id").compare (config_id) == 0)
            {
              if (this->control
                        ->getConfigModel ()
                          ->getValue (e, "in_use").compare ("1") == 0)
                details->setEditable (false);
              else
                details->setEditable (true);
              break;
            }
          i++;
        }
      details->load ();
      QSettings settings ("Greenbone", "GSD");
      settings.beginGroup ("DockWidgets");
      if (!this->restoreDockWidget (details))
        {
          int area = this->dockWidgetArea (details_widgets.value (name+config_id));
          QMapIterator <QString, dock_details*> i (details_widgets);
          while (i.hasNext ())
            {
              i.next ();
              int usedArea = this->dockWidgetArea (i.value ());
              if (usedArea == Qt::BottomDockWidgetArea &&
                  i.key ().compare (name+config_id) != 0 &&
                  i.value ()->isVisible ())
                {
                  details->show ();
                  this->tabifyDockWidget (i.value (), details);
                  details->raise ();
                  details_widgets.insert (name+config_id, details);
                  return;
                }
             }
           this->addDockWidget (Qt::BottomDockWidgetArea, details);
        }
      settings.endGroup ();
      if (this->dockWidgetArea (details) == Qt::BottomDockWidgetArea)
        {
          QMapIterator <QString, dock_details*> i (details_widgets);
          while (i.hasNext ())
            {
              i.next ();
              int usedArea = this->dockWidgetArea (i.value ());
              if (usedArea == Qt::BottomDockWidgetArea &&
                  i.key ().compare (name+config_id) != 0 &&
                  i.value ()->isVisible ())
                {
                  this->tabifyDockWidget (i.value (), details);
                }
            }
        }


      details->show ();
      details->raise ();
      details_widgets.insert (name+config_id, details);
    }
}


/**
 * @brief Requests nvt details.
 */
void
gsd_mw::details_nvt_dlg (QString config_id, QString oid)
{
  if (config_id.compare ("") == 0 || oid.compare ("") == 0)
    return;

  emit sig_details_nvt (config_id, oid);
  if (details_widgets.contains (oid+config_id))
    {
      QSettings settings ("Greenbone", "GSD");
      settings.beginGroup ("DockWidgets");
      details_widgets[oid+config_id]->restoreGeometry (settings.value ("geometrie").toByteArray ());
      settings.endGroup ();
      details_widgets.value (oid+config_id)->show ();
      details_widgets.value (oid+config_id)->update ();
      details_widgets.value (oid+config_id)->raise ();
    }
  else
    {
      dock_details_nvt *details =
        new dock_details_nvt (this->control);
      connect (control,
               SIGNAL (sig_nvt_details_finished (QString, QString)),
               details,
               SLOT (update (QString, QString)));
      connect (details,
               SIGNAL (sig_modify (int,
                                   QString,
                                   model_omp_entity*,
                                   QMap<QString, QString>)),
               this->control,
               SLOT (modify (int,
                             QString,
                             model_omp_entity*,
                             QMap<QString, QString>)));

      details->setId (config_id);
      details->setOid (oid);
      int i = 0;
      while (i < this->control->getConfigModel ()->rowCount ())
        {
          QDomElement e = this->control->getConfigModel ()->getEntity (i);
          if (this->control
                    ->getConfigModel ()
                      ->getAttr (e, "config id").compare (config_id) == 0)
            {
              if (this->control
                        ->getConfigModel ()
                          ->getValue (e, "in_use").compare ("0") == 0)
                details->setEditable (true);
              else
                details->setEditable (false);
              break;
            }
          i++;
        }
      details->load ();
      QSettings settings ("Greenbone", "GSD");
      settings.beginGroup ("DockWidgets");
      if (!this->restoreDockWidget (details))
        {
          int area = this->dockWidgetArea (details_widgets.value (oid+config_id));
          QMapIterator <QString, dock_details*> i (details_widgets);
          while (i.hasNext ())
            {
              i.next ();
              int usedArea = this->dockWidgetArea (i.value ());
              if (usedArea == Qt::BottomDockWidgetArea &&
                  i.key ().compare (oid+config_id) != 0 &&
                  i.value ()->isVisible ())
                {
                  details->show ();
                  this->tabifyDockWidget (i.value (), details);
                  details->raise ();
                  details_widgets.insert (oid+config_id, details);
                  return;
                }
             }
           this->addDockWidget (Qt::BottomDockWidgetArea, details);
        }
      settings.endGroup ();
      if (this->dockWidgetArea (details) == Qt::BottomDockWidgetArea)
        {
          QMapIterator <QString, dock_details*> i (details_widgets);
          while (i.hasNext ())
            {
              i.next ();
              int usedArea = this->dockWidgetArea (i.value ());
              if (usedArea == Qt::BottomDockWidgetArea &&
                  i.key ().compare (oid+config_id) != 0 &&
                  i.value ()->isVisible ())
                {
                  this->tabifyDockWidget (i.value (), details);
                }
            }
        }

      details->show ();
      details->raise ();
      details_widgets.insert (oid+config_id, details);

    }
}


/**
 * @brief Requests credential details.
 */
void
gsd_mw::details_credential_dlg ()
{
  QModelIndexList selected = credentials->getTable ()->selectionModel ()
                                                       ->selectedRows (0);
  if (selected.at (0).isValid ())
    {
      QDomElement credential = control->getCredentialModel ()
                                        ->getEntity (selected.at (0).row ());
      QString selected_id = control->getCredentialModel ()
                                     ->getAttr (credential, "credential id");
      if (details_widgets.contains (selected_id))
        {
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          details_widgets[selected_id]->restoreGeometry (settings.value ("geometrie").toByteArray ());
          settings.endGroup ();
          details_widgets.value (selected_id)->show ();
          details_widgets.value (selected_id)->update ();
          details_widgets.value (selected_id)->raise ();
        }
      else
        {
          dock_details_credential *details =
            new dock_details_credential (this->control);
          connect (details,
                   SIGNAL (sig_modify (int,
                                       QString,
                                       model_omp_entity*,
                                       QMap<QString, QString>)),
                   this->control,
                   SLOT (modify (int,
                                 QString,
                                 model_omp_entity*,
                                 QMap<QString, QString>)));
          connect (control,
                   SIGNAL (sig_lsc_credential_finished ()),
                   details,
                   SLOT (update ()));
         details->setId (selected_id);
          details->load ();
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          if (!this->restoreDockWidget (details))
            {
              int area = this->dockWidgetArea (details_widgets.value (selected_id));
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      details->show ();
                      details->update ();
                      this->tabifyDockWidget (i.value (), details);
                      details->raise ();
                      details_widgets.insert (selected_id, details);
                      return;
                    }
                 }
               this->addDockWidget (Qt::BottomDockWidgetArea, details);
            }

          settings.endGroup ();
          if (this->dockWidgetArea (details) == Qt::BottomDockWidgetArea)
            {
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      this->tabifyDockWidget (i.value (), details);
                    }
                }
            }
          details->show ();
          details->update ();
          details->raise ();
          details_widgets.insert (selected_id, details);
        }
    }
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No credential selected!"));
}


/**
 * @brief Requests escalator details.
 */
void
gsd_mw::details_escalator_dlg ()
{
  QModelIndexList selected = escalators->getTable ()->selectionModel ()
                                                       ->selectedRows (0);
  if (selected.at (0).isValid ())
    {
      QDomElement escalator = control->getEscalatorModel ()
                                        ->getEntity (selected.at (0).row ());
      QString selected_id = control->getEscalatorModel ()
                                     ->getAttr (escalator, "escalator id");
      if (details_widgets.contains (selected_id))
        {
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          details_widgets[selected_id]->restoreGeometry (settings.value ("geometrie").toByteArray ());
          settings.endGroup ();
          details_widgets.value (selected_id)->show ();
          details_widgets.value (selected_id)->update ();
          details_widgets.value (selected_id)->raise ();
        }
      else
        {
          dock_details_escalator *details =
            new dock_details_escalator (this->control);
          details->setId (selected_id);
          details->load ();
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          if (!this->restoreDockWidget (details))
            {
              int area = this->dockWidgetArea (details_widgets.value (selected_id));
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      details->show ();
                      details->update ();
                      this->tabifyDockWidget (i.value (), details);
                      details->raise ();
                      details_widgets.insert (selected_id, details);
                      return;
                    }
                 }
               this->addDockWidget (Qt::BottomDockWidgetArea, details);
            }

          settings.endGroup ();
          if (this->dockWidgetArea (details) == Qt::BottomDockWidgetArea)
            {
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      this->tabifyDockWidget (i.value (), details);
                    }
                }
            }

          details->show ();
          details->update ();
          details->raise ();
          details_widgets.insert (selected_id, details);
        }
    }
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                             tr ("No escalator selected!"));
}


/**
 * @brief Requests schedule details.
 */
void
gsd_mw::details_schedule_dlg ()
{
  QModelIndexList selected = schedules->getTable ()->selectionModel ()
                                                       ->selectedRows (0);
  if (selected.at (0).isValid ())
    {
      QDomElement schedule = control->getScheduleModel ()
                                        ->getEntity (selected.at (0).row ());
      QString selected_id = control->getScheduleModel ()
                                     ->getAttr (schedule, "schedule id");
      if (details_widgets.contains (selected_id))
        {
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          details_widgets[selected_id]->restoreGeometry (settings.value ("geometrie").toByteArray ());
          settings.endGroup ();
          details_widgets.value (selected_id)->show ();
          details_widgets.value (selected_id)->update ();
          details_widgets.value (selected_id)->raise ();
        }
      else
        {
          dock_details_schedule *details =
            new dock_details_schedule (this->control);
          details->setId (selected_id);
          details->load ();
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          if (!this->restoreDockWidget (details))
            {
              int area = this->dockWidgetArea (details_widgets.value (selected_id));
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      details->show ();
                      details->update ();
                      this->tabifyDockWidget (i.value (), details);
                      details->raise ();
                      details_widgets.insert (selected_id, details);
                      return;
                    }
                 }
               this->addDockWidget (Qt::BottomDockWidgetArea, details);
            }

          settings.endGroup ();
          if (this->dockWidgetArea (details) == Qt::BottomDockWidgetArea)
            {
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      this->tabifyDockWidget (i.value (), details);
                    }
                }
            }

          details->show ();
          details->update ();
          details->raise ();
          details_widgets.insert (selected_id, details);
        }
    }
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                             tr ("No schedule selected!"));
}

/**
 * @brief Activates the port_list actions.
 */
void
gsd_mw::activate_port_list_actions()
{
    actionNew_PortList->setDisabled (false);
}

/**
 * @brief Requests port_list details.
 */
void
gsd_mw::details_port_list_dlg ()
{
  QModelIndexList selected = portlists->getTable ()->selectionModel ()
                                                     ->selectedRows (0);
  if (selected.at (0).isValid ())
    {
      QDomElement port_list = control->getPortListModel()
                                  ->getEntity (selected.at (0).row ());
      QString selected_id = control->getPortListModel()
                                    ->getAttr (port_list, "port_list id");
      emit sig_details_port_list (selected_id);
      if (details_widgets.contains (selected_id))
        {
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          details_widgets[selected_id]->restoreGeometry (settings.value ("geometrie").toByteArray ());
          settings.endGroup ();
          details_widgets.value (selected_id)->show ();
          details_widgets.value (selected_id)->update ();
          details_widgets.value (selected_id)->raise ();
        }
      else
        {
          dock_details_port_list *details = new dock_details_port_list (this->control);
          connect (control,
                   SIGNAL (sig_port_list_details_finished (QString)),
                   details,
                   SLOT (update (QString)));
          details->setId (selected_id);
          details->load ();
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          if (!this->restoreDockWidget (details))
            {
              int area = this->dockWidgetArea (details_widgets.value (selected_id));
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      details->show ();
                      details->update ();
                      this->tabifyDockWidget (i.value (), details);
                      details->raise ();
                      details_widgets.insert (selected_id, details);
                      return;
                    }
                 }
               this->addDockWidget (Qt::BottomDockWidgetArea, details);
            }

          settings.endGroup ();
          if (this->dockWidgetArea (details) == Qt::BottomDockWidgetArea)
            {
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      this->tabifyDockWidget (i.value (), details);
                    }
                }
            }

          details->show ();
          details->update ();
          details->raise ();
          details_widgets.insert (selected_id, details);
        }
    }
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No port_list selected!"));
}

/**
 * @brief Requests target details.
 */
void
gsd_mw::details_target_dlg ()
{
  QModelIndexList selected = targets->getTable ()->selectionModel ()
                                                    ->selectedRows (0);
  if (selected.at (0).isValid ())
    {
      QDomElement target = control->getTargetModel ()
                                  ->getEntity (selected.at (0).row ());
      QString selected_id = control->getTargetModel ()
                                     ->getAttr (target, "target id");
      if (details_widgets.contains (selected_id))
        {
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          details_widgets[selected_id]->restoreGeometry (settings.value ("geometrie").toByteArray ());
          settings.endGroup ();
          details_widgets.value (selected_id)->show ();
          details_widgets.value (selected_id)->update ();
          details_widgets.value (selected_id)->raise ();
        }
      else
        {
          dock_details_target *details = new dock_details_target (this->control);
          details->setId (selected_id);
          details->load ();
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          if (!this->restoreDockWidget (details))
            {
              int area = this->dockWidgetArea (details_widgets.value (selected_id));
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      details->show ();
                      details->update ();
                      this->tabifyDockWidget (i.value (), details);
                      details->raise ();
                      details_widgets.insert (selected_id, details);
                      return;
                    }
                 }
               this->addDockWidget (Qt::BottomDockWidgetArea, details);
            }

          settings.endGroup ();
          if (this->dockWidgetArea (details) == Qt::BottomDockWidgetArea)
            {
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      this->tabifyDockWidget (i.value (), details);
                    }
                }
            }

          details->show ();
          details->update ();
          details->raise ();
          details_widgets.insert (selected_id, details);
        }
    }
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No target selected!"));
}


/**
 * @brief Requests task details.
 */
void
gsd_mw::details_task_dlg ()
{
  QModelIndexList selected = tasks->getTable ()->selectionModel ()
                                                 ->selectedRows (0);
  if (selected.at (0).isValid ())
    {
      QDomElement task = control->getTaskModel ()
                                  ->getEntity (selected.at (0).row ());
      QString selected_id = control->getTaskModel ()
                                     ->getAttr (task, "task id");
      if (details_widgets.contains (selected_id))
        {
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          details_widgets[selected_id]->restoreGeometry (settings.value ("geometrie").toByteArray ());
          settings.endGroup ();
          details_widgets.value (selected_id)->show ();
          details_widgets.value (selected_id)->update ();
          details_widgets.value (selected_id)->raise ();
        }
      else
        {
          dock_details_task *details = new dock_details_task (this->control);
          connect (details,
                   SIGNAL (sig_report (QString, QString)),
                   this,
                   SLOT (show_report (QString, QString)));
          connect (details,
                   SIGNAL (sig_delete_report (QString)),
                   control,
                   SLOT (report_delete (QString)));
          connect (control,
                   SIGNAL (sig_task_details_finished ()),
                   details,
                   SLOT (update ()));
          details->setId (selected_id);
          details->load ();
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          if (!this->restoreDockWidget (details))
            {
              int area = this->dockWidgetArea (details_widgets.value (selected_id));
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      details->show ();
                      details->update ();
                      this->tabifyDockWidget (i.value (), details);
                      details->raise ();
                      details_widgets.insert (selected_id, details);
                      return;
                    }
                 }
               this->addDockWidget (Qt::BottomDockWidgetArea, details);
            }

          settings.endGroup ();
          if (this->dockWidgetArea (details) == Qt::BottomDockWidgetArea)
            {
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      this->tabifyDockWidget (i.value (), details);
                    }
                }
            }

          details->show ();
          details->raise ();
          details->update ();
          details_widgets.insert (selected_id, details);
        }
    }
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No task selected!"));
}


/**
 * @brief Requests note details.
 */
void
gsd_mw::details_note_dlg ()
{
  QModelIndexList selected = notes->getTable ()->selectionModel ()
                                                 ->selectedRows (0);
  if (selected.at (0).isValid ())
    {
      QString note_id = control->getNoteModel ()
                                 ->getAttr (control->getNoteModel ()
                                                     ->getEntity (selected.at (0)
                                                                   .row ()),
                                                                  "note id");

      emit sig_details_note (note_id);
      QDomElement note = control->getNoteModel ()
                                  ->getEntity (selected.at (0).row ());
      QString selected_id = control->getNoteModel ()
                                     ->getAttr (note, "task id");
      if (details_widgets.contains (selected_id))
        {
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          details_widgets[selected_id]->restoreGeometry (settings.value ("geometrie").toByteArray ());
          settings.endGroup ();
          details_widgets.value (selected_id)->show ();
          details_widgets.value (selected_id)->update ();
          details_widgets.value (selected_id)->raise ();
        }
      else
        {
          dock_details_note *details = new dock_details_note (this->control);
          connect (control,
                   SIGNAL (sig_note_details_finished (QString)),
                   details,
                   SLOT (update (QString)));
          connect (details,
                   SIGNAL (sig_modify (int,
                                       QString,
                                       model_omp_entity*,
                                       QMap<QString, QString>)),
                   this->control,
                   SLOT (modify (int,
                                 QString,
                                 model_omp_entity*,
                                 QMap<QString, QString>)));
          details->setId (selected_id);
          details->load ();
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          if (!this->restoreDockWidget (details))
            {
              int area = this->dockWidgetArea (details_widgets.value (selected_id));
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      details->show ();
                      this->tabifyDockWidget (i.value (), details);
                      details->raise ();
                      details_widgets.insert (selected_id, details);
                      return;
                    }
                 }
               this->addDockWidget (Qt::BottomDockWidgetArea, details);
            }

          settings.endGroup ();
          if (this->dockWidgetArea (details) == Qt::BottomDockWidgetArea)
            {
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      this->tabifyDockWidget (i.value (), details);
                    }
                }
            }

          details->show ();
          details->raise ();
          details_widgets.insert (selected_id, details);
        }
    }
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No note selected!"));
}


/**
 * @brief Request override details.
 */
void
gsd_mw::details_override_dlg ()
{
  QModelIndexList selected = overrides->getTable ()->selectionModel ()
                                                     ->selectedRows (0);
  if (selected.at (0).isValid ())
    {
      QString overr_id = control->getOverrideModel ()
                                 ->getAttr (control->getOverrideModel ()
                                                   ->getEntity (selected.at (0)
                                                                 .row ()),
                                                                "override id");

      emit sig_details_override (overr_id);
      QDomElement override = control->getOverrideModel ()
                                  ->getEntity (selected.at (0).row ());
      QString selected_id = control->getOverrideModel ()
                                     ->getAttr (override, "task id");
      if (details_widgets.contains (selected_id))
        {
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          details_widgets[selected_id]->restoreGeometry (settings.value ("geometrie").toByteArray ());
          settings.endGroup ();
          details_widgets.value (selected_id)->show ();
          details_widgets.value (selected_id)->update ();
          details_widgets.value (selected_id)->raise ();
        }
      else
        {
          dock_details_override *details =
            new dock_details_override (this->control);
          connect (control,
                   SIGNAL (sig_override_details_finished (QString)),
                   details,
                   SLOT (update (QString)));
          connect (details,
                   SIGNAL (sig_modify (int,
                                       QString,
                                       model_omp_entity*,
                                       QMap<QString, QString>)),
                   this->control,
                   SLOT (modify (int,
                                 QString,
                                 model_omp_entity*,
                                 QMap<QString, QString>)));
          details->setId (selected_id);
          details->load ();
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          if (!this->restoreDockWidget (details))
            {
              int area = this->dockWidgetArea (details_widgets.value (selected_id));
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      details->show ();
                      this->tabifyDockWidget (i.value (), details);
                      details->raise ();
                      details_widgets.insert (selected_id, details);
                      return;
                    }
                 }
               this->addDockWidget (Qt::BottomDockWidgetArea, details);
            }

          settings.endGroup ();
          if (this->dockWidgetArea (details) == Qt::BottomDockWidgetArea)
            {
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      this->tabifyDockWidget (i.value (), details);
                    }
                }
            }

          details->show ();
          details->raise ();
          details_widgets.insert (selected_id, details);
        }
    }
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No override selected!"));
}


/**
 * @brief Requests slave details.
 */
void
gsd_mw::details_slave_dlg ()
{
  QModelIndexList selected = slaves->getTable ()->selectionModel ()
                                                   ->selectedRows (0);
  if (selected.at (0).isValid ())
    {
      QDomElement slave = control->getSlaveModel ()
                                  ->getEntity (selected.at (0).row ());
      QString selected_id = control->getSlaveModel ()
                                     ->getAttr (slave, "target id");
      if (details_widgets.contains (selected_id))
        {
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          details_widgets[selected_id]->restoreGeometry (settings.value ("geometrie").toByteArray ());
          settings.endGroup ();
          details_widgets.value (selected_id)->show ();
          details_widgets.value (selected_id)->update ();
          details_widgets.value (selected_id)->raise ();
        }
      else
        {
          dock_details_slave *details = new dock_details_slave (this->control);
          details->setId (selected_id);
          details->load ();
          QSettings settings ("Greenbone", "GSD");
          settings.beginGroup ("DockWidgets");
          if (!this->restoreDockWidget (details))
            {
              int area = this->dockWidgetArea (details_widgets.value (selected_id));
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      details->show ();
                      details->update ();
                      this->tabifyDockWidget (i.value (), details);
                      details->raise ();
                      details_widgets.insert (selected_id, details);
                      return;
                    }
                 }
               this->addDockWidget (Qt::BottomDockWidgetArea, details);
            }

          settings.endGroup ();
          if (this->dockWidgetArea (details) == Qt::BottomDockWidgetArea)
            {
              QMapIterator <QString, dock_details*> i (details_widgets);
              while (i.hasNext ())
                {
                  i.next ();
                  int usedArea = this->dockWidgetArea (i.value ());
                  if (usedArea == Qt::BottomDockWidgetArea &&
                      i.key ().compare (selected_id) != 0 &&
                      i.value ()->isVisible ())
                    {
                      this->tabifyDockWidget (i.value (), details);
                    }
                }
            }

          details->show ();
          details->update ();
          details->raise ();
          details_widgets.insert (selected_id, details);
        }
    }
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No slave selected!"));
}
/**
 * @brief Requests a config download.
 */
void
gsd_mw::config_download ()
{
  QModelIndexList selected = configs->getTable ()->selectionModel ()
                                                   ->selectedRows (0);
  if (selected.at (0).isValid ())
    {
      emit sig_download_config (selected.at (0).row ());
    }
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                              tr ("No config selected!"));
}


/**
 * @brief Imports a config.
 */
void
gsd_mw::config_upload ()
{
  QString filename = QFileDialog::getOpenFileName (this,
                                                   tr ("Open File ..."),
                                                   QDir::homePath (),
                                                   tr ("XML files *.xml"));
  QFile file (filename);
  if (!file.open(QIODevice::ReadOnly))
    return;

  QTextStream in(&file);
  QString xml;
  while (!in.atEnd())
    {
      QString line = in.readLine();
      xml += line;
    }
  QByteArray utf8_str = xml.toUtf8 ();

  QMap<QString, QString> parameter;
  parameter.insert ("upload", utf8_str.data ());

  emit sig_upload (omp_utilities::CONFIG, parameter);
}


/**
 * @brief Saves a dowloaded config.
 */
void
gsd_mw::save_config ()
{
  QString filename = QFileDialog::getSaveFileName (this,
                                                   tr ("Save File ..."),
                                                   QDir::homePath () +
                                                   "/config.xml",
                                                   tr ("XML files *.xml"));
  model_omp_entity *tmp = this->control->getConfigDownloadModel ();

  QDomElement element = tmp->getEntity (0);
  const int indent = 4;
  if (!filename.endsWith (".xml"))
    filename += ".xml";
  QFile wfile (filename);
  if (!wfile.open (QFile::WriteOnly))
    {
      QMessageBox::information (NULL, tr ("File Error"),
                                tr ("Could not open file!"));
      return;
    }
  QTextStream out (&wfile);
  element.save (out, indent);
  wfile.close ();
  return;
}


/**
 * @brief SLOT to show schedules widget
 */
void
gsd_mw::schedule_dw ()
{
  schedules->show ();
  schedules->raise ();
}


/**
 * @brief SLOT to show target widget
 */
void
gsd_mw::target_dw ()
{
  targets->show ();
  targets->raise ();
}


/**
 * @brief SLOT to show scan configs widget
 */
void
gsd_mw::config_dw ()
{
  configs->show ();
  configs->raise ();
}


/**
 * @brief SLOT to show escalators widget
 */
void
gsd_mw::escalator_dw ()
{
  escalators->show ();
  escalators->raise ();
}


/**
 * @brief SLOT to show lsc_credentials widget
 */
void
gsd_mw::credential_dw ()
{
  credentials->show ();
  credentials->raise ();
}


/**
 * @brief SLOT to show agents widget
 */
void
gsd_mw::agent_dw ()
{
  agents->show ();
  agents->raise ();
}


/**
 * @brief SLOT to show notes widget
 */
void
gsd_mw::note_dw ()
{
  notes->show ();
  notes->raise ();
}


/**
 * @brief SLOT to show overrides widget
 */
void
gsd_mw::override_dw ()
{
  overrides->show ();
  overrides->raise ();
}


/**
 * @brief SLOT to show overrides widget
 */
void
gsd_mw::slave_dw ()
{
  slaves->show ();
  slaves->raise ();
}


/**
 * @brief SLOT to show report formats widget.
 */
void
gsd_mw::reportformat_dw ()
{
  reportformats->show ();
  reportformats->raise ();
}


/**
 * @brief SLOT to show performance widget
 */
void
gsd_mw::performance_dw ()
{
  performance->show ();
  performance->load ();
  performance->raise ();
}


/**
 * @brief SLOT to show portlists widget
 */
void
gsd_mw::portlists_dw ()
{
  portlists->show ();
  portlists->raise ();
}


/**
 * @brief SLOT called when the task dockwidget becomes visible or invisble
 */
void
gsd_mw::focus_tasks (bool focus)
{
  if (focus)
    tasks->getTable ()->setFocus ();
  tasks->getTable ()->viewport ()->update ();
  if (!tasks->getTable ()->selectionModel ()->hasSelection ())
    {
      QModelIndex start;
      QModelIndex end;
      start = control->getTaskModel ()->index (0, 0, QModelIndex ());
      end = control->getTaskModel ()->index (0,
                                             control->getTaskModel ()
                                                      ->columnCount () -1,
                                             QModelIndex ());
      QItemSelection sel (start, end);
      tasks->getTable ()
             ->selectionModel ()
               ->select (sel, QItemSelectionModel::Select);
      return;

    }
  task_selectionchanged (tasks->getTable ()->selectionModel ()->selection (),
                         QItemSelection ());
}


/**
 * @brief SLOT called when the schedules dockwidget becomes visible or invisble
 */
void
gsd_mw::focus_schedules (bool focus)
{
  if (focus)
    schedules->getTable ()->setFocus ();
  schedules->getTable ()->viewport ()->update ();
  if (!schedules->getTable ()->selectionModel ()->hasSelection ())
    {
      QModelIndex start;
      QModelIndex end;
      start = control->getScheduleModel ()->index (0, 0, QModelIndex ());
      end = control->getScheduleModel ()->index (0,
                                                 control->getScheduleModel ()
                                                          ->columnCount () -1,
                                                 QModelIndex ());
      QItemSelection sel (start, end);
      schedules->getTable ()
                 ->selectionModel ()
                   ->select (sel, QItemSelectionModel::Select);
      return;
    }
  schedule_selectionchanged (schedules->getTable ()->selectionModel ()->selection (),
                             QItemSelection ());
}


/**
 * @brief SLOT called when the targets dockwidget becomes visible or invisble
 */
void
gsd_mw::focus_targets (bool focus)
{
  if (focus)
    targets->getTable ()->setFocus ();
  targets->getTable ()->viewport ()->update ();
  if (!targets->getTable ()->selectionModel ()->hasSelection ())
    {
      QModelIndex start;
      QModelIndex end;
      start = control->getTargetModel ()->index (0, 0, QModelIndex ());
      end = control->getTargetModel ()->index (0,
                                               control->getTargetModel ()
                                                        ->columnCount () -1,
                                               QModelIndex ());
      QItemSelection sel (start, end);
      targets->getTable ()
               ->selectionModel ()
                 ->select (sel, QItemSelectionModel::Select);
      return;
    }
  target_selectionchanged (targets->getTable ()->selectionModel ()->selection(),
                           QItemSelection ());
}


/**
 * @brief SLOT called when the configs dockwidget becomes visible or invisble
 */
void
gsd_mw::focus_configs (bool focus)
{
  if (focus)
    configs->getTable ()->setFocus ();
  configs->getTable ()->viewport ()->update ();
  if (!configs->getTable ()->selectionModel ()->hasSelection ())
    {
      QModelIndex start;
      QModelIndex end;
      start = control->getConfigModel ()->index (0, 0, QModelIndex ());
      end = control->getConfigModel ()->index (0,
                                               control->getConfigModel ()
                                                        ->columnCount () -1,
                                               QModelIndex ());
      QItemSelection sel (start, end);
      configs->getTable ()
               ->selectionModel ()
                 ->select (sel, QItemSelectionModel::Select);
      return;
    }

  config_selectionchanged (configs->getTable ()->selectionModel ()->selection(),
                           QItemSelection ());
}


/**
 * @brief SLOT called when the escalators dockwidget becomes visible or invisble
 */
void
gsd_mw::focus_escalators (bool focus)
{
  if (focus)
    escalators->getTable ()->setFocus ();
  escalators->getTable ()->viewport ()->update ();
  if (!escalators->getTable ()->selectionModel ()->hasSelection ())
    {
      QModelIndex start;
      QModelIndex end;
      start = control->getEscalatorModel ()->index (0, 0, QModelIndex ());
      end = control->getEscalatorModel ()->index (0,
                                                  control->getEscalatorModel ()
                                                           ->columnCount () -1,
                                                  QModelIndex ());
      QItemSelection sel (start, end);
      escalators->getTable ()
               ->selectionModel ()
                 ->select (sel, QItemSelectionModel::Select);
      return;
    }
 escalator_selectionchanged (escalators->getTable ()->selectionModel ()->selection (),
                              QItemSelection ());
}


/**
 * @brief SLOT called when the lsc_credentials dockwidget becomes visible or
 * invisble.
 */
void
gsd_mw::focus_credentials (bool focus)
{
  if (focus)
    credentials->getTable ()->setFocus ();
  credentials->getTable ()->viewport ()->update ();
  if (!credentials->getTable ()->selectionModel ()->hasSelection ())
    {
      QModelIndex start;
      QModelIndex end;
      start = control->getCredentialModel ()->index (0, 0, QModelIndex ());
      end = control->getCredentialModel ()
                     ->index (0,
                              control->getCredentialModel ()
                                       ->columnCount () - 1,
                              QModelIndex ());
      QItemSelection sel (start, end);
      credentials->getTable ()
                   ->selectionModel ()
                     ->select (sel, QItemSelectionModel::Select);
      return;
    }
 credential_selectionchanged (credentials->getTable ()->selectionModel ()->selection (),
                               QItemSelection ());
}


/**
 * @brief SLOT called when the agents dockwidget becomes visible or
 * invisble.
 */
void
gsd_mw::focus_agents (bool focus)
{
  if (focus)
    agents->getTable ()->setFocus ();
  agents->getTable ()->viewport ()->update ();
  if (!agents->getTable ()->selectionModel ()->hasSelection ())
    {
      QModelIndex start;
      QModelIndex end;
      start = control->getAgentModel ()->index (0, 0, QModelIndex ());
      end = control->getAgentModel ()->index (0,
                                              control->getAgentModel ()
                                                       ->columnCount () -1,
                                              QModelIndex ());
      QItemSelection sel (start, end);
      agents->getTable ()
               ->selectionModel ()
                 ->select (sel, QItemSelectionModel::Select);
      return;
    }
 agent_selectionchanged (agents->getTable ()->selectionModel ()->selection (),
                          QItemSelection ());
}


/**
 * @brief SLOT called when the notes dockwidget becomes visible or
 * invisble.
 */
void
gsd_mw::focus_notes (bool focus)
{
  if (focus)
    notes->getTable ()->setFocus ();
  notes->getTable ()->viewport ()->update ();
  if (!notes->getTable ()->selectionModel ()->hasSelection ())
    {
      QModelIndex start;
      QModelIndex end;
      start = control->getNoteModel ()->index (0, 0, QModelIndex ());
      end = control->getNoteModel ()->index (0,
                                             control->getNoteModel ()
                                                      ->columnCount () -1,
                                             QModelIndex ());
      QItemSelection sel (start, end);
      notes->getTable ()
             ->selectionModel ()
               ->select (sel, QItemSelectionModel::Select);
      return;
    }
  note_selectionchanged (notes->getTable ()->selectionModel ()->selection (),
                         QItemSelection ());
}


/**
 * @brief SLOT called when the overrides dockwidget becomes visible or
 * invisble.
 */
void
gsd_mw::focus_overrides (bool focus)
{
  if (focus)
    overrides->getTable ()->setFocus ();
  overrides->getTable ()->viewport ()->update ();
  if (!overrides->getTable ()->selectionModel ()->hasSelection ())
    {
      QModelIndex start;
      QModelIndex end;
      start = control->getOverrideModel ()->index (0, 0, QModelIndex ());
      end = control->getOverrideModel ()->index (0,
                                                 control->getOverrideModel ()
                                                          ->columnCount () -1,
                                               QModelIndex ());
      QItemSelection sel (start, end);
      overrides->getTable ()
                 ->selectionModel ()
                   ->select (sel, QItemSelectionModel::Select);
      return;
    }
 override_selectionchanged (overrides->getTable ()->selectionModel ()->selection (),
                             QItemSelection ());
}


/**
 * @brief SLOT called when the slaves dockwidget becomes visible or
 * invisble.
 */
void
gsd_mw::focus_slaves (bool focus)
{
  if (focus)
    slaves->getTable ()->setFocus ();
  slaves->getTable ()->viewport ()->update ();
  if (!slaves->getTable ()->selectionModel ()->hasSelection ())
    {
      QModelIndex start;
      QModelIndex end;
      start = control->getSlaveModel ()->index (0, 0, QModelIndex ());
      end = control->getSlaveModel ()->index (0,
                                              control->getSlaveModel ()
                                                       ->columnCount () -1,
                                               QModelIndex ());
      QItemSelection sel (start, end);
      slaves->getTable ()
              ->selectionModel ()
                ->select (sel, QItemSelectionModel::Select);
      return;
    }
 slave_selectionchanged (slaves->getTable ()->selectionModel ()->selection (),
                             QItemSelection ());
}


/**
 * @brief SLOT called when the reportformat dockwidget becomes visible or
 * invisble.
 */
void
gsd_mw::focus_reportformats (bool focus)
{
  if (focus)
    reportformats->getTable ()->setFocus ();
  reportformats->getTable ()->viewport ()->update ();
  if (!reportformats->getTable ()->selectionModel ()->hasSelection ())
    {
      QModelIndex start;
      QModelIndex end;
      start = control->getReportFormatModel ()->index (0, 0, QModelIndex ());
      end = control->getReportFormatModel ()->index (0,
                                              control->getReportFormatModel ()
                                                       ->columnCount () -1,
                                               QModelIndex ());
      QItemSelection sel (start, end);
      reportformats->getTable ()
              ->selectionModel ()
                ->select (sel, QItemSelectionModel::Select);
      return;
    }
}

/**
 * @brief SLOT called when the portlists dockwidget becomes visible or invisble
 */
void
gsd_mw::focus_portlists (bool focus)
{
  if (focus)
    portlists->getTable ()->setFocus ();
  portlists->getTable ()->viewport ()->update ();
  if (!portlists->getTable ()->selectionModel ()->hasSelection ())
    {
      QModelIndex start;
      QModelIndex end;
      start = control->getPortListModel ()->index (0, 0, QModelIndex ());
      end = control->getPortListModel ()->index (0,
                                                  control->getPortListModel ()
                                                           ->columnCount () -1,
                                                  QModelIndex ());
      QItemSelection sel (start, end);
      portlists->getTable ()
               ->selectionModel ()
               ->select (sel, QItemSelectionModel::Select);
      return;
    }
  portlist_selectionchanged (portlists->getTable ()->selectionModel ()->selection (),
                             QItemSelection ());
}

/**
 * @brief SLOT called when the overrides dockwidget becomes visible or
 * invisble.
 *
 * @param[in]  focus  Boolean indicating wether focus gained or lost.
 */
void
gsd_mw::focus_performance (bool focus)
{
  if (focus)
    {
      performance->load ();
    }
}


/**
 * @brief SLOT that saves the id of the selected task and manipulates the UI
 *
 * @param[in]  sel    new selected index.
 * @param[in]  desel  previous selected index.
 */
void
gsd_mw::task_selectionchanged (const QItemSelection &sel,
                               const QItemSelection &desel)
{
  int row = -1;
  if (tasks->getTable ()->selectionModel () == NULL)
    return;
  if (sel.empty ())
    {
      actionDetails_Task->setEnabled (false);
      actionPause->setEnabled (false);
      actionRun->setEnabled (false);
      actionStop->setEnabled (false);
      actionResume->setEnabled (false);
      actionDelete->setEnabled (false);
      actionModify_Task->setEnabled (false);
      return;
    }
  if (sel.at (0).top () >= 0 )
    {
      row = sel.at (0).top ();
    }
  else
    {
      row = 0;
    }
  QDomNode ent = control->getTaskModel ()->getEntity (row);
  if (ent.isNull ())
    {
      actionDetails_Task->setEnabled (false);
      return;
    }
  QString stat = control->getTaskModel ()->getValue (ent.toElement (),
                                                     "status");
  QString schedule = control->getTaskModel ()->getAttr (ent.toElement (),
                                                        "task schedule id");
  bool scheduled = false;
  if (schedule.compare ("") != 0)
    scheduled = true;
  if (stat.compare ("New") == 0 ||
      stat.compare ("Done") == 0)
    {
      actionPause->setVisible (false);
      actionRun->setVisible (true);
      if (scheduled)
        actionRun->setEnabled (false);
      else
        actionRun->setEnabled (true);
      actionStop->setEnabled (false);
      actionResume->setVisible (false);
      actionDelete->setEnabled (true);
      actionModify_Task->setEnabled (true);
    }

  if (stat.compare ("Stopped") == 0)
    {
      actionPause->setVisible (false);
      actionRun->setVisible (true);
      actionRun->setEnabled (true);
      actionResume->setVisible (true);
      actionResume->setEnabled (true);
      actionStop->setVisible (false);
      actionDelete->setEnabled (true);
      actionModify_Task->setEnabled (true);
    }

  if (stat.compare ("Error") == 0)
    {
      actionPause->setVisible (false);
      actionRun->setVisible (true);
      actionRun->setEnabled (false);
      actionResume->setEnabled (false);
      actionStop->setEnabled (false);
      actionDelete->setEnabled (true);
      actionModify_Task->setEnabled (true);
    }

  if (stat.compare ("Delete Requested") == 0 ||
      stat.compare ("Stop Requested") == 0||
      stat.compare ("Pause Requested") == 0 ||
      stat.compare ("Resume Requested") == 0 )
    {
      actionPause->setVisible (false);
      actionRun->setVisible (true);
      actionRun->setEnabled (false);
      actionStop->setEnabled (false);
      actionResume->setEnabled (false);
      actionDelete->setEnabled (false);
      actionModify_Task->setEnabled (false);
    }

  if (stat.compare ("Requested") == 0)
    {
      actionRun->setVisible (false);
      actionPause->setVisible (true);
      actionPause->setEnabled (false);
      actionResume->setEnabled (false);
      actionStop->setEnabled (false);
      actionDelete->setEnabled (false);
      actionModify_Task->setEnabled (false);
    }

  if (stat.compare ("Paused") == 0)
    {
      actionPause->setVisible (false);
      actionRun->setVisible (false);
      actionResume->setVisible (true);
      actionResume->setEnabled (true);
      actionStop->setVisible (true);
      actionStop->setEnabled (true);
      actionDelete->setEnabled (true);
      actionModify_Task->setEnabled (true);
    }

  if (stat.compare ("Running") == 0)
    {
      actionRun->setVisible (false);
      actionPause->setVisible (true);
      actionPause->setEnabled (true);
      actionRun->setEnabled (false);
      actionResume->setEnabled (false);
      actionStop->setEnabled (true);
      actionDelete->setEnabled (false);
      actionModify_Task->setEnabled (false);
    }
  actionDetails_Task->setEnabled (true);
}


/**
 * @brief SLOT to enable/disable action buttons in config widget.
 *
 * @param[in]  sel    new selected index.
 * @param[in]  desel  previous selected index.
 */
void
gsd_mw::config_selectionchanged (const QItemSelection &sel,
                                 const QItemSelection &desel)
{
  int row = -1;
  if (configs->getTable ()->selectionModel () == NULL)
    return;
  QModelIndexList selected = configs->getTable ()->selectionModel ()
                                                 ->selectedRows (0);
  if (sel.empty ())
    {
      actionDelete_Config->setEnabled (false);
      actionDetails_Config->setEnabled (false);
      actionExport_Config->setEnabled (false);
      return;
    }
  if (sel.at (0).top () >= 0 )
    {
      row = sel.at (0).top ();
    }
  else
    {
      row = 0;
    }
  QDomNode ent = control->getConfigModel ()->getEntity (row);
  if (ent.isNull ())
    {
      actionDelete_Config->setEnabled (false);
      actionDetails_Config->setEnabled (false);
      actionExport_Config->setEnabled (false);
      return;
    }
  QString stat = control->getConfigModel ()->getValue (ent.toElement (),
                                                       "in_use");
  if (stat.compare ("0") == 0)
    actionDelete_Config->setEnabled (true);
  else
    actionDelete_Config->setEnabled (false);
  actionDetails_Config->setEnabled (true);
  actionExport_Config->setEnabled (true);
}


/**
 * @brief SLOT to enable/disable action buttons in config widget.
 *
 * @param[in]  sel    new selected index.
 * @param[in]  desel  previous selected index.
 */
void
gsd_mw::target_selectionchanged (const QItemSelection &sel,
                                 const QItemSelection &desel)
{
  int row = -1;
  if (targets->getTable ()->selectionModel () == NULL)
    return;
  if (sel.empty ())
    {
      actionDelete_Target->setEnabled (false);
      actionDetails_Target->setEnabled (false);
      return;
    }
  if (sel.at (0).top () >= 0 )
    {
      row = sel.at (0).top ();
    }
  else
    {
      row = 0;
    }
  QDomNode ent = control->getTargetModel ()->getEntity (row);
  if (ent.isNull ())
    {
      actionDetails_Target->setEnabled (false);
      actionDelete_Target->setEnabled (false);
      return;
    }
  QString stat = control->getTargetModel ()->getValue (ent.toElement (),
                                                       "in_use");
  if (stat.compare ("0") == 0)
    actionDelete_Target->setEnabled (true);
  else
    actionDelete_Target->setEnabled (false);
  actionDetails_Target->setEnabled (true);
}


/**
 * @brief SLOT to enable/disable action buttons in schedule widget.
 *
 * @param[in]  sel    new selected index.
 * @param[in]  desel  previous selected index.
 */
void
gsd_mw::schedule_selectionchanged (const QItemSelection &sel,
                                   const QItemSelection &desel)
{
  int row = -1;
  if (schedules->getTable ()->selectionModel () == NULL)
    return;
  if (sel.empty ())
    {
      actionDelete_Schedule->setEnabled (false);
      actionDetails_Schedule->setEnabled (false);
      return;
    }
  if (sel.at (0).top () >= 0 )
    {
      row = sel.at (0).top ();
    }
  else
    {
      row = 0;
    }
  QDomNode ent = control->getScheduleModel ()->getEntity (row);
  if (ent.isNull ())
    {
      actionDetails_Schedule->setEnabled (false);
      actionDelete_Schedule->setEnabled (false);
      return;
    }
  QString stat = control->getScheduleModel ()->getValue (ent.toElement (),
                                                      "in_use");
  if (stat.compare ("0") == 0)
    actionDelete_Schedule->setEnabled (true);
  else
    actionDelete_Schedule->setEnabled (false);
  actionDetails_Schedule->setEnabled (true);
}


/**
 * @brief SLOT to enable/disable action buttons in escalator widget.
 *
 * @param[in]  sel    new selected index.
 * @param[in]  desel  previous selected index.
 */
void
gsd_mw::escalator_selectionchanged (const QItemSelection &sel,
                                    const QItemSelection &desel)
{
  int row = -1;
  if (escalators->getTable ()->selectionModel () == NULL)
    return;
  if (sel.empty ())
    {
      actionDelete_Escalator->setEnabled (false);
      actionDetails_Escalator->setEnabled (false);
      return;
    }
  if (sel.at (0).top () >= 0 )
    {
      row = sel.at (0).top ();
    }
  else
    {
      row = 0;
    }
  QDomNode ent = control->getEscalatorModel ()->getEntity (row);
  if (ent.isNull ())
    {
      actionDelete_Escalator->setEnabled (false);
      actionDetails_Escalator->setEnabled (false);
      return;
    }
  QString stat = control->getEscalatorModel ()->getValue (ent.toElement (),
                                                          "in_use");
  if (stat.compare ("0") == 0)
    actionDelete_Escalator->setEnabled (true);
  else
    actionDelete_Escalator->setEnabled (false);
  actionDetails_Escalator->setEnabled (true);
}


/**
 * @brief SLOT to enable/disable action buttons in credential widget.
 *
 * @param[in]  sel    new selected index.
 * @param[in]  desel  previous selected index.
 */
void
gsd_mw::credential_selectionchanged (const QItemSelection &sel,
                                     const QItemSelection &desel)
{
  int row = -1;
  if (credentials->getTable ()->selectionModel () == NULL)
    return;
  if (sel.empty ())
    {
      actionDelete_Credential->setEnabled (false);
      actionDetails_Credential->setEnabled (false);
      return;
    }
  if (sel.at (0).top () >= 0 )
    {
      row = sel.at (0).top ();
    }
  else
    {
      row = 0;
    }
  QDomNode ent = control->getCredentialModel ()->getEntity (row);
  if (ent.isNull ())
    {
      actionDelete_Credential->setEnabled (false);
      actionDetails_Credential->setEnabled (false);
      return;
    }
  QString stat = control->getCredentialModel ()->getValue (ent.toElement (),
                                                           "in_use");
  if (stat.compare ("0") == 0)
    actionDelete_Credential->setEnabled (true);
  else
    actionDelete_Credential->setEnabled (false);
  actionDetails_Credential->setEnabled (true);
}


/**
 * @brief SLOT to enable/disable action buttons in agent widget.
 *
 * @param[in]  sel    new selected index.
 * @param[in]  desel  previous selected index.
 */
void
gsd_mw::agent_selectionchanged (const QItemSelection &sel,
                                const QItemSelection &desel)
{
  int row = -1;
  if (agents->getTable ()->selectionModel () == NULL)
    return;
  if (sel.empty ())
    {
      actionDelete_Agent->setEnabled (false);
      return;
    }
  if (sel.at (0).top () >= 0 )
    {
      row = sel.at (0).top ();
    }
  else
    {
      row = 0;
    }
  QDomNode ent = control->getAgentModel ()->getEntity (row);
  if (ent.isNull ())
    {
      actionDelete_Agent->setEnabled (false);
      return;
    }
  QString stat = control->getAgentModel ()->getValue (ent.toElement (),
                                                      "in_use");
  if (stat.compare ("0") == 0)
    actionDelete_Agent->setEnabled (true);
  else
    actionDelete_Agent->setEnabled (false);
}


/**
 * @brief SLOT to enable/disable action buttons in note widget.
 *
 * @param[in]  sel    new selected index.
 * @param[in]  desel  previous selected index.
 */
void
gsd_mw::note_selectionchanged (const QItemSelection &sel,
                               const QItemSelection &desel)
{
  int row = -1;
  if (notes->getTable ()->selectionModel () == NULL)
    return;
  if (sel.empty ())
    {
      actionEdit_Note->setEnabled (false);
      actionDelete_Note->setEnabled (false);
      actionDetails_Note->setEnabled (false);
      return;
    }
  if (sel.at (0).top () >= 0 )
    {
      row = sel.at (0).top ();
    }
  else
    {
      row = 0;
    }
  QDomNode ent = control->getNoteModel ()->getEntity (row);
  if (ent.isNull ())
    {
      actionEdit_Note->setEnabled (false);
      actionDelete_Note->setEnabled (false);
      actionDetails_Note->setEnabled (false);
      return;
    }
  actionEdit_Note->setEnabled (true);
  actionDelete_Note->setEnabled (true);
  actionDetails_Note->setEnabled (true);
}


/**
 * @brief SLOT to enable/disable action buttons in override widget.
 *
 * @param[in]  sel    new selected index
 * @param[in]  desel  previous selected index
 */
void
gsd_mw::override_selectionchanged (const QItemSelection &sel,
                                   const QItemSelection &desel)
{
  int row = -1;
  if (overrides->getTable ()->selectionModel () == NULL)
    return;
  if (sel.empty ())
    {
      actionEdit_Override->setEnabled (false);
      actionDelete_Override->setEnabled (false);
      actionDetails_Override->setEnabled (false);
      return;
    }
  if (sel.at (0).top () >= 0 )
    {
      row = sel.at (0).top ();
    }
  else
    {
      row = 0;
    }
  QDomNode ent = control->getOverrideModel ()->getEntity (row);
  if (ent.isNull ())
    {
      actionEdit_Override->setEnabled (false);
      actionDelete_Override->setEnabled (false);
      actionDetails_Override->setEnabled (false);
      return;
    }
  actionEdit_Override->setEnabled (true);
  actionDelete_Override->setEnabled (true);
  actionDetails_Override->setEnabled (true);
}


/**
 * @brief SLOT to enable/disable action buttons in slave widget.
 *
 * @param[in]  sel    new selected index
 * @param[in]  desel  previous selected index
 */
void
gsd_mw::slave_selectionchanged (const QItemSelection &sel,
                                   const QItemSelection &desel)
{
  int row = -1;
  if (slaves->getTable ()->selectionModel () == NULL)
    return;
  if (sel.empty ())
    {
      actionDelete_Slave->setEnabled (false);
      actionDetails_Slave->setEnabled (false);
      return;
    }
  if (sel.at (0).top () >= 0 )
    {
      row = sel.at (0).top ();
    }
  else
    {
      row = 0;
    }
  QDomNode ent = control->getSlaveModel ()->getEntity (row);
  if (ent.isNull ())
    {
      actionDelete_Slave->setEnabled (false);
      actionDetails_Slave->setEnabled (false);
      return;
    }
  actionDelete_Slave->setEnabled (true);
  actionDetails_Slave->setEnabled (true);
}


/**
 * @brief SLOT to enable/disable action buttons in portlists widget.
 *
 * @param[in]  sel    new selected index.
 * @param[in]  desel  previous selected index.
 */
void
gsd_mw::portlist_selectionchanged(const QItemSelection &sel,
                                  const QItemSelection &desel)
{
  int row = -1;
  if (portlists->getTable ()->selectionModel () == NULL)
    return;
  if (sel.empty ())
    {
      actionDelete_PortList->setEnabled (false);
      actionDetails_PortList->setEnabled (false);
      return;
    }
  if (sel.at (0).top () >= 0 )
    {
      row = sel.at (0).top ();
    }
  else
    {
      row = 0;
    }
  QDomElement ent = control->getPortListModel ()->getEntity (row);
  if (ent.isNull ())
    {
      actionDelete_PortList->setEnabled (false);
      actionDetails_PortList->setEnabled (false);
      return;
    }
  if (control->getPortListModel ()->getValue (ent, "in_use") == "1" ||
      control->getPortListModel ()->getValue (ent, "writable") == "0")
    {
      actionDelete_PortList->setEnabled (false);
    }
  else
    {
      actionDelete_PortList->setEnabled (true);
    }
  actionDetails_PortList->setEnabled (true);
}

/**
 * @brief Eventfiler for task tablewidget. Controls wether action buttons are
 * enabled.
 *
 * @param[in]  obj  Unused
 * @param[in]  e    Event that indicates if task tableview gained or lost focus.
 *
 * @return false.
 */
bool
gsd_mw::eventFilter (QObject *obj, QEvent *e)
{
  if (e->type () == QEvent::ToolTip)
    {
      QHelpEvent *he = static_cast <QHelpEvent *> (e);
      QWidget *widget = QApplication::focusWidget ();
      QPoint pos = he->pos ();
      pos.setY (pos.y ()-30);

      if (widget->objectName ().compare ("tw_tasks") == 0)
        {
         if (tw_tasks->indexAt (pos).isValid ())
            {
              tasks->showToolTip (pos,he->globalPos ());
            }
        }
      if (widget->objectName ().compare ("tw_schedules") == 0)
        {
          if (schedules->getTable ()->indexAt (pos).isValid ())
            {
              schedules->showToolTip (pos, he->globalPos ());
            }
        }
      if (widget->objectName ().compare ("tw_targets") == 0)
        {
          if (targets->getTable ()->indexAt (pos).isValid ())
            {
              targets->showToolTip (pos, he->globalPos ());
            }
        }
      if (widget->objectName ().compare ("tw_configs") == 0)
        {
          if (configs->getTable ()->indexAt (pos).isValid ())
            {
              configs->showToolTip (pos, he->globalPos ());
            }
        }
      if (widget->objectName ().compare ("tw_escalators") == 0)
        {
          if (escalators->getTable ()->indexAt (pos).isValid ())
            {
              escalators->showToolTip (pos, he->globalPos ());
            }
        }
      if (widget->objectName ().compare ("tw_credentials") == 0)
        {
          if (credentials->getTable ()->indexAt (pos).isValid ())
            {
              credentials->showToolTip (pos, he->globalPos ());
            }
        }
      if (widget->objectName ().compare ("tw_agents") == 0)
        {
          if (agents->getTable ()->indexAt (pos).isValid ())
            {
              agents->showToolTip (pos, he->globalPos ());
            }
        }
    }
  return false;
}


/**
 * @brief SLOT called if authentication with the manager fails.
 *
 * @param[in]  err  Error code.
 */
void
gsd_mw::authentication_failed (int err)
{
  if (err == -1)
    {
      QMessageBox::information (NULL,tr ("Login Error"),
                                tr ("Error while connecting to manager!"
                                    "\nPlease check server"
                                    " address and port!"));
      emit sig_login ();
    }
  else if (err == -5)
    {
      QMessageBox::information (NULL,tr ("Login Error"),
                                tr ("Manager closed connection!"));
      emit sig_login ();
    }
  else if (err == 2)
    {
      QMessageBox::information (NULL,tr ("Login Error"),
                                tr ("Authentication failed!\n"
                                    "Please check username and password!"));
      emit sig_login ();
    }
}


/**
 * @brief Selects a row in the table and sets focus to this table.
 *
 * @param[in]  table   Tableview.
 * @param[in]  model   Model containing data.
 */
void
gsd_mw::selectRow (QTableView *table, model_omp_entity *model)
{
  int ndx = -1;
  if (table->selectionModel () == NULL)
    return;
  QModelIndexList selected = table->selectionModel ()->selectedRows (0);
  if (selected.empty ())
    {
      ndx = 0;
    }
  else
    {
      ndx = selected.at (0).row ();
    }
  //prepare selection range, row = index, all columns
  int col = model->columnCount ();
  QModelIndex start;
  QModelIndex end;
  start = model->index (ndx, 0, QModelIndex ());
  end = model->index (ndx, col - 1, QModelIndex ());
  QItemSelection sel (start, end);
  //set selection
  table->selectionModel ()->select (sel, QItemSelectionModel::Select);
  QWidget *viewport = table->viewport ();
}


void
gsd_mw::update_stopped()
{}


void
gsd_mw::modify_config_dlg()
{}


/**
 * @brief SLOT to trigger with the update interval.
 */
void
gsd_mw::update_trigger()
{
  pbar_update->setValue (0);
  pbar_update->setMaximum (updateInterval);
  QString form;
  form.setNum (updateInterval);
  form.append (tr (" sec"));
  pbar_update->setFormat (form);

}


void
gsd_mw::update_started()
{
}

void
gsd_mw::update_finished()
{
}


/**
 * @brief SLOT to make the progress icon visible.
 */
void
gsd_mw::in_progress ()
{
  la_status_progress->setVisible (true);
}


/**
 * @brief SLOT to hide the progress icon.
 */
void
gsd_mw::progress_ready ()
{
  la_status_progress->setVisible (false);
}


/**
 * @brief Modify the update progress bar every second.
 */
void
gsd_mw::progress_timeout()
{
  int p = pbar_update->value ();
  pbar_update->setValue ( ++p );
  int format_value = pbar_update->maximum () - p;
  if (format_value <= 0)
    {
      format_value = 0;
    }
  QString form;
  form.setNum (format_value);
  form.append (tr (" sec"));
  pbar_update->setFormat (form);

}


/**
 * @brief Deletes a selected task.
 */
void
gsd_mw::task_delete()
{
  QModelIndexList selected = tasks->getTable ()->selectionModel ()
                                                ->selectedRows (0);
  if (selected.at (0).isValid ())
    emit sig_delete_task (selected.at (0).row ());
  else
    QMessageBox::information (NULL,tr ("Status Error"),
                                tr ("No task selected!"));
}


void
gsd_mw::system_report_download(QString, int)
{}


/**
 * @brief Show update widget.
 */
void
gsd_mw::update_dw()
{
  dw_update->show ();
}


/**
 * @brief Perform actions on application close.
 *
 * @param[in]  event  Close event.
 */
void
gsd_mw::closeEvent (QCloseEvent *event)
{
  write_settings ();
  QFile f (QDir::currentPath ()+"/.report.html");
  if (f.exists ())
    f.remove ();
}


/**
 * @brief Save current application and widget settings.
 */
void
gsd_mw::write_settings ()
{
  QSettings settings ("Greenbone", "GSD");
  settings.setValue ("version", GSD_VERSION);
  settings.beginGroup ("MainWindow");
  settings.setValue ("size", size ());
  settings.setValue ("position", pos ());
  settings.setValue ("state", saveState ());
  settings.setValue ("language", lang);
  settings.endGroup ();
}


/**
 * @brief Restore last application and widget settings
 */
void
gsd_mw::read_settings ()
{
  QSettings settings ("Greenbone", "GSD");
  settings.beginGroup ("MainWindow");
  resize (settings.value ("size", QSize (770, 550)).toSize ());
  move (settings.value ("position", QPoint (0,0)).toPoint ());
  restoreState (settings.value ("state").toByteArray ());
  settings.endGroup ();
}


/**
 * @brief Show report widget.
 *
 * @param[in]  id    Report id.
 * @param[in]  task  Task id.
 */
void
gsd_mw::show_report (QString id, QString task)
{
  if (details_widgets.contains (id))
    {
      details_widgets.value (id)->show ();
      details_widgets.value (id)->raise ();

    }
  else
    {
      dock_reports *reports = new dock_reports (this->control);
      if (details_widgets.size () > 0)
        {
          QMapIterator <QString, dock_details*> i (details_widgets);
          i.toFront ();
          i.next ();
          dock_details *tmp = i.value ();
          if (i.hasNext ())
            tmp = i.next ().value ();
          if (tmp->isVisible () && !tmp->isFloating ())
            this->tabifyDockWidget (tmp, reports);
          else
            this->addDockWidget (Qt::BottomDockWidgetArea,
                                 reports);
       }
      else
        this->addDockWidget (Qt::BottomDockWidgetArea, reports);

      connect (reports,
               SIGNAL (sig_details_nvt (QString, QString)),
               this,
               SLOT (details_nvt_dlg (QString, QString)));

      details_widgets.insert (id, reports);
      connect (control,
               SIGNAL (sig_report_finished (QString)),
               reports,
               SLOT (update (QString)));
      connect (control,
               SIGNAL (sig_report_format_finished ()),
               reports,
               SLOT (updateReportFormats ()));
      connect (reports,
               SIGNAL (sig_request_report (QMap<QString, QString>)),
               control,
               SLOT (request_report (QMap<QString, QString>)));
      connect (reports,
               SIGNAL (sig_report_download (QMap<QString, QString>)),
               control,
               SLOT (report_download (QMap<QString, QString>)));
      connect (control,
               SIGNAL (sig_request_report ()),
               reports,
               SLOT (request_update ()));
      reports->setTask (task);
      reports->setId (id);
      reports->load ();
      reports->show ();
      reports->raise ();
    }
}


/**
 * @brief Clears setting for dock widgets.
 */
void
gsd_mw::clear_dock_widget_settings ()
{
  foreach (dock_details *widget, details_widgets)
    {
      if (widget->isVisible () &&
          this->dockWidgetArea (widget) != Qt::BottomDockWidgetArea)
        {
          bool found = false;
          QMapIterator <QString, dock_details*> i (details_widgets);
          while (i.hasNext ())
            {
              i.next ();
              int usedArea = this->dockWidgetArea (i.value ());
              if (usedArea == Qt::BottomDockWidgetArea &&
                  i.value ()->isVisible ())
                {
                  this->tabifyDockWidget (i.value (), widget);
                  found = true;
                  break;
                }
            }
          if (!found)
            this->addDockWidget (Qt::BottomDockWidgetArea, widget);
        }
    }
}



void
gsd_mw::setLangDefault ()
{
  actionEnglish->setChecked (false);
  actionGerman->setChecked (false);
  actionFrench->setChecked (false);
  actionIndonesian->setChecked (false);
  actionSpanish->setChecked (false);
  actionSystem->setChecked (true);
  lang = "default";
}

void
gsd_mw::setLangEnglish ()
{
  actionSystem->setChecked (false);
  actionGerman->setChecked (false);
  actionFrench->setChecked (false);
  actionEnglish->setChecked (true);
  actionIndonesian->setChecked (false);
  actionSpanish->setChecked (false);
  lang = "en";
}

void
gsd_mw::setLangGerman ()
{
  actionEnglish->setChecked (false);
  actionSystem->setChecked (false);
  actionFrench->setChecked (false);
  actionGerman->setChecked (true);
  actionIndonesian->setChecked (false);
  actionSpanish->setChecked (false);
  lang = "de";
}

void
gsd_mw::setLangFrench ()
{
  actionEnglish->setChecked (false);
  actionSystem->setChecked (false);
  actionGerman->setChecked (false);
  actionFrench->setChecked (true);
  actionIndonesian->setChecked (false);
  actionSpanish->setChecked (false);
  lang = "fr";
}

void
gsd_mw::setLangIndonesian ()
{
  actionEnglish->setChecked (false);
  actionSystem->setChecked (false);
  actionGerman->setChecked (false);
  actionFrench->setChecked (false);
  actionIndonesian->setChecked (true);
  actionSpanish->setChecked (false);
  lang = "id";
}

void
gsd_mw::setLangSpanish ()
{
  actionEnglish->setChecked (false);
  actionSystem->setChecked (false);
  actionGerman->setChecked (false);
  actionFrench->setChecked (false);
  actionIndonesian->setChecked (false);
  actionSpanish->setChecked (true);
  lang = "es";
}


void
gsd_mw::setLanguage (QString l)
{
  if (l.contains ("_"))
    {
      setLangDefault ();
    }
  else if (l.compare ("en") == 0)
    {
      setLangEnglish ();
    }
  else if (l.compare ("de") == 0)
    {
      setLangGerman ();
    }
  else if (l.compare ("fr") == 0)
    {
      setLangFrench ();
    }
  else if (l.compare ("id") == 0)
    {
      setLangIndonesian ();
    }
  else if (l.compare ("es") == 0)
    {
      setLangSpanish ();
    }
}


void
gsd_mw::languageChanged ()
{
  QMessageBox::information (this, tr ("Language Selection"), tr ("This setting"
                         " takes effect after restarting the application."));
}


/**
 * Dashboard
 */
void
gsd_mw::updateDashboard ()
{
  bool ok;
  QDomElement elem = control->getTaskModel ()->getEntity (0);
  int total = 0;
  int running = 0;
  int done = 0;
  int newtask = 0;
  int progress = 0;
  int error = 0;

  int high = 0;
  int medium = 0;
  int low = 0;
  int none = 0;

  int up = 0;
  int more = 0;
  int same = 0;
  int less = 0;
  int down = 0;
  int unknown = 0;

  int high_all = 0;
  int medium_all = 0;
  int low_all = 0;
  int fp_all = 0;

  QList<QDomElement> top5;
  for (int i = 0; i < control->getTaskModel ()->rowCount (); i++)
    {
      total++;
      QDomElement elem = control->getTaskModel ()->getEntity (i);
      if (control->getTaskModel ()->getValue (elem, "status")
                                     .compare ("Running") == 0)
        running++;
      if (control->getTaskModel ()->getValue (elem, "status")
                                     .compare ("Stopped") == 0 ||
          control->getTaskModel ()->getValue (elem, "status")
                                     .contains ("Requested") ||
          control->getTaskModel ()->getValue (elem, "status")
                                     .compare ("Paused") == 0)
        progress++;
      if (control->getTaskModel ()->getValue (elem, "status")
                                     .compare ("Done") == 0)
        done++;
      if (control->getTaskModel ()->getValue (elem, "status")
                                     .compare ("New") == 0)
        newtask++;
      if (control->getTaskModel ()->getValue (elem, "status")
                                     .compare ("Internal Error") == 0)
        error++;

      QString s = control->getTaskModel ()->getValue (elem, "last_report report"
                                                      " result_count hole");
      int t_hole = s.toInt (&ok, 10);
      s = control->getTaskModel ()->getValue (elem, "last_report report"
                                              " result_count warning");
      int t_warn = s.toInt (&ok, 10);
      s = control->getTaskModel ()->getValue (elem, "last_report report"
                                              " result_count info");
      int t_low = s.toInt (&ok, 10);
      s = control->getTaskModel ()->getValue (elem, "last_report report"
                                              " result_count false_positive");
      int t_false_pos = s.toInt (&ok, 10);

      high_all += t_hole;
      medium_all += t_warn;
      low_all += t_low;
      fp_all += t_false_pos;

      if (t_hole > 0)
        high++;
      else if (t_warn > 0)
        medium++;
      else if (t_low > 0)
        low++;
      else
        none++;

      QString t = control->getTaskModel ()->getValue (elem, "trend");
      if (t.compare ("up") == 0)
        up++;
      else if (t.compare ("more") == 0)
        more++;
      else if (t.compare ("same") == 0)
        same++;
      else if (t.compare ("less") == 0)
        less++;
      else if (t.compare ("down") == 0)
        down++;
      else
        unknown++;

      top5.insert (top5.size (), elem);
    }

  //sort high
  model_omp_entity *tmp = control->getTaskModel ();
  QString path = "last_report report result_count hole";
  int i = 0;
  while (i < top5.size () - 1)
    {
      if (tmp->getValue (top5.at (i), path).toInt (&ok, 10) <
          tmp->getValue (top5.at (i+1), path).toInt (&ok, 10))
        {
          top5.move (i+1, i);
          i = 0;
        }
      else
        i++;
    }

  //sort medium
  QString path_m = "last_report report result_count warning";
  i = 0;
  while (i < top5.size () - 1)
    {
      if (tmp->getValue (top5.at (i), path).toInt (&ok, 10) ==
          tmp->getValue (top5.at (i+1), path).toInt (&ok, 10))
        {
          if (tmp->getValue (top5.at (i), path_m).toInt (&ok, 10) <
              tmp->getValue (top5.at (i+1), path_m).toInt (&ok, 10))
            {
              top5.move (i+1, i);
              i = 0;
            }
          else
            i++;
        }
      else
        i++;
    }

  //sort low
  QString path_l = "last_report report result_count info";
  i = 0;
  while (i < top5.size () - 1)
    {
      if ((tmp->getValue (top5.at (i), path).toInt (&ok, 10) ==
          tmp->getValue (top5.at (i+1), path).toInt (&ok, 10)) &&
          (tmp->getValue (top5.at (i), path_m).toInt (&ok, 10) ==
           tmp->getValue (top5.at (i+1), path_m).toInt (&ok, 10)))
        {
          if (tmp->getValue (top5.at (i), path_l).toInt (&ok, 10) <
              tmp->getValue (top5.at (i+1), path_l).toInt (&ok, 10))
            {
              top5.move (i+1, i);
              i = 0;
            }
          else
            i++;
        }
      else
        i++;
    }

#if defined(WIN32) || defined(__APPLE__)
  QFile file1 ("dash_task.html");
  QFile file2 ("dash_trend.html");
  QFile file3 ("dash_task_count.html");
  QFile file4 ("dash_resources.html");
  QFile file5 ("dash_vulnerabilities.html");
  QFile file6 ("dash_task_top5.html");
#else
  QFile file1 (QString (OPENVAS_DATA_DIR).append ("/dash_task.html"));
  QFile file2 (QString (OPENVAS_DATA_DIR).append ("/dash_trend.html"));
  QFile file3 (QString (OPENVAS_DATA_DIR).append ("/dash_task_count.html"));
  QFile file4 (QString (OPENVAS_DATA_DIR).append ("/dash_resources.html"));
  QFile file5 (QString (OPENVAS_DATA_DIR).append ("/dash_vulnerabilities.html"));
  QFile file6 (QString (OPENVAS_DATA_DIR).append ("/dash_task_top5.html"));
#endif

  QString tasks, trends, task_count, resources, color, label,
          vulnerabilities, color_v, label_v, task_top5;

  if (!file1.open (QIODevice::ReadOnly))
    {
      //set fallback text to webwidgets
      return;
    }
  if (!file2.open (QIODevice::ReadOnly))
    {
      //set fallback text to webwidgets
      return;
    }

  if (!file3.open (QIODevice::ReadOnly))
    {
      return;
    }

  if (!file4.open (QIODevice::ReadOnly))
    {
      return;
    }
  if (!file5.open (QIODevice::ReadOnly))
    {
      return;
    }
  if (!file6.open (QIODevice::ReadOnly))
    {
      return;
    }
  QTextStream stream1 (&file1);
  tasks = stream1.readAll ();

  QTextStream stream2 (&file2);
  trends = stream2.readAll ();

  QTextStream stream3 (&file3);
  task_count = stream3.readAll ();

  QTextStream stream4 (&file4);
  resources = stream4.readAll ();

  QTextStream stream5 (&file5);
  vulnerabilities = stream5.readAll ();

  QTextStream stream6 (&file6);
  task_top5 = stream6.readAll ();


  QString data_tasks;
  if (high != 0)
    {
      data_tasks.append (QString ("%1, ").arg (high));
      color.append ("\"#ff3101\", ");
      label.append ("\"High\", ");
    }
  if (medium != 0)
    {
      data_tasks.append (QString ("%1, ").arg (medium));
      color.append ("\"#ffc400\", ");
      label.append ("\"Medium\", ");
    }
  if (low != 0)
    {
      data_tasks.append (QString ("%1, ").arg (low));
      color.append ("\"#8bc5ed\", ");
      label.append ("\"Low\", ");
    }
  if (none != 0)
    {
      data_tasks.append (QString ("%1").arg (none));
      color.append ("\"#ffffff\"");
      label.append ("\"None\"");
    }

  if (high_all > 0)
    {
      color_v.append ("\"#ff3101\", ");
      label_v.append ("\"High\", ");
    }
  if (medium_all > 0)
    {
      color_v.append ("\"#ffc400\", ");
      label_v.append ("\"Medium\", ");
    }
  if (low_all > 0)
    {
      color_v.append ("\"#8bc5ed\", ");
      label_v.append ("\"Low\", ");
    }
  if (fp_all > 0)
    {
      color_v.append ("\"#e5e5e5\"");
      label_v.append ("\"False Positive\"");
    }
  QString data_trends = QString ("%1, %2, %3, %4, %5, %6").arg (up)
                                                          .arg (more)
                                                          .arg (same)
                                                          .arg (less)
                                                          .arg (down)
                                                          .arg (unknown);
  QString data_count = QString ("%1, %2, %3, %4, %5, %6").arg (total)
                                                         .arg (running)
                                                         .arg (progress)
                                                         .arg (done)
                                                         .arg (newtask)
                                                         .arg (error);
  QString data_resources = QString ("%1, %2, %3, %4, %5, %6, %7, %8")
                            .arg (control->getTargetModel ()->rowCount ())
                            .arg (control->getConfigModel ()->rowCount ())
                            .arg (control->getScheduleModel ()->rowCount ())
                            .arg (control->getEscalatorModel ()->rowCount ())
                            .arg (control->getCredentialModel ()->rowCount ())
                            .arg (control->getAgentModel ()->rowCount ())
                            .arg (control->getOverrideModel ()->rowCount ())
                            .arg (control->getNoteModel ()->rowCount ());
  QString data_vulnerabilities = QString ("%1, %2, %3, %4").arg (high_all)
                                                           .arg (medium_all)
                                                           .arg (low_all)
                                                           .arg (fp_all);

  QStringList val;
  QStringList name;
  QStringList col;
  for (int ndx = 0; ndx < 5 && ndx < tmp->rowCount (); ndx++)
    {
      if (tmp->getValue (top5.at (ndx), path).toInt (&ok, 10) > 0)
        {
          val.append (tmp->getValue (top5.at (ndx), path));
          name.append (tmp->getValue (top5.at (ndx), "name"));
          col.append ("\"#ff3101\"");
        }
      else if (tmp->getValue (top5.at (ndx), path_m).toInt (&ok, 10) > 0)
        {
          val.append (tmp->getValue (top5.at (ndx), path_m));
          name.append (tmp->getValue (top5.at (ndx), "name"));
          col.append ("\"#eeb900\"");
        }
      else
        {
          val.append (tmp->getValue (top5.at (ndx), path_l));
          name.append (tmp->getValue (top5.at (ndx), "name"));
          col.append ("\"#8bc5ed\"");
        }
    }

  for (int ndx = 0; ndx < name.size (); ndx++)
    {
      if (name.at (ndx).length () > 20)
        {
          QString temp = name.at (ndx);
          temp.remove (17, name.at (ndx).length () - 17);
          temp.append ("...");
          name.replace (ndx, temp);
        }
    }

  QString data_top5_val;
  QString data_top5_col;
  for (int ndx = 0; ndx < val.size (); ndx++)
    {
      if (ndx == val.size () - 1)
        {
          data_top5_val.append (QString ("%1").arg (val[ndx]));
          data_top5_col.append (QString ("%1").arg (col[ndx]));
        }
      else
        {
          data_top5_val.append (QString ("%1, ").arg (val[ndx]));
          data_top5_col.append (QString ("%1, ").arg (col[ndx]));
        }
      QString s = QString ("__name%1__").arg (ndx+1);
      task_top5.replace (s, name[ndx]);
    }
  tasks.replace ("__data__", data_tasks);
  tasks.replace ("__color__", color);
  tasks.replace ("__labels__", label);
  trends.replace ("__data__", data_trends);
  task_count.replace ("__data__", data_count);
  resources.replace ("__data__", data_resources);
  vulnerabilities.replace ("__data__", data_vulnerabilities);
  vulnerabilities.replace ("__color__", color_v);
  vulnerabilities.replace ("__labels__", label_v);
  task_top5.replace ("__data__", data_top5_val);
  task_top5.replace ("__color__", data_top5_col);

  //Replace translated strings
  tasks.replace ("__title__", tr ("Scan Tasks"));

  trends.replace ("__title__", tr ("Trends"));

  task_count.replace ("__title__", tr ("Task Overview"));
  task_count.replace ("__total__", tr ("Total"));
  task_count.replace ("__running__", tr ("Running"));
  task_count.replace ("__progress__", tr ("Progress"));
  task_count.replace ("__done__", tr ("Done"));
  task_count.replace ("__new__", tr ("New"));
  task_count.replace ("__error__", tr ("Error"));

  resources.replace ("__title__", tr ("Resources Overview"));
  resources.replace ("__targets__", tr ("Targets"));
  resources.replace ("__configs__", tr ("Scan Configs"));
  resources.replace ("__schedules__", tr ("Schedules"));
  resources.replace ("__escalators__", tr ("Escalators"));
  resources.replace ("__credentials__", tr ("Credentials"));
  resources.replace ("__agents__", tr ("Agents"));
  resources.replace ("__overrides__", tr ("Overrides"));
  resources.replace ("__notes__", tr ("Notes"));

  vulnerabilities.replace ("__title__", tr ("Vulnerabilities"));

  task_top5.replace ("__title__", tr ("Top 5 Tasks"));

  wv_tasks->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal,
                                                    Qt::ScrollBarAlwaysOff);
  wv_tasks->page()->mainFrame()->setScrollBarPolicy(Qt::Vertical,
                                                    Qt::ScrollBarAlwaysOff);
  wv_trends->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal,
                                                     Qt::ScrollBarAlwaysOff);
  wv_trends->page()->mainFrame()->setScrollBarPolicy(Qt::Vertical,
                                                         Qt::ScrollBarAlwaysOff);
  wv_task_count->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal,
                                                         Qt::ScrollBarAlwaysOff);
  wv_task_count->page()->mainFrame()->setScrollBarPolicy(Qt::Vertical,
                                                         Qt::ScrollBarAlwaysOff);
  wv_resources->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal,
                                                         Qt::ScrollBarAlwaysOff);
  wv_resources->page()->mainFrame()->setScrollBarPolicy(Qt::Vertical,
                                                         Qt::ScrollBarAlwaysOff);
  wv_vulnerabilities->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal,
                                                              Qt::ScrollBarAlwaysOff);
  wv_vulnerabilities->page()->mainFrame()->setScrollBarPolicy(Qt::Vertical,
                                                         Qt::ScrollBarAlwaysOff);
  wv_task_top5->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal,
                                                         Qt::ScrollBarAlwaysOff);
  wv_task_top5->page()->mainFrame()->setScrollBarPolicy(Qt::Vertical,
                                                         Qt::ScrollBarAlwaysOff);
  wv_tasks->setHtml (tasks);
  wv_trends->setHtml (trends);
  wv_task_count->setHtml (task_count);
  wv_resources->setHtml (resources);
  wv_vulnerabilities->setHtml (vulnerabilities);
  wv_task_top5->setHtml (task_top5);
}

void
gsd_mw::clearDashboard ()
{
  wv_tasks->setHtml ("");
  wv_task_count->setHtml ("");
  wv_trends->setHtml ("");
  wv_resources->setHtml ("");
  wv_vulnerabilities->setHtml ("");
  wv_task_top5->setHtml ("");
}
