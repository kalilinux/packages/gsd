/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "omp_string_builder.h"

/**
 * @file omp_string_builder.cpp
 * @brief This class can be used to build omp commands.
 *        Note that not all commands are implemented jet.
 */

omp_string_builder::omp_string_builder ()
{
}

omp_string_builder::~omp_string_builder ()
{
}


/**
 * @brief Create an omp conform string to request an omp entity.
 *
 * @param[in]   type      omp entity type.
 * @param[in]   parameter omp paramter
 *
 * @return omp conform string or a empty string.
 */
QString
omp_string_builder::requestString (omp_utilities::omp_type type,
                                   QMap<QString, QString> parameter)
{
  if (type == omp_utilities::NONE)
    return QString ("");

  QVariant var_prot_version = qApp->property("protocol_version");
  float protocol_version = -1;
  if (var_prot_version.isValid())
    protocol_version = var_prot_version.toFloat();

  QString command ("<get_");
  switch (type)
    {
      case omp_utilities::AGENT: command.append ("agents ");
                                 break;
      case omp_utilities::CONFIG: command.append ("configs ");
                                  break;
      case omp_utilities::DEPENDENCY: command.append ("dependencies ");
                                      break;
      case omp_utilities::ESCALATOR: if (protocol_version <= 3)
                                         command.append ("escalators ");
                                     else
                                         command.append ("alerts ");
                                     break;
      case omp_utilities::LSC_CREDENTIAL: command.append ("lsc_credentials ");
                                          break;
      case omp_utilities::NOTE: command.append ("notes ");
                                break;
      case omp_utilities::NVT: command.append ("nvts ");
                               break;
      case omp_utilities::NVT_FAMILY: command.append ("nvt_families ");
                                      break;
      case omp_utilities::NVT_FEED_CHECKSUM: command.append ("nvt_feed_i"
                                                             "checksum ");
                                             break;
      case omp_utilities::OVERRIDE: command.append ("overrides ");
                                    break;
      case omp_utilities::SLAVE: command.append ("slaves");
                                 break;
      case omp_utilities::PREFERENCE: command.append ("preferences ");
                                      break;
      case omp_utilities::REPORT: command.append ("reports ");
                                  break;
      case omp_utilities::RESULT: command.append ("results ");
                                  break;
      case omp_utilities::SCHEDULE: command.append ("schedules ");
                                    break;
      case omp_utilities::SYSTEM_REPORT: command.append ("system_reports ");
                                         break;
      case omp_utilities::TARGET_LOCATOR: command.append ("target_locators ");
                                          break;
      case omp_utilities::TARGET: command.append ("targets ");
                                  break;
      case omp_utilities::TASK: command.append ("tasks ");
                                break;
      case omp_utilities::VERSION: command.append ("version ");
                                   break;
      case omp_utilities::REPORT_FORMAT: command.append ("report_formats");
                                         break;
      case omp_utilities::PORT_LIST: command.append ("port_lists ");
                                         break;
      default: return QString ();
    }

  if (parameter.size ()> 0)
    {
      QList <QString> attributes = parameter.keys ();
      int i = 0;
      while (i < attributes.size ())
        {
          command.append (attributes.at (i));
          command.append ("=\"");
          command.append (parameter.take (attributes.at (i)));
          command.append ("\" ");
          i++;
        }
    }

  command.append ("/>");

  return command;
}


/**
 * @brief Create an omp conform string to create an omp entity.
 *
 * @param[in]   type      omp entity type.
 * @param[in]   parameter omp paramter
 *
 * @return omp conform string or a empty string.
 */
QString
omp_string_builder::createString (omp_utilities::omp_type type,
                                  QMap<QString, QString> parameter)
{
  if (type == omp_utilities::NONE)
    return QString ("");

  QVariant var_prot_version = qApp->property("protocol_version");
  float protocol_version = -1;
  if (var_prot_version.isValid())
    protocol_version = var_prot_version.toFloat();

  QString command ("<create_");
  QString typeStr;
  switch (type)
    {
      case omp_utilities::AGENT: typeStr ="agent ";
                                 break;
      case omp_utilities::CONFIG: typeStr = "config ";
                                  break;
      case omp_utilities::ESCALATOR: if (protocol_version <= 3)
                                       typeStr = "escalator ";
                                     else
                                       typeStr = "alert ";
                                     break;
      case omp_utilities::LSC_CREDENTIAL: typeStr = "lsc_credential ";
                                          break;
      case omp_utilities::NOTE: typeStr = "note ";
                                break;
      case omp_utilities::OVERRIDE: typeStr = "override ";
                                    break;
      case omp_utilities::SCHEDULE: typeStr = "schedule ";
                                    break;
      case omp_utilities::TARGET: typeStr = "target ";
                                  break;
      case omp_utilities::TASK: typeStr = "task ";
                                break;
      case omp_utilities::SLAVE: typeStr = "slave ";
                                 break;
      case omp_utilities::PORT_LIST: typeStr = "port_list";
                                 break;
      default: return QString ("");
    }
  command.append (typeStr + ">");
  command.append (this->appendCreateParameter (type, parameter));
  command.append (QString ("</create_%1>").arg (typeStr));

  return command;
}


/**
 * @brief Create an omp conform string to delete an omp entity.
 *
 * @param[in]   type      omp entity type.
 * @param[in]   id        entity id
 *
 * @return omp conform string or a empty string.
 */
QString
omp_string_builder::deleteString (omp_utilities::omp_type type,
                                  QString id)
{
  if (type == omp_utilities::NONE)
    return QString ("");

  QVariant var_prot_version = qApp->property("protocol_version");
  float protocol_version = -1;
  if (var_prot_version.isValid())
    protocol_version = var_prot_version.toFloat();

  QString command ("<delete_");
  QString typeString;
  switch (type)
    {
      case omp_utilities::AGENT: typeString = "agent";
                                 break;
      case omp_utilities::CONFIG: typeString = "config";
                                  break;
      case omp_utilities::ESCALATOR: if (protocol_version <= 3)
                                         typeString = "escalator";
                                     else
                                         typeString = "alert";
                                     break;
      case omp_utilities::LSC_CREDENTIAL: typeString = "lsc_credential";
                                          break;
      case omp_utilities::NOTE: typeString = "note";
                                break;
      case omp_utilities::OVERRIDE: typeString = "override";
                                    break;
      case omp_utilities::REPORT: typeString = "report";
                                  break;
      case omp_utilities::SCHEDULE: typeString = "schedule";
                                    break;
      case omp_utilities::TARGET: typeString = "target";
                                  break;
      case omp_utilities::TASK: typeString = "task";
                                break;
      case omp_utilities::SLAVE: typeString = "slave";
                                 break;
      case omp_utilities::PORT_LIST: typeString = "port_list";
                                 break;
      default: return QString ("");
    }

  command.append (typeString);
  command.append (" " + typeString +"_id=\"");
  command.append (id);
  command.append ("\" />");

  return command;
}


/**
 * @brief Create an omp conform string to modify an omp entity.
 *
 * @param[in]   type      omp entity type.
 * @param[in]   parameter omp paramter
 *
 * @return omp conform string or a empty string.
 */
QString
omp_string_builder::modifyString (omp_utilities::omp_type type,
                                  QString id,
                                  QMap<QString, QString> parameter,
                                  model_omp_entity *m)
{
  if (type == omp_utilities::NONE)
    return QString ("");

  QString command ("<modify_");
  QString typeStr;
  switch (type)
    {
      case omp_utilities::CONFIG: typeStr = "config ";
                                  break;
      case omp_utilities::NOTE: typeStr = "note ";
                                break;
      case omp_utilities::OVERRIDE: typeStr = "override ";
                                    break;
      case omp_utilities::REPORT: typeStr = "report ";
                                    break;
      case omp_utilities::TASK: typeStr = "task ";
                                break;
      case omp_utilities::LSC_CREDENTIAL: typeStr = "lsc_credential";
                                          break;
      default: return QString ("");
    }
  command.append (typeStr);
//TODO  validateModificationParameter (type, parameter);
  command.append (appendModificationParameter (type, parameter));
  command.append (QString ("</modify_%1>").arg (typeStr));
  if (parameter.contains("nvt family name"))
    command = appendConfigParameter (command, parameter ["nvt family name"], m);
  else if (parameter.contains ("nvt selection"))
    command = appendConfigParameter (command, parameter ["nvt oid"], m);
  return command;
}


/**
 * @brief Adds modification parameter to a config string.
 */
QString
omp_string_builder::appendConfigParameter (QString command,
                                           QString name,
                                           model_omp_entity *m)
{
  if (!command.endsWith ("</modify_config >"))
    {
    return QString ("");
    }
  else
    {
      command.remove ("</modify_config >");
      if (command.endsWith ("</family_selection>"))
        {
          command.remove ("</family_selection>");
          int i = 0;
          while (i < m->rowCount ())
            {

              QDomElement family = m->getEntity (i);
              QString name_f = m->getValue (family, "name");
              if (name_f.compare (name) != 0)
                {
                  QString growing = m->getValue (family, "growing");
                  QString nvt_count = m->getValue (family, "nvt_count");
                  QString max_nvt_count = m->getValue (family, "max_nvt_count");
                  QString nvts;
                  if (nvt_count == max_nvt_count)
                    nvts = "<all>1</all>";
                  else
                    nvts = "<all>0</all>";

                  command.append (QString ("<family><name>%1</name>%2"
                                        "<growing>%3</growing>"
                                        "</family>").arg (name_f)
                                                    .arg (nvts)
                                                    .arg (growing));
                }
              i++;
            }

          command.append ("</family_selection>");
        }
      else if (command.endsWith ("</preference>"))
        {
          //TODO add preferences as needed for a complete and working command.
        }
      else if (command.endsWith ("</nvt_selection>"))
        {
          command.remove ("</nvt_selection>");
          int i = 0;
          while (i < m->rowCount ())
            {
              QDomElement family = m->getEntity (i);
              QString oid_f = m->getAttr (family, "nvt oid");
              if (oid_f.compare (name) != 0)
                {
                  command.append (QString ("<nvt oid=\"%1\"/>").arg (oid_f));
                }
              i++;
            }
          command.append ("</nvt_selection>");
        }
      command.append ("</modify_config>");

      return command;
    }
}


/**
 * @brief Create an omp conform string to modify an omp entity.
 *
 * @param[in]   type      omp entity type.
 * @param[in]   parameter omp paramter
 *
 * @return omp conform string or a empty string.
 */
QString
omp_string_builder::taskString (omp_utilities::omp_task_command type,
                                QString id)
{
  if (id.isNull ())
    return QString ("");

  QString command ("<");

  switch (type)
    {
      case omp_utilities::START: command.append ("resume_or_start_");
                                  break;
      case omp_utilities::STOP: command.append ("stop_");
                                     break;
      case omp_utilities::PAUSE: command.append ("pause_");
                                          break;
      case omp_utilities::RESUME_PAUSED: command.append ("resume_paused_");
                                break;
      case omp_utilities::RESUME_STOPPED: command.append ("resume_stopped_");
                                    break;
      default: return QString ("");
    }

  command.append (QString ("task task_id=\"%1\"/>").arg (id));

  return command;
}


QString
omp_string_builder::appendCreateParameter (omp_utilities::omp_type type,
                                           QMap<QString, QString> parameter)
{
  QVariant var_prot_version = qApp->property("protocol_version");
  float protocol_version = -1;
  if (var_prot_version.isValid())
    protocol_version = var_prot_version.toFloat();

  QString com ("");
  if (type == omp_utilities::TASK)
    {
      if (!parameter.contains ("name") ||
          !parameter.contains ("target") ||
          !parameter.contains ("config"))
        return QString ("");
      else
        {
          QString comment ("");
          QString escalator ("");
          QString schedule ("");
          QString slave ("");
          if (parameter.contains ("comment"))
            comment = QString ("<comment>%1"
                               "</comment>").arg (parameter["comment"]);
          if (protocol_version <= 3)
            {
              if (parameter.contains ("escalator") &&
                  parameter["escalator"].compare ("--") != 0)
                {
                  escalator = QString ("<escalator id=\"%1\">"
                                          "</escalator>")
                                           .arg (parameter ["escalator"]);
                }
            }
          else
            {
              if (parameter.contains ("escalator") &&
                  parameter["escalator"].compare ("--") != 0)
                {
                  escalator = QString ("<alert id=\"%1\">"
                                          "</alert>")
                                           .arg (parameter ["escalator"]);
                }
            }
          if (parameter.contains ("schedule") &&
              parameter["schedule"].compare ("--") != 0)
            schedule = QString ("<schedule id=\"%1\">"
                                 "</schedule>").arg (parameter ["schedule"]);
          if (parameter.contains ("slave") &&
              parameter["slave"].compare ("--") != 0)
            slave = QString ("<slave id=\"%1\">"
                             "</slave>").arg (parameter ["slave"]);

          com =  QString ("<name>%1</name>"
                          "%2"
                          "<config id=\"%3\"/>"
                          "<target id=\"%4\"/>"
                          "%5%6%7").arg (parameter["name"])
                                 .arg (comment)
                                 .arg (parameter["config"])
                                 .arg (parameter["target"])
                                 .arg (schedule)
                                 .arg (escalator)
                                 .arg (slave);
          return com;
        }
    }
  if (type == omp_utilities::TARGET)
    {
      if (!parameter.contains ("name") ||
          !parameter.contains ("hosts"))
        return QString ("");
      else
        {
          QString comment ("");
          QString lsc_sshcredential ("");
          QString lsc_smbcredential ("");
          QString portListparam("");
          if (parameter.contains ("comment"))
            comment = QString ("<comment>%1"
                               "</comment>").arg (parameter["comment"]);

          if (parameter.contains ("ssh_lsc_credential") &&
              parameter["ssh_lsc_credential"].compare ("--") != 0)
            lsc_sshcredential = QString ("<ssh_lsc_credential id=\"%1\">"
                                          "</ssh_lsc_credential>")
                                          .arg (parameter ["ssh_lsc_credential"]);
          if (parameter.contains ("smb_lsc_credential") &&
              parameter["smb_lsc_credential"].compare ("--") != 0)
            lsc_smbcredential = QString ("<smb_lsc_credential id=\"%1\">"
                                          "</smb_lsc_credential>")
                                          .arg (parameter ["smb_lsc_credential"]);
          if (parameter.contains ("port_list") &&
              !parameter ["port_list"].isEmpty())
            portListparam = QString ("<port_list id=\"%1\">"
                                     "</port_list>")
                                     .arg (parameter ["port_list"]);

          com =  QString ("<name>%1</name>"
                          "%2"
                          "<hosts>%3</hosts>"
                          "%4%5%6").arg (parameter["name"])
                                   .arg (comment)
                                   .arg (parameter["hosts"])
                                   .arg (lsc_sshcredential)
                                   .arg (lsc_smbcredential)
                                   .arg (portListparam);
          return com;
        }
    }
  if (type == omp_utilities::CONFIG)
    {
      if (parameter.contains ("upload"))
        {
          com = QString ("%1").arg (parameter["upload"]);
          return com;
        }
      else if (!parameter.contains ("name") ||
               !parameter.contains ("base"))
        return QString ("");
      else
        {
          QString comment ("");
          if (parameter.contains ("comment"))
            comment = QString ("<comment>%1"
                               "</comment>").arg (parameter["comment"]);
            com = QString ("<name>%1</name>"
                           "<copy>%2</copy>"
                           "%3").arg (parameter["name"])
                                .arg (parameter["base"])
                                .arg (comment);
          return com;
        }
    }
  if (type == omp_utilities::SCHEDULE)
    {
      if (!parameter.contains ("name") ||
          !parameter.contains ("minute") ||
          !parameter.contains ("hour") ||
          !parameter.contains ("day") ||
          !parameter.contains ("month") ||
          !parameter.contains ("year") ||
          !parameter.contains ("duration") ||
          !parameter.contains ("period"))
        return QString ("");
      else
        {
          QString comment ("");
          QString d_unit ("");
          QString p_unit ("");
          if (parameter.contains ("comment"))
            comment = QString ("<comment>%1"
                               "</comment>").arg (parameter["comment"]);
          if (parameter.contains ("duration unit"))
            d_unit = QString ("<unit>%1"
                               "</unit>").arg (parameter["duration unit"]);
          if (parameter.contains ("period unit"))
            p_unit = QString ("<unit>%1"
                               "</unit>").arg (parameter["period unit"]);
          com = QString ("<name>%1</name>"
                         "%2"
                         "<first_time>"
                         "<minute>%3</minute>"
                         "<hour>%4</hour>"
                         "<day_of_month>%5</day_of_month>"
                         "<month>%6</month>"
                         "<year>%7</year>"
                         "</first_time>"
                         "<duration>%8%9</duration>"
                         "<period>%10%11</period>").arg (parameter["name"])
                                                   .arg (comment)
                                                   .arg (parameter ["first_min"])
                                                   .arg (parameter ["first_hour"])
                                                   .arg (parameter ["first_day"])
                                                   .arg (parameter ["first_month"])
                                                   .arg (parameter ["first_year"])
                                                   .arg (parameter ["duration"])
                                                   .arg (d_unit)
                                                   .arg (parameter ["period"])
                                                   .arg (p_unit);
          return com;
        }
    }
  if (type == omp_utilities::ESCALATOR)
    {
      if (!parameter.contains ("name") ||
          !parameter.contains ("condition") ||
          !parameter.contains ("condition data") ||
          !parameter.contains ("condition name") ||
          !parameter.contains ("event") ||
          !parameter.contains ("event data") ||
          !parameter.contains ("event name") ||
          !parameter.contains ("method") ||
          !parameter.contains ("method data") ||
          !parameter.contains ("method name"))
        return QString ("");
      else
        {
          QString t_cond ("");
          t_cond.append ("<condition>");
          if (parameter["condition"].compare ("Always") == 0)
            {
              t_cond.append ("Always</condition>");
            }
          else if (parameter["condition"].compare ("Threat level at least")
                   == 0 ||
                   parameter["condition"].compare ("Threat level changed")
                   == 0)
            {
              t_cond.append (QString ("%1"
                          "<data>%2"
                          "<name>%3</name>"
                          "</data>"
                          "</condition>").arg (parameter ["condition"])
                                         .arg (parameter ["condition data"])
                                         .arg (parameter ["condition name"]));
            }
          if (parameter["event"].compare ("Task run status changed") == 0)
            {
              t_cond.append (QString ("<event>%1"
                                      "<data>%2"
                                      "<name>%3</name>"
                                      "</data>"
                                      "</event>").arg (parameter ["event"])
                                        .arg (parameter ["event data"])
                                        .arg (parameter ["event name"]));
            }
          else
            {
              return QString ("");
            }
          if (parameter ["method"].compare ("Email") == 0)
            {
              t_cond.append (QString ("<method>%1"
                                      "<data>%2"
                                      "<name>%3</name>"
                                      "</data>"
                                      "<data>%4"
                                      "<name>%5</name>"
                                      "</data>"
                                      "</method>").arg (parameter ["method"])
                                         .arg (parameter ["method data"])
                                         .arg (parameter ["method name"])
                                         .arg (parameter ["method data 2"])
                                         .arg (parameter ["method name 2"]));
            }
          else if (parameter["method"].compare ("syslog") == 0)
            {
              t_cond.append (QString ("<method>%1</method>")
                                  .arg (parameter ["method"]));
            }
          else if (parameter["method"].compare ("SNMP") == 0)
            {
              t_cond.append (QString ("<method>Syslog"
                                      "<data>%1"
                                      "<name>submethod</name>"
                                      "</data>"
                                      "</method>").arg (parameter ["method"]));
            }
          else
            {
              return QString ("");
            }
          if (parameter.contains("comment"))
            {
              t_cond.append( QString ("<comment>%1"
                              "</comment>").arg (parameter ["comment"]));
            }
          QString com = QString ("<name>%1</name>"
                                 "%2").arg (parameter ["name"])
                                      .arg (t_cond);

          return com;
        }
    }
  if (type == omp_utilities::LSC_CREDENTIAL)
    {
      if (!parameter.contains ("name") ||
          !parameter.contains ("login"))
        return QString ("");
      else
        {
          QString comment ("");
          QString password ("");
          if (parameter.contains ("comment"))
            comment = QString ("<comment>%1"
                               "</comment>").arg (parameter["comment"]);
          if (parameter.contains ("password") &&
              parameter["password"].compare ("") != 0)
             password= QString ("<password>%1"
                                 "</password>").arg (parameter ["password"]);
          else
            password = "";
          com.append (QString ("<name>%1</name>"
                       "%2"
                       "<login>%3</login>"
                       "%4").arg (parameter ["name"])
                            .arg (comment)
                            .arg (parameter ["login"])
                            .arg (password));
          return com;
        }
    }
  if (type == omp_utilities::AGENT)
    {
      if (!parameter.contains ("name") ||
          !parameter.contains ("installer"))
        return QString ("");
      else
        {
          QString comment ("");
          QString signature;
          if (parameter.contains ("comment"))
            comment = QString ("<comment>%1"
                               "</comment>").arg (parameter["comment"]);
          gchar *inst = parameter["installer"].toLatin1 ().data ();
          gchar *b64_inst = openvas_file_read_b64_encode (inst);
          if (parameter.contains ("signature"))
            {
              gchar *sign = parameter["signature"].toLatin1 ().data ();
              gchar *b64_sign = openvas_file_read_b64_encode (sign);
              signature = QString ("<signature>%1</signature>").arg (b64_sign);
            }
          com = QString ("<name>%1</name>"
                         "%2"
                         "<installer>%3%4</installer>").arg (parameter ["name"])
                              .arg (comment)
                              .arg (b64_inst)
                              .arg (signature);
          return com;
        }

    }
  if (type == omp_utilities::NOTE)
    {
      if (!parameter.contains ("text")||
          !parameter.contains ("nvt"))
        return QString ("");
      else
        {
          QString task, hosts, threat, result, port;
          if (parameter.contains ("hosts"))
            hosts = QString ("<hosts>%1</hosts>").arg (parameter["hosts"]);
          if (parameter.contains ("port"))
            port = QString ("<port>%1</port>").arg (parameter["port"]);
          if (parameter.contains ("threat"))
            threat = QString ("<threat>%1</threat>").arg (parameter["threat"]);
          if (parameter.contains ("task"))
            task = QString ("<task id=\"%1\"></task>").arg (parameter["task"]);
          if (parameter.contains ("result"))
            result = QString ("<result id=\"%1\">"
                              "</result>").arg (parameter["result"]);
          com = QString ("<text>%1</text>"
                         "<nvt oid=\"%2\"></nvt>"
                         "%3%4%5%6%7").arg (parameter["text"])
                                      .arg (parameter["nvt"])
                                      .arg (hosts)
                                      .arg (port)
                                      .arg (threat)
                                      .arg (task)
                                      .arg (result);
          return com;
        }
    }
  if (type == omp_utilities::OVERRIDE)
    {
      if (!parameter.contains ("text") ||
          !parameter.contains ("nvt") ||
          !parameter.contains ("new_threat"))
        return QString ("");
      else
        {
          QString task, hosts, threat, result, port;
          if (parameter.contains ("hosts"))
            hosts = QString ("<hosts>%1</hosts>").arg (parameter["hosts"]);
          if (parameter.contains ("port"))
            port = QString ("<port>%1</port>").arg (parameter["port"]);
          if (parameter.contains ("threat"))
            threat = QString ("<threat>%1</threat>").arg (parameter["threat"]);
          if (parameter.contains ("task"))
            task = QString ("<task id=\"%1\"></task>").arg (parameter["task"]);
          if (parameter.contains ("result"))
            result = QString ("<result id=\"%1\">"
                              "</result>").arg (parameter["result"]);
          if (parameter.contains ("new_threat"))
          com = QString ("<text>%1</text>"
                         "<nvt oid=\"%2\"></nvt>"
                         "<new_threat>%3</new_threat>"
                         "%4%5%6%7%8").arg (parameter["text"])
                                      .arg (parameter["nvt"])
                                      .arg (parameter["new_threat"])
                                      .arg (hosts)
                                      .arg (port)
                                      .arg (threat)
                                      .arg (task)
                                      .arg (result);
          return com;
        }
    }
  if (type == omp_utilities::SLAVE)
    {
      if (parameter["name"].compare ("") == 0 ||
          parameter["host"].compare ("") == 0 ||
          parameter["port"].compare ("") == 0 ||
          parameter["login"].compare ("") == 0 ||
          parameter["password"].compare ("") == 0)
        return QString ("");
      else
        {
          com = QString ("<name>%1</name>"
                         "<comment>%2</comment>"
                         "<host>%3</host>"
                         "<port>%4</port>"
                         "<login>%5</login>"
                         "<password>%6</password>").arg (parameter["name"])
                                                   .arg (parameter["comment"])
                                                   .arg (parameter["host"])
                                                   .arg (parameter["port"])
                                                   .arg (parameter["login"])
                                                   .arg (parameter["password"]);
          return com;
        }
    }
  if (type == omp_utilities::PORT_LIST)
    {
      if (parameter["name"].compare ("") == 0 ||
          parameter["port_range"].compare ("") == 0)
        return QString ("");
      else
        {
          com = QString ("<name>%1</name>"
                         "<comment>%2</comment>"
                         "<port_range>%3</port_range>"
                         ).arg (parameter["name"])
                          .arg (parameter["comment"])
                          .arg (parameter["port_range"]);
          return com;
        }
    }
  return QString ("");
}


QString
omp_string_builder::appendModificationParameter (omp_utilities::omp_type type,
                                           QMap<QString, QString> parameter)
{
  QString com ("");
  QVariant var_prot_version = qApp->property("protocol_version");
  float protocol_version = -1;
  if (var_prot_version.isValid())
    protocol_version = var_prot_version.toFloat();
  if (type == omp_utilities::TASK)
    {
      if (!parameter.contains ("task_id"))
        return QString("");
      else
        {
          QString comment ("");
          QString escalator ("");
          QString name ("");
          QString schedule ("");
          QString slave ("");
          com = QString ("task_id=\"%1\">").arg (parameter["task_id"]);
          if (parameter.contains ("comment"))
            comment = QString("<comment>%1"
                              "</comment>").arg(parameter["comment"]);
          if (parameter.contains ("escalator"))
            {
              if (protocol_version <= 3)
                {
                  if (parameter["escalator"].compare ("--") == 0)
                    escalator = "<escalator id=\"0\"/>";
                  else
                    escalator = QString("<escalator"
                              " id=\"%1\"/>").arg(parameter["escalator"]);
                }
              else
                {
                  if (parameter["alert"].compare ("--") == 0)
                    escalator = "<alert id=\"0\"/>";
                  else
                    escalator = QString("<alert"
                              " id=\"%1\"/>").arg(parameter["alert"]);
                }
            }
          if (parameter.contains ("name"))
            name = QString ("<name>%1</name>").arg(parameter["name"]);
          if (parameter.contains ("schedule"))
            {
              if (parameter["schedule"].compare ("--") == 0)
                schedule = "<schedule id=\"0\"/>";
              else
                schedule = QString ("<schedule"
                                    " id=\"%1\"/>").arg(parameter["schedule"]);
            }
          if (parameter.contains ("slave"))
            {
              if (parameter["slave"].compare ("--") == 0)
                slave = "<slave id=\"0\"/>";
              else
                slave = QString ("<slave id=\"%1\"/>").arg(parameter["slave"]);
            }

          com.append (QString ("%1%2%3%4%5").arg (comment)
                                            .arg (escalator)
                                            .arg (name)
                                            .arg (schedule)
                                            .arg (slave));
        }
    }
  if (type == omp_utilities::NOTE)
    {
      if (!parameter.contains ("note id") ||
          !parameter.contains ("text"))
        return QString ("");
      else
        {
          QString hosts ("");
          QString port ("");
          QString threat ("");
          QString task ("");
          QString result ("");
          com = QString ("note_id=\"%1\">").arg (parameter["note id"]);
          if (parameter.contains ("hosts"))
            hosts = QString ("<hosts>%1</hosts>").arg (parameter["hosts"]);
          if (parameter.contains ("port"))
            port = QString ("<port>%1</port>").arg (parameter ["port"]);
          if (parameter.contains ("threat"))
            threat = QString ("<threat>%1</threat>").arg (parameter ["threat"]);
          if (parameter.contains ("task"))
            task = QString ("<task id=\"%1\"></task>").arg (parameter ["task"]);
          if (parameter.contains ("result"))
            result = QString ("<result id=\"%1\"></result>").arg (parameter ["result"]);
          com.append (QString ("<text>%1</text>"
                               "%2%3%4%5%6").arg (parameter ["text"])
                                            .arg (hosts)
                                            .arg (port)
                                            .arg (threat)
                                            .arg (task)
                                            .arg (result));
          return com;
        }
    }
  if (type == omp_utilities::OVERRIDE)
    {
      if (!parameter.contains ("override id") ||
          !parameter.contains ("text"))
        return QString ("");
      else
        {
          QString hosts ("");
          QString port ("");
          QString threat ("");
          QString new_threat ("");
          QString task ("");
          QString result ("");
          com = QString ("override_id=\"%1\">").arg (parameter["override id"]);
          if (parameter.contains ("hosts"))
            hosts = QString ("<hosts>%1</hosts>").arg (parameter["hosts"]);
          if (parameter.contains ("port"))
            port = QString ("<port>%1</port>").arg (parameter ["port"]);
          if (parameter.contains ("threat"))
            threat = QString ("<threat>%1</threat>").arg (parameter ["threat"]);
          if (parameter.contains ("task"))
            task = QString ("<task id=\"%1\"></task>").arg (parameter ["task"]);
          if (parameter.contains ("result"))
            result = QString ("<result id=\"%1\"></result>").arg (parameter ["result"]);
          if (parameter.contains ("new threat"))
            new_threat = QString ("<new_threat>%1"
                                "</new_threat>").arg (parameter ["new threat"]);

          com.append (QString ("<text>%1</text>"
                               "%2%3%4%5%6%7").arg (parameter ["text"])
                                              .arg (hosts)
                                              .arg (port)
                                              .arg (threat)
                                              .arg (new_threat)
                                              .arg (task)
                                              .arg (result));
          return com;
        }
    }
  if (type == omp_utilities::LSC_CREDENTIAL)
    {
      if (!parameter.contains ("id") || parameter["id"].compare ("") == 0)
        return QString ("");
      else
        {
          com.append (QString (" lsc_credential_id=\"%1\">")
                       .arg (parameter["id"]));
          com.append (QString ("<name>%1</name>").arg (parameter["name"]));
          com.append (QString ("<comment>%1</comment>")
                       .arg (parameter["comment"]));
          if (parameter.contains ("login"))
            com.append (QString ("<login>%1</login>").arg (parameter["login"]));
          if (parameter.contains ("password"))
            com.append (QString ("<password>%1</password>")
                         .arg (parameter["password"]));
        }
      return com;
    }
  if (type == omp_utilities::CONFIG)
    {
      if (!parameter.contains ("id") ||
          !(parameter.contains ("preference name") ||
            parameter.contains ("nvt selection") ||
            parameter.contains ("nvt family name")))
        return QString ("");
      else
        {
          com.append (QString ("config_id=\"%1\">").arg (parameter["id"]));
          if (parameter.contains ("preference name") &&
              parameter.contains ("preference value"))
            {
              QString nvt ("");
              if (parameter.contains ("nvt oid"))
                nvt = QString ("<nvt oid=\"%1\"/>").arg (parameter["nvt oid"]);
              QString value_b64 = parameter ["preference"
                                             " value"].toLatin1 ().toBase64 ();
              com.append (QString ("<preference>%1<name>%2</name>"
                              "<value>%3"
                              "</value>"
                              "</preference>").arg (nvt)
                                              .arg (parameter ["preference"
                                                               " name"])
                                              .arg (value_b64));
            }
          else if (parameter.contains ("nvt family name"))
            {
              QString growing ("");
              QString fam ("");
              QString fam_all ("");
              QString fam_grow ("");
              if (parameter.contains ("nvt selection growing"))
                growing = QString ("<growing>%1"
                               "</growing>").arg (parameter ["nvt"
                                                             " selection"
                                                             " growing"]);
              if (parameter.contains ("nvt family name"))
                {
                  fam = QString ("<name>%1</name>").arg (parameter ["nvt"
                                                                    " family"
                                                                    " name"]);
                  if (parameter.contains ("nvt family all"))
                    fam_all = QString ("<all>%1"
                                       "</all>").arg (parameter ["nvt"
                                                                 " family"
                                                                 " all"]);
                  if (parameter.contains ("nvt family growing"))
                    fam_grow = QString ("<growing>%1"
                                     "</growing>").arg (parameter ["nvt"
                                                                   " family"
                                                                   " growing"]);
                }
              com.append (QString ("<family_selection>%1"
                             "<family>%2%3%4"
                             "</family>"
                             "</family_selection>").arg (growing)
                                                   .arg (fam)
                                                   .arg (fam_all)
                                                   .arg (fam_grow));
            }
          else if (parameter.contains ("nvt selection"))
            {
              if (parameter["nvt selection"].compare ("add") == 0)
                com.append (QString ("<nvt_selection>"
                                     "<family>%1"
                                     "</family><nvt oid=\"%2\"/>"
                                     "</nvt_selection>").arg (parameter ["nvt"
                                                                 " selection"
                                                                 " name"])
                                                        .arg (parameter ["nvt"
                                                                        " oid"]));
              else
                com.append (QString ("<nvt_selection>"
                                     "<family>%1"
                                     "</family>"
                                     "</nvt_selection>").arg (parameter ["nvt"
                                                                 " selection"
                                                                 " name"]));
            }
        }
    }
  return com;
}
