/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file gsd_config.cpp
 *
 * @brief The GSD configuration managment
 *
 * This file saves, reads and manages login-profiles for the OpenVAS Manager,
 * using XML-format. Data is stored in the 'gsd_cfg.xml'
 */

#include "gsd_config.h"

/**
 * @brief Creates a new OpenVAS-Login configuration
 *
 * Reads the configuration file "gsd_cfg.xml" and checks whether a
 * root element named "openvas_logindata" exists. If the file is missing, it
 * is created in the parent Folder.
 */
gsd_config::gsd_config ()
{
  rootDoc = new QDomDocument ();
  filename = new QString (QDir::homePath ());
  filename->append ("/.gsd_cfg.xml");
  QFile rfile (*filename);

  if (!rfile.open (QIODevice::ReadOnly))
    {
      // if file can not be opened and no childnodes in DOM-Document, create a
      // valid config file
      if (!rootDoc->hasChildNodes ())
        {
          QDomProcessingInstruction header =
            rootDoc->createProcessingInstruction
             ("xml", QString ("version=\"1.0\" encoding=\"utf-8\"" ));
          rootDoc->appendChild (header);
          QDomElement rootElem = rootDoc->createElement
                                           ("openvas_logindata");
          rootDoc->appendChild (rootElem);
          QFile wfile (*filename);
          if (wfile.open (QFile::WriteOnly|QFile::Text))
            {
              writeConfig (&wfile);
            }
          wfile.close ();
        }
    }
  readConfig (&rfile);
  rfile.close ();
}

gsd_config::~gsd_config ()
  {
  }


/**
 * @brief Reads content of configuration-file
 *
 * @param device: The device to read from
 *
 * @return TRUE on success, FALSE on error
 */
bool
gsd_config::readConfig (QIODevice *device)
{
  QString errStr;
  int errLn;
  int errCln;

  if (!rootDoc->setContent (device, true, &errStr, &errLn, &errCln))
    {
      // Error mapping the file to DOM-document
      QMessageBox::information (NULL,tr ("DOM Error"), 
                                tr ("Error in line %1, Column %2: %3")
                                    .arg(errLn)
                                    .arg (errCln)
                                    .arg (errStr));
      return false;
    }
  rootElement = rootDoc->firstChildElement ("openvas_logindata");

  if ( rootElement.tagName () != "openvas_logindata")
    {
      // No valid config file: missing root tag
      QMessageBox::information (NULL,tr ("DOM Error"),
                                tr ("Not a valid openvas-config file!"));
      return false;
    }
  return true;
}


/**
 * @brief Writes login-configuration to the given device
 *
 * @param device: The device to write to
 *
 * @return returns 0
 *
 * @todo error-handling
 */
int
gsd_config::writeConfig (QIODevice *device)
{
  const int IdentSize = 4;
  QTextStream out (device);
  rootDoc->save (out, IdentSize);
  return 0;
}


/**
 * @brief Adds login-data to configuration
 *
 * @param profile: Profilename for login-credentials
 * @param crd: credentials, containing serveraddress, -port and username
 */
void
gsd_config::addCredentials (QString profile, omp_credentials crd)
{
  if (!rootDoc->isNull ())
    {
      int i = 0;
      bool exists = false;
      QDomNodeList elements = rootElement.childNodes ();

      // Check if there is a profile with the same name
      while (i < getProfileCount ())
        {
          QDomNode node = elements.at (i);
          QString name = node.attributes ().namedItem ("id").nodeValue ();

          if (name == profile)
            {
              exists = true;
            }
          i++;
        }

      // If no profile found, creating a new one
      if (!exists)
        {
          QDomElement newProfile = rootDoc->createElement ("profile");
          newProfile.setAttribute ("id", profile);
          QDomElement newCrd = rootDoc->createElement ("credentials");
          newCrd.setAttribute ("address", crd.getServerAddress ());
          newCrd.setAttribute ("port", crd.getServerPort ());
          newCrd.setAttribute ("username", crd.getUserName ());
          newProfile.appendChild (newCrd);
          rootElement.appendChild (newProfile);
        }
      else
        {
          QMessageBox::information (NULL,tr ("ProfileManager"),
                                    tr ("Profile allready exists!"));
        }
    }
}


/**
 * @brief Removes login-credentials from an existing config
 *
 * Searches through the config data for a profile given by the parameter.
 * Deletes the profile and writes the new config-file.
 *
 * @param profile: The name of the profile that has to be deleted
 *
 * @return todo (0)
 */
int
gsd_config::removeCredentials (QString profile)
{
  if (!rootElement.isNull ())
    {
      int i = 0;
      QDomNodeList elements = rootElement.childNodes ();

      // Search in all profiles for profilename
      while (i < elements.count ())
        {
          QDomNode node = elements.at (i);
          QString name = node.attributes ().namedItem ("id").nodeValue ();

          // If profile found, remove it and overwrite file
          if (name == profile)
            {
              rootElement.removeChild (node);
              QFile wfile (*filename);

              if (wfile.open (QFile::WriteOnly| QFile::Text))
                {
                  writeConfig (&wfile);
                }
              wfile.close ();
            }
          i++;
        }
    }
  return 0;
}


/**
 * @brief Get a Profile by number
 *
 * @param number: The number of the profile
 *
 * @return Name of the profile
 *
 */
QString
gsd_config::getProfile (int number)
{
  if (!rootDoc->isNull ())
    {
      QDomElement elem = rootElement.childNodes ().at (number).toElement ();

      if (elem.nodeName () != "profile")
        {
          return NULL;
        }
      else
        {
          return elem.attributeNode ("id").value ();
        }
    }
    return NULL;
}


/**
 * @brief Get credentials to a profilename
 *
 * @param profile: Name of the profile
 *
 * @return Logindata to the given profilename
 */
omp_credentials *
gsd_config::getCredentials (QString profile)
{
  omp_credentials *c = new omp_credentials ();

  if (!rootDoc->isNull ())
    {
      int i = 0;
      bool ok;
      QDomNodeList elements = rootElement.childNodes ();

      // Search in all profiles
      while (i < elements.count ())
        {
          QDomNode node = elements.at (i);
          QString name = node.attributes ().namedItem ("id").nodeValue ();

          // If profile found, return the credentials mapping to the profile
          if (name == profile)
            {
              QDomNode crdnode = node.namedItem ("credentials");
              c->setServerAddress (crdnode.attributes ()
                                    .namedItem ("address").nodeValue ());
              c->setServerPort (crdnode.attributes ()
                                 .namedItem ("port")
                                 .nodeValue ().toInt (&ok, 10));
              c->setUserName (crdnode.attributes ()
                               .namedItem ("username").nodeValue ());
              return c;
            }
          i++;
        }
    }
  return c;
}


/**
 * @brief Get the number of Profiles stored in the config.
 *
 * @return Number of Profiles
 */
int
gsd_config::getProfileCount ()
{
  if (!rootElement.isNull () && rootElement.hasChildNodes ())
    {
      return rootElement.childNodes ().count ();
    }
  return 0;
}


/**
 * @brief Get the Filename of the config-file
 *
 * @return Filename
 */
QString
*gsd_config::getFilename ()
{
  return filename;
}

