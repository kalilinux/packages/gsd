/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "dock_logging.h"

/**
 * @file dock_logging.cpp
 *
 * @brief Dock widget for log messages
 *
 * This widget shows log messages on different log level.
 */

/**
 * @brief Creates a new log widget
 *
 * Creates a log widget and configures signal/slot connections.
 */
dock_logging::dock_logging ()
{
  setupUi (this);

  QSizePolicy sizePolicy (QSizePolicy::Expanding, QSizePolicy::Expanding);

  this->setSizePolicy (sizePolicy);

  this->setAutoFillBackground (true);
  this->setFloating (true);
  this->setFeatures (QDockWidget::DockWidgetFloatable|
                     QDockWidget::DockWidgetMovable|
                     QDockWidget::DockWidgetClosable);
  this->setAllowedAreas (Qt::AllDockWidgetAreas);
  this->setContextMenuPolicy (Qt::NoContextMenu);
  this->setWidget (dockWidgetContents);

  connect (doubleSpinBox, SIGNAL (valueChanged (double)), this,
           SLOT (level_changed (double)));
  connect (pushButton, SIGNAL (released ()), this, SLOT (clear_list ()));
}

dock_logging::~dock_logging ()
{
}


/**
 * @brief Getter for list widget.
 *
 * @return list widget containing the log messages.
 */
QListWidget *
dock_logging::getList ()
{
  return listWidget;
}


/**
 * @brief SLOT to change the log level.
 *
 * @param lvl log level
 */
void
dock_logging::level_changed (double lvl)
{
  emit sig_log_lvl (lvl);
}


/**
 * @brief SLOT to clear the log list.
 *
 */
void
dock_logging::clear_list ()
{
  listWidget->clear();
}

