/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "delegate_progress.h"

/**
 * @file delegate_progress.cpp
 * @brief Delegate for progressbars
 *
 * Used for painting progressbars in widgets like tableview, treeview, etc.
 */

delegate_progress::delegate_progress ()
{
}


delegate_progress::~delegate_progress ()
{
}

/**
 * @brief New implemented paintfunction
 *
 * Implementation to paint a progressbar in a viewwidget.
 * Paints progressbars depending on the status of a task.
 *
 * @param painter Painter to draw the progressbar
 * @param option  Styleoptions
 * @param index   viewindex
 */
void
delegate_progress::paint (QPainter* painter,
                          const QStyleOptionViewItem& option,
                          const QModelIndex& index) const
{
  if (index.column () == 2)
    {
      model_omp_entity * model = (model_omp_entity*) index.model ();

      if (model == 0)
        {
          QStyledItemDelegate::paint (painter, option, index);
          return;
        }

      QStyleOptionProgressBarV2 opts;
      QDomElement pItem = model->getEntity (index.row ());

      if (!pItem.isNull ())
        {
          if (option.state & QStyle::State_Selected)
            {
              if (!(option.state & QStyle::State_Active))
                painter->fillRect (option.rect,
                                   option.palette.color (QPalette::Inactive,
                                                         QPalette::Highlight));
              else
                painter->fillRect (option.rect,
                                   option.palette.color (QPalette::Active,
                                                         QPalette::Highlight));
            }

          opts.rect = option.rect;
          opts.rect.setRight (option.rect.right ()-8);
          opts.rect.setLeft (option.rect.left ()+8);
          opts.rect.setTop (option.rect.top ()+5);
          opts.rect.setHeight (option.rect.height ()-10);
          opts.textVisible = true;
          opts.textAlignment = Qt::AlignCenter;
          opts.maximum = 100;
          opts.minimum =   0;
          opts.palette.setColor (QPalette::HighlightedText, QColor (0,0,0));
          QString tmp = model->getValue (pItem, "status");
          if (tmp.compare ("New") == 0 )
            {
              opts.text = QString ("New");
              opts.progress = 100;
              opts.palette.setColor (QPalette::Highlight,
                                     QColor (155,207,113));
              QCleanlooksStyle *style = new QCleanlooksStyle ();
              style->drawControl (QStyle::CE_ProgressBar,
                                  &opts, painter, NULL);
              return;
            }
          else if (tmp.compare ("Delete Requested") == 0 ||
              tmp.compare ("Stop Requested") == 0 ||
              tmp.compare ("Requested") == 0 ||
              tmp.compare ("Pause Requested") == 0 ||
              tmp.compare ("Resume Requested") == 0)
            {
              opts.text = QString (tmp);
              opts.progress = 100;
              opts.palette.setColor (QPalette::Highlight,
                                     QColor (239,187,37));
              QCleanlooksStyle *style = new QCleanlooksStyle ();
              style->drawControl (QStyle::CE_ProgressBar,
                                  &opts, painter, NULL);
              return;
            }
          else if (tmp.compare ("Paused") == 0)
            {
              QString s1 = model->getValue (pItem, "progress");
              opts.text = QString (tmp) + tr (" at ") + s1 + "%";
              bool *ok = false;
              opts.progress = s1.toInt (ok, 10);
              opts.palette.setColor (QPalette:: Highlight,
                                     QColor (239, 187, 37));
              QCleanlooksStyle *style = new QCleanlooksStyle ();
              style->drawControl (QStyle::CE_ProgressBar,
                                  &opts, painter, NULL);
              return;
            }
          else if (tmp.compare ("Stopped") == 0)
            {
              QString s1 = model->getValue (pItem, "progress");
              opts.text = QString (tmp) + tr (" at ") + s1 + "%";
              bool *ok = false;
              opts.progress = s1.toInt (ok, 10);
              opts.palette.setColor (QPalette::Highlight,
              QColor (239,187,37));
              QCleanlooksStyle *style = new QCleanlooksStyle ();
              style->drawControl (QStyle::CE_ProgressBar,
                                  &opts, painter, NULL);
              return;
            }
          else if (tmp.compare ("Internal Error") == 0)
            {
              opts.text = QString (tmp);
              opts.progress = 100;
              opts.palette.setColor (QPalette::Highlight,
                                     QColor (255,81,67));
              QCleanlooksStyle *style = new QCleanlooksStyle ();
              style->drawControl (QStyle::CE_ProgressBar,
                                  &opts, painter, NULL);
              return;
            }
          else if (tmp.compare ("Done") == 0)
            {
              opts.text = QString ("Done");
              opts.progress = 100;
              opts.palette.setColor (QPalette::Highlight,
                                     QColor (89,169,205));
              QCleanlooksStyle *style = new QCleanlooksStyle ();
              style->drawControl (QStyle::CE_ProgressBar,
                                  &opts, painter, NULL);
              return;
            }
          else if (tmp.compare ("Running") == 0)
            {
              QString s = model->getValue (pItem, "progress");
              opts.text = tr ("%1%").arg (s);
              bool *ok = false;
              opts.progress = s.toInt (ok, 10);
              opts.palette.setColor (QPalette::Highlight,
                                     QColor (21,209,58));
              QCleanlooksStyle *style = new QCleanlooksStyle ();
              style->drawControl (QStyle::CE_ProgressBar,
                                  &opts, painter, NULL);
              return;
            }
        }
    }
  QStyledItemDelegate::paint (painter, option, index);
}

