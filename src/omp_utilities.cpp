/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "omp_utilities.h"

/**
 * @file omp_utilities.cpp
 * @brief Utilities for comunication with the omp protocol.
 */

omp_utilities::omp_utilities ()
{
}

omp_utilities::~omp_utilities ()
{
}


/**
 * @brief Checks if response contains an error.
 *
 * @param session session to manager.
 *
 * @return -1 Internal error (http error 500).
 *         -2 Http error 40X.
 *          0 no error (http 20X).
 */
int
omp_utilities::checkResponse (QString response)
{
  if (response.contains ("status=\"500\""))
    {
      return -1;
    }
  if (response.contains ("status=\"40"))
    {
      return -2;
    }
  return 0;

}


omp_utilities::omp_type
omp_utilities::getType (QString type)
{
  if (type.compare ("agent") == 0)
    return omp_utilities::AGENT;
  else if (type.compare ("config") == 0)
    return omp_utilities::CONFIG;
  else if (type.compare ("dependency") == 0)
    return omp_utilities::DEPENDENCY;
  else if (type.compare ("escalator") == 0 ||
           type.compare ("alert") == 0)
    return omp_utilities::ESCALATOR;
  else if (type.compare ("lsc_credential") == 0)
    return omp_utilities::LSC_CREDENTIAL;
  else if (type.compare ("") == 0)
    return omp_utilities::NONE;
  else if (type.compare ("note") == 0)
    return omp_utilities::NOTE;
  else if (type.compare ("nvt") == 0)
    return omp_utilities::NVT;
  else if (type.compare ("family") == 0)
    return omp_utilities::NVT_FAMILY;
  else if (type.compare ("nvt_fee_checksum") == 0)
    return omp_utilities::NVT_FEED_CHECKSUM;
  else if (type.compare ("override") == 0)
    return omp_utilities::OVERRIDE;
  else if (type.compare ("preference") == 0)
    return omp_utilities::PREFERENCE;
  else if (type.compare ("report") == 0)
    return omp_utilities::REPORT;
  else if (type.compare ("result") == 0)
    return omp_utilities::RESULT;
  else if (type.compare ("schedule") == 0)
    return omp_utilities::SCHEDULE;
  else if (type.compare ("system_report") == 0)
    return omp_utilities::SYSTEM_REPORT;
  else if (type.compare ("target") == 0)
    return omp_utilities::TARGET;
  else if (type.compare ("target_locator") == 0)
    return omp_utilities::TARGET_LOCATOR;
  else if (type.compare ("task") == 0)
    return omp_utilities::TASK;
  else if (type.compare ("version") == 0)
    return omp_utilities::VERSION;
  else if (type.compare ("slave") == 0)
    return omp_utilities::SLAVE;
  else if (type.compare ("port_list") == 0)
    return omp_utilities::PORT_LIST;
  else if (type.compare ("report_format") == 0)
    return omp_utilities::REPORT_FORMAT;
  else
    {
      qDebug() << "Unknown message from server of type: " << type;
      return omp_utilities::NONE;
    }
}
