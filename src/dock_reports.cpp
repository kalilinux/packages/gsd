/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "dock_reports.h"
#include "gsd_control.h"
/**
 * @file dock_reports.cpp
 * @brief Implements the slots and functions for report dock widgets.
 */

void
dock_reports::update (QString report_id)
{
  if (fileName.compare ("") != 0)
    {
      saveReport ();
      return;
    }
  if (!cb_high->isChecked () && !cb_medium->isChecked () &&
      !cb_low->isChecked () && !cb_log->isChecked () &&
      !cb_false_pos->isChecked ())
    {
      emptyFilter ();
      return;
    }
  if (report_id.contains ("##xml"))
    {
      report_id.remove ("##xml");
      for (int i = 0; i < control->getXmlReportModel ()->rowCount (); i++)
        {
          report->addEntity (control->getXmlReportModel ()->getEntity (i), i);
        }
      return;
    }
  report_id.remove ("##html");
  if (this->id.compare (report_id) != 0)
    return;

  for (int i = 0; i < control->getHtmlReportModel ()->rowCount (); i++)
    {
      htmlReport->addEntity (control->getHtmlReportModel ()->getEntity (i), i);
    }

  cb_format->clear ();
  for (int i = 0; i < control->getReportFormatModel ()->rowCount (); i++)
    {
      QDomElement elem = control->getReportFormatModel ()->getEntity (i);
      cb_format->addItem (control->getReportFormatModel ()->getValue (elem,
                                                                      "name"));
    }


  QDomElement tmp_rep;
  if (control->getProtocolVersion () == 1)
    tmp_rep = report->getEntity (0);
  else if (control->getProtocolVersion () == 2 ||
           control->getProtocolVersion () == 3)
    tmp_rep = report->getEntity (1);
  model_omp_entity *results = new model_omp_entity ();
  QDomElement res = report->getEntities (tmp_rep, "results result", "results");
  int result_count = 0;
  if (res.hasChildNodes ())
    {
      QDomNodeList list = res.childNodes ();
      int j = 0;
      while (j < list.size ())
        {
          results->addEntity (list.at (j).toElement (), j);
          j++;
        }
      result_count = j;
    }

  QString first = report->getAttr (tmp_rep, "report results start");
  QString filtered = report->getValue (tmp_rep, "result_count filtered");

  bool *ok = false;
  int first_report = first.toInt (ok, 10);
  ok = false;
  int filtered_results = filtered.toInt (ok, 10);
  la_results->setText (tr ("Results %1 - %2 of %3").arg (first_report)
                                               .arg (first_report+result_count-1)
                                               .arg (filtered));

  if ((first_report + result_count -1) < filtered_results)
    tb_next_page->setEnabled (true);
  else
    tb_next_page->setEnabled (false);

  if (first_report > 10)
    tb_prev_page->setEnabled (true);
  else
    tb_prev_page->setEnabled (false);

  firstResult = first_report;
  maxResults = sb_results->value ();

  cb_selection->clear ();
  cb_selection->addItem (tr ("Filtered results %1-%2").arg (firstResult)
                                             .arg (first_report+result_count-1));
  cb_selection->addItem (tr ("All filtered results"));
  cb_selection->addItem (tr ("Full report"));


  QString b64 = htmlReport->getEntity (0).text ();
  QByteArray htmlData = QByteArray::fromBase64 (b64.toLatin1 ());
  QString htmlText = QString::fromUtf8 (htmlData.data (), htmlData.size ());
  int count = htmlText.count ("\"><pre>"), i = 0, pos = 0;
  while (i < count)
    {
      pos = htmlText.indexOf ("\"><pre>", pos)+2;
      QString addNote = tr("Add Note");
      QString addOverride = tr("Add Override");
      QString insert (QString ("\n<div style=\"text-align:left\"><a href=\""
                      "?cmd=new_note&amp;result_id=%1&amp;oid=%2&amp;port=%3"
                      "&amp;threat=%4&amp;target=%5\" title=\"Add Note\""
                      " style=\"margin-left:3px;\">"
                      "%6</a></div>\n"
                      "<div style=\"text-align:left\"><a href=\""
                      "?cmd=new_override&amp;result_id=%1&amp;oid=%2&amp;"
                      "port=%3&amp;threat=%4&amp;target=%5\""
                      " title=\"Add Override\" style=\"margin-left:3px;\">"
                      "%7</a></div>\n")
                       .arg (results->getAttr (results->getEntity (i),
                                               "result id"))
                       .arg (results->getAttr (results->getEntity (i),
                                               "result nvt oid"))
                       .arg (results->getValue (results->getEntity (i),
                                                "port"))
                       .arg (results->getValue (results->getEntity (i),
                                                "threat"))
                       .arg (results->getValue (results->getEntity (i),
                                                "host"))
                       .arg (addNote)
                       .arg (addOverride));
      htmlText.insert (pos, insert);
      pos = pos + insert.length () + QString ("\"><pre>").length ();
      i++;
    }
  QString filename = QDir::currentPath ()+"/.report.html";
  if (!filename.isEmpty())
    {
    // save to file
      QFile file(filename);
      if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
          QMessageBox::information(this, tr("Unable to open file"),
          file.errorString());
          return;
        }

      QTextStream in(&file);
      in.setCodec ("utf-8");
      in << htmlText;
    }
  webView->setUrl (QUrl::fromLocalFile (filename));

  model_omp_entity *tasks = control->getTaskModel ();
  QDomElement task;
  for (int i = 0; i < tasks->rowCount (); i++)
    {
      if (tasks->getAttr (tasks->getEntity (i), "task id").compare (task_id)
          == 0)
        {
          task = tasks->getEntity (i);
          break;
        }
    }

  QString headline = tasks->getValue (task, "name");

  model_omp_entity *tmp = new model_omp_entity ();
  QDomElement reps = tasks->getEntities (task, "reports report", "reports");
  if (reps.hasChildNodes ())
    {
      QDomNodeList list = reps.childNodes ();
      int j = 0;
      while (j < list.size ())
        {
          tmp->addEntity (list.at (j).toElement (), j);
          j++;
        }
    }
  QDomElement rep;
  for (int i = 0; i < tmp->rowCount (); i++)
    {
      if (tmp->getAttr (tmp->getEntity (i), "report id").compare (this->id)
          == 0)
        {
          rep = tmp->getEntity (i);
          break;
        }
    }

  int high = 0, med = 0, low = 0, log = 0, fp = 0;
  for (int i = 0; i < results->rowCount (); i++)
    {
      QString threat = results->getValue (results->getEntity (i), "threat");
      if (threat.compare ("High") == 0)
        high++;
      else if (threat.compare ("Medium") == 0)
        med++;
      else if (threat.compare ("Low") == 0)
        low++;
      else if (threat.compare ("Log") == 0)
        log++;
      else if (threat.compare ("False Positive") == 0)
        fp++;
    }
  delegate_date_time *dateDelegate = new delegate_date_time ();
  QString date = dateDelegate->formatDate (tmp->getValue (rep, "timestamp"),
                                          true);
  headline += " (" + date + ")";
  this->setWindowTitle (tr("Report ") + headline);
  la_high_count->setText (tr ("%1 of %2").arg (high)
                                         .arg (report->getValue (tmp_rep,
                                                                 "result_count"
                                                                 " hole full")));
  la_medium_count->setText (tr ("%1 of %2").arg (med)
                                           .arg (tmp->getValue (tmp_rep,
                                                                "result_count"
                                                                " warning full")));
  la_low_count->setText (tr ("%1 of %2").arg (low)
                                        .arg (tmp->getValue (tmp_rep,
                                                             "result_count"
                                                             " info full")));
  la_log_count->setText (tr ("%1 of %2").arg (log)
                                        .arg (tmp->getValue (tmp_rep,
                                                             "result_count"
                                                             " log full")));
  la_false_pos_count->setText (tr ("%1 of %2").arg (fp)
                                              .arg (tmp->getValue (tmp_rep,
                                                                  "result_count"
                                                                  " false_"
                                                                  "positive full")));
  pb_save->setEnabled (true);
}

dock_reports::dock_reports (gsd_control *ctl)
{
  setupUi (this);
  this->control = ctl;
  report = new model_omp_entity ();
  htmlReport = new model_omp_entity ();
  firstResult = 1;
};

void
dock_reports::updateReportFormats ()
{
  for (int i = 0; i < (int) control->getReportFormatModel ()->rowCount (); i++)
    {
      QDomElement elem = control->getReportFormatModel ()->getEntity (i);
      QString curItem = control->getReportFormatModel ()->getValue (elem,
                                                                     "name");
      if (cb_format->findText (curItem) == -1)
        cb_format->addItem (curItem);

      if (curItem == QString::fromLatin1("HTML"))
        {
          html_format_id = control->getReportFormatModel ()->getAttr (elem,
                                                                     "report_format id");
        }
      else if (curItem == QString::fromLatin1("XML"))
        {
          xml_format_id = control->getReportFormatModel ()->getAttr (elem,
                                                                     "report_format id");
        }
    }
  request_update ();
}

void
dock_reports::load ()
{
  updateReportFormats ();
  maxResults = sb_results->value ();
  tb_prev_page->setEnabled (false);
  tb_next_page->setEnabled (false);

  pb_save->setEnabled (false);

  cb_selection->addItem (tr ("Filtered results %1-%2").arg (firstResult)
                                             .arg (maxResults));
  cb_selection->addItem (tr ("All filtered results"));
  cb_selection->addItem (tr ("Full report"));

  webView->setUrl (QUrl (""));
  webView->page ()->setLinkDelegationPolicy (QWebPage::DelegateAllLinks);
  QMovie *processIcon = new QMovie (":/img/gb-animated.gif");
  processIcon->start ();
  la_icon->setMovie (processIcon);
  cb_overrides->setChecked (true);

  cb_sort->addItem (tr("port ascending"));
  cb_sort->addItem (tr("port descending"));
  cb_sort->addItem (tr("threat ascending"));
  cb_sort->addItem (tr("threat descending"));
  cb_sort->setCurrentIndex (3);

  cb_high->setChecked (true);
  cb_medium->setChecked (true);

  hs_cvss->setMaximum (100);
  hs_cvss->setValue (0);
  la_cvss_v->setText ("0");
  hs_cvss->setTracking (false);

  QMap<QString, QString> parameter;
  parameter.insert ("report_id", id);
  parameter.insert ("format_id", xml_format_id);
  parameter.insert ("notes", "1");
  parameter.insert ("note_details", "1");
  parameter.insert ("overrides", "1");
  parameter.insert ("overrides_details", "1");
  parameter.insert ("apply_overrides", "1");
  parameter.insert ("result_hosts_only", "1");
  parameter.insert ("first_result", "1");
  parameter.insert ("max_results", QString ("%1").arg (maxResults));
  parameter.insert ("levels", "hm");
  parameter.insert ("min_cvss_base", "");
  parameter.insert ("sort_field", "threat");
  parameter.insert ("sort_order", "descending");

  webView->setVisible (false);
  la_text->setText ("Requesting Report");
  wi_loading->setVisible (true);

  emit sig_request_report (parameter);
  parameter.insert ("format_id", html_format_id);
  emit sig_request_report (parameter);
  connect (cb_sort,
           SIGNAL (currentIndexChanged (int)),
           this,
           SLOT (new_request ()));
  connect (hs_cvss,
           SIGNAL (valueChanged (int)),
           this,
           SLOT (new_request ()));
  connect (cb_high,
           SIGNAL (stateChanged (int)),
           this,
           SLOT (new_request ()));
  connect (cb_medium,
           SIGNAL (stateChanged (int)),
           this,
           SLOT (new_request ()));
  connect (cb_low,
           SIGNAL (stateChanged (int)),
           this,
           SLOT (new_request ()));
  connect (cb_log,
           SIGNAL (stateChanged (int)),
           this,
           SLOT (new_request ()));
  connect (cb_false_pos,
           SIGNAL (stateChanged (int)),
           this,
           SLOT (new_request ()));
  connect (cb_overrides,
           SIGNAL (stateChanged (int)),
           this,
           SLOT (new_request ()));
  connect (pb_search,
           SIGNAL (released ()),
           this,
           SLOT (new_request ()));
  connect (webView->page (),
           SIGNAL (linkClicked (const QUrl&)),
           this,
           SLOT (web_link (const QUrl&)));
  connect (hs_cvss,
           SIGNAL (valueChanged (int)),
           this,
           SLOT (cvss_changed (int)));
  connect (pb_save,
           SIGNAL (released ()),
           this,
           SLOT (download_report ()));
  connect (tb_next_page,
           SIGNAL (released ()),
           this,
           SLOT (next_page ()));
  connect (tb_prev_page,
           SIGNAL (released ()),
           this,
           SLOT (previous_page ()));
  connect (tb_refresh,
           SIGNAL (released ()),
           this,
           SLOT (new_request ()));
  connect (webView,
           SIGNAL (loadStarted ()),
           this,
           SLOT (load_started ()));
  connect (webView,
           SIGNAL (loadFinished (bool)),
           this,
           SLOT (load_finished ()));
}


void
dock_reports::request_update ()
{
  QString overrides = "0";
  if (cb_overrides->isChecked ())
    overrides = "1";

  QString sort_type = "", sort_order = "";
  if (cb_sort->currentIndex () == 0)
    {
      sort_type = "port";
      sort_order = "ascending";
    }
  else if (cb_sort->currentIndex () == 1)
    {
      sort_type = "port";
      sort_order = "descending";
    }
  else if (cb_sort->currentIndex () == 2)
    {
      sort_type = "threat";
      sort_order = "ascending";
    }
  else if (cb_sort->currentIndex () == 3)
    {
      sort_type = "threat";
      sort_order = "descending";
    }


  QString level ("");
  if (cb_high->isChecked ())
    level += "h";
  if (cb_medium->isChecked ())
    level += "m";
  if (cb_low->isChecked ())
    level += "l";
  if (cb_log->isChecked ())
    level += "g";
  if (cb_false_pos->isChecked ())
    level += "f";
  QString search = le_search->text ();
  double cvss = (double)hs_cvss->value () / 10;

  if (level.compare ("") == 0)
    {
      emptyFilter ();
      return;
    }
  QMap<QString, QString> parameter;
  parameter.insert ("report_id", id);
  parameter.insert ("format_id", html_format_id);
  parameter.insert ("notes", "1");
  parameter.insert ("note_details", "1");
  parameter.insert ("overrides", "1");
  parameter.insert ("overrides_details", "1");
  parameter.insert ("apply_overrides", overrides);
  parameter.insert ("result_hosts_only", "1");
  parameter.insert ("first_result", QString ("%1").arg (firstResult));
  parameter.insert ("max_results", QString ("%1").arg (sb_results->value ()));
  parameter.insert ("levels", level);
  parameter.insert ("search_phrase", search);
  if (cvss != 0)
    parameter.insert ("min_cvss_base", QString ("%1").arg (cvss));
  else
    parameter.insert ("min_cvss_base", "");
  parameter.insert ("sort_field", sort_type);
  parameter.insert ("sort_order", sort_order);

  webView->setVisible (false);
  wi_loading->setVisible (true);
  la_text->setText ("Requesting Report");
  emit sig_request_report (parameter);

  parameter.insert ("format_id", xml_format_id);
  emit sig_request_report (parameter);
}


void
dock_reports::web_link (const QUrl &url)
{
  QString value;
  if (url.hasQueryItem ("cmd"))
    value = url.queryItemValue ("cmd");

  model_omp_entity *tasks = control->getTaskModel ();
  QDomElement task;
  for (int i = 0; i < tasks->rowCount (); i++)
    {
      if (tasks->getAttr (tasks->getEntity (i), "task id").compare (task_id)
          == 0)
        {
          task = tasks->getEntity (i);
          break;
        }
    }
  if (value.compare ("get_nvt_details") == 0)
    {
      QString oid = url.queryItemValue ("oid");
      QString config_id = tasks->getAttr (task, "task config id");
      emit sig_details_nvt (config_id, oid);
    }
  else if (value.compare ("new_note") == 0)
    {
      QString hosts = url.queryItemValue ("target");
      QString nvt_oid = url.queryItemValue ("oid");
      QString port = url.queryItemValue ("port");
      QString threat = url.queryItemValue ("threat");
      QString task_name = tasks->getValue (task, "name");
      QString result = url.queryItemValue ("result_id");
      dlg_new_note *new_note = new dlg_new_note ();
      connect (new_note,
               SIGNAL (sig_create (int, QMap<QString, QString>)),
               control,
               SLOT (create (int, QMap<QString, QString>)));
      new_note->setHosts (hosts);
      new_note->setPort (port);
      new_note->setThreat (threat);
      new_note->setTask (task_name);
      new_note->setResult (result);
      new_note->setNVT (nvt_oid);
      new_note->setTaskId (task_id);
      new_note->show ();
    }
  else if (value.compare ("new_override") == 0)
    {
      QString hosts = url.queryItemValue ("target");
      QString nvt_oid = url.queryItemValue ("oid");
      QString port = url.queryItemValue ("port");
      QString threat = url.queryItemValue ("threat");
      QString task_name = tasks->getValue (task, "name");
      QString result = url.queryItemValue ("result_id");
      dlg_new_override *new_override = new dlg_new_override ();
      connect (new_override,
               SIGNAL (sig_create (int, QMap<QString, QString>)),
               control,
               SLOT (create (int, QMap<QString, QString>)));
      new_override->setHosts (hosts);
      new_override->setPort (port);
      new_override->setThreat (threat);
      new_override->setTask (task_name);
      new_override->setResult (result);
      new_override->setNVT (nvt_oid);
      new_override->setTaskId (task_id);
      new_override->show ();
    }
}


void
dock_reports::cvss_changed (int value)
{
  la_cvss_v->setText (QString ("%1").arg ((double)value /10));
}


void
dock_reports::download_report ()
{
  QString overrides = "0";
  if (cb_overrides->isChecked ())
    overrides = "1";

  QString sort_type = "", sort_order = "";
  if (cb_sort->currentIndex () == 0)
    {
      sort_type = "port";
      sort_order = "ascending";
    }
  else if (cb_sort->currentIndex () == 1)
    {
      sort_type = "port";
      sort_order = "descending";
    }
  else if (cb_sort->currentIndex () == 2)
    {
      sort_type = "threat";
      sort_order = "ascending";
  }
  else if (cb_sort->currentIndex () == 3)
    {
      sort_type = "threat";
      sort_order = "descending";
    }


  QString level ("");
  if (cb_high->isChecked ())
    level += "h";
  if (cb_medium->isChecked ())
    level += "m";
  if (cb_low->isChecked ())
    level += "l";
  if (cb_log->isChecked ())
    level += "g";
  if (cb_false_pos->isChecked ())
    level += "f";
  QString search = le_search->text ();
  double cvss = (double)hs_cvss->value () / 10;

  QDomElement elem = control->getReportFormatModel ()
                              ->getEntity (cb_format->currentIndex ());
  QString format = control->getReportFormatModel ()->getValue (elem,
                                                               "extension");
  fileName = QFileDialog::getSaveFileName (this,
                                           tr ("Save File ..."),
                                           QDir::homePath () +
                                           "/report-" + this->id + "." + format,
                                           tr ("%1 files *.%1").arg (format));
  int first = 1;
  int max = 0;

  if (cb_selection->currentText ().compare ("Full report") == 0)
    {
      level = "hmlgf";
      search = "";
      cvss = 0;
      first = 1;
      max = -1;
    }
  else if (cb_selection->currentText ().compare ("All filtered results") == 0)
    {
      first = 1;
      max = -1;
    }
  else
    {
      first = firstResult;
      max = sb_results->value ();
    }

  QMap<QString, QString> parameter;
  parameter.insert ("report_id", id);
  parameter.insert ("format_id",
                    control->getReportFormatModel ()->getAttr (
                        elem,
                        "report_format id"));
  parameter.insert ("notes", "1");
  parameter.insert ("note_details", "1");
  parameter.insert ("overrides", "1");
  parameter.insert ("overrides_details", "1");
  parameter.insert ("apply_overrides", overrides);
  parameter.insert ("first_result", QString ("%1").arg (first));
  parameter.insert ("max_results", QString ("%1").arg (max));
  parameter.insert ("result_hosts_only", "1");
  parameter.insert ("levels", level);
  parameter.insert ("search_phrase", search);
  parameter.insert ("min_cvss_base", QString ("%1").arg (cvss));
  parameter.insert ("sort_field", sort_type);
  parameter.insert ("sort_order", sort_order);

  emit sig_report_download (parameter);
}


void
dock_reports::saveReport ()
{
  model_omp_entity *tmp = this->control->getReportDownloadModel ();

  QDomElement element = tmp->getEntity (0);
  QString ext = tmp->getAttr (element, "report extension");
  if (ext.compare ("") == 0)
    ext = tmp->getAttr (element, "report format");
  if (ext.compare ("") == 0 || ext.compare ("xml") == 0)
    {
      const int indent = 4;
      if (!fileName.endsWith (".xml"))
        fileName += ".xml";
      QFile wfile (fileName);
      if (!wfile.open (QFile::WriteOnly|QFile::Text))
        {
          QMessageBox::information (NULL, tr ("File Error"),
                                    tr ("Could not open file!"));
          return;
        }
      QTextStream out (&wfile);
      element.save (out, indent);
      wfile.close ();
    }
  else
    {
      if (!fileName.endsWith (ext))
        {
          fileName += ".";
          fileName += ext;
        }
      QFile wfile (fileName);
      if (!wfile.open (QFile::WriteOnly))
        {
          QMessageBox::information (NULL, tr ("File Error"),
                                    tr ("Could not open file!"));
          return;
        }
      QByteArray data = QByteArray::fromBase64 (element.text ().toLatin1 ());
      QDataStream out (&wfile);
      out.writeRawData (data.data (), data.length ());
      wfile.close ();
    }
  fileName = "";
}


void
dock_reports::emptyFilter ()
{
  webView->setHtml ("<div style=\"text-align:center\">The current filter"
                    " settings for threat level are empty.\n Please select"
                    " a threat level to display the report.</div>");
  QString tmp = la_high_count->text ();
  tmp.remove (0, tmp.indexOf (" "));
  tmp.prepend ("0");
  la_high_count->setText (tmp);
  tmp = la_medium_count->text ();
  tmp.remove (0, tmp.indexOf (" "));
  tmp.prepend ("0");
  la_medium_count->setText (tmp);
  tmp = la_low_count->text ();
  tmp.remove (0, tmp.indexOf (" "));
  tmp.prepend ("0");
  la_low_count->setText (tmp);
  tmp = la_log_count->text ();
  tmp.remove (0, tmp.indexOf (" "));
  tmp.prepend ("0");
  la_log_count->setText (tmp);
  tmp = la_false_pos_count->text ();
  tmp.remove (0, tmp.indexOf (" "));
  tmp.prepend ("0");
  la_false_pos_count->setText (tmp);
  pb_save->setEnabled (false);
}


void
dock_reports::next_page ()
{
  firstResult = firstResult + maxResults;
  request_update ();
}


void
dock_reports::previous_page ()
{
  firstResult = firstResult - maxResults;
  if (firstResult < 1)
    firstResult = 1;

  request_update ();
}


void
dock_reports::new_request ()
{
  firstResult = 1;
  request_update ();
}


void
dock_reports::load_started ()
{
  webView->setVisible (false);
  la_icon->setVisible (false);
  la_text->setText ("Loading...");
  wi_loading->setVisible (true);
}


void
dock_reports::load_finished ()
{
  webView->setVisible (true);
  la_icon->setVisible (true);
  wi_loading->setVisible (false);
}
