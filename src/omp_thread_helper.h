/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * @file omp_thread_helper.h
 * @class omp_thread_helper
 * @brief Protos and data structures for omp_thread_helper.
 */

#ifndef OMP_THREAD_HELPER_H
#define OMP_THREAD_HELPER_H

#include <QObject>
#include "omp_utilities.h"

class omp_thread_helper : public QObject
{
  Q_OBJECT

  signals:
/**
 * @brief SIGNAL to synchronize with update.
 */
    void sig_update_trigger ();

/**
 * @brief SIGNAL indicating that an update has started.
 */
    void sig_update_finished ();

/**
 * @brief SIGNAL indicating that an update has finished.
 */
    void sig_thread_started ();

/**
 * @brief SIGNAL indicating that the thread terminated.
 */
    void sig_thread_finished ();

/**
 * @brief SIGNAL indicating an successful request.
 */
    void sig_request_finished (int, int, QString);

/**
 * @brief SIGNAL indicating an updatefailure
 */
    void sig_request_failed (int, int, int);

/**
 * @brief SIGNAL containing a debug message
 */
    void sig_log (QString, int);

  public:
    omp_thread_helper ();
    ~omp_thread_helper ();

    void triggerUpdate ();
    void finishedUpdate ();
    void threadStarted ();
    void threadFinished ();
    void requestFinished (int, int, QString);
    void requestFailed (int, int, int err);
    void log (QString msg, int loglvl);
};
#endif

