/**
 *   Copyright:
 *   Copyright (C) 2010 by Greenbone Networks GmbH
 *
 *   Authors:
 *   Raimund Renkert <raimund.renkert@greenbone.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "delegate_text.h"

/**
 * @file delegate_text.cpp
 * @brief Delegate for text.
 *
 * Used for painting the text in widgets like tableview, treeview, etc.
 */

delegate_text::delegate_text ()
{
}

delegate_text::~delegate_text()
{
}


/**
 * @brief New implemented paintfunction.
 *
 * Implementation to paint text in a view widget.
 *
 * @param painter Painter to draw the text
 * @param option  Styleoptions
 * @param index   viewindex
 */
void
delegate_text::paint (QPainter* painter,
                      const QStyleOptionViewItem& option,
                      const QModelIndex& index) const
{
  model_omp_entity * model = (model_omp_entity*) index.model ();

  if (model == 0)
    {
      QStyledItemDelegate::paint (painter, option, index);
      return;
    }
  QColor c;
  QStyleOptionViewItem opts (option);
  QDomElement pItem = model->getEntity (index.row ());
  if (!pItem.isNull ())
    {
      if (option.state & QStyle::State_Selected)
        {
          if (!(option.state & QStyle::State_Active))
            {
              painter->fillRect (option.rect,
                                 option.palette.color
                                  (QPalette::Inactive,
                                   QPalette::Highlight));
              c = option.palette.color (QPalette::Inactive,
                                               QPalette::HighlightedText);
            }
          else
            {
              painter->fillRect (option.rect,
                               option.palette.color (QPalette::Active,
                                                     QPalette::Highlight));
              c = option.palette.color (QPalette::Active,
                                               QPalette::HighlightedText);
            }
        }
      opts.palette.setColor (QPalette::HighlightedText, c);
      bool *ok = false;
      QString temp = model->getValue (pItem, model->getHeader (index.column ()));
      if (model->getHeader (index.column ()).compare ("event") == 0)
        {
          QString s1 = model->getValue (pItem, "event");
          QString s2 = model->getValue (pItem, "event data");
          QString out = s1 + " (to " + s2 + ")";
          QApplication::style ()->drawItemText(painter,
                                               option.rect,
                                               Qt::AlignVCenter,
                                               opts.palette,
                                               true,
                                               out,
                                               QPalette::HighlightedText);
          return;
        }
      if (model->getHeader (index.column ()).compare ("condition") == 0)
        {
          QString out;
          QString s1 = model->getValue (pItem, "condition");
          if (s1.compare ("Always") == 0)
            out = s1;
          else if (s1.compare ("Threat level changed") == 0)
            {
              QDomNodeList list = pItem.namedItem ("condition").childNodes ();
              unsigned int i = 0;
              while (i < list.length ())
                {
                  if (list.at (i).firstChildElement ().text ().compare ("direction") == 0)
                    {
                      out = s1 + " (" + list.at (i).toElement ().text ().replace ("direction", "") + ")";
                      break;

                    }
                  else
                    out = s1;
                  i++;
                }
            }
          else if (s1.compare ("Threat level at least") == 0)
            {
              QDomNodeList list = pItem.namedItem ("condition").childNodes ();
              unsigned int i = 0;
              while (i < list.length ())
                {
                  if (list.at (i).firstChildElement ().text ().compare ("level") == 0)
                    out = s1 + " (" + list.at (i).toElement ().text ().replace ("level", "") + ")";
                  i++;
                }
            }
          QApplication::style ()->drawItemText(painter,
                                               option.rect,
                                               Qt::AlignVCenter,
                                               opts.palette,
                                               true,
                                               out,
                                               QPalette::HighlightedText);
          return;
        }
      if (model->getHeader (index.column ()).compare ("method") == 0)
        {
          QString out = "";
          QString s1 = model->getValue (pItem, "method");
          QDomNodeList list = pItem.namedItem ("method").childNodes ();
          unsigned int i = 0;
          while (i <= list.length ())
            {
              if (list.at (i).firstChildElement ().text ().compare ("to_address") == 0 &&
                  s1.compare ("Email") == 0)
                {
                  out = s1 + " (to " + list.at (i).toElement ().text ().replace ("to_address", "") + ")";
                  break;
                }
              else if (s1.compare ("Syslog") == 0 &&
                       list.at (i).firstChildElement ().text ().compare ("submethod") == 0)
                {
                  out = "SNMP";
                  break;
                }
              else
                out = s1;
              i++;
            }
          QApplication::style ()->drawItemText(painter,
                                               option.rect,
                                               Qt::AlignVCenter,
                                               opts.palette,
                                               true,
                                               out,
                                               QPalette::HighlightedText);
          return;
        }
      if (model->getHeader (index.column ()).compare ("nvt_count") == 0)
        {
          QString out = "";
          QString s1 = model->getValue (pItem, "nvt_count");
          QString s2 = model->getValue (pItem, "max_nvt_count");
          out = s1 + " of " +s2;
          QApplication::style ()->drawItemText(painter,
                                               option.rect,
                                               Qt::AlignVCenter,
                                               opts.palette,
                                               true,
                                               out,
                                               QPalette::HighlightedText);
          return;
        }
      if (model->getHeader (index.column ()).compare ("preference_count") == 0)
        {
          QString out = "";
          QString s1 = model->getValue (pItem, "preference_count");
          if (s1.compare ("0") == 0 || s1.compare ("-1") == 0)
            out = "";
          else
            out = " " + s1;
          QApplication::style ()->drawItemText(painter,
                                               option.rect,
                                               Qt::AlignVCenter,
                                               opts.palette,
                                               true,
                                               out,
                                               QPalette::HighlightedText);
          return;
        }
      if (model->getHeader (index.column ()).compare ("timeout") == 0)
        {
          QString out = "";
          QString s1 = model->getValue (pItem, "timeout");
          if (s1.compare ("") == 0)
            out = " default";
          else
            out = " " + s1;
          QApplication::style ()->drawItemText(painter,
                                               option.rect,
                                               Qt::AlignVCenter,
                                               opts.palette,
                                               true,
                                               out,
                                               QPalette::HighlightedText);
          return;
        }
      if (model->getHeader (index.column ()).compare ("oid") == 0)
        {
          QString out = "";
          QString s1 = model->getAttr (pItem, "nvt oid");
          if (s1.compare ("") == 0)
            out = "";
          else
            out = " "+ s1;
          QApplication::style ()->drawItemText(painter,
                                               option.rect,
                                               Qt::AlignVCenter,
                                               opts.palette,
                                               true,
                                               out,
                                               QPalette::HighlightedText);
          return;
        }
      // 1 / 0  to -> yes / no
      if (model->getHeader (index.column ()).compare ("active") == 0)
        {
          QString out = "";
          QString s1 = model->getValue (pItem, "active");
          if (s1.compare ("1") == 0)
            out = "yes";
          else
            out = "no";
          QApplication::style ()->drawItemText(painter,
                                               option.rect,
                                               Qt::AlignCenter,
                                               opts.palette,
                                               true,
                                               out,
                                               QPalette::HighlightedText);
          return;
        }
      if (model->getHeader (index.column ()).compare ("in_use") == 0)
        {
          QString out = "";
          QString s1 = model->getValue (pItem, "in_use");
          // Value is used in the Gui as "deletable"
          if (s1.compare ("1") == 0)
            out = "no";
          else
            out = "yes";
          QApplication::style ()->drawItemText(painter,
                                               option.rect,
                                               Qt::AlignCenter,
                                               opts.palette,
                                               true,
                                               out,
                                               QPalette::HighlightedText);
          return;
        }
   }
  QStyledItemDelegate::paint (painter, option, index);
}

