gsd (1.2.2-1kali1) kali; urgency=low

  * Update build dependencies to rebuild with libopenvas6.
  * Enable dh_install --list-missing
  * Switch to “3.0 (quilt)” source format.
  * Add disable-werror patch to disable -Werror build flag.
  * Convert changes form .diff.gz into debian/patches/fix-build
  * Add new patch build-with-qt-no-keywords to get gsd to compile with recent
    versions of Glib.

 -- Raphaël Hertzog <hertzog@debian.org>  Thu, 02 May 2013 15:59:12 +0200

gsd (1.2.2-1kali0) kali; urgency=low

  [ Stephan Kleine ]
  * New upstream release.
    - Added support for building on MacOS.
    - Fixed blocking of login dialog while as-you-type server check.
    - Added support for port lists.
    - Fixed problems with download of reports.
    - Various improvements for OMP compatibilities.

  [ Mati Aharoni ]
  * Kali import

 -- Mati Aharoni <muts@kali.org>  Tue, 04 Dec 2012 11:18:44 -0500

gsd (1.2.1-2) UNRELEASED; urgency=low
  
  * Fix build with gcc 4.7
  
 -- Claudio Freire <klaussfreire@gmail.com>  Tue, 10 Apr 2012 18:40:50 -0300

gsd (1.2.1-1) UNRELEASED; urgency=low

  * New upstream release.
    - Fixed: Inconsistencies in the interface have been fixed.
    - Fixed: Build process now works without doxygen.
    - Fixed: The FSF address has been corrected in the GPL license text.
    - Updated: German and French translations.
    - New: Spanish translation.
    - New: Support for modifying tasks.
    - New: Support for OMP 3.0.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Fri, 09 Dec 2011 17:50:23 +0100

gsd (1.2.0-1) UNRELEASED; urgency=low

  * New upstream release.
    - New: A dashboard for current vulnerability scan results.
    - New: Support for executing tasks on slaves.
    - New: Support for using report format plugins.
    - New: Indonesian translation.
    - Updated: German and French translations.
    - Updated: Copyright and License information for a number of files.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Thu, 07 Apr 2011 19:08:35 +0200

gsd (1.1.1-1) UNRELEASED; urgency=low

  * New upstream release.
    - Added French translation.
    - Improved login dialog.
    - Added credentials editor.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Thu, 10 Mar 2011 21:16:41 +0100

gsd (1.1.0-1) UNRELEASED; urgency=low

  * New upstream release.
    - Added man page to gsd.
    - Removed OMP 1.0 compatibility.
    - Added commandline options --version and --help.
    - Split target credentials into SSH and SMB credentials to match OMP 2.0.
    - Updated about dialog.
    - Added dialog to start gsa in external web browser.
    - Added shortcuts: "Ctrl+q" to quit, "F5" to refresh.
    - Added custom make target to create Linux desktop menu item for GSD.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Fri, 11 Feb 2011 22:53:38 +0100

gsd (1.0.98.1-1) UNRELEASED; urgency=low

  * New upstream release.
    - A couple of small GUI improvements. 
    - Added language configuration via menu (German/English/Default).
    - Fixed Umlaut/UTF-8 problems.
    - Code hardening activated.
    - Improved build environment, now uses pkg-config and makes
      OpenVAS Libraries 4.0 mandatory requirement.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Fri, 04 Feb 2011 22:53:38 +0100

gsd (1.0.1-1) UNRELEASED; urgency=low

  * New upstream release.
    - Added pagination for reports.
    - Added animated process working icon to report widget.
    - Reload reports after adding or deleting notes or overrides.
    - Added report download size selection.
    - Added context menu to notes and overrides.
    - Added logo to login dialog.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Mon, 20 Dec 2010 13:47:59 +0100

gsd (1.0.0-1) UNRELEASED; urgency=low

  * New upstream release.
    - Added support for scan configuration import.
    - Disable the manual start for scheduled tasks.
    - Cleaned up UI components and about dialog.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Thu, 02 Dec 2010 16:31:52 +0100

gsd (0.9.99.2-1) UNRELEASED; urgency=low

  * New upstream release.
    - Improved editing of NVT preferences.
    - Improved editing of scanner preferences.
    - Fixed a bug that caused crash on windows systems.
    - Added support for report format download.
    - Added support for deleting reports.
    - Improved report presentation and filter.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Fri, 19 Nov 2010 22:12:48 +0100

gsd (0.9.99.1-1) UNRELEASED; urgency=low

  * New upstream release.
    - Added details dialog for NVT families.
    - Added details dialog for NVTs.
    - Added functionality to modify the nvt families for a scan config.
    - Added functionality to modify scanner configuration.
    - Added functionality to modify NVT preferences.
    - Added new feature: Display and filter reports in html format.
    - Added New Feature: Notes can be added to a report now.
    - Added New Feature: Overrides can be added to a report now.
    - Changed details windows to dockwidgets.
    - Load system reports on startup.
    - Saving window state on close.
    - Improved dock widget behavior.
    - Added function to clear dock widget settings.
    - Reports can be requested from manager using omp version 1.0 or 2.0.
    - Refactored major parts of gsd, espacially the object hierachy and
      the data request layer for omp 1.0.
    - Renamed GSA-Desktop to GSD (Greenbone Security Desktop).

 -- Stephan Kleine <bitshuffler@opensuse.org>  Thu, 28 Oct 2010 19:08:19 +0200

gsd (0.3.0-1) UNRELEASED; urgency=low

  * New upstream release.
    - Details dialogs for tasks and scan configurations have been added.
    - Scan configurations can be downloaded in xml format.
    - Support for system reports has been added.
    - System reports can be saved as png image.
    - Improved style for progress bar. The bar now looks better especially
      on Windows systems.
    - Improved focus handling for widgets.
    - Doubleclicks in lists now issue default action.
    - Improved About Dialog.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Wed, 08 Sep 2010 14:10:54 +0200

gsd (0.2.0-1) UNRELEASED; urgency=low

  * New upstream release.
    - Missing details dialogs for omp resources have been added.
    - Support for notes and overrides has been added.
    - German translation has been improved and completed.
    - Cross compile support for mingw32 has been added.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Thu, 12 Aug 2010 19:08:11 +0200

gsd (0.1.0-1) UNRELEASED; urgency=low

  * Initial package.

 -- Stephan Kleine <bitshuffler@opensuse.org>  Thu, 29 Jul 2010 17:18:29 +0200
